#!dev-python/bin/python2

import sys
import subprocess
import os
import datetime
import time
import argparse
import portage
import re
import shutil
import tempfile
import stat
import json
import urllib

from pwd import getpwnam
from grp import getgrnam

from worker_common import settings
from controller.md5hash import md5hash

import logging.config
logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger('test')

# Setup ENV variables
os.environ["PATH"] = ":".join(["dev-python/bin", os.environ["PATH"]])
os.environ["PYTHONPATH"] = ":".join([".", os.environ["PATH"]])

# Constants
OVERLAY_LOCATION = "../worker/overlay"
TEMPLATE_LOCATION = "../worker/overlay/templates"
BENCHMARK_EBUILD_TEMPLATE = "datamill-benchmark-template.ebuild"
CONTROLLER_EBUILD_TEMPLATE = "datamill-controller-template.ebuild"
KEYWORD_LINE = 'KEYWORDS="x86 amd64 arm sparc mips"'
TEST_KEYWORD_LINE = 'KEYWORDS="~x86 ~amd64 ~arm ~sparc"'
LOCAL_OVERLAY_TMP = "/tmp/datamill-overlay"
CHECK_INTEGRATION_RETRY_DELAY_SECS = 30

# ==================== Sub Commands ====================
def test(args):
    """Runs only tests marked as local testable."""
    command = 'nosetests --ignore-files="manage.py" -a "local_testable" {args}'.format(args=" ".join(args.passthrough))

    # We're calling interactively, so just dump test output to STDOUT
    subprocess.call(command, shell=True)

def destructive_test(args):
    """Runs all unit tests (caution, highly destructive to local machine)."""

    # Back up /etc/fstab, we are going to change it
    shutil.copy('/etc/fstab', '/tmp/fstab.bak')

    clean_controller_status_files()
    start_vsftpd()
    command = '/usr/bin/nosetests --ignore-files="manage.py" {args}'.format(args=" ".join(args.passthrough))
    test_output, error = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).communicate()
    logger.info(test_output)
    logger.info(error)
    print test_output
    print error

    # Restore backup
    shutil.copy('/tmp/fstab.bak', '/etc/fstab')

def run_integration_test(args, package_file, user_package=False):
    """Runs a full integration test, using a fake local master."""

    clean_controller_status_files()
    start_vsftpd()

    # REPLACE CONTROLLER-SIDE INIT SCRIPT
    if os.path.isfile('/etc/local.d/00_controller.start'):
        os.remove('/etc/local.d/00_controller.start')

    integ_check_param = 'user' if user_package else ''

    integration_init_script = """#!/usr/bin/env bash
export PYTHONPATH="/datamill:${{PYTHONPATH}}"
/datamill/controller/controller.py >> /dev/null 2>&1 &
python2 /datamill/manage.py check-integration-results {user}
""".format(user=integ_check_param) # would have user param if it was a user package

    init_script_filename = '/etc/local.d/00_integ_test.start'
    with open(init_script_filename, "w") as init_script:
        init_script.write(integration_init_script)

    os.chmod(init_script_filename, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)

    workerdir = '/home/ftp/worker-{}'.format(settings.WORKER_UUID)

    ftp_package_file = os.path.join(workerdir, os.path.basename(package_file))
    shutil.move(package_file, ftp_package_file)
    md5hash.generate_md5(ftp_package_file)

    # JOB CONFIG
    config_name = os.path.join(workerdir, '{}{}'.format(settings.INTEGRATION_JOB_NAME,
        settings.CONF_EXTENSION))

    job_config = """{{ "name":"{}",
"replicate_no":4,
"address_randomization":"on",
"autogroup": "on",
"compiler": "gcc",
"drop_caches": "off",
"env_padding": "0",
"filesystem": "ext4",
"freq_scaling": "off",
"link_order": "alphabetical",
"opt_flag": "-O3",
"fpie": "off",
"sys_time": "on",
"swap": "on" }}
""".format(settings.INTEGRATION_JOB_NAME)

    with open(config_name, "w") as f:
        f.write(job_config)

    md5hash.generate_md5(config_name)

    # GO!
    subprocess.check_call('/usr/bin/python2 /datamill/controller/controller.py 2>&1 | tee /tmp/controller_stdout.log', shell=True)

def destructive_integration_test(args):
    # JOB SCRIPTS
    tmpdir = tempfile.mkdtemp()

    setupsh = """#!/bin/sh
echo I\\'m compiling!
"""
    with open(os.path.join(tmpdir, 'setup.sh'), 'w') as f:
        f.write(setupsh)

    runsh = """#!/bin/sh
sleep 5
echo 5 > /integration_results.txt
"""
    with open(os.path.join(tmpdir, 'run.sh'), 'w') as f:
        f.write(runsh)

    collectsh = """#!/bin/sh
tar czf /integration_results.tar.gz /integration_results.txt
echo /integration_results.tar.gz
"""
    with open(os.path.join(tmpdir, 'collect.sh'), 'w') as f:
        f.write(collectsh)

    # TAR PACKAGE
    package_name = os.path.join(tmpdir, '{}{}'.format(settings.INTEGRATION_JOB_NAME,
        settings.PACKAGE_EXTENSION))
    subprocess.check_call('tar czf {} -C {} setup.sh run.sh collect.sh'.format(package_name, tmpdir), shell=True)

    run_integration_test(args, package_name)

def test_package(args):
    """Test a user package, using a fake local master."""

    clean_controller_status_files()
    start_vsftpd()

    # REPLACE CONTROLLER-SIDE INIT SCRIPT
    if os.path.isfile('/etc/local.d/00_controller.start'):
        os.remove('/etc/local.d/00_controller.start')

    integration_init_script = """#!/usr/bin/env bash
export PYTHONPATH="/datamill:${PYTHONPATH}"
/datamill/controller/controller.py &
python2 /datamill/manage.py check-integration-results user
"""

    init_script_filename = '/etc/local.d/00_integ_test.start'
    with open(init_script_filename, "w") as init_script:
        init_script.write(integration_init_script)

    os.chmod(init_script_filename, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)

    # JOB SCRIPTS
    workerdir = '/home/ftp/worker-{}'.format(settings.WORKER_UUID)

    # GET TAR PACKAGE
    package_name = os.path.join(workerdir, '{}{}'.format(settings.INTEGRATION_JOB_NAME,
        settings.PACKAGE_EXTENSION))
    urllib.urlretrieve(sys.argv[2], package_name)

    md5hash.generate_md5(package_name)

    # JOB CONFIG
    config_name = os.path.join(workerdir, '{}{}'.format(settings.INTEGRATION_JOB_NAME,
        settings.CONF_EXTENSION))

    job_config = """{{ "name":"{}",
"address_randomization":"on",
"autogroup": "on",
"compiler": "gcc",
"drop_caches": "off",
"env_padding": "0",
"filesystem": "ext4",
"freq_scaling": "off",
"link_order": "alphabetical",
"opt_flag": "-O3",
"swap": "on" }}
""".format(settings.INTEGRATION_JOB_NAME)

    with open(config_name, "w") as f:
        f.write(job_config)

    md5hash.generate_md5(config_name)

    # GO!
    subprocess.check_call('/usr/bin/python2 /datamill/controller/controller.py 2>&1 | tee /tmp/controller_stdout.log', shell=True)

def check_integration_results(args):
    workerdir = '/home/ftp/worker-{}/'.format(settings.WORKER_UUID)

    done_name = os.path.join(workerdir,
            '{}.user_results'.format(settings.INTEGRATION_JOB_NAME))

    logger.info('waiting for results file')

    start_time = datetime.datetime.now()
    while not os.path.exists(done_name):

        if (datetime.datetime.now() - start_time).seconds > CHECK_INTEGRATION_RETRY_DELAY_SECS:
            break

        logger.info('still waiting for results file...')
        time.sleep(3)

    logger.info('got results or timed out. going on with verification:')

    try:
        assert(os.path.exists(done_name))
        if len(sys.argv) > 2 and sys.argv[2] == 'user':
            shutil.move(done_name, '/tmp')
            print 'Benchmark Finished!\n===================\nYour results file has been put in /tmp.\nPlease verify its contents before submitting it to DataMill.\n[Sleeping for 15s, then giving you a terminal]'
            time.sleep(15)
        else:
            tmpdir = tempfile.mkdtemp()
            subprocess.check_call("tar xzf {} -C {}".format(done_name, tmpdir), shell=True)
        logger.info('results found and unpacked ok')
    except Exception, e:
        logger.error('results file missing/corrupted: {}'.format(e))
        return

    user_results = os.path.join(tmpdir, 'integration_results.txt')

    try:
        assert(os.path.exists(user_results))
        logger.info('content of user tarball is as expected')
    except AssertionError, e:
        logger.error('user results file missing: {}'.format(e))
        return

    try:
        with open(user_results, 'r') as f:
            contents = f.readlines()
        assert(contents[0].strip() == '5')
        logger.info('results match up')
    except AssertionError, e:
        logger.error('user results incorrect: {}'.format(e))
        return

    shelf_name = os.path.join(workerdir,
            '{}.{}'.format(settings.INTEGRATION_JOB_NAME,
            settings.JOB_STATUS_FILE))

    try:
        assert(os.path.exists(shelf_name))
        logger.info('job shelf found')
    except AssertionError, e:
        logger.error('job shelf file missing: {}'.format(e))
        return

    try:
        with open(shelf_name) as js:
            jobshelf = json.load(js)

        assert(jobshelf['worker_common.job_do_run_time'] > 4.999) # actual run is 5s
        logger.info('job shelf consistent')
    except Exception, e:
        logger.error('job shelf incorrect: {}'.format(e))
        return

    logger.info('integration results all ok!')

def upload_log(args):
    """Upload test results to test master."""
    from controller.master import Master
    m = Master(address='datamill.uwaterloo.ca')
    filename = 'test_log_{}_{}'.format(os.uname()[1],
                datetime.datetime.utcnow().strftime("%y-%m-%d-%H:%M:%S"))
    m.upload("/tmp/datamill_test.log", settings.ERROR_DIR, filename)

def deploy_test_package(args):
    deploy_package(args)

def deploy_package(args, test=True):

    global OVERLAY_LOCATION
    OVERLAY_LOCATION = LOCAL_OVERLAY_TMP
    try:
        shutil.rmtree(LOCAL_OVERLAY_TMP)
    except:
        pass

    rsync_datamill_overlay(LOCAL_OVERLAY_TMP)
    worker_code_version = newest_version()
    new_version = increment_code_version(worker_code_version, level="maintenance")
    if test:
        make_test_ebuild(worker_code_version, new_version)
    else:
        make_production_ebuild(worker_code_version, new_version)
    make_package(new_version)
    copy_to_master(new_version)
    # queue = pubsub.PubSub(settings.UPDATES_CHANNEL)
    # queue.publish({settings.NEW_VERSION_FLAG:1})

    print """

Success!

Your worker package has been deployed to the DataMill ftp.

And your new ebuilds have been deployed
"""

def deploy_production_package(args):
    """Deploy a production worker package."""

    answer = raw_input("""WARNING, deploying a worker package is very dangerous. NO MATTER HOW
SMALL YOUR CHANGE you should have run all of: destructive tests,
destructive integration tests, and a test hitting the test master.

Are you still sure that you want to deploy? """)

    if answer.lower() == "yes":
        deploy_package(args, False)

def make_package(args):
    """Make a package for a given version"""

    if type(args) == argparse.Namespace:
        new_version = args.version[0]
    else:
        new_version = vstring_from_cpv(args)

    staging_dir = get_staging_dir(new_version)
    exclude_patterns = ["dev-python", "worker_status", "overlay", "worker_common/local_settings.py"]

    try:
        # Remove old staging dir for this if it exists
        if os.path.exists(staging_dir):
            shutil.rmtree(staging_dir)

        # Prep staging dir
        shutil.copytree(".", staging_dir)

        # tarfile is dissapointing yet again

        excludes=" ".join(map(lambda pattern: "--exclude='../datamill-worker-{version}/{pattern}'"\
                              .format(version=new_version, pattern=pattern),
                              exclude_patterns))
        excludes=excludes + " --exclude=*.pyc"
        command = "tar cavf {staging}.tar.gz {staging} --anchored {excludes}".format(
            staging=staging_dir,
            excludes=excludes
        )

        print >> sys.stderr, "command: " + command
        subprocess.check_call(command, shell=True)

    finally:
        # Cleanup staging dir
        try:
            shutil.rmtree(staging_dir)
        except:
            pass
        try:
            shutil.move(staging_dir + ".tar.gz", ".")
        except:
            pass


def all_tests(args):
    """This function runs all unit and integration tests."""

    # Simulate args passed from commandline
    destructive_test_args = object()
    destructive_test_args.passthrough = []

    destructive_test(destructive_test_args)

def cleanup_ebuilds(args):
    """Delete worker ebuild versions."""
    pass

def mark_stable(args):
    """Mark the target version of the worker as stable."""
    pass

def shell(args):
    """Run an ipython shell if available."""

    command = 'ipython {args}'.format(args=" ".join(args.passthrough))
    if not os.path.exists("dev-python/bin/ipython"):
        print "Could not find ipython, using :\ndev-python/bin/pip install ipython"
        subprocess.call("dev-python/bin/pip install ipython", shell=True)

    # TODO: setup path if necessary
    subprocess.call(command, shell=True)

# ==================== Utilities ====================

def clean_ftp_dir():
    try:
        shutil.rmtree('/home/ftp')
        os.mkdir('/home/ftp')
    except:
        pass

def clean_controller_status_files():

    for f in [ '/datamill/job_status', '/datamill/worker_status',
            '/datamill/hello.json.md5', '/datamill/hello.json', '/datamill/ip.json', '/datamill/ip.json.md5' ]:
        try:
            os.remove(f)
        except:
            pass

def start_vsftpd():
    # START FTP
    clean_ftp_dir()

    workerdir = '/home/ftp/worker-{}'.format(settings.WORKER_UUID)
    unitworkerdir = '/home/ftp/worker-{}'.format(settings.UNITTEST_WORKER_UUID)
    newworkerdir = '/home/ftp/worker-new/'
    errordir = '/home/ftp/worker-errors/'
    ipdir = '/home/ftp/worker-ip/'

    try:
        uid = getpwnam('ftp')[2]
        gid = getgrnam('ftp')[2]

        for dir in [ workerdir, unitworkerdir, newworkerdir, errordir, ipdir ]:
            os.makedirs(dir)
            os.chown(dir, uid, gid)

    except OSError:
        pass

    try:
        ftp_conf = """anonymous_enable=YES
write_enable=YES
anon_upload_enable=YES
dirmessage_enable=YES
xferlog_enable=YES
connect_from_port_20=YES
listen=YES
hide_file={worker-*,hello-*,datamill-*,internal}
port_enable=YES
seccomp_sandbox=NO
"""
        with open('/etc/vsftpd/vsftpd.conf', 'w') as f:
            f.write(ftp_conf)

        subprocess.check_call('/etc/init.d/vsftpd restart', shell=True)
    except subprocess.CalledProcessError: # ignore if already running (or anything else, really)
        pass

def rsync_datamill_overlay(datamill_overlay_tmp):

    subprocess.check_call("rsync -avz rsync://datamill.uwaterloo.ca/datamill {target_location}"\
                          .format(target_location=datamill_overlay_tmp), shell=True)

def make_ebuild(worker_code_version, new_version, keyword_line):
    # Assemble target cpvs
    new_ebuilds = map(lambda package: tuple([os.path.join("sys-apps", package)] + list(new_version[1:])),
        ["datamill-controller", "datamill-benchmark"])

    # Get the physical paths of the new ebuilds
    new_ebuilds = map(ebuild_from_cpv, new_ebuilds)
    ebuild_templates = map(lambda template: os.path.join(TEMPLATE_LOCATION, template),
                           [CONTROLLER_EBUILD_TEMPLATE, BENCHMARK_EBUILD_TEMPLATE])

    for i in xrange(len(new_ebuilds)):
        try:
            with open(ebuild_templates[i]) as template:
                with open(new_ebuilds[i], "w") as ebuild:
                    for line in template.readlines():
                        ebuild.write(line)
                    ebuild.write(keyword_line + "\n")

        except Exception:
            try:
                map(os.remove, new_ebuilds)
            except:
                pass
            raise

def make_production_ebuild(worker_code_version, new_version):

    make_ebuild(worker_code_version, new_version, KEYWORD_LINE)

def make_test_ebuild(worker_code_version, new_version):

    make_ebuild(worker_code_version, new_version, TEST_KEYWORD_LINE)

def list_versions(location):
    return map(lambda ebuild: re.sub(r"\.ebuild", "", os.path.join("sys-apps", ebuild)),
               filter(lambda f: f.endswith("ebuild"),
                      os.listdir(os.path.join(OVERLAY_LOCATION, location))))

def max_ebuild_version(ebuilds):
    return sorted(map(portage.pkgsplit, ebuilds), cmp=portage.pkgcmp)[-1]

def ebuild_from_cpv(cpv):
    category, package = cpv[0].split('/')
    ebuild_prefix = os.path.join(OVERLAY_LOCATION, category, package, package)

    return "-".join([ebuild_prefix, vstring_from_cpv(cpv)]) + ".ebuild"

def vstring_from_cpv(cpv):

    # Are we already a string?
    if type(cpv) is str:
        return cpv

    # If we are at revision zero
    if cpv[2] == "r0":
        # Ignore revision
        return cpv[1]
    else:
        return "-".join(cpv[1:])

def newest_version():

    newest_controller = max_ebuild_version(list_versions("sys-apps/datamill-controller"))
    newest_benchmark = max_ebuild_version(list_versions("sys-apps/datamill-benchmark"))

    if newest_controller[1:] != newest_benchmark[1:]:
       raise Exception("Warning, newest controller and benchmark version are not the same! controller: {} benchmark: {}".format(newest_controller, newest_benchmark))

    return newest_controller

def increment_code_version(worker_code_version, level="maintenance"):
    mapping= {
        "major": 0,
        "minor": 1,
        "maintenance": 2
    }

    version = map(int, worker_code_version[1].split("."))
    version[mapping[level]] = version[mapping[level]] + 1
    # Set version, reset revision to 0
    worker_code_version = list(worker_code_version)
    worker_code_version[1:] = [".".join(map(str, version)), "r0"]
    return tuple(worker_code_version)

def copy_to_master(new_version):
    package_path = "datamill-worker-{version}.tar.gz".format(version=vstring_from_cpv(new_version))

    # Copy tarball to ftp
    subprocess.check_call("scp {package} root@datamill.uwaterloo.ca:/home/ftp".format(package=package_path), shell=True)
    # Copy overlay, as is, to tmpdir
    subprocess.check_call("ssh root@datamill.uwaterloo.ca 'rm -rf /tmp/datamill-overlay-tmp'", shell=True)
    subprocess.check_call("scp -r {overlaydir} root@datamill.uwaterloo.ca:/tmp/datamill-overlay-tmp".format(overlaydir=OVERLAY_LOCATION), shell=True)

    overlay_update_script = """
    if [[ -f /var/lock/datamill_overlay_update ]]; then exit 1; fi
    touch /var/lock/datamill_overlay_update

    /etc/init.d/rsyncd stop
    rm -rf /tmp/datamill-overlay-bak
    cp -r /var/datamill-overlay /tmp/datamill-overlay-bak
    cd /tmp/datamill-overlay-tmp
    rm -rf templates
    make clean || exit 1
    make || exit 1
    rm -rf /var/datamill-overlay/* || exit 1
    cp -r /tmp/datamill-overlay-tmp/* /var/datamill-overlay || exit 1
    /etc/init.d/rsyncd start

    rm /var/lock/datamill_overlay_update
    """

    # If any of the above fails, this should return us to the previous
    # overlay state
    recovery_script = """
    if [[ -f /var/lock/datamill_overlay_update ]]; then exit 1; fi
    touch /var/lock/datamill_overlay_update

    cp -r /tmp/datamill-overlay-bak/* /var/datamill-overlay/ || exit 1
    /etc/init.d/rsyncd restart

    rm /var/lock/datamill_overlay_update
    """

    overlay_update_script = pack_script(overlay_update_script)
    recovery_script = pack_script(recovery_script)

    try:
        subprocess.check_call("ssh root@datamill.uwaterloo.ca '{command}'".format(command=overlay_update_script), shell=True)
    except Exception as e:
        print "overlay_update_script failed with: " + str(e)
        print "Attempting to recover"
        subprocess.check_call("ssh root@datamill.uwaterloo.ca '{command}'".format(command=recovery_script), shell=True)
        raise

    # Final cleanup
    os.remove(package_path)

def get_staging_dir(new_version):
    return "../datamill-worker-{version}".format(version=new_version)

def pack_script(script):
    return "; ".join(filter(lambda line: not line == "",
                            map(lambda line: line.strip(),
                                script.split("\n"))))

# ******************** manage.py app ********************
parser = argparse.ArgumentParser(description="Manage DataMill worker tests and deployment.")
subparsers = parser.add_subparsers(help='sub-command help')

# ==================== Running local tests ====================
parser_test = subparsers.add_parser('test',
    help=test.__doc__)

parser_test.add_argument('passthrough', metavar='nosearg', type=str, nargs='*',
    help="Argument to be passed to nosetests.")

parser_test.set_defaults(func=test)

# ==================== Running virtual tests ====================
parser_destructive_test = subparsers.add_parser('destructive-test',
    help=destructive_test.__doc__)

parser_destructive_test.add_argument('passthrough', metavar='nosearg', type=str, nargs='*',
    help="Argument to be passed to nosetests.")

parser_destructive_test.set_defaults(func=destructive_test)

# ==================== Running integration tests ====================
parser_destructive_integration_test = subparsers.add_parser('destructive-integration-test',
    help=destructive_integration_test.__doc__)

parser_destructive_integration_test.add_argument('passthrough', metavar='nosearg', type=str, nargs='*',
    help="Argument to be passed to integration tests.")

parser_destructive_integration_test.set_defaults(func=destructive_integration_test)

# ==================== Checking integration tests ====================
parser_check_integration_results = subparsers.add_parser('check-integration-results',
    help=check_integration_results.__doc__)

parser_check_integration_results.add_argument('passthrough', metavar='nosearg', type=str, nargs='*',
    help="Argument to be passed to integration results checker.")

parser_check_integration_results.set_defaults(func=check_integration_results)

# ==================== Run test package ====================
parser_test_package = subparsers.add_parser('test-package',
    help=test_package.__doc__)

parser_test_package.add_argument('passthrough', metavar='nosearg', type=str, nargs='*',
    help="Argument to be passed to integration tests.")

parser_test_package.set_defaults(func=test_package)

# ==================== Uploading tests to test master ====================
parser_upload_log = subparsers.add_parser('upload-log',
    help=upload_log.__doc__)

parser_upload_log.set_defaults(func=upload_log)

# ==================== deploying test packages ====================
parser_deploy_test_package = subparsers.add_parser('deploy-test-package',
    help=deploy_test_package.__doc__)

parser_deploy_test_package.set_defaults(func=deploy_test_package)

# ==================== deploying production packages ====================
parser_deploy_production_package = subparsers.add_parser('deploy-production-package',
    help=deploy_production_package.__doc__)

parser_deploy_production_package.set_defaults(func=deploy_production_package)

# ==================== marking packages as stable ====================
parser_mark_stable = subparsers.add_parser('mark-stable',
    help=mark_stable.__doc__)

parser_mark_stable.set_defaults(func=mark_stable)

# ==================== Make a package ====================
parser_make_package = subparsers.add_parser('make-package',
    help=make_package.__doc__)
parser_make_package.add_argument('version', metavar='version', type=str, nargs=1, help="Version number of package")

parser_make_package.set_defaults(func=make_package)

# ==================== Running local shell ====================
parser_shell = subparsers.add_parser('shell',
    help=shell.__doc__)

parser_shell.add_argument('passthrough', metavar='nosearg', type=str, nargs='*',
    help="Argument to be passed to ipython.")

parser_shell.set_defaults(func=shell)

# ******************** Run manage.py ********************
args = parser.parse_args(sys.argv[1:])
args.func(args)
