import unittest, os, sys
import subprocess
from unittest.case import SkipTest
from compiler_wrapper.compiler_wrapper import Llvm

class TestArgumentManipulation(unittest.TestCase):

    def setUp(self):
        os.environ['WRAPPER_LINK_ORDER'] = 'alphabetical'
        os.environ['WRAPPER_OPT_FLAG'] = '-O2'
        os.environ['WRAPPER_CC'] = "llvm"
        os.environ['WRAPPER_POSITION_INDEPENDENT'] = "off"
    
    def test_link_order(self):
        os.environ['WRAPPER_LINK_ORDER'] = 'alphabetical'
        os.environ['WRAPPER_OPT_FLAG'] = '-O2'
        llvm = Llvm('clang++', ['-c', 'test1.cpp', 'test2.cpp'])
        commands = llvm.compile(pretend=True)
        self.assertEqual(commands, [['/usr/bin/clang++', '-c', '-O2', 'test1.cpp', 'test2.cpp']])
    test_link_order.local_testable = True

    def test_link_order_reverse(self):
        os.environ['WRAPPER_LINK_ORDER'] = 'reverse alphabetical'
        os.environ['WRAPPER_OPT_FLAG'] = '-O2'
        llvm = Llvm('clang++', ['-emit-llvm', '-c', 'test1.cpp', 'test2.cpp'])
        commands = llvm.compile(pretend=True)
        self.assertEqual(commands[0:2], [['/usr/bin/clang++', '-c', '-O2', '-emit-llvm', 'test1.cpp', 'test2.cpp']])
    test_link_order_reverse.local_testable = True

    def test_file_args_only(self):
        os.environ['WRAPPER_LINK_ORDER'] = 'reverse alphabetical'
        os.environ['WRAPPER_OPT_FLAG'] = '-O3'
        llvm = Llvm('clang++', ['test1.cpp', 'test2.cpp'])
        commands = llvm.compile(pretend=True)
        self.assertEqual(commands[0:2], [['/usr/bin/clang++', '-c', '-O3', 'test1.cpp', 'test2.cpp'],
                                         ['/usr/bin/clang++', '-O3', '-o', 'a.out', 'test2.o', 'test1.o']])
    test_file_args_only.local_testable = True

    def test_optimization_flag(self):
        os.environ['WRAPPER_LINK_ORDER'] = 'reverse alphabetical'
        os.environ['WRAPPER_OPT_FLAG'] = '-O3'
        llvm = Llvm('clang++', ['-emit-llvm', '-O2', '-c', 'test1.cpp', 'test2.cpp'])
        commands = llvm.compile(pretend=True)
        self.assertEqual(commands[0:2], [['/usr/bin/clang++', '-c', '-O3', '-emit-llvm', 'test1.cpp', 'test2.cpp']])
    test_optimization_flag.local_testable = True

    def test_build_executable(self):

        # Skip test if we don't have clang.
        if not os.path.exists('/usr/bin/clang'):
            raise SkipTest()

        os.environ['PATH'] = '../../../compiler_wrapper/bin:' + os.environ['PATH']
        os.environ['PYTHONPATH'] = os.path.realpath('../../..') + ":" + os.environ['PYTHONPATH']
        os.environ['TESTING'] = str(True)
        os.environ['CC'] = "clang"
        example_c = os.path.join('compiler_wrapper', 'tests', 'example_c')

        compiler_log = os.path.join(example_c, 'compiler.log')
        if os.path.exists(compiler_log):
            os.remove(compiler_log)
            
        lines = subprocess.check_output('make options', shell=True, cwd=example_c)

        options = map(lambda string: string.split(), lines.split("\n"))
        self.assertEqual(options[3][2], "clang")

        subprocess.check_output('make clean', stderr=subprocess.STDOUT, shell=True, cwd=example_c)
        subprocess.check_output('make', stderr=subprocess.STDOUT, shell=True, cwd=example_c)

        with open(os.path.join(example_c, 'compiler.log'), 'r') as f:
            lines = f.read().split("\n")
            
        self.assertEqual(lines, ["compiler_wrapper.Llvm['-c', '-g', '-std=c99', '-pedantic', '-Wall', '-O2', 'main.c']",
                                 "compiler_wrapper.Llvm['-o', 'example', 'main.o']", ''])

        self.assertTrue(os.path.exists(os.path.join(example_c, 'example')))

    test_build_executable.local_testable = True

if __name__ == '__main__': unittest.main()
