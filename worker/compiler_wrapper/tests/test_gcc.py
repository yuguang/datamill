import unittest, os, sys
import subprocess
from unittest.case import SkipTest
from compiler_wrapper.compiler_wrapper import Gcc

class TestArgumentManipulation(unittest.TestCase):
    def setUp(self):
        os.environ['WRAPPER_LINK_ORDER'] = 'alphabetical'
        os.environ['WRAPPER_OPT_FLAG'] = '-O2'
        os.environ['WRAPPER_CC'] = "gcc"
        os.environ['WRAPPER_POSITION_INDEPENDENT'] = 'off'

    def test_no_manipulation(self):
        gcc = Gcc('gcc',['input.cpp', '-o', 'test'])
        commands = gcc.compile(pretend=True)
        self.assertEqual(commands, [['/usr/bin/gcc', '-c', '-O2', 'input.cpp'],
                                    ['/usr/bin/gcc', '-O2', '-o', 'test', 'input.o']])

    test_no_manipulation.local_testable = True

    def test_extra_options(self):
        gcc = Gcc('gcc', ['input.cpp', '-flag', '-o', 'test'])
        commands = gcc.compile(pretend=True)
        self.assertEqual(commands, [['/usr/bin/gcc', '-c', '-O2', '-flag', 'input.cpp'],
                                    ['/usr/bin/gcc', '-O2', '-flag', '-o', 'test', 'input.o']])
    test_extra_options.local_testable = True

    def test_link_order_manipulation_alphabetical(self):
        os.environ['WRAPPER_LINK_ORDER'] = 'alphabetical'
        gcc = Gcc('gcc', ['input1.c', 'input2.c', 'input3.c', 'input4.c', '-o', 'test'])
        commands = gcc.compile(pretend=True)
        self.assertEqual(commands, [['/usr/bin/gcc', '-c', '-O2', 'input1.c', 'input2.c', 'input3.c', 'input4.c'],
                                    ['/usr/bin/gcc', '-O2', '-o', 'test', 'input1.o', 'input2.o', 'input3.o', 'input4.o']])
    test_link_order_manipulation_alphabetical.local_testable = True

    def test_link_order_manipulation_reverse_alphabetical(self):
        os.environ['WRAPPER_LINK_ORDER'] = 'reverse alphabetical'
        gcc = Gcc('gcc', ['input1.c', 'input3.c', 'input2.c', 'input4.c', '-o', 'test'])
        commands = gcc.compile(pretend=True)
        self.assertEqual(commands, [['/usr/bin/gcc', '-c', '-O2', 'input1.c', 'input3.c', 'input2.c', 'input4.c'],
                                    ['/usr/bin/gcc', '-O2', '-o', 'test', 'input4.o', 'input3.o', 'input2.o', 'input1.o']])
    test_link_order_manipulation_reverse_alphabetical.local_testable = True

    def test_optimization_manipulation(self):
        gcc = Gcc('gcc', ['-O0', 'input1.c', 'input2.c', 'input3.c', 'input4.c', '-o', 'test'])
        commands = gcc.compile(pretend=True)
        self.assertEqual(commands, [['/usr/bin/gcc', '-c', '-O2', 'input1.c', 'input2.c', 'input3.c', 'input4.c'],
                                    ['/usr/bin/gcc', '-O2', '-o', 'test', 'input1.o', 'input2.o', 'input3.o', 'input4.o']])
    test_optimization_manipulation.local_testable = True

    def test_optimization_manipulation_with_options(self):
        os.environ['WRAPPER_OPT_FLAG'] = '-O2'
        gcc = Gcc('gcc', ['input1.c', 'input2.c', 'input3.c', 'input4.c'])
        commands = gcc.compile(pretend=True)
        self.assertEqual(commands, [['/usr/bin/gcc', '-c', '-O2', 'input1.c', 'input2.c', 'input3.c', 'input4.c'],
                                    ['/usr/bin/gcc', '-O2', '-o', 'a.out', 'input1.o', 'input2.o', 'input3.o', 'input4.o']])
    test_optimization_manipulation_with_options.local_testable = True

    def test_fpie(self):
        os.environ['WRAPPER_POSITION_INDEPENDENT'] = 'on'
        gcc = Gcc('gcc', ['input.cpp'])
        commands = gcc.compile(pretend=True)
        self.assertEqual(commands, [['/usr/bin/gcc', '-c', '-fpie', '-O2', 'input.cpp'],
                                    ['/usr/bin/gcc', '-O2', '-o', 'a.out', 'input.o']])

    test_fpie.local_testable = True

    def test_compile_no_link(self):
        # When a -c is specified explicitly in the gcc command (as it
        # often is) we should try to immitate usage as much as
        # possible.
        gcc = Gcc('gcc', ['-c', 'main.c'])
        commands = gcc.compile(pretend=True)
        self.assertEqual(commands, [['/usr/bin/gcc', '-c', '-O2', 'main.c']])
    test_compile_no_link.local_testable = True

    def test_build_executable(self):

        # Skip test if we don't have gcc.
        if not os.path.exists('/usr/bin/gcc'):
            raise SkipTest()

        # Set path relative to example_c
        os.environ['PATH'] = '../../../compiler_wrapper/bin:' + os.environ['PATH']
        os.environ['PYTHONPATH'] = os.path.realpath('../../..') + ":" + os.environ['PYTHONPATH']
        os.environ['TESTING'] = str(True)
        example_c = os.path.join('compiler_wrapper', 'tests', 'example_c')

        compiler_log = os.path.join(example_c, 'compiler.log')
        if os.path.exists(compiler_log):
            os.remove(compiler_log)
            
        lines = subprocess.check_output('make options', shell=True, cwd=example_c)

        options = map(lambda string: string.split(), lines.split("\n"))
        self.assertEqual(options[3][2], "cc")

        subprocess.check_output('make clean', stderr=subprocess.STDOUT, shell=True, cwd=example_c)
        subprocess.check_output('make', stderr=subprocess.STDOUT, shell=True, cwd=example_c)

        with open(os.path.join(example_c, 'compiler.log'), 'r') as f:
            lines = f.read().split("\n")
            
        self.assertEqual(lines, ["compiler_wrapper.Gcc['-c', '-g', '-std=c99', '-pedantic', '-Wall', '-O2', 'main.c']",
                                 "compiler_wrapper.Gcc['-o', 'example', 'main.o']", ''])

        self.assertTrue(os.path.exists(os.path.join(example_c, 'example')))

    test_build_executable.local_testable = True

if __name__ == '__main__': unittest.main()
