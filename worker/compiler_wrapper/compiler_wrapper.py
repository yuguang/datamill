import os
import sys
import argparse
import subprocess

class CompilerWrapper:
    def __init__(self, compiler_string, args):
        self.args = args
        self.args = self.remove_optimization_flags(self.args)
        self.compiler = self.__abs_path_executable(compiler_string)

        parser = argparse.ArgumentParser()
        parser.add_argument("-o", action="store", dest="output")
        parser.add_argument("-c", action="store_true")
        parser.add_argument("-E", action="store_true")
        parser.add_argument("files", nargs='*')
        (self.parsed, self.unparsed) = parser.parse_known_args(self.args)

        self.optimization_flag = os.environ.get('WRAPPER_OPT_FLAG', '-O2')
        self.link_order = os.environ.get('WRAPPER_LINK_ORDER', 'default')
        self.fpie = os.environ.get('WRAPPER_POSITION_INDEPENDENT')

    def remove_optimization_flags(self, flags):
        opt_flags = ['-O', '-O0', '-O1', '-O2', '-O3', '-Os', '-Ofast', '-fpie']
        new_flags = filter(lambda flag: flag not in opt_flags,
                           flags)
        return new_flags

    def apply_link_order(self, object_files):
        if self.link_order == 'alphabetical':
            object_files.sort()
        elif self.link_order == 'reverse alphabetical':
            object_files.sort()
            object_files.reverse()
        return object_files

    def get_input_files(self):
        return self.parsed.files

    def get_output_file(self):
        if self.parsed.output:
            return self.parsed.output
        else:
            return 'a.out'

    def get_object_files(self):
        input_files = self.get_input_files()
        object_files = []
        for inputFile in input_files:
            fileName, fileExtension = os.path.splitext(inputFile)
            object_files.append(fileName + '.o')
        return object_files

    def __abs_path_executable(self, executable):
        executable_name = executable.split('/')[-1]

        old_path = os.environ.get("PATH")
        new_path = ":".join(filter(lambda path: "/datamill/compiler_wrapper/bin" not in path,
                                   old_path.split(":")))
        try:
            os.environ["PATH"] = new_path
            executable_path = subprocess.check_output("which {compiler}".format(executable=executable_name),
                                                    stderr=subprocess.STDOUT,
                                                    shell=True).strip()

        except Exception:
            executable_path = '/usr/bin/{compiler}'.format(compiler=executable_name)

        finally:
            os.environ["PATH"] = old_path

        return executable_path


class Gcc(CompilerWrapper):

    def compile(self, pretend=False):
        # Need to find a better way of including the flag for pie then a couple of
        # if statments.
        # Inserting -c is pretty safe, most commands that don't compile ignore it
        if self.fpie == 'on':
            compile_to_object_files = [self.compiler, '-c'] + ['-fpie'] +[self.optimization_flag] \
                                      + self.unparsed + self.get_input_files()
        else:
            compile_to_object_files = [self.compiler, '-c'] + [self.optimization_flag] \
                                      + self.unparsed + self.get_input_files()

        if self.parsed.c or self.parsed.E or self.parsed.files == []:
            commands = [compile_to_object_files]
        else:
            object_files = self.apply_link_order(self.get_object_files())
            generate_native_executable = [self.compiler] + [self.optimization_flag] + self.unparsed \
                                         + ['-o', self.get_output_file()] + object_files

            commands = [compile_to_object_files, generate_native_executable]

        if pretend:
            return commands
        for command in commands:
            try:
                command = ' '.join(command)
                subprocess.check_call(command, shell=True)
            except subprocess.CalledProcessError as e:
                return e
            except OSError as e:
                return e

class Llvm(Gcc): pass
