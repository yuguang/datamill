from unittest import TestCase
import os
import subprocess
import portage
import shutil

from controller.md5hash import md5hash
from controller.master import Master
from worker_common.worker import Worker
from worker_common import settings

class Master_Tests(TestCase):

    # assumes ftp running on localhost

    def setUp(self):
        self.m = Master('127.0.0.1')
        self.w = Worker(self.m, '/tmp/tmpstatus', force_uuid=settings.UNITTEST_WORKER_UUID)

        ftpdirs = filter(lambda path: os.path.isdir(path),
                         map(lambda path: os.path.join('/home/ftp', path),
                             os.listdir('/home/ftp')))
        for d in ftpdirs:
            for f in os.listdir(d):
                filename = os.path.join(d, f)
                if os.path.isdir(filename):
                    shutil.rmtree(filename)
                else:
                    os.remove(filename)

    def tearDown(self):

        os.remove('/tmp/tmpstatus')

        ftpdirs = filter(lambda path: os.path.isdir(path),
                         map(lambda path: os.path.join('/home/ftp', path),
                             os.listdir('/home/ftp')))
        for d in ftpdirs:
            for f in os.listdir(d):
                filename = os.path.join(d, f)
                if os.path.isdir(filename):
                    shutil.rmtree(filename)
                else:
                    os.remove(filename)

    def test_warn(self):
        open(os.path.join(settings.ROOT, settings.WORKER_LOG_DIR, settings.CONTROLLER_STDOUT_LOG), 'a').close()
        open(os.path.join(settings.ROOT, settings.WORKER_LOG_DIR, settings.STDERR_LOG), 'a').close()
        self.m.warn(self.w)

        my_files_in_tmp = [ f for f in os.listdir('/tmp')
                if f.startswith('{}_'.format(os.uname()[1])) ]

        for f in my_files_in_tmp:
            self.assertTrue(os.path.exists(
                os.path.join('/home/ftp/', settings.ERROR_DIR, f))
                )

        for f in my_files_in_tmp:
            os.remove(os.path.join('/tmp', f))
        for f in os.listdir(os.path.join('/home/ftp/', settings.ERROR_DIR)):
            os.remove(os.path.join('/home/ftp', settings.ERROR_DIR, f))

    def test_connect(self):
        self.m.connect()
        self.assertEquals(self.m.ftp.pwd(), '/')

        # test that calling connect 2x doesn't break anything
        self.m.connect()
        self.assertEquals(self.m.ftp.pwd(), '/')

    def test_worker_ftp_dir(self):
        self.assertEquals(Master.worker_ftp_dir(self.w),
                'worker-{}'.format(self.w.uuid()))

    def test_upload(self):
        self.m.upload('/tmp/tmpstatus', '/worker-new/', 'tmpstatus')
        self.assertTrue(os.path.exists('/home/ftp/worker-new/tmpstatus'))
        os.remove('/home/ftp/worker-new/tmpstatus')

    def test_download(self):
        testfile = '/home/ftp/worker-new/tmpfile'
        open(testfile, 'w').close() # touch
        md5hash.generate_md5(testfile)

        self.m.download('/worker-new', 'tmpfile', '/tmp/tmpfile')
        self.assertTrue(os.path.exists('/tmp/tmpfile'))
        os.remove('/tmp/tmpfile')
        os.remove(testfile)

    def test_register(self):
        self.m.register(self.w)
        self.assertTrue(os.path.exists(
            '/home/ftp/worker-new/{}v{}r{}{}'.format(
                self.w.uuid(),
                self.w.hello_version(),
                self.w.hardware_revision(),
                settings.HELLO_EXTENSION)))

    def test_update_ip(self):
        self.w.make_ip_file()
        self.m.update_ip(self.w)
        self.assertTrue(os.path.exists(
            '/home/ftp/worker-ip/{}{}'.format(self.w.uuid(), settings.IP_EXTENSION)))

    def test_pick_job(self):

        jobfiles = [ '00-dummy.tar.gz', '00-dummy.done',
                     '01-dummy.tar.gz', '00-dummy.wip',
                     '02-dummy.tar.gz' ]

        workerdir = self.m.worker_ftp_dir(self.w)

        for j in jobfiles:
            open(os.path.join('/home/ftp/', workerdir, j), 'w').close() # touch

        jname = self.m.pick_job(self.w)
        self.assertEqual(jname, '01-dummy')

    def test_get_job(self):
        jobfiles = [ '00-dummy.tar.gz', '00-dummy.wip', '00-dummy.done', '00-dummy.json',
                     '01-dummy.tar.gz', '01-dummy.json',
                     '02-dummy.tar.gz', '02-dummy.json' ]

        workerdir = self.m.worker_ftp_dir(self.w)

        for j in jobfiles:
            filename = os.path.join('/home/ftp/', workerdir, j)
            with open(filename, 'w') as f:
                f.write('{}\n') # make the json config loader load an empty config
            md5hash.generate_md5(filename)

        job = self.m.get_job(self.w)
        self.assertEquals(job.name(), '01-dummy')
        self.assertTrue(os.path.exists(job.package_file))
        self.assertTrue(os.path.exists(job.configuration_file))

        for j in jobfiles:
            os.remove(os.path.join('/home/ftp/', workerdir, j))

        for f in [ job.package_file,
                job.configuration_file,
                os.path.join(settings.ROOT, settings.DATAMILL_DIR, settings.JOB_STATUS_FILE) ]:
            os.remove(f)

    def test_put_wip(self):
        jobfiles = [ '00-dummy.tar.gz', '00-dummy.json',
                     '01-dummy.tar.gz', '01-dummy.json',
                     '02-dummy.tar.gz', '02-dummy.json' ]

        workerdir = self.m.worker_ftp_dir(self.w)

        for j in jobfiles:
            filename = os.path.join('/home/ftp/', workerdir, j)
            with open(filename, 'w') as f:
                f.write('{}\n') # make the json config loader load an empty config
            md5hash.generate_md5(filename)

        job = self.m.get_job(self.w)
        self.m.put_wip(self.w, job)
        wip_filename = os.path.join('/home/ftp/', workerdir, '{}{}'.format(job.name(), settings.WIP_EXTENSION))

        self.assertTrue(os.path.exists(wip_filename))

    def test_put_job(self):
        jobfiles = [ '00-dummy.tar.gz', '00-dummy.json',
                     '01-dummy.tar.gz', '01-dummy.json',
                     '02-dummy.tar.gz', '02-dummy.json' ]

        workerdir = self.m.worker_ftp_dir(self.w)

        for j in jobfiles:
            filename = os.path.join('/home/ftp/', workerdir, j)
            with open(filename, 'w') as f:
                f.write('{}\n') # make the json config loader load an empty config
            md5hash.generate_md5(filename)

        job = self.m.get_job(self.w)
        job.finish()
        self.m.put_job(self.w, job)

        shelf_filename = os.path.join('/home/ftp', Master.worker_ftp_dir(self.w),
                '{}.{}'.format(job.name(), settings.JOB_STATUS_FILE))
        self.assertTrue(os.path.exists(shelf_filename))

        for f in [ job.package_file,
                job.configuration_file,
                os.path.join(settings.ROOT, settings.DATAMILL_DIR, settings.JOB_STATUS_FILE) ]:
            os.remove(f)
