#!/usr/bin/env python2

import os
import time
from worker_common import settings

import logging.config
logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger('controller')

from worker_common.worker import Worker
from master import Master
from worker_common.emerge import Emerge
from datetime import datetime
import time, ftplib

logger.info('controller up')
last_success = datetime.now()

m = None
w = None

try:
    while True:
        try:
            # 1: UPDATE CODE
            if Emerge.worker_needs_update():
                logger.info("updating controller code")
                Emerge.update_controller_package()
                Worker.reboot()

            if not m:
                m = Master()

            if not w:
                w = Worker(m, force_uuid=settings.WORKER_UUID)

            # 2: UPDATE BENCHMARK BACKUP
            if w.backup_needs_update():
                logger.info("updating backup tarball")
                w.filesystem.create_backup()

            # 3: REGISTER
            if not w.is_registered():
                logger.info("registering with master node")
                w.register()

            # 4: UPDATE IP
            if w.ip_changed():
                w.update_ip()

            # 5: REPORT RESULTS
            if w.has_pending_results():
                logger.info("uploading results")
                w.upload_pending_results()

            # 6: GET WORK
            if w.get_job():
                logger.info("got work, rebooting")
                Worker.reboot()
            else:
                logger.info("nothing to do, sleeping")
                last_success = datetime.now()
                time.sleep(settings.CONTROLLER_LOOP_SLEEP_TIME)

        except Exception as e:
            logger.warning('Controller exception: {}'.format(e))

            w.boot_loader.set_boot_to_controller()

            if w.job is not None:
                w.job.fail(str(e))
                w.upload_job_status()

            if (datetime.now() - last_success).days > settings.WORKER_TIMEOUT_DAYS or (isinstance(e, ftplib.error_perm) and e.args[0][:3] == "550"):
                logger.warning('Controller timed out, raising to clear: {}'.format(e))
                raise
            else:
                time.sleep(settings.CONTROLLER_LOOP_SLEEP_TIME)

except Exception as e:
    # SOMETHING BAD HAS HAPPENED, THIS RECOVERY _MUST NOT_ FAIL
    logger.warning('Reached outer exception handler, clearing and starting over: {}'.format(e))

    worker_status_file = os.path.join(settings.ROOT,
                                      settings.DATAMILL_DIR,
                                      settings.WORKER_STATUS_FILE)

    try:
        if not m:
            m = Master()
        if not w:
            w = Worker(m, force_uuid=settings.WORKER_UUID, fail_safe=True)
        m.warn(w)
        w.clear()
    except:
        if os.path.exists(worker_status_file):
            os.remove(worker_status_file)

    # sleep to allow an ssh connection to fix machines
    time.sleep(settings.CONTROLLER_LOOP_SLEEP_TIME)
    Worker.reboot()
