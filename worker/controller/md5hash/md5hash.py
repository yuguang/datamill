import hashlib

class FileCorrupted(Exception):
    def __init__(self, filename, good_hash, bad_hash):
        self.filename = filename
        self.good_hash = good_hash
        self.bad_hash = bad_hash
    def __str__(self):
        return self.filename + ' is corrupted: ' + self.bad_hash + ' != ' + self.good_hash

def generate_md5(filename):
    md5 = hashlib.md5()
    with open(filename,'rb') as f:
        for chunk in iter(lambda: f.read(128 * md5.block_size), b''):
            md5.update(chunk)

    hash_filename = '{}.md5'.format(filename)
    with open(hash_filename, 'w') as hf:
        hf.write(md5.hexdigest())

    return hash_filename

def check_md5(filename):
    md5 = hashlib.md5()
    with open(filename,'rb') as f:
        for chunk in iter(lambda: f.read(128 * md5.block_size), b''):
            md5.update(chunk)
    with open(filename + '.md5', 'r') as hf:
        hash = hf.read()
    if hash != md5.hexdigest():
        raise FileCorrupted(filename, hash, md5.hexdigest())

    # Return true if all is well.
    return True
