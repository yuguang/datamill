#!/usr/bin/env python2

import os

from worker_common import settings
from worker_common.worker import Worker

import logging.config
logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger('benchmark')

from worker_common.job import Job

# should succeed in all cases
try:
    logger.info('benchmark up')
    j = Job.from_package(
        os.path.join(settings.ROOT, settings.DATAMILL_DIR,
                     settings.STANDARD_PACKAGE_FILENAME),
        os.path.join(settings.ROOT, settings.DATAMILL_DIR,
                     settings.STANDARD_CONFIGURATION_FILENAME))
except Exception as e:
    logger.error('failed to create job object, fatal, rebooting: {}'.format(e))
    Worker.reboot()

try:
    os.environ["DATAMILL_REPLICATE_NO"] = str(j.configuration.get('replicate_no', 0))

    workdir = os.path.join(settings.ROOT, settings.BENCHMARK_WORK_DIR)
    os.makedirs(workdir)

    logger.info('unpacking')
    j.unpack(workdir)

    logger.info('doing setup')
    j.do_presetup_hooks()
    j.do_setup()

    logger.info('doing run')
    j.do_prerun_hooks()
    j.do_run()

    logger.info('doing collect')
    j.do_precollect_hooks()
    j.do_collect()

    logger.info('verifying factor levels')
    j.verify_factor_levels()

    j.finish()
    logger.info('benchmark done!')

except Exception as e:
    logger.error('failed to run job, fatal, rebooting: {}'.format(e))
    j.fail('failed to run job, fatal, rebooting: {}'.format(e))

Worker.reboot()
