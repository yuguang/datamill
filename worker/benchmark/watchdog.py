#!/usr/bin/env python2

import time

from worker_common import settings

import logging.config
logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger('watchdog')

from worker_common.boot_loader import BootLoaderFactory
from worker_common.worker_info import get_architecture
from worker_common.worker import Worker

try:
    bl = BootLoaderFactory(get_architecture())
    bl.set_boot_to_controller()

    logger.info('watchdog going to sleep')
    time.sleep(settings.BENCHMARK_TIME_LIMIT_HOURS * 3600)
    logger.warning('watchdog woke up, rebooting')

    Worker.reboot()

except Exception as e:
    logger.error('watchdog exception: {}. rebooting'.format(e))
    Worker.reboot()
