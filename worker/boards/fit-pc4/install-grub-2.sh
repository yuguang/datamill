#!/bin/bash
mount /dev/sda3 /mnt/gentoo
mount /dev/sda1 /mnt/gentoo/boot
mount -t proc none /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
chroot /mnt/gentoo /bin/bash
screen
source /etc/profile
echo 'GRUB_PLATFORMS="pc"' >> /etc/portage/make.conf
emerge -C sys-boot/grub:0
emerge -N sys-boot/grub:2
mount -o remount,rw /boot
grub2-install /dev/sda
mv /boot/kernel /boot/kernel-x86_64-3.18.11-gentoo
grub2-mkconfig -o /boot/grub/grub.cfg
cat /boot/grub/grub.cfg
