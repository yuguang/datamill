# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit eutils

DESCRIPTION="This is the ebuild for the worker side of the eval-lab project."
HOMEPAGE="http://mini.resl.uwaterloo.ca"
SRC_URI="ftp://mini.resl.uwaterloo.ca/datamill-worker-${PV}.tar.gz"
SLOT="0"
KEYWORDS="~x86 ~amd64 ~arm"
RDEPEND=">=dev-util/perf-3.2
         >=sys-process/time-1.7-r1"

IUSE="testing"

S="${WORKDIR}/datamill-worker-${PV}"

src_install() {
    dodir /datamill/
    cp -r "${S}/benchmark/"    "${D}/datamill/"
    cp -r "${S}/worker_common" "${D}/datamill/"

    if use testing; then
        cp "${S}/worker_common/local_settings.py" "${D}/datamill/worker_common/"
        cp "${S}/manage.py"                       "${D}/datamill/"
        cp "${S}/setup-virtualenv.sh"             "${D}/datamill/"
        cp "${S}/dependencies.pip"                "${D}/datamill/"
    fi
    
    dodir /etc/local.d/
    mv "${D}/datamill/benchmark/"*.start "${D}/etc/local.d/"

}
