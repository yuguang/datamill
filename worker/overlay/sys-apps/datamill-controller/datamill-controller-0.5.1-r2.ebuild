# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit eutils

DESCRIPTION="This is the ebuild for the worker side of the eval-lab project."
HOMEPAGE="http://mini.resl.uwaterloo.ca"
SRC_URI="ftp://mini.resl.uwaterloo.ca/datamill-worker-${PV}.tar.gz"
# Add license later
# LICENSE=""
SLOT="0"
KEYWORDS="x86 amd64 arm sparc"
RDEPEND=">=dev-python/pyyaml-3.09"

IUSE="testing"

S="${WORKDIR}/datamill-worker-${PV}"

src_install() {
    # TODO: Change these so that they use more official locations
    
    dodir /datamill/
    cp -r "${S}/controller/"* "${D}/datamill/"
    cp -r "${S}/lib"          "${D}/datamill/"
    cp -r "${S}/install"      "${D}/datamill/"

    # If we are in a testing environment, do not overwrite
    # controller_environment
    if use testing; then
        rm "${D}/datamill/"controller_environment
    fi

    local DETECTED_ARCH=$(uname -m)
    if [[ $DETECTED_ARCH = arm* ]]
    then
        cp "${S}/controller/uboot_set_bench_boot.sh"   "${D}/datamill/set_bench_boot.sh"
        cp "${S}/controller/uboot_set_control_boot.sh" "${D}/datamill/set_control_boot.sh"
    elif [[ $DETECTED_ARCH = ppc* ]]
    then
        cp "${S}/controller/ppc_set_bench_boot.sh"     "${D}/datamill/set_bench_boot.sh"
        cp "${S}/controller/ppc_set_control_boot.sh"   "${D}/datamill/set_control_boot.sh"
    elif [[ $DETECTED_ARCH = sparc* ]]
    then
        cp "${S}/controller/sparc_set_bench_boot.sh"   "${D}/datamill/set_bench_boot.sh"
        cp "${S}/controller/sparc_set_control_boot.sh" "${D}/datamill/set_control_boot.sh"
    else # DEFAULT TO GRUB
        cp "${S}/controller/grub_set_bench_boot.sh"    "${D}/datamill/set_bench_boot.sh"
        cp "${S}/controller/grub_set_control_boot.sh"  "${D}/datamill/set_control_boot.sh"
    fi

    dodir /etc/local.d/
    mv "${D}/datamill/"*.start "${D}/etc/local.d/"

    dodir /datamill/results
    
}
