# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit eutils

DESCRIPTION="This is the ebuild for the worker side of the eval-lab project."
HOMEPAGE="http://mini.resl.uwaterloo.ca"
SRC_URI="ftp://mini.resl.uwaterloo.ca/datamill-worker-${PV}.tar.gz"
SLOT="0"
RDEPEND=">=sys-apps/portage-2.1.11.31"

IUSE="testing"

KEYWORDS="sparc amd64 x86 arm"

S="${WORKDIR}/datamill-worker-${PV}"

src_install() {
    dodir /datamill/
    cp -r "${S}/controller/"   "${D}/datamill/"
    cp -r "${S}/worker_common" "${D}/datamill/"
    rm "${D}/datamill/worker_common/virtual_worker_settings.py"

    if use testing; then
        cp "${S}/worker_common/virtual_worker_settings.py" "${D}/datamill/worker_common/"
        cp "${S}/manage.py"                                "${D}/datamill/"
        cp "${S}/setup-virtualenv.sh"                      "${D}/datamill/"
        cp "${S}/dependencies.pip"                         "${D}/datamill/"
    fi

    dodir /etc/local.d/
    mv "${D}/datamill/controller/"*.start "${D}/etc/local.d/"
    
}

pkg_postrm() {
    find ${ROOT}/datamill/ -name "*.pyc" -exec rm {} \;
}
