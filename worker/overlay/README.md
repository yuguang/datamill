datamill
========

The datamill gentoo overlay.

Updating The Workers
====================

There are two possible scenarios, that I can think of at the moment,
where you will want to update the workers.


Updating to a New Version
-------------------------

If the workers are all currently running version the worker software
version 0.1 and you wish to update them to the current version that
you have dubbed 0.2 the following must occur.


Before running any scripts or making new ebuilds, you should do the following:

1. You must ensure that you have committed all the software that you
   wish to be in version 0.2 and that you have checked out the
   appropriate git ref on the master node (mini.resl.uwaterloo.ca).

2. You must check the current ebuilds (datamill-controller-0.1.ebuild
   and datamill-benchmark-0.1.ebuild in our example) properly install
   all the files as you expect. This is particularly important if you
   have created new files or moved old files.

Once the above conditions are satisfied, you may follow the process listed below

1. Create the new ebuilds (datamill-controller-0.2.ebuild and
   datamill-benchmark-0.2.ebuild in our example) and edit them as
   necessary (to account for new or moved files).

    cd worker/overlay/sys-apps/datamill-controller/
    cp datamill-controller-0.1.ebuild datamill-controller-0.2.ebuild
    
    cd ../datamill-benchmark/
    cp datamill-benchmark-0.1.ebuild datamill-benchmark-0.2.ebuild

2. Now that the ebuilds are in place, you must run the makepkg.sh
   script in the worker directory, and pass the new version number as
   it's argument.

    cd worker
    ./makepkg 0.2

Updating the Code of a Previous Version
---------------------------------------

If the workers are all currently running a particular version of the
worker software and you need to change a small file that doesn't
warrent a brand new version, you may update the ebuild manifests and
the source tarball of the worker software, without adding an extra
ebuild.

Before running any scripts or making new ebuilds, you should do the following:

1. You must ensure that you have committed all the software that you
   wish to be in version 0.2 and that you have checked out the
   appropriate git ref on the master node (mini.resl.uwaterloo.ca).

2. You must check the current ebuilds (datamill-controller-0.1.ebuild
   and datamill-benchmark-0.1.ebuild in our example) properly install
   all the files as you expect. This is particularly important if you
   have created new files or moved old files.

Once the above conditions are satisfied, you may run the following to
update the packages:

    cd worker
    ./makepkg 0.2
    