#!/bin/sh
killall python2
screen
echo 'GENTOO_MIRRORS="rsync://mini.resl.uwaterloo.ca/distfiles"' >> /etc/portage/make.conf
eix-sync
if ! [[ -e /etc/init.d/ntpd ]]
then
    emerge net-misc/ntp
    rc-update add ntp-client default
    ntpdate pool.ntp.org
fi
perl-cleaner --all
emerge portage

eselect python set --python3 python3.3
python-updater

FEATURES="-distcc" emerge -1 linux-headers
emerge -C sys-power/cpufrequtils
emerge -vuDN --with-bdeps=y --keep-going world && emerge --depclean && revdep-rebuild -i

# after exiting screen
screen -dr
yes | etc-update --automode -7
perl -pi -e 's/clock_systohc="(.*)"/clock_systohc="YES"/;' /etc/conf.d/hwclock
perl -pi -e 's/clock_hctosys="(.*)"/clock_hctosys="YES"/;' /etc/conf.d/hwclock

gcc-config -l
gcc-config 1

rm -rf /var/tmp/portage/*
rm -rf /usr/portage/distfiles/*

reboot

# if world update was unsuccessful and there is a lack of disk space
rm /datamill/backups/*