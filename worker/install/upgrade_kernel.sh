#!/bin/bash
killall python2
screen
eselect kernel list
emerge genkernel gentoo-sources
eselect kernel set 2

pushd /usr/src/linux
wget "ftp://datamill.uwaterloo.ca/internal/kernels/kernel-config-$(arch)-3.18.11-gentoo" -O .config
cat >> .config <<EOF
# Filesystems
CONFIG_EXT2_FS=y
CONFIG_EXT3_FS=y
CONFIG_EXT4_FS=y
CONFIG_REISERFS_FS=y
CONFIG_XFS_FS=y
CONFIG_JFS_FS=y
CONFIG_FS_MBCACHE=y
CONFIG_GFS2_FS=n
CONFIG_OCFS2_FS=n
CONFIG_BTRFS_FS=y
CONFIG_FS_POSIX_ACL=y
CONFIG_CRYPTO_CRC32C=y
CONFIG_CRYPTO_CRC32C_INTEL=y
# CPU Frequency scaling
CONFIG_CPU_FREQ=y
CONFIG_CPU_FREQ_TABLE=m
CONFIG_CPU_FREQ_GOV_COMMON=y
CONFIG_CPU_FREQ_STAT=m
CONFIG_CPU_FREQ_STAT_DETAILS=n
CONFIG_CPU_FREQ_DEFAULT_GOV_PERFORMANCE=y
CONFIG_CPU_FREQ_DEFAULT_GOV_USERSPACE=n
CONFIG_CPU_FREQ_DEFAULT_GOV_ONDEMAND=n
CONFIG_CPU_FREQ_DEFAULT_GOV_CONSERVATIVE=n
CONFIG_CPU_FREQ_GOV_PERFORMANCE=y
CONFIG_CPU_FREQ_GOV_POWERSAVE=m
CONFIG_CPU_FREQ_GOV_USERSPACE=m
CONFIG_CPU_FREQ_GOV_ONDEMAND=m
CONFIG_CPU_FREQ_GOV_CONSERVATIVE=m
CONFIG_X86_ACPI_CPUFREQ=m
CONFIG_X86_ACPI_CPUFREQ_CPB=y
CONFIG_X86_POWERNOW_K8=m
CONFIG_X86_SPEEDSTEP_CENTRINO=m
CONFIG_X86_P4_CLOCKMOD=m
CONFIG_SCHED_AUTOGROUP=y
EOF
make -j$(nproc) && make modules_install
mount -o remount,rw /boot
cp arch/x86/boot/bzImage /boot/kernel
popd
genkernel --install initramfs
mount -o remount,rw /boot
cp /boot/initramfs-genkernel-*-3.18.11-gentoo /boot/initramfs
cat > /boot/grub/grub.conf <<EOF
default 0
timeout 5
#splashimage=(hd0,0)/boot/grub/splash.xpm.gz

title Gentoo Linux 3.18.11
root (hd0,0)
kernel /boot/kernel root=/dev/ram0 real_root=/dev/sda3
initrd /boot/initramfs

title Gentoo Linux 3.18.11
root (hd0,0)
kernel /boot/kernel root=/dev/ram0 real_root=/dev/sda4
initrd /boot/initramfs

# vim:ft=conf:
EOF


# after the upgrade is finished, check /boot for kernel and initramfs files
screen -dr
ls -l /boot

