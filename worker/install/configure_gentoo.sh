#!/bin/bash

read -r -d '' HELP <<EOF
Usage: gentoo-utils.sh [options]
Fix problematic configurations, and add missing configurations.

    -h            Print this help documentation.
    -H hostname   Specify the hostname.
    -i netif      Specify the net interface.
    -r            Install on embedded device.
    -e            Install on embedded device.

    Test:
    -l            Just list resulting specification.
EOF

CONFD_HOSTNAME="/etc/conf.d/hostname"
ETC_INITD="/etc/init.d"
ETC_PROFILE="/etc/profile"
ETC_RUNLEVELS="/etc/runlevels/default"
HOST=controller
HOSTS="/etc/hosts"
TIMEZONE='Canada/Eastern'
HWCLOCKFILE=/etc/conf.d/hwclock
LOCALE=/etc/locale.gen

HOSTNAME=
NETIF=
USE_RASPBERRY=
EMBEDDED=
LIST=
DISK=

WORLD=/var/lib/portage/world

while getopts ":hH:i:d:rel" opt; do
    case $opt in
        h ) echo "$HELP";
            exit 0;;
        H ) HOSTNAME=$OPTARG ;;
        i ) NETIF=$OPTARG ;;
        d ) DISK=$OPTARG ;;
        r ) USE_RASPBERRY="true" ;;
        e ) EMBEDDED="true" ;;
        l ) LIST="true" ;;
        \? ) echo "-$opt is not a valid option.";
             echo "$HELP"
             exit 1;;
    esac
done
shift $(($OPTIND - 1))

# ======================== Argument Validation =========================

if [[ -z $HOSTNAME || -z $NETIF ]]; then
    echo "Must specify a hostname and net interface!"
    exit 1
fi

if [[ $USE_RASPBERRY == "true" ]]; then
    RASPBERRY='-r'
fi

if [[ -n $LIST ]]; then
    echo "Just listing specifications, taking no action"
    echo
    echo "CONFD_HOSTNAME: $CONFD_HOSTNAME"
    echo "ETC_INITD: $ETC_INITD"
    echo "ETC_PROFILE: $ETC_PROFILE"
    echo "ETC_RUNLEVELS: $ETC_RUNLEVELS"
    echo "HOST: $HOST"
    echo "HOSTS: $HOSTS"
    echo "TIMEZONE: $TIMEZONE"
    echo "HWCLOCKFILE: $HWCLOCKFILE"
    echo "LOCALE: $LOCALE"
    echo "HOSTNAME: $HOSTNAME"
    echo "NETIF: $NETIF"
    echo "USE_RASPBERRY: $USE_RASPBERRY"
    echo "EMBEDDED: $EMBEDDED"
    echo "LIST: $LIST"
    exit 0
fi

# ======================== Program begins =========================

# Find the dir we were called from
SOURCE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${SOURCE_PATH}"/setup
source "${SOURCE_PATH}"/lib/gentoo-utils.sh

function fix_hwclock() {
    local hwclockfile=$1

    perl -pi -e 's/clock="(.*)"/clock="UTC"/;' $hwclockfile
    perl -pi -e 's/clock_systohc="(.*)"/clock_systohc="YES"/;' $hwclockfile
    perl -pi -e 's/clock_hctosys="(.*)"/clock_hctosys="YES"/;' $hwclockfile
}

function fix_locale() {
    local locale=$1
    cat >> $locale <<EOF
en_US ISO-8859-1
en_US.UTF-8 UTF-8
EOF

    locale-gen
    env-update
    source $ETC_PROFILE
}


function fake_mount_entries() {
    grep -v rootfs /proc/mounts > /etc/mtab
}

function fix_raspberry() {

    local initd=$1
    local runlevels=$2

    runlevel_symlink $initd $runlevels "ntp-client"

    add_world $WORLD net-misc/ntp
}

function fix_timezone() {
    local timezone=$1
    cp "/usr/share/zoneinfo/${timezone}" /etc/localtime
    echo $timezone > /etc/timezone
}

function fix_host_config() {
    local hostname=$1
    local hosts=$2
    local confd_hostname=$3

    # TODO: do this in a less terrible way
    if ! grep $hostname $hosts > /dev/null; then
        perl -pi -e "s/([\d+.]+).*(localhost)/\$1 $hostname \$2/;" $hosts
    fi        

    if ! grep $hostname $confd_hostname > /dev/null; then
        perl -pi -e "s/hostname=\".*\"/hostname=\"$hostname\"/;" $confd_hostname
    fi
}

function fix_rc_scripts() {
    local interface=$1
    local etc_initd=$2
    local etc_runlevels=$3

    pushd $etc_initd > /dev/null

    if [[ -z $interface ]]; then
        echo "Error, no interface specified." 1>&2
        return 1
    fi

    if [[ ! -L  "net.$interface" ]]; then
        ln -s "net.lo" "net.$interface" 
    fi

    local runlevel_link="${etc_runlevels}/net.$interface"

    if [[ ! -L $runlevel_link ]]; then
        ln -s "/etc/init.d/net.lo" $runlevel_link 
    fi

    popd > /dev/null
}

function fix_profile() {

    local host=$1
    local etc_profile=$2
    local string="PS1=\"(${host}) \$PS1\""
    if ! grep -E "PS1.*controller" $etc_profile > /dev/null; then
        echo $string >> $etc_profile
    fi
}

function prepare_for_emerge_update() {

    local profile_path=
    local profile_symlink=

    if ! profile_path=$(find_profile_link '/etc'); then
        # Guess the first profile
        eselect profile set 1;
        if ! profile_path=$(find_profile_link '/etc'); then
            print_header "WARNING: Portage profile likely needs repair!!!!! " 1>&2
        fi
    fi 

    profile_symlink=$(readlink $profile_path)
    
    # unlink so that it cannot accidentally remove a dir
    unlink $profile_path;

    if echo $profile_symlink | grep -E "10\.0$" > /dev/null; then
        profile_symlink="${profile_symlink}/server"
    fi
    ln -s $profile_symlink $profile_path

    add_world $WORLD \
      app-portage/gentoolkit \
      app-portage/eix \
      app-admin/logrotate \
      app-admin/syslog-ng \
      sys-process/vixie-cron \
      app-editors/nano

      for service in syslog-ng vixie-cron sshd; do
        runlevel_symlink '/etc/init.d' '/etc/runlevels/default' $service
    done

}

function configure_portage() {
    mkdir -p /usr/portage
    emerge-webrsync
    echo 'SYNC="rsync://mini.resl.uwaterloo.ca/gentoo-portage"' >> /etc/portage/make.conf
    echo 'GENTOO_MIRRORS="rsync://mini.resl.uwaterloo.ca/distfiles"' >> /etc/portage/make.conf
    emerge --sync
    rm /usr/portage/metadata/timestamp.chk
    emerge --sync
}

function install_kernel() {
    emerge =sys-kernel/gentoo-sources-3.18.11
    emerge genkernel
    if [[ $ARCH == sparc64 ]]; then
        emerge sys-devel/kgcc64
    fi
    pushd /usr/src/linux
    wget "ftp://datamill.uwaterloo.ca/internal/kernels/kernel-config-$(arch)-3.18.11-gentoo" -O .config
cat >> .config <<EOF
# Filesystems
CONFIG_EXT2_FS=y
CONFIG_EXT3_FS=y
CONFIG_EXT4_FS=y
CONFIG_REISERFS_FS=y
CONFIG_XFS_FS=y
CONFIG_JFS_FS=y
CONFIG_FS_MBCACHE=y
CONFIG_GFS2_FS=n
CONFIG_OCFS2_FS=n
CONFIG_BTRFS_FS=y
CONFIG_FS_POSIX_ACL=y
CONFIG_CRYPTO_CRC32C=y
CONFIG_CRYPTO_CRC32C_INTEL=y
# CPU Frequency scaling
CONFIG_CPU_FREQ=y
CONFIG_CPU_FREQ_TABLE=m
CONFIG_CPU_FREQ_GOV_COMMON=y
CONFIG_CPU_FREQ_STAT=m
CONFIG_CPU_FREQ_STAT_DETAILS=n
CONFIG_CPU_FREQ_DEFAULT_GOV_PERFORMANCE=y
CONFIG_CPU_FREQ_DEFAULT_GOV_USERSPACE=n
CONFIG_CPU_FREQ_DEFAULT_GOV_ONDEMAND=n
CONFIG_CPU_FREQ_DEFAULT_GOV_CONSERVATIVE=n
CONFIG_CPU_FREQ_GOV_PERFORMANCE=y
CONFIG_CPU_FREQ_GOV_POWERSAVE=m
CONFIG_CPU_FREQ_GOV_USERSPACE=m
CONFIG_CPU_FREQ_GOV_ONDEMAND=m
CONFIG_CPU_FREQ_GOV_CONSERVATIVE=m
CONFIG_X86_ACPI_CPUFREQ=m
CONFIG_X86_ACPI_CPUFREQ_CPB=y
CONFIG_X86_POWERNOW_K8=m
CONFIG_X86_SPEEDSTEP_CENTRINO=m
CONFIG_X86_P4_CLOCKMOD=m
PROC_FS=y
IKCONFIG=y
IKCONFIG_PROC=y
EOF
    make -j$(nproc) && make modules_install
    if [[ $ARCH != sparc64 ]]; then
        cp arch/x86/boot/bzImage /boot/kernel
    else
        cp arch/sparc/boot/image /boot/kernel
    fi
    popd
    genkernel --install initramfs
    mount -o remount,rw /boot
    cp /boot/initramfs* /boot/initramfs

}

function install_grub() {
    local disk=$1
    perl-cleaner --all #fix any perl environment problems
    mkdir -p /etc/portage/package.use/
    echo "sys-libs/ncurses abi_x86_32" >> /etc/portage/package.use/grub
    emerge --noreplace sys-boot/grub:0
    mount -o remount,rw {$disk}1 /boot
cat > /boot/grub/grub.conf <<EOF
default 0
timeout 5
#splashimage=(hd0,0)/boot/grub/splash.xpm.gz

title Gentoo Linux 3.18.11
root (hd0,0)
kernel /boot/kernel root=/dev/ram0 real_root=/dev/sda3
initrd /boot/initramfs

title Gentoo Linux 3.18.11
root (hd0,0)
kernel /boot/kernel root=/dev/ram0 real_root=/dev/sda4
initrd /boot/initramfs

# vim:ft=conf:
EOF
    grep -v rootfs /proc/mounts > /etc/mtab
    grub-install --root-directory=/boot --no-floppy $disk
}

function write_fstab() {
cat > /etc/fstab <<EOF
/dev/sda1               /boot           ext2            noauto,noatime  1 2
/dev/sda2               none            swap            sw              0 0
/dev/sda3               /               btrfs            noatime,compress=zlib         0 1
/dev/sda4               /mnt/benchmark  btrfs            noauto,noatime  0 0
EOF
}

function make_benchmark_mountpoint() {
    target='/mnt/benchmark'
    mkdir -p $target
}

function set_makeconfig() {
    sed -i 's/-O2/-march=native -O2 -fomit-frame-pointer -ftracer/' /etc/portage/make.conf
    echo "MAKEOPTS='-j$(nproc)'" >> /etc/portage/make.conf
}

function configure_ssh() {
    sed -i 's/UsePAM\ yes/UsePAM no/' /etc/ssh/sshd_config
    mkdir /root/.ssh
    wget http://yuguangzhang.com/linux/authorized_keys -O /root/.ssh/authorized_keys
}

function install_ntp() {
    emerge net-misc/ntp
    rc-update add ntp-client default
    ntpdate pool.ntp.org
}

function main() {

    print_header "Setting up Portage"
    configure_portage

    if [[ -z $EMBEDDED ]]; then
        print_header "Emerging kernel sources"
        install_kernel
        write_fstab
    fi
    make_benchmark_mountpoint

    print_header "Configuring system"
    set_makeconfig
    configure_ssh
    fake_mount_entries
    if [[ $ARCH != sparc64 && -z $EMBEDDED ]]; then
        install_grub $DISK
    fi
    fix_locale $LOCALE
    fix_hwclock $HWCLOCKFILE
    fix_timezone $TIMEZONE
    fix_host_config $HOSTNAME $HOSTS $CONFD_HOSTNAME
    fix_rc_scripts $NETIF $ETC_INITD $ETC_RUNLEVELS
    fix_profile $HOST $ETC_PROFILE
    install_ntp
    if [[ -n $USE_RASPBERRY || -z $EMBEDDED ]]; then
        fix_raspberry $ETC_INITD $ETC_RUNLEVELS
    fi
    emerge app-editors/vim app-misc/screen app-text/wgetpaste net-misc/dhcpcd
    emerge layman
    prepare_for_emerge_update
}

main;

cat <<EOF
Remaining steps:
1. Check /boot/grub/grub.conf
2. Check /etc/fstab
3. Set the password with passwd
4. Reboot
5. Run the following:

emerge -uDN world
emerge --depclean
revdep-rebuild -i

EOF
