#!/bin/sh
killall python2
emerge --sync
etc-update

if ! [[ -e /etc/init.d/ntpd ]]
then
    emerge net-misc/ntp
    rc-update add ntp-client default
    ntpdate pool.ntp.org
fi
perl-cleaner --all
emerge -C app-portage/gentoolkit
emerge portage

emerge -1 -u gcc
gcc-config -l
gcc-config 1
env-update && source /etc/profile
emerge -uDN gcc

yes | etc-update --automode -7

cat > /etc/portage/repos.conf/gentoo.conf <<EOF
[DEFAULT]
main-repo = gentoo
[gentoo]
sync-type = rsync
sync-uri = rsync://mini.resl.uwaterloo.ca/gentoo-portage
EOF
perl -pi -e 's/SYNC="rsync:\/\/mini.resl.uwaterloo.ca\/gentoo-portage(.*)"//;' /etc/portage/make.conf

FEATURES="-distcc" emerge -1 linux-headers
emerge -vuDN --with-bdeps=y --keep-going world && emerge --depclean && revdep-rebuild -i

yes | etc-update --automode -7

# upgrade kernel first
emerge -C sys-kernel/gentoo-sources
emerge genkernel sys-kernel/gentoo-sources
eselect kernel list
eselect kernel set 1

pushd /usr/src/linux
wget "ftp://datamill.uwaterloo.ca/internal/kernels/kernel-config-$(arch)-4.4.6-gentoo" -O .config
make -j$(nproc) && make modules_install
mount -o rw /boot
cp arch/x86/boot/bzImage /boot/kernel
popd
genkernel --install initramfs
mount -o remount,rw /boot
cp /boot/initramfs-genkernel-*-4.4.6-gentoo /boot/initramfs
cat > /boot/grub/grub.conf <<EOF
default 0
timeout 5
#splashimage=(hd0,0)/boot/grub/splash.xpm.gz

title Gentoo Linux 4.4.6
root (hd0,0)
kernel /boot/kernel root=/dev/ram0 real_root=/dev/sda3
initrd /boot/initramfs

title Gentoo Linux 4.4.6
root (hd0,0)
kernel /boot/kernel root=/dev/ram0 real_root=/dev/sda4
initrd /boot/initramfs

# vim:ft=conf:
EOF

rm -rf /var/tmp/portage/*
rm -rf /usr/portage/distfiles/*

echo "app-emulation/docker device-mapper seccomp btrfs" >> /etc/portage/package.use/datamill.use
emerge --autounmask-write -v =app-emulation/containerd-0.2.0 =app-emulation/runc-0.1.0 =app-emulation/docker-1.11.0
yes | etc-update --automode -3
emerge -v =app-emulation/containerd-0.2.0 =app-emulation/runc-0.1.0 =app-emulation/docker-1.11.0
rc-update add docker default

ls -l /boot
