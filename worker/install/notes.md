# Installing Gentoo
./install_gentoo.sh -a $(arch) -d /dev/sda
chroot /mnt/gentoo /bin/bash
source /etc/profile
emerge genkernel && genkernel all
ls /boot/kernel* /boot/initramfs*
emerge grub
nano -w /boot/grub/grub.conf
grep -v rootfs /proc/mounts > /etc/mtab
grub-install --no-floppy /dev/sda
nano /etc/fstab

# General installation steps in the Gentoo Handbook
5.c. Configuring the Compile Options
6.a. Chrooting
7. Configuring the Kernel
8.a. Filesystem Information
10. Configuring the Bootloader
nano -w /etc/hosts
    127.0.0.1 benchmarkxx localhost
    ::1 benchmarkxx localhost
nano -w /etc/conf.d/hostname
passwd
mkdir .ssh
cat id_rsa.pub >> .ssh/authorized_keys

# SPARC
format disk with fdisk before running ./install-gentoo.sh
leave "whole disk" partition
format disk with "-i 1024" option

vim /etc/silo.conf
partition = 1
root = /dev/sda4
timeout = 1
image = /kernel
        label = linux
        initrd=/initramfs-genkernel-sparc64-3.8.13-gentoo

vim /etc/silo.bench
partition = 1
root = /dev/sda5
timeout = 1
image = /kernel
        label = linux
        initrd=/initramfs-genkernel-sparc64-3.8.13-gentoo

cp /etc/silo.conf /etc/silo.cont

vim /etc/fstab
/dev/sda4               /               ext4            noatime         0 1
/dev/sda5       /mnt/benchmark  ext4    noauto,noatime  0       3
/dev/sda1               /boot           ext2            noatime         0 1
/dev/sda2               none            swap            sw              0 0
# You must add the rules for openprom
openprom    /proc/openprom  openpromfs  defaults             0 0

/dev/cdrom  /mnt/cdrom      auto        noauto,user          0 0

# Set up SSH tunnel on Gentoo CD
net-setup eth0
/etc/init.d/sshd start
wget id_rsa.pub
mkdir ~/.ssh
mv id_rsa.pub ~/.ssh/authorized_keys

# SSH  tunnel to machine with ip 10.10.10.9
ssh -f -N -L 8888:10.10.10.9:22 root@datamill.uwaterloo.ca
ssh -p 8888 root@localhost
# terminate SSH connection
Enter ~ .

vim /etc/portage/make.conf
  SYNC="rsync://mini.resl.uwaterloo.ca/gentoo-portage"

emerge --sync && rm /usr/portage/metadata/timestamp.chk && emerge portage && emerge -uDN world && emerge --depclean && revdep-rebuild -i

mkdir -p /etc/layman/overlays
cp overlay.xml /etc/layman/overlays
emerge layman
layman -o file:///etc/layman/overlays/overlay.xml -f
layman -a datamill
emerge -a datamill-controller

vim /etc/portage/make.conf
    source /var/lib/layman/make.conf

./create_benchmark.sh /dev/sda4

vim /mnt/benchmark/etc/portage/package.accept_keywords
    #required by sys-apps/datamill-controller-0.4::datamill, required by datamill-controller (argument)
    =dev-python/pyyaml-3.10-r1 **
    #required by datamill-controller (argument)
    =sys-apps/datamill-controller-0.4 *
emerge --update --buildpkg --usepkg --binpkg-respect-use --root=/mnt/benchmark --config-root=/mnt/benchmark -q sys-apps/datamill-benchmark
/etc/local.d/01_start_datamill_worker.start

ARM install notes
-----------------
set kernel boot argument /etc/inittab
eg "$bootargs console=ttymxc1,115200 vmalloc=400M consoleblank=0 rootwait"

Reset board variables
U-Boot > run clearenv
U-Boot > reset

Extract IMX6 tarball and edit hostname
touch /tmp/install/lib/rc/cache/shutdowntime
sfdisk -f /dev/sdd < mmc_partitions
mkfs.btrfs /dev/sdd2 && mkfs.ext2 /dev/sdd1
mount -o compress=zlib /dev/sdd2 /mnt/p1 && mkdir /mnt/p1/boot && mount /dev/sdd1 /mnt/p1/boot && cp -afr /tmp/install/* /mnt/p1 && vim /mnt/p1/etc/conf.d/hostname
umount /mnt/p1/boot && umount /mnt/p1


Create swap and benchmark partitions on SD cards
# partition table of /dev/sdb
unit: sectors

/dev/sdb1 : start=     2048, size= 12582912, Id=83
/dev/sdb2 : start= 12584960, size=  2537472, Id=83
/dev/sdb3 : start=        0, size=        0, Id= 0
/dev/sdb4 : start=        0, size=        0, Id= 0

Crossdev notes
--------------
When building crossdev compilers, disable CFLAGS or use

    CFLAGS="-O2 -pipe -frename-registers -fomit-frame-pointer -ftracer"
    
also add the -v flag to see Portage output

    crossdev -v -S -t armv7a-hardfloat-linux-gnueabi


Portage mirror configuration
----------------------------

For Portage version 2.2.16 and higher, `/etc/portage/repos.conf/gentoo.conf` is required with

    [DEFAULT]
    main-repo = gentoo
    [gentoo]
    sync-type = rsync
    sync-uri = rsync://mini.resl.uwaterloo.ca/gentoo-portage-latest
    
In addition, the file `/etc/portage/repos.conf/layman.conf` needs to contain 

    [datamill]
    priority = 50
    location = /var/lib/layman/datamill
    layman-type = rsync
    auto-sync = N

For details, see [the Gentoo wiki](https://wiki.gentoo.org/wiki/SYNC) and [announcement](https://www.gentoo.org/support/news-items/2015-02-04-portage-sync-changes.html)

Virtual machine notes
---------------------

Files specific to the virtual machine after running installation scripts are:

/etc/inittab
/root/*
/etc/issue
/etc/vsftpd/vsftpd.conf
/etc/portage/package.use/datamill

In addition, install dev-python/nose to run tests

Update distfiles
----------------

    rsync --recursive --links --safe-links --perms --times --omit-dir-times --compress --stats --human-readable --progress --timeout=180 rsync://gentoo.gossamerhost.com/gentoo-distfiles/distfiles /mirror


CRV15 Scripts
-------------

    #!/bin/bash
    emerge --autounmask-write oracle-jre-bin
    etc-update --automode -5
    wget ftp://datamill.uwaterloo.ca/internal/distfiles/jre-8u40-linux-x64.tar.gz -O /usr/portage/distfiles/jre-8u40-linux-x64.tar.gz
    emerge oracle-jre-bin
    emerge --autounmask-write =dev-lang/ocaml-4.02.1
    etc-update --automode -5
    emerge mercurial =dev-lang/ocaml-4.02.1
    java-config --list-available-vms
    java-config --set-system-vm oracle-jre-bin-1.8
    
MIPS Keywords
-------------

Mips by default will install the testing datamill-controller package. To prevent this, add the following to `/etc/portage/make.conf`
 
    ACCEPT_KEYWORDS="mips"
    
Alternatively, make the file `/etc/portage/package.accept_keywords/datamill` with the following content

    sys-apps/datamill-controller mips
    sys-apps/datamill-benchmark mip
       