#!/bin/bash

# This function adds a package atom to the world file. The first
# argument should be the location of the world file. Each subsequent
# argument should be one fully unambiguous package atom. E.G.:
#
# add_world /var/lib/portage/world sys-app/datamill-controller
# 
function add_world(){
    local world=$1
    shift
    local packages="$@"
    packages=${packages// /'\n'}

    local world_packages="$(cat ${world})"
    local new_packages="${packages}\n${world_packages}"
    
    new_packages=$(echo -e "$new_packages" | sort | uniq)
    echo "$new_packages" > "$world"

}

# This function adds a SINGLE use flag definition to the package.use
# file specified. Example:
#
# add_use /etc/portage/package.use/misc.use dev-db/mysql embedded -minimal
# 
function add_use() {
    local use_file=$1
    local package_atom=$2
    shift 2
    local flags="$@"

    local current_use="$(cat "${use_file}")"

    local new_line=
    if new_line=$(echo -e "$current_use" | grep "$package_atom"); then
        # remove the package atom, keep the flags
        new_line=$(echo "$new_line" | cut -d' ' -f 2-)

        # merge flags
        new_line=${new_line// /'\n'}
        flags=${flags// /'\n'}
        new_line="${new_line}\n${flags}"
        new_line="$(echo -e "$new_line" | sort | uniq)"
        new_line="$package_atom $new_line"
        # Not quoted to avoid literal newlines
    else
        new_line="$package_atom $flags"
    fi

    # Straight up apply it to the end. It will override old entries.
    echo "# The following entry was added automatically by gentoo-utils" >> "$use_file"
    echo $new_line >> "$use_file"

}

# This function adds a runlevel symlink to a package specified in it's
# arguments.
#
# The symlink is added REGARGLESS of whether or not the package is
# installed. In-fact, this function should ONLY be used if you want to
# ensure that the symlink is created no matter what. Otherwise the
# rc-update utility should be used.
#
# The three arguments are the location of the init.d directory, the
# location of the desired runlevel sub-directory and the target file
# to be symlinked. E.G.:
#
# runlevel_symlink /etc/init.d /etc/runlevels/default sshd
# 
function runlevel_symlink() {

    local initd=$1
    local runlevels=$2
    local target=$3
    local target_link="${runlevels}/${target}" 

    pushd $initd > /dev/null

    if [[ ! -L $target_link ]]; then
        ln -s "${initd}/${target}" $target_link
    fi

    popd > /dev/null

}

# This function prints the location of the profile link. This is the
# function to use to find the link since there is a possibility that
# the Gentoo install is using the old location.
#
# The function also sets $? appropriately to indicate whether or not
# it was successful.
# 
# Takes only the etc directory as it's argument.
#
# find_profile_link /etc
# 
function find_profile_link() {

    local etcd=$1
    local profile_path=
    pushd $etcd > /dev/null 
    
    if [[ -L 'make.profile' ]]; then 
        profile_path='make.profile'
    elif [[ -L 'portage/make.profile' ]]; then
        profile_path='portage/make.profile'
    else
        echo 'No make profile selected!' 1>&2
        return 1
    fi

    popd > /dev/null

    echo "${etcd}/${profile_path}"
    return 0

}

# This function prints the location of the make.conf file. This is the
# function to use to find the make.conf file since there is a
# possibility that the Gentoo install is using the old location.
#
# The function also sets $? appropriately to indicate whether or not
# it was successful.
# 
# Takes only the etc directory as it's argument.
#
# find_make_conf /etc
# 
function find_make_conf() {

    local etcd=$1
    local make_conf=

    pushd $etcd > /dev/null

    if [[ -f make.conf ]]; then
        make_conf=make.conf
    elif [[ -f portage/make.conf ]]; then
        make_conf=portage/make.conf
    else
        say_error "Couldn't find make.conf"
        return 1
    fi

    popd > /dev/null

    echo "${etcd}/${make_conf}"
    return 0
}

# This function chroots into the location specified and then runs
# whatever command follows.
#
# This function should be used in preference to plain chroot since it
# handles the mounting of directories and cleaning up of the
# environment in order to ensure the command runs in the context of
# the other gentoo install.
#
# chroot_cmd /mnt/gentoo ls -al
#
function chroot_cmd() {

    local D=$1
    shift
    
    echo "root: ${D}, command: $@"

    if [[ $PRETEND != "true" ]]; then

        echo "Copying resolv.conf"
        cp -v -L /etc/resolv.conf "${D}/etc/resolv.conf"
        echo "Mounting proc and dev"
        mount -t proc none "${D}/proc"
        mount --rbind /dev "${D}/dev"
        mount --rbind /sys "${D}/sys"

        COMMAND="env-update; source /etc/profile; $@"
        echo $COMMAND
        chroot "${D}" /bin/bash -c "$COMMAND"

    fi

}
