#!/bin/bash

SOURCE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${SOURCE_PATH}"/lib/datamill-utils.sh

install_datamill sys-apps/datamill-controller

SOURCE_PATH=/update-staging

# Make sure the correct python is selected
emerge python:2.7
eselect python set python2.7

benchmark_prep /mnt/benchmark

# Add keywords
echo "dev-util/perf" >> /mnt/benchmark/etc/portage/package.keywords

if [ "$(uname -m | grep -i arm)" != "" ]; then
    echo "dev-util/perf **" >> /mnt/benchmark/etc/portage/package.keywords
    echo "dev-python/pyyaml" >> /mnt/benchmark/etc/portage/package.keywords
fi

benchmark_chroot /mnt/benchmark emerge python:2.7
benchmark_chroot /mnt/benchmark eselect python set python2.7
benchmark_chroot /mnt/benchmark install_datamill sys-apps/datamill-benchmark


mkdir -p /root/.ssh
mkdir -p /mnt/benchmark/root/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnM8w15+e1ghGnSaK+5nUK13ZM1qzoaI7zxjON6Xza49eNnwzdebLkQ/SyGcRLota+jx/75EUjAFAlpt/npknYBfiDTz1m3OKhotWz9bIL5TXjd4ok+kBQ826abgiWt5he3WpsgMZ40DRr3DNiSsQuE+HmlpDfnK8HGBazVov5BXbWundFi4AInValiMShC44id8MmH927Y9v189sq2vl/cZaC8tPbkf8t4ctzp+paK99nYtPZqI83UkvcON2EDgd0zQzQJ/LAGXZylTpV6PAO3k704WCujYKcq4bWFmETkjiogmnc0+m/lI4E2ygvVdDPqpaVLMzkv6o1mWB600+n root@mini-master" >> /root/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnM8w15+e1ghGnSaK+5nUK13ZM1qzoaI7zxjON6Xza49eNnwzdebLkQ/SyGcRLota+jx/75EUjAFAlpt/npknYBfiDTz1m3OKhotWz9bIL5TXjd4ok+kBQ826abgiWt5he3WpsgMZ40DRr3DNiSsQuE+HmlpDfnK8HGBazVov5BXbWundFi4AInValiMShC44id8MmH927Y9v189sq2vl/cZaC8tPbkf8t4ctzp+paK99nYtPZqI83UkvcON2EDgd0zQzQJ/LAGXZylTpV6PAO3k704WCujYKcq4bWFmETkjiogmnc0+m/lI4E2ygvVdDPqpaVLMzkv6o1mWB600+n root@mini-master" >> /mnt/benchmark/root/.ssh/authorized_keys

#reboot
