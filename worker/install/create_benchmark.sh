#!/usr/bin/env sh

copied_directories="bin boot etc home lib lib64 lib32 mnt media opt root run sbin tmp var usr"
created_directories="proc sys dev"

target='/mnt/benchmark'

# first argument, partition
partition=$1

mkdir -p $target
mount $partition $target

# check for the directories which exist from the list of possibles,
# complain if one is not found
pushd '/'
found_directories=""
for dir in $copied_directories
do
    if [ -d $dir ]; then
        echo "Found $dir"
        found_directories="$dir $found_directories"
    else
        echo "Could not find $dir"
    fi
done
copied_directories=$found_directories
popd

pushd '/'
cp -a $copied_directories $target
popd


# Created manually to avoid copying a dev folder on a running
# partition, which is annoying
pushd $target
mkdir $created_directories
pushd 'dev'
mknod        console c 0 0
mknod -m 666 null    c 1 3
mknod -m 666 zero    c 1 5
chown root:root null zero 
popd
popd

echo "$partition		/mnt/benchmark	ext4		noauto,noatime		0 3" >> /etc/fstab

# Edit fstab appropriately
vim "/etc/fstab"
vim "$target/etc/fstab"

# Edit grub.conf
mount "/boot"
if [ -e "/boot/grub/grub.conf" ]; then
    vim "/boot/grub/grub.conf"
fi
