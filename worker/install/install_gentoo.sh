#!/bin/bash

read -r -d '' HELP <<EOF
Usage: install_gentoo.sh [options]
Install Gentoo for DataMill (or for other purposes).
Automates most of the busywork of installing Gentoo.

    Install options:
    -h            Print this help documentation.
    -a arch       Specify the architecture.
    -d disk       Specify the target disk.
    -t type       Specify partition type.
    -H hostname   Specify the hostname.
    -i netif      Specify the net interface.
    -r            Install on embedded device.

    Advanced/test:
    -f directory  Specify fallback ftp directory.
    -p            Pretend to install.
    -l            Just list resulting specification.
EOF

# Defaults
ARCH=i486
DISK=
TYPE=btrfs
HOSTNAME=gentoo-machine
NETIF=eth0
RASPBERRY=
export PRETEND=

FALLBACK=
LIST=

while getopts ":ha:d:t:H:i:rpf:l" opt; do
    case $opt in
        h ) echo "$HELP";
            exit 0;;
        a ) ARCH=$OPTARG ;;
        d ) DISK=$OPTARG ;;
        t ) TYPE=$OPTARG ;;
        H ) HOSTNAME=$OPTARG ;;
        i ) NETIF=$OPTARG ;;
        r ) RASPBERRY="true" ;;
        p ) PRETEND="true" ;;
        f ) FALLBACK=$OPTARG ;;
        l ) LIST="true" ;;
        \? ) echo "-$opt is not a valid option.";
             echo "$HELP"
             exit 1;;
    esac
done
shift $(($OPTIND - 1))

# ======================== Argument Validation =========================

if [[ -z $DISK ]]; then
    echo "You must specify a target disk!"
    exit 1
fi

case $ARCH in
    i686|i486) ARCH=x86 ;;
    x86_64|amd64)
        ARCH=amd64
        ;;
    *)  if [[ -z $FALLBACK ]]; then
        echo "Unknown architecture, use the -f option to specify a release directory in the gentoo mirror"
        exit 1
        else
        ARCH=$FALLBACK
        fi
esac

if [[ -n $RASPBERRY ]]; then
    RASPBERRY='-r'
fi

D=/mnt/gentoo

if [[ -n $LIST ]]; then
    echo "Just listing specification, performing no install."
    echo
    echo "Final Specification:"
    echo "ARCH: $ARCH"
    echo "DISK: $DISK"
    echo "TYPE: $TYPE"
    echo "HOSTNAME: $HOSTNAME"
    echo "NETIF: $NETIF"
    echo "RASPBERRY: $RASPBERRY"
    echo "PRETEND: $PRETEND"
    echo "FALLBACK: $FALLBACK"
    echo "D: $D"
    exit 0
fi

echo "Installing to /mnt/gentoo"

# ======================== Program begins =========================

# Find the dir we were called from

SOURCE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${SOURCE_PATH}"/setup
source "${SOURCE_PATH}"/lib/gentoo-utils.sh

function partition_drive() {
    local disk=$1

    local command=

    print_header "Running sfdisk"
    
    if [[ $PRETEND == "true" ]]; then
        command=cat
    else
        command="sfdisk -f ${disk}"
    fi

    # TODO: Better sfdisk specification?

    $command <<SFDISK_COMMAND
# partition table of ${disk}
unit: sectors

${disk}1 : start=     2048, size=   204800, Id=83
${disk}2 : start=   206848, size=  4194304, Id=82
${disk}3 : start=  4401152, size= 20971520, Id=83
${disk}4 : start= 25372672, size=, Id=83
SFDISK_COMMAND

    echo "...Done"

}

function create_filesystems {

    local disk=$1
    local type=$2

    print_header "Make filesystems, type: ${type}"

    # TODO: make sure file systems are unmounted and check error
    # codes.

    if [[ $PRETEND != "true" ]]; then
        mkfs.ext2 -FF ${disk}1
        mkswap        ${disk}2
        if [[ $TYPE == "ext4" ]]; then
            mkfs.${type} -i 1024 ${disk}3
        else
            mkfs.${type} -f ${disk}3
        fi
        mkfs.${type} -f ${disk}4
    fi
    echo "...Done"
}

function mount_filesystems() {
    local disk=$1
    local D=$2

    local boot="${D}/boot"
    
    print_header "Mounting filesystems"

    if [[ $PRETEND == "true" ]]; then
        echo "mount ${disk}3 $D"
        echo "mount ${disk}1 $boot"
    else
        mkdir -p $D
        if [[ $TYPE == "btrfs" ]]; then
            mount -o compress=zlib "${disk}3" $D
        else
            mount "${disk}3" $D
        fi

        mkdir -p $boot
        mount "${disk}1" $boot
        swapon ${disk}2
    fi

    echo "...Done"
    
}

function wget_stage3_tarball() {

    local directory=$1

    pushd $directory
    
    print_header "Getting stage3"

    if [[ $PRETEND != "true" ]]; then
        wget "ftp://datamill.uwaterloo.ca/internal/stage3/$ARCH.tar.bz2" -O stage3.tar.bz2
    fi
    
    popd

    echo "...Done"
}

function extract_tarballs() {

    local directory=$1

    pushd $directory

    print_header "Extract stage3 and portage"

    echo "Extracting..."

    if [[ $PRETEND != "true" ]]; then
        tar xavpf stage3.tar.bz2
    fi

    popd

    echo "...Done"

}


function install_files() {
    local D=$1
    local package_dir=$SOURCE_PATH

    pushd $package_dir

    print_header "Installing configure scripts"

    if [[ $PRETEND == "true" ]]; then
        echo "Installing setup to ${D}/root/"
        echo "Installing configure_gentoo.sh ${D}/root/"
        echo "Installing gentoo-utils.sh ${D}/root/lib"
    else
        cp -v setup               "${D}/root/"
        cp -v configure_gentoo.sh "${D}/root/"
        cp -v overlay.xml "${D}/root/"
        mkdir -pv                 "${D}/root/lib"
        cp -v lib/gentoo-utils.sh "${D}/root/lib"
    fi
    
    popd

}

function main() {
    if [[ $ARCH != sparc64 ]]; then
        partition_drive $DISK
        create_filesystems $DISK $TYPE
        mount_filesystems $DISK $D
    fi
    wget_stage3_tarball $D
    extract_tarballs $D

    install_files $D

    print_header "Chrooting!"
    chroot_cmd $D /root/configure_gentoo.sh -H $HOSTNAME -d $DISK -i $NETIF $RASPBERRY
}

main;
