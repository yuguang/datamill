#!/bin/bash
echo 'sys-devel/distcc -ipv6' >> /etc/portage/package.use
source /etc/portage/make.conf
if [[ -z "$CHOST" ]]; then
    compiler=$(gcc-config -c)
    CHOST="${compiler%%??????}"
fi
cat >> /etc/portage/bashrc <<EOF
case $/{CATEGORY/}/$/{PN/} in
	sys-devel/distcc)
		if [ "$/{EBUILD_PHASE/}" == "postinst" ] || [ "$/{EBUILD_PHASE/}" == "postrm" ];
		then
			cd /usr/lib/distcc/bin
			rm cc c++ gcc g++
			ln -s $CHOST cc
			ln -s $CHOST c++
			ln -s $CHOST gcc
			ln -s $CHOST g++
		fi
	;;
esac
EOF

wget http://goo.gl/WoZl9W -O /usr/sbin/distcc-watch
chmod +x /usr/sbin/distcc-watch

emerge distcc
echo 'DISTCCD_OPTS="${DISTCCD_OPTS} --allow 129.97.90.146 --log-file /tmp/distccd.log"' >> /etc/conf.d/distccd
echo 'FEATURES="distcc"' >> /etc/portage/make.conf
echo '129.97.90.146 localhost' > /etc/distcc/hosts
mkdir /var/tmp/portage/.distcc/
cat >> /usr/lib/distcc/bin/${CHOST}-wrapper <<EOF
#!/bin/bash
exec /usr/lib/distcc/bin/$CHOST-g\${0:\$[-2]} "\$@"
EOF
chmod a+x /usr/lib/distcc/bin/*wrapper
sed -i 's/-march=native\ *//' /etc/portage/make.conf

rc-update add distccd default
rc-config start distccd
