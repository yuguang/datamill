#!/bin/bash
if="$(ls /sys/class/net | grep enp)"
echo "config_$if='dhcp'" >> /etc/conf.d/net
cd /etc/init.d
ln -s net.lo net."$if"
rm /etc/init.d/net.eth0
rc-update add net."$if" default
rc-update del net.eth0 default
