import re
import subprocess
import socket
import fcntl
import struct
import os

import worker_common

from worker_common.utilities import get_worker_logger
logger = get_worker_logger('worker_info')

def get_hostname():
    return socket.gethostname()

def get_ip_address():

    ip = None

    ifaces = [i for i in os.listdir('/sys/class/net')
              if i.startswith(('en', 'wl', 'eth'))]

    ifaces.sort()

    for iface in ifaces:
        if ip is None:
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, # SIOCGIFADDR
                                                  struct.pack('256s', iface))[20:24])
            except:
                continue

    if ip is None:
        try:
            ip = socket.gethostbyname(socket.gethostname())
        except:
            pass

    if ip is None:
        ip = '127.0.0.1'

    return ip

def safe_int(number, round_number=False):
    if type(number) is str and len(number) == 0:
        return 0
    # the number is assumed to be a float or int
    if round:
        number = round(float(number))
    number = int(number)
    return number

def get_info_field(field_name, lines):
    """
    Grab a field value from output lines.
    """
    for line in lines:
        match = re.search(r'^\s*' + re.escape(field_name) + r'\s*:\s+(.*)', line)
        if match:
            value = match.group(1)
            if not (value in ['', 'Unknown']):
                return value
    return ''

def get_caches(lines):
    caches = {
        'L1': 'N/A',
        'L2': 'N/A',
        'L3': 'N/A',
    }
    """
    Grab a field value from lshw output.
    """
    for line in lines:
        match = re.search(r'^\S+\s+[a-z]+\s+([\w]*)\s(L1|L2|L3)\scache\s*', line)
        if not match:
            continue
        groups = match.groups()
        print groups
        if groups[1] in caches.keys():
            caches[groups[1]] = groups[0]
    return caches

def get_disk(lines):
    """
    Grab a field value from lshw output.
    """
    for line in lines:
        match = re.search(r'^\S+\s+(\S+)\s+disk\s+(.*)$', line)
        if match and match.groups()[0] == '/dev/sda':
            return match.groups()[1]
    return 'Unknown'

def get_cpuinfo_field(field_name, line):
    """
    Grab the value from /proc/cpuinfo field name.
    """

    return re.search(r'^' + re.escape(field_name) + r'\s*:\s+(.*)', line)


def get_architecture():
    return subprocess.check_output("uname -m", shell=True).strip()

def get_pagesize():
    return subprocess.check_output("getconf PAGESIZE", shell=True).strip()

def get_features():
    return worker_common.factor_factory.get_features()

def get_info(command):
    # lshw can fail on some arm machines, we need to allow for some failure
    info = ""
    try:
        info = subprocess.check_output(command, shell=True).split('\n')
    except:
        pass

    return info

def get_worker_info():
    """
    This gets worker info, puts it into a map and returns it
    """

    architecture = model = chipsetflags = ip = ""
    herz = numcpu = pagesize = memtotal = 0

    architecture = get_architecture()
    pagesize = get_pagesize()
    hostname = get_hostname()
    ip = get_ip_address()

    gpuinfo = get_info("lshw -C display")
    gpu_model = get_info_field("product", gpuinfo)                 or "Unknown"
    gpu_chipsetflags = get_info_field("capabilities", gpuinfo)     or ""
    gpu_herz = get_info_field("clock", gpuinfo).replace('MHz', '') or "0"
    raminfo = get_info("dmidecode --type 17")
    speed_field = get_info_field("Speed", raminfo)
    caches = get_caches(get_info("lshw -short -C memory"))
    disk = get_disk(get_info("lshw -short -C disk"))
    if speed_field.find('MHz') > -1:
       ram_herz =  speed_field.replace(' MHz', '')
    else:
        ram_herz = '0'
    ram_type = get_info_field("Type", raminfo)

    with open('/proc/cpuinfo', 'r') as cpuinfo:
        numcpu = 0

        if re.match(r"^arm", architecture):
            for line in cpuinfo:
                match = get_cpuinfo_field("Processor", line) or get_cpuinfo_field("model name", line)
                if match:
                    model = match.group(1)
                match = get_cpuinfo_field("BogoMIPS", line)
                if match:
                    herz = match.group(1)
                match = get_cpuinfo_field("Features", line)
                if match:
                    chipsetflags = match.group(1)
                if re.search("processor", line):
                    numcpu += 1

        elif re.match(r"^sparc64", architecture):
            for line in cpuinfo:
                match = get_cpuinfo_field("cpu", line)
                if match:
                    model = match.group(1)
                match = get_cpuinfo_field("Cpu0ClkTck", line)
                if match:
                    herz = int(match.group(1), 16) / 1000000
                match = get_cpuinfo_field("cpucaps", line)
                if match:
                    chipsetflags = match.group(1)
                match = get_cpuinfo_field("ncpus active", line)
                if match:
                    numcpu = int(match.group(1))

        else: # x86, x86_64
            for line in cpuinfo:
                match = get_cpuinfo_field("model name", line)
                if match:
                    model = match.group(1)
                match = get_cpuinfo_field("cpu MHz", line)
                if match:
                    herz = match.group(1)
                match = get_cpuinfo_field("flags", line)
                if match:
                    chipsetflags = match.group(1)
                if re.match("processor", line):
                    numcpu += 1

    with open('/proc/meminfo', 'r') as meminfo:
        for line in meminfo:
            m = re.match(r"^MemTotal\s*:\s+(\d+)", line)
            if m:
                memtotal = m.group(1)

    worker_info = {
        'hostname': hostname,
        'ip': ip,
        'num_cpus': numcpu,
        'cpu_arch': architecture,
        'cpu_model': model,
        'cpu_flags': chipsetflags,
        'cpu_mhz': safe_int(herz, True),
        'page_sz': safe_int(pagesize),
        'ram_sz': safe_int(memtotal),
        'gpu_model': gpu_model,
        'gpu_flags': gpu_chipsetflags,
        'gpu_mhz': safe_int(gpu_herz),
        'ram_mhz': safe_int(ram_herz),
        'ram_type': ram_type,
    }

    return worker_info

def worker_info_matches(saved_dict, curr_dict):

    try:
        del curr_dict['ip'] # remove before comparing since if it changes we don't care
        del curr_dict['hostname'] # remove before comparing since if it changes we don't care
        del curr_dict['cpu_mhz'] # remove before comparing since it might be very volatile
    except KeyError:
        logger.warning("curr worker info is missing data")

    # compare only keys that are in the curr dict since we inject extra stuff in saved_dict
    for key in curr_dict:
        try:
            if curr_dict[key] != saved_dict[key]:
                return False
        except KeyError:
            logger.warning("saved worker info is missing data")
            return False

    return True
