# portage_config.py - controls portage config on either benchmark or controller side.

import re
import os
import settings
import subprocess
import shutil
import tempfile
from worker_common.utilities import get_worker_logger
logger = get_worker_logger('worker')

# Template of the DataMill portage configuration file.
config_template = """
WORKER_USEFLAGS="{use_flags}"
USE="$USE $WORKER_USEFLAGS"
"""

keywords_template = """
# DataMill Keywords
{keyword_entries}
"""

class PortageConfig:

    def __init__(self, worker_status, root = None, portage_config = None):

        if root is None:
            root = settings.ROOT

        if portage_config is None:
            portage_config = settings.CONTROLLER_PORTAGE_CONFIG

        # This object has a different idea of root than worker.py,
        # self.root is actually the root of the portage config, not
        # the current root.
        self.root = root
        self.portage_config = portage_config
        self.status = worker_status

    def configure_portage(self):
        """Cleanup the configuration to match DataMill's expectations. This
        includes adding a source statement to pull in our datamill
        specific package configurations.

        """

        # 1. FIX OLDSTYLE `make.conf`

        # /etc/make.conf used to be the canonical location for make.conf
        old_make_conf = os.path.join(self.root, 'etc', 'make.conf')

        # First let's check for oldstyle configs, and make them newstyle.
        if os.path.exists(old_make_conf):
            shutil.move(old_make_conf,
                        os.path.join(self.root, 'etc', 'portage', 'make.conf'))

        # 2. FIX make.conf SOURCE
        self.clean_make_conf()

        # 3. FIX MISSING KEYWORD CONF
        self.write_portage_conf()

    def clean_make_conf(self):

        make_conf = os.path.join(self.root, 'etc', 'portage', 'make.conf')

        source_statement = 'source {datamill_config}\n'.format(
            # The use of the literal '/' is not a mistake or
            # typo. This path must be absolute from the perspective of
            # `self.root` as if it were currently the real root.
            datamill_config=os.path.join('/', settings.DATAMILL_PORTAGE_CONF)
        )

        source_statement_pattern = r"source.*{datamill_config_basename}".format(
            datamill_config_basename = os.path.basename(settings.DATAMILL_PORTAGE_CONF)
        )

        mc_lines = None
        with open(make_conf, "r") as mc:
            mc_lines = mc.readlines()

        # 2. IF SOURCED ALREADY, ENSURE CORRECTNESS
        mc_matching_lines = [re.search(source_statement_pattern, line) for line in mc_lines]
        if any(mc_matching_lines):
            # Cast to bool
            mc_matching_lines = map(bool, mc_matching_lines)

            # More than one source line?
            if len(filter(lambda b: b, mc_matching_lines)) > 1:
                logger.warning("make.conf may have more than one source command for {datamill_portage_conf}".format(
                    settings.DATAMILL_PORTAGE_CONF
                ))

            # Update to new source statement
            mc_lines[mc_matching_lines.index(True)] = source_statement

            with open(make_conf, "w") as mc:
                mc.writelines(mc_lines)

            # Early return
            return

        # 3. IF NOT SOURCED, ADD SOURCE STATEMENT
        with open(make_conf, "w") as mc:
            mc_lines += "\n" + source_statement
            mc.writelines(mc_lines)

    def write_portage_conf(self):

        for filespec, contentspec in self.portage_config.iteritems():

            filespec = os.path.join(self.root, filespec)
            filespec_dir = os.path.dirname(filespec)

            # 1. IS THE DIR NOT EXISTANT?
            if filespec_dir == '':
                logger.warning('Portage Configuration filepath {file} looks incorrect.'.format(
                    file = filespec
                ))

                # Try with the rest
                continue

            # 2. IS IT A FILE?
            if os.path.exists(filespec_dir) and not os.path.isdir(filespec_dir):
                logger.info('Portage Configuration file {file} being moved to a more appropriate location.'.format(
                    file = filespec_dir
                ))

                temp = tempfile.mktemp()
                shutil.move(filespec_dir, temp)
                os.makedirs(filespec_dir)
                shutil.move(temp, os.path.join(filespec_dir, os.path.basename(filespec_dir)))

            # 3. IS IT MISSING?
            if not os.path.exists(filespec_dir):
                os.makedirs(filespec_dir)

            # After all that, we should have an appropriate dir for
            # the file we are about to write to

            with open(filespec, 'w') as f:
                f.write(contentspec)

    def get_portage_variable(self, variable):
        """Return a portage variable. This returns a string with the
        variable's contents. This should accurately reflect the state
        of the portage configuration files.

        """
        return subprocess.check_output("portageq envvar {variable}".format(
            variable = variable
        ), shell=True)

    def useflags_configured(self):
        """Check that the useflags in the DataMill portage config are correct.

        """

        return set(self.get_portage_variable("WORKER_USEFLAGS").split()) == set(settings.WORKER_USEFLAGS)
