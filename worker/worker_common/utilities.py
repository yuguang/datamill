# LOGGING UTILITIES

import logging
import sys
import os
import settings

def get_worker_logger(mod_name):
    if sys.argv[0] != '':
        script_name = os.path.splitext(
                os.path.basename(sys.argv[0])
                )[0]

        return logging.getLogger('{}.{}'.format(script_name, mod_name))
    else:
        return logging.getLogger('datamill_common')

class SingletonErrorLogFile(object):
    _instance = open(os.path.join(settings.ROOT, settings.WORKER_LOG_DIR, settings.STDERR_LOG), 'w', 0)
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Singleton, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance

# EXECUTION TIME UTILITIES
import time

def timed_method(method):

    def time_wrapped(self, *args, **kwargs):
        start = time.time()
        result = method(self, *args, **kwargs)
        stop = time.time()
        self.status['{}_{}_time'.format(method.__module__, method.__name__)] = stop - start
        return result

    return time_wrapped

def lock(method):

    def lock(self, *args, **kwargs):
        self.status['busy'] = True
        result = method(self, *args, **kwargs)
        self.status['busy'] = False
        return result

    return lock

# JSON SERIALIZATION HELP

def date_json_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj))
