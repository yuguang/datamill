from unittest import TestCase
from worker_common import settings, job
from worker_common.factor import Factor

import tempfile
import os
import shutil
from worker_common.status_shelve import shelve
import shelve as shelve_old
import logging
import stat

class MyFactor(Factor):

    def presetup(self):
        self.level = "it was executed"

    def prepopulate(self):
        self.level = "it was executed"

    def preboot(self):
        self.level = "it was executed"

    def prerun(self):
        self.level = "it was executed"

    def precollect(self):
        self.level = "it was executed"

    def system_level(self):
        return "it was executed"

class Job_Tests(TestCase):

    def mock_file(self, file_path, content="some content"):

        try:
            os.makedirs(os.path.dirname(file_path))
        except OSError:
            pass

        with open(file_path, 'w') as f:
            f.write(content)

    def setUp(self):

        self.tempdir = tempfile.mkdtemp()
        settings.ROOT = self.tempdir

        self.unpack_dir = os.path.join(settings.ROOT, "package")
        os.makedirs(self.unpack_dir)

        os.makedirs(os.path.join(settings.ROOT, settings.DATAMILL_DIR))
        os.makedirs(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, settings.DATAMILL_DIR))

    def tearDown(self):
        try:
            self.job.status.close()
        except:
            pass

        shutil.rmtree(self.tempdir)

    def prep_script(self, scriptname):

        # Make a collect script and make it executable
        script_path = os.path.join(settings.ROOT, settings.BENCHMARK_WORK_DIR, scriptname)
        self.mock_file(script_path, """
        echo supbro
        """)
        os.chmod(script_path, stat.S_IRWXU)

        return script_path

    def prep_user_results(self):

        # Make a user results file matching the collect script output
        user_results = os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, settings.BENCHMARK_WORK_DIR, "supbro")
        self.mock_file(user_results, """
        this is a file.
        """)

        return user_results

    def prep_bench_status_file(self):

        # Make a bench_status_file
        bsf_dir = os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, settings.DATAMILL_DIR)
        if not os.path.exists(bsf_dir):
            os.makedirs(bsf_dir)
        bsf = os.path.join(bsf_dir, settings.JOB_STATUS_FILE)

        bsf_shelve = shelve_old.open(bsf)
        bsf_shelve.sync()
        bsf_shelve.close()

        return bsf

    def test_constructor(self):
        self.job = j = job.Job()

        self.assertNotEqual(None, j)
        self.assertEqual(j.status.__class__, shelve)
        self.assertEqual(j.factor_objects, [])

    test_constructor.local_testable = True

    def test_from_package(self):
        self.job = j = job.Job.from_package("hithere", os.path.join("worker_common", "tests", "test_job.json"))

        self.assertNotEqual(j, None)
        self.assertEqual(j.status.__class__, shelve)
        self.assertEqual("x86_64", j.configuration['factors']['cpu_arch'])
        self.assertEqual({}, j.status)

    test_from_package.local_testable = True

    def test_from_results_ideal(self):

        bsf = os.path.join(self.tempdir, "bench_status_file")

        bsf_shelve = shelve_old.open(bsf)
        bsf_shelve.sync()
        bsf_shelve.close()

        self.job = j = job.Job.from_results(bench_status_file = bsf)

        self.assertNotEqual(j, None)
        self.assertEqual(j.status.__class__, shelve)
        self.assertEqual({}, j.status)

    test_from_results_ideal.local_testable = True

    def test_from_results_missing(self):

        bsf = os.path.join(self.tempdir, "bench_missing_status_file")

        self.job = j = job.Job.from_results(bench_status_file = bsf)

        self.assertNotEqual(j, None)
        self.assertEqual(j.status.__class__, shelve)
        self.assertEqual(j.factor_objects, [])

    test_from_results_missing.local_testable = True

    def test_name(self):

        bsf = self.prep_bench_status_file()

        x = shelve.open(bsf)
        x['name'] = 'whatever'
        x.close()

        from_constructor = job.Job()
        from_constructor.status['name'] = 'hithere'
        self.assertEqual('hithere', from_constructor.name())
        from_constructor.status.close()

        from_package = job.Job.from_package("hithere", os.path.join("worker_common", "tests", "test_job.json"))
        from_package.status['name'] = "sup"
        self.assertEqual('sup', from_package.name())
        from_package.status.close()
        
        from_results = job.Job.from_results(bench_status_file = bsf)
        self.assertEqual('whatever', from_results.name())
        from_results.status.close()

    test_name.local_testable = True

    def test_initialize_factors(self):

        j = job.Job.from_package("hithere", os.path.join("worker_common", "tests", "test_job2.json"))

        from worker_common.factors.address_randomization import address_randomization
        self.assertEqual(address_randomization, j.factor_objects[0].__class__)

        for f in j.factor_objects:
            self.assertTrue(isinstance(f, Factor))

    test_initialize_factors.local_testable = True

    def test_finish(self):

        self.job = j = job.Job.from_package("hithere", os.path.join("worker_common", "tests", "test_job2.json"))

        j.finish("sup", "hello")

        self.assertEqual(float, type(j.status['finish_time']))
        self.assertEqual("hello", j.status['finish_msg'])
        self.assertEqual("sup", j.status['success'])

    test_finish.local_testable = True

    def test_fail(self):
        self.job = j = job.Job.from_package("hithere", os.path.join("worker_common", "tests", "test_job2.json"))

        j.fail("hello")

        self.assertEqual(float, type(j.status['finish_time']))
        self.assertEqual("hello", j.status['finish_msg'])
        self.assertEqual(False, j.status['success'])

    test_fail.local_testable = True

    def test_results_file_dict_has_results(self):

        self.prep_script("collect.sh")
        user_results = self.prep_user_results()
        bsf = self.prep_bench_status_file()

        self.job = j = job.Job.from_results(bsf)
        # A bit of a hack doing this now.
        j.do_collect()

        results = j.results_file_dict()

        self.assertEqual("supbro", j.status['job_do_collect_output'].strip())
        self.assertEqual(True, j.status['job_do_collect_success'])
        self.assertEqual(user_results, results['user_results'])
        self.assertTrue(os.path.exists(results['job_status']))

    test_results_file_dict_has_results.local_testable = True

    def test_results_file_list_no_results(self):

        # Make a bench_status_file

        bsf = self.prep_bench_status_file()

        self.job = j = job.Job.from_results(bench_status_file=bsf)

        # A bit of a hack doing this now.
        j.do_collect()

        results = j.results_file_dict()

        self.assertEqual("", j.status['job_do_collect_output'].strip())
        self.assertEqual(False, j.status['job_do_collect_success'])

        # Should at least have a job status file
        self.assertTrue(os.path.exists(results['job_status']))

    test_results_file_list_no_results.local_testable = True

    def run_do_x_hook(self, hookname):

        # TODO: How to test this?
        self.job = j = job.Job.from_package("hithere", os.path.join("worker_common", "tests", "test_job.json"))

        j.factor_objects = [MyFactor("sup")]
        # Should not raise exceptions
        j.do_x_hooks(hookname)
        j.verify_factor_levels()


        self.assertTrue(j.status["MyFactor_verify"])
        self.assertEqual("it was executed", j.factor_objects[0].level)

    def test_do_x_hooks(self):

        self.run_do_x_hook('prepopulate')

    test_do_x_hooks.local_testable = True

    def test_do_prepopulate_hooks(self):

        self.run_do_x_hook('prepopulate')

    test_do_prepopulate_hooks.local_testable = True

    def test_do_preboot_hooks(self):

        self.run_do_x_hook('preboot')

    test_do_preboot_hooks.local_testable = True

    def test_do_presetup_hooks(self):

        self.run_do_x_hook('presetup')

    test_do_presetup_hooks.local_testable = True

    def test_do_prerun_hooks(self):

        self.run_do_x_hook('prerun')

    test_do_prerun_hooks.local_testable = True

    def test_do_precollect_hooks(self):

        self.run_do_x_hook('precollect')

    test_do_precollect_hooks.local_testable = True

    def test_install(self):
        self.job = j = job.Job.from_package("hithere", os.path.join("worker_common", "tests", "test_job.json"))

        j.package_file = os.path.join(self.tempdir, "new_package.tar.gz")
        j.configuration_file = os.path.join(self.tempdir, "new_package.json")
        self.mock_file(j.package_file, """this is a package""")
        self.mock_file(j.configuration_file, """{ "some json": "or whatever" }""")

        j.install()

        self.assertTrue(os.path.exists(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, settings.DATAMILL_DIR, settings.STANDARD_PACKAGE_FILENAME)))
        self.assertTrue(os.path.exists(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, settings.DATAMILL_DIR, settings.STANDARD_CONFIGURATION_FILENAME)))

    test_install.local_testable = True

    def test_unpack(self):
        self.job = j = job.Job.from_package("hithere", os.path.join("worker_common", "tests", "test_job.json"))
        j.package_file = os.path.join(os.path.join('worker_common', 'tests', 'fake_benchmark.tar.gz'))
        j.unpack(self.unpack_dir)

        expected_listing = map( lambda path: os.path.normpath(os.path.join(self.unpack_dir, path)),
                                ['',
                                 'bin',
                                 'bin/store_this',
                                 'etc',
                                 'etc/store_this',
                                 'etc/local.d'])

        actual_listing = []
        for root, sub_folders, files in os.walk(self.unpack_dir):
            actual_listing.append(root)
            for f in files:
                actual_listing.append(os.path.join(root, f))

        self.assertEqual(set(expected_listing), set(actual_listing))

    test_unpack.local_testable = True

    def run_package_script_ideal(self, scriptname):

        # Make a collect script and make it executable
        self.prep_script(scriptname + ".sh")

        # Make a user results file matching the collect script output
        self.prep_user_results()

        # Make a bench_status_file
        bsf = self.prep_bench_status_file()

        self.job = j = job.Job.from_results(bsf)
        # A bit of a hack doing this now.
        getattr(j, 'do_' + scriptname)()

        self.assertEqual("supbro", j.status['job_do_{}_output'.format(scriptname)].strip())
        self.assertEqual(True, j.status['job_do_{}_success'.format(scriptname)])

    def run_package_script_missing_file(self, scriptname):

        # Make a bench_status_file
        bsf = self.prep_bench_status_file()

        self.job = j = job.Job.from_results(bsf)
        # A bit of a hack doing this now.
        getattr(j, 'do_' + scriptname)()

        self.assertEqual("", j.status['job_do_{}_output'.format(scriptname)].strip())
        self.assertEqual(False, j.status['job_do_{}_success'.format(scriptname)])

    def test_do_x_ideal(self):
        # Make a collect script and make it executable

        self.run_package_script_ideal('collect')

    test_do_x_ideal.local_testable = True

    def test_do_x_missing_file(self):

        self.run_package_script_missing_file('collect')

    test_do_x_missing_file.local_testable = True

    def test_do_setup_ideal(self):
        
        self.run_package_script_ideal('setup')
        
    test_do_setup_ideal.local_testable = True
        
    def test_do_setup_missing_file(self):
        
        self.run_package_script_missing_file('setup')
        
    test_do_setup_missing_file.local_testable = True
        
    def test_do_run_ideal(self):
        
        self.run_package_script_ideal('run')
        
    test_do_run_ideal.local_testable = True
        
    def test_do_run_missing_file(self):
        
        self.run_package_script_missing_file('run')
        
    test_do_run_missing_file.local_testable = True
        
    def test_do_collect_ideal(self):
        
        self.run_package_script_ideal('collect')
        
    test_do_collect_ideal.local_testable = True
        
    def test_do_collect_missing_file(self):
        
        self.run_package_script_missing_file('collect')
        
    test_do_collect_missing_file.local_testable = True
        
