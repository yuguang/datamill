# import os
# import shutil
# import tempfile
# import controller.master
# import portage

# from unittest import TestCase
# from worker_common import settings
# from worker_common import worker

# import UserDict

# class ConfigFake(UserDict.DictMixin):
#     def __init__(self, config_root=None): pass
#     def __setitem__(self, index): pass
#     def __getitem__(self, index): return "one two three four"
#     def keys(self): return "USE"
#     def clear(self): pass

# class ControllerPartitionConfigTests(TestCase):

#     @classmethod
#     def mock_file(self, file_path, content="some content"):

#         try:
#             os.makedirs(os.path.dirname(file_path))
#         except OSError:
#             pass

#         with open(file_path, 'w') as f:
#             f.write(content)

#     def setUp(self):

#         self.tempdir = tempfile.mkdtemp()
#         settings.ROOT = self.tempdir

#         try:
#             os.makedirs(os.path.join(settings.ROOT, worker.settings.BENCHMARK_MOUNT))
#         except:
#             pass

#         # Make fstab link, needed for some tests
#         fstab_link = os.path.join(settings.ROOT, 'etc', 'fstab')
#         os.makedirs(os.path.dirname(fstab_link))
#         os.symlink('/etc/fstab', fstab_link)

#         # Create a worker object to play with.
#         master = controller.master.Master()
#         worker_status_file = os.path.join(settings.ROOT, settings.DATAMILL_DIR, settings.WORKER_STATUS_FILE)
#         if not os.path.exists(os.path.dirname(worker_status_file)):
#             os.makedirs(os.path.dirname(worker_status_file))
#         self.worker_object = worker.Worker(master, force_uuid=worker.settings.UNITTEST_WORKER_UUID, status_file=worker_status_file)

#     def tearDown(self):
#         self.worker_object.status.close()
#         self.worker_object.filesystem.unmount_benchmark_partition()
#         self.worker_object = None
#         shutil.rmtree(self.tempdir)

#     def test_configured_ideal(self):
#         old_config = portage.config
#         portage.config = ConfigFake

#         old_flags = settings.WORKER_USEFLAGS
#         settings.WORKER_USEFLAGS = 'one two three four'.split()

#         self.assertTrue(self.worker_object.filesystem.controller_partition_config.is_configured())

#         portage.config = old_config

#     def test_configured_insufficient(self):
#         old_config = portage.config
#         portage.config = ConfigFake

#         old_flags = settings.WORKER_USEFLAGS
#         settings.WORKER_USEFLAGS = 'one two three four five'.split()

#         self.assertFalse(self.worker_object.filesystem.controller_partition_config.configured())

#         portage.config = old_config
