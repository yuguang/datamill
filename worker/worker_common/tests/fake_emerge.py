# fake_emerge.py - mock Emerge class for running tests on machines that don't have portage.

class Emerge(object):

    installed_package_version_return = "whut-1.00-r0"
    worker_needs_update_return       = True
    installed_package_atom_return    = "whut-1.00-r0"
    version_string_from_atom_return  = "1.00"
    update_controller_package_return = None
    update_benchmark_package_return  = None
    available_package_atom_return    = "whut-1.00-r0"
    available_package_version_return = "1.00"
    layman_sync_return               = None

    @classmethod
    def installed_package_version(cls, package):
        return cls.installed_package_version_return

    @classmethod
    def worker_needs_update(cls):
        return cls.worker_needs_update_return

    @classmethod
    def installed_package_atom(cls, pattern):
        return cls.installed_package_atom_return

    @classmethod
    def version_string_from_atom(cls, atom):
        return cls.version_string_from_atom_return

    @classmethod
    def update_controller_package(cls, pattern="sys-apps/datamill-controller"):
        pass

    @classmethod
    def update_benchmark_package(cls, pattern="sys-apps/datamill-benchmark"):
        pass

    @classmethod
    def available_package_atom(cls, pattern):
        return cls.available_package_atom_return

    @classmethod
    def available_package_version(cls, pattern):
        return cls.available_package_version_return

    @classmethod
    def layman_sync(cls):
        return cls.layman_sync_return
