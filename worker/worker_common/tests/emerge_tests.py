import portage
import subprocess
from unittest import TestCase
from worker_common.emerge import Emerge

class EmergeTests(TestCase):

    def test_package_version(self):
        master_packspec = Emerge.available_package_atom('coreutils')
        cmdline_packspec = subprocess.check_output('find /var/db/pkg/ -mindepth 2 -maxdepth 2 -printf "%P\n" | grep coreutils'.strip(),
                shell=True)

        self.assertEqual(0, portage.pkgcmp(portage.pkgsplit(master_packspec), portage.pkgsplit(cmdline_packspec)))

    def test_layman_sync(self):
        try:
            Emerge.layman_sync()
        except Exception as e:
            self.fail('layman sync threw something: {}'.format(e))

    def test_needs_update(self):
        # should return false in the virtual worker, since it layman syncs
        # and emerges every time during boot
        self.assertFalse(Emerge.worker_needs_update())

    def test_package_version_ideal(self):

        package_version = Emerge.installed_package_atom("sys-devel/gcc")
        package_version = portage.pkgsplit(package_version)

        # Via shell

        via_shell = subprocess.check_output('find /var/db/pkg/ -mindepth 2 -maxdepth 2 -printf "%P\n" | grep sys-devel/gcc',
                                            stderr = subprocess.STDOUT,
                                            shell=True)

        via_shell = portage.pkgsplit(via_shell.split("\n")[0].strip())

        # If they are equal pkgcmp returns 0
        self.assertEqual(portage.pkgcmp(package_version, via_shell), 0)

    def test_package_version_no_package(self):

        # This should not find a package no matter what.
        package_version = Emerge.installed_package_atom("sys-devel/whut")

        self.assertEqual(None, package_version)

    def test_update_controller_package(self):

        # Rebuild the nano editor, should raise an exception on
        # failure.
        Emerge.update_controller_package('nano')

    # TODO: put this test back in. requires formatting/populating the bench partition
    # def test_update_benchmark_package_ideal(self):
    #     self.worker_object.update_benchmark_package()

    def test_update_benchmark_package_no_package(self):

        self.assertRaises(subprocess.CalledProcessError,
                Emerge.update_benchmark_package, pattern="sys-devel/not-found")

