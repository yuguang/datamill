from unittest import TestCase
import random
import logging
import sys

from worker_common.utilities import get_worker_logger, timed_method
from worker_common.factor_factory import factor_list

class Utilities_Tests(TestCase):

    def test_get_worker_logger(self):
        l = get_worker_logger('test')
        self.assertEquals(l.getEffectiveLevel(), logging.NOTSET)

    test_get_worker_logger.local_testable = True

    def test_factor_factory_factor_list(self):
        fl = factor_list()
        self.assertTrue(len(fl) > 0) # how else to test this

    test_factor_factory_factor_list.local_testable = True

    def test_timed_method(self):

        class Timed_Tester():

            status = {}

            @timed_method
            def some_method(self):
                numbers = []
                for i in range(9999):
                    numbers.append(random.randint(0, 255))

        tt = Timed_Tester()
        tt.some_method()

        self.assertTrue(tt.status['worker_common.tests.utilities_tests_some_method_time'] > 0)

    test_timed_method.local_testable = True
