from unittest import TestCase
from worker_common import worker
from worker_common import settings
import controller.master
import subprocess
import os
import tempfile
import shutil
import json
import re
from pwd import getpwnam
from grp import getgrnam

from controller.md5hash import md5hash
from worker_common.tests import fake_worker

class WorkerTests(TestCase):

    @classmethod
    def mock_file(self, file_path, content="some content"):

        try:
            os.makedirs(os.path.dirname(file_path))
        except OSError:
            pass

        with open(file_path, 'w') as f:
            f.write(content)

    def setUp(self):

        self.tempdir = tempfile.mkdtemp()
        settings.ROOT = self.tempdir

        try:
            os.makedirs(os.path.join(settings.ROOT, worker.settings.BENCHMARK_MOUNT))
        except:
            pass

        self.worker_object = fake_worker.make_worker()

    def tearDown(self):

        self.worker_object.status.close()
        self.worker_object.filesystem.unmount_benchmark_partition()
        self.worker_object = None
        shutil.rmtree(self.tempdir)

    def prep_fake_grub_conf(self):
        # Setup a fake grub.conf
        grub_conf = os.path.join(settings.ROOT, 'boot', 'grub', 'grub.conf')
        os.makedirs(os.path.dirname(grub_conf))
        self.mock_file(grub_conf, """
        default 0
        """)

        self.assertTrue(os.path.exists(grub_conf))

    def test_constructor(self):

        w = worker.Worker("Some Master Object")
        self.assertEqual(w.master, "Some Master Object")

    def test_status_file_dict(self):
        loglist = self.worker_object.status_file_dict()
        self.assertTrue(len(loglist) > 0)

    def test_clear(self):
        self.prep_fake_grub_conf()
        self.worker_object.clear()
        self.assertFalse('registered' in self.worker_object.status)

    def test_uuid(self):
        tmp_uuid = self.worker_object.uuid()
        self.assertEquals(len(tmp_uuid), len('550e8400-e29b-41d4-a716-446655440000'))

    def test_make_ip_file(self):
        self.worker_object.make_ip_file()
        with open(os.path.join(settings.ROOT, settings.IP_FILENAME), 'r') as infile:
            saved_info = json.load(infile)

        self.assertTrue('ip' in saved_info)

    test_make_ip_file.local_testable = True

    def test_make_hello_file(self):
        self.worker_object.make_hello_file()
        with open(os.path.join(settings.ROOT, settings.HELLO_FILENAME), 'r') as infile:
            saved_info = json.load(infile)

        self.assertTrue('cpu_mhz' in saved_info)

    def test_hello_file(self):
        self.assertEquals(os.path.join(settings.ROOT, settings.HELLO_FILENAME), self.worker_object.hello_file())

    def test_hardware_changed(self):
        self.worker_object.make_hello_file()
        with open(self.worker_object.hello_file(), 'r') as infile:
            saved_info = json.load(infile)

        saved_info['num_cpus'] = 9999

        with open(self.worker_object.hello_file(), 'w') as outfile:
            json.dump(saved_info, outfile)

        self.assertTrue(self.worker_object.hardware_changed())

    def test_untar_backup(self):

        fake_tarball =  os.path.join('worker_common', 'tests', 'fake_benchmark.tar.gz')

        benchmark_backup_dir = os.path.dirname(os.path.join(settings.ROOT, settings.BENCHMARK_BACKUP))
        if not os.path.exists(benchmark_backup_dir):
            os.makedirs(benchmark_backup_dir)

        shutil.copy(fake_tarball, os.path.join(settings.ROOT, settings.BENCHMARK_BACKUP))

        self.worker_object.filesystem.untar_backup()

        expected_listing = map( lambda path: os.path.normpath(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, path)),
                                ['',
                                 'bin/',
                                 'bin/store_this',
                                 'etc/',
                                 'etc/local.d/',
                                 'etc/store_this'])

        actual_listing = []
        for root, sub_folders, files in os.walk(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT)):
            actual_listing.append(root)
            for f in files:
                actual_listing.append(os.path.join(root, f))

        self.assertEqual(set(expected_listing), set(actual_listing))

    test_untar_backup.local_testable = True

    def test_backup_needs_update_factory(self,
                                         backup_version = None,
                                         installed_version = None,
                                         desired_output = None):

        def test_backup_needs_update(self):

            if backup_version:
                self.worker_object.status['backup_version'] = backup_version

            class EmergeMock(object):
                @classmethod
                def installed_package_version(cls, string):
                    return installed_version

            old_emerge = worker.Emerge

            try:
                worker.Emerge = EmergeMock
                self.assertEqual(desired_output, self.worker_object.backup_needs_update())
            finally:
                worker.Emerge = old_emerge

        return test_backup_needs_update

    def test_backup_needs_update_true(self):

        self.test_backup_needs_update_factory('0.0.1',
                                              '0.0.2',
                                              True)(self)

    test_backup_needs_update_true.local_testable = True

    def test_backup_needs_update_false(self):

        self.test_backup_needs_update_factory('0.0.1',
                                              '0.0.1',
                                              False)(self)

    test_backup_needs_update_false.local_testable = True

    def test_backup_needs_update_missing(self):

        self.test_backup_needs_update_factory(None,
                                              '0.0.1',
                                              True)(self)

    test_backup_needs_update_missing.local_testable = True

    def test_register(self):
        self.worker_object.register()
        self.assertTrue(os.path.exists(
            '/home/ftp/worker-new/{}v{}r{}{}'.format(
                self.worker_object.uuid(),
                self.worker_object.hello_version(),
                self.worker_object.hardware_revision(),
                settings.HELLO_EXTENSION)))

    @classmethod
    def create_job_files(cls, w):
        # We are going to play with /home/ftp, so let's clear it out

        ftpdirs = filter(lambda path: os.path.isdir(path),
                         map(lambda path: os.path.join('/home/ftp', path),
                             os.listdir('/home/ftp')))
        for d in ftpdirs:
            for f in os.listdir(d):
                filename = os.path.join(d, f)
                if os.path.isdir(filename):
                    shutil.rmtree(filename)
                else:
                    os.remove(filename)

        # takes a worker obj and makes a fake job for it on a local ftp
        workerdir = os.path.join('/home/ftp/', controller.master.Master.worker_ftp_dir(w))

        # JOB SCRIPTS

        cls.mock_file(os.path.join(workerdir, 'setup.sh'), content="""
        #!/bin/sh
        echo I'm compiling!
        """)

        cls.mock_file(os.path.join(workerdir, 'run.sh'), content="""
        #!/bin/sh
        sleep 5
        echo 5 > /unit_results.txt
        """)

        cls.mock_file(os.path.join(workerdir, 'collect.sh'), content = """
        #!/bin/sh
        tar czf /unit_results.tar.gz /unit_results.txt
        echo /unit_results.tar.gz
        """)

        # TAR PACKAGE
        job_name = '00-unittest-dummy'
        package_name = os.path.join(workerdir, '{}{}'.format(job_name,
            settings.PACKAGE_EXTENSION))

        # Basename because we want a relative path
        subprocess.check_call('tar czf {package_name} *.sh -C {directory}'\
                              .format(package_name=package_name,
                                      directory=workerdir),
                              shell=True)

        md5hash.generate_md5(package_name)

        # JOB CONFIG
        config_name = os.path.join(workerdir, '{}{}'.format(package_name.partition('.')[0], settings.CONF_EXTENSION))

        cls.mock_file(config_name, content="""
        {{ "name":"{}",
          "address_randomization":"on",
          "autogroup": "on",
          "compiler": "gcc",
          "drop_caches": "off",
          "env_padding": "0",
          "filesystem": "ext4",
          "freq_scaling": "off",
          "link_order": "alphabetical",
          "opt_flag": "-O3",
          "swap": "on" }}
        """.format(settings.INTEGRATION_JOB_NAME))

        md5hash.generate_md5(config_name)

    def get_benchmark_mountpoint(self):
        fstab_file = '/etc/fstab'
        with open(fstab_file, 'r') as f:
            fstab_contents = f.readlines()

        # Edit controller fstab
        for i in xrange(len(fstab_contents)):
            cols = re.split('\s+', fstab_contents[i])
            if cols[1].find('benchmark') != -1:
                return cols[1]

    def set_benchmark_mountpoint(self, mp):
        fstab_file = '/etc/fstab'
        with open(fstab_file, 'r') as f:
            fstab_contents = f.readlines()

        # Edit controller fstab
        for i in xrange(len(fstab_contents)):
            cols = re.split('\s+', fstab_contents[i])
            if cols[1].find('benchmark') != -1:
                cols[1] = mp
                fstab_contents[i] = "\t".join(cols) + "\n"
                break

        with open(fstab_file, 'w') as f:
            f.writelines(fstab_contents)

    def test_upload_pending_results(self):

        fake_worker.switch_to_real_emerge()

        WorkerTests.create_job_files(self.worker_object)

        old_bench_mountpoint = self.get_benchmark_mountpoint()
        self.set_benchmark_mountpoint(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT))

        self.worker_object.filesystem.create_backup(root_dir='/')
        self.prep_fake_grub_conf()
        self.worker_object.get_job() # will call stuff like populate partition!

        self.worker_object.boot_loader.set_boot_to_controller() # undo boot flip
        self.set_benchmark_mountpoint(old_bench_mountpoint) # undo fstab change

        self.worker_object.upload_pending_results()

        workerdir = '/home/ftp/worker-{}/'.format(self.worker_object.uuid())
        worker_dirlist = os.listdir(workerdir)

        matches = []
        for f in self.worker_object.status_file_dict():
            matches.extend([upload for upload in worker_dirlist if upload.find(f) != -1])

        # not all files need to match (we'll be missing user_results, for instance)
        self.assertGreater(len(matches), 0)

        fake_worker.switch_to_fake_emerge()

        shutil.rmtree(workerdir)
        os.mkdir(workerdir)

        uid = getpwnam('ftp')[2]
        gid = getgrnam('ftp')[2]
        os.chown(workerdir, uid, gid)

    def test_get_job(self):

        fake_worker.switch_to_real_emerge()

        WorkerTests.create_job_files(self.worker_object)

        old_bench_mountpoint = self.get_benchmark_mountpoint()
        self.set_benchmark_mountpoint(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT))

        self.worker_object.filesystem.create_backup(root_dir='/')
        self.assertTrue(os.path.exists(os.path.join(settings.ROOT, settings.BENCHMARK_BACKUP)))

        self.prep_fake_grub_conf()

        self.worker_object.get_job()

        self.worker_object.boot_loader.set_boot_to_controller() # undo boot flip
        self.set_benchmark_mountpoint(old_bench_mountpoint) # undo fstab change

        self.assertTrue('worker_common.filesystem_populate_benchmark_partition_time' in self.worker_object.status)

        ftpdirs = filter(lambda path: os.path.isdir(path),
                         map(lambda path: os.path.join('/home/ftp', path),
                             os.listdir('/home/ftp')))

        fake_worker.switch_to_fake_emerge()

        for d in ftpdirs:
            for f in os.listdir(d):
                filename = os.path.join(d, f)
                if os.path.isdir(filename):
                    shutil.rmtree(filename)
                else:
                    os.remove(filename)
