from unittest import TestCase
import tempfile
import shutil
import os
import subprocess

from worker_common import settings
from worker_common.boot_loader import Grub, Silo, BeagleXM

class BootLoaderTests(TestCase):
    def mock_file(self, file_path, content="some content"):

        try:
            os.makedirs(os.path.dirname(file_path))
        except OSError:
            pass

        with open(file_path, 'w') as f:
            f.write(content)


    def setUp(self):
        self.tempdir = tempfile.mkdtemp()
        settings.ROOT = self.tempdir

    def tearDown(self):
        shutil.rmtree(self.tempdir)

    def test_grub_set_boot_to_benchmark(self):
        grub_conf = os.path.join(settings.ROOT, 'boot', 'grub', 'grub.conf')
        os.makedirs(os.path.dirname(grub_conf))
        self.mock_file(grub_conf, """
        default 0
        """)
        self.bootloader = Grub()
        self.bootloader._edit_grub_config(1)

        with open(grub_conf, 'r') as f:
            self.assertTrue("default 1" in f.read())

    test_grub_set_boot_to_benchmark.local_testable = True

    def test_grub_set_boot_to_controller(self):
        grub_conf = os.path.join(settings.ROOT, 'boot', 'grub', 'grub.conf')
        os.makedirs(os.path.dirname(grub_conf))
        self.mock_file(grub_conf, """
        default 1
        """)
        self.bootloader = Grub()
        self.bootloader._edit_grub_config(0)

        with open(grub_conf, 'r') as f:
            self.assertTrue("default 0" in f.read())

    test_grub_set_boot_to_controller.local_testable = True

    def test_silo_set_boot_to_benchmark(self):
        silo_controller = os.path.join(settings.ROOT, 'etc', 'silo.cont')
        silo_benchmark  = os.path.join(settings.ROOT, 'etc', 'silo.bench')
        silo_conf       = os.path.join(settings.ROOT, 'etc', 'silo.conf')
        os.makedirs(os.path.dirname(silo_conf))
        self.mock_file(silo_controller, """
        controller!
        """)

        self.mock_file(silo_benchmark, """
        benchmark!
        """)


        shutil.copy(silo_controller, silo_conf)

        self.assertTrue(os.path.exists(silo_controller))
        self.assertTrue(os.path.exists(silo_benchmark))
        self.assertTrue(os.path.exists(silo_conf))

        self.bootloader = Silo()

        try:
            self.bootloader.set_boot_to_benchmark()
        except subprocess.CalledProcessError:
            pass

        with open(silo_conf, 'r') as config:
            self.assertTrue('benchmark!' in config.read())

    def test_silo_set_boot_to_controller(self):
        silo_controller = os.path.join(settings.ROOT, 'etc', 'silo.cont')
        silo_benchmark  = os.path.join(settings.ROOT, 'etc', 'silo.bench')
        silo_conf       = os.path.join(settings.ROOT, 'etc', 'silo.conf')
        os.makedirs(os.path.dirname(silo_conf))
        self.mock_file(silo_controller, """
        controller!
        """)

        self.mock_file(silo_benchmark, """
        benchmark!
        """)

        shutil.copy(silo_benchmark, silo_conf)

        self.assertTrue(os.path.exists(silo_controller))
        self.assertTrue(os.path.exists(silo_benchmark))
        self.assertTrue(os.path.exists(silo_conf))

        self.bootloader = Silo()

        try:
            self.bootloader.set_boot_to_controller()
        except subprocess.CalledProcessError:
            pass

        with open(silo_conf, 'r') as config:
            self.assertTrue('controller!' in config.read())

    def test_uboot_set_boot_to_benchmark(self):
        uboot_conf = os.path.join(settings.ROOT, 'boot', 'bootcmd')
        os.makedirs(os.path.dirname(uboot_conf))

        self.mock_file(uboot_conf, """
        mmcblk0p2
        """)

        self.assertTrue(os.path.exists(uboot_conf))

        self.bootloader = BeagleXM()
        self.bootloader.edit_uboot_config('benchmark')

        with open(uboot_conf, 'r') as conf:
            self.assertTrue('mmcblk0p3' in conf.read())

    test_uboot_set_boot_to_benchmark.local_testable = True

    def test_uboot_set_boot_to_controller(self):

        uboot_conf = os.path.join(settings.ROOT, 'boot', 'bootcmd')
        os.makedirs(os.path.dirname(uboot_conf))

        self.mock_file(uboot_conf, """
        mmcblk0p3
        """)

        self.assertTrue(os.path.exists(uboot_conf))

        self.bootloader = BeagleXM()
        self.bootloader.edit_uboot_config('controller')

        with open(uboot_conf, 'r') as conf:
            self.assertTrue('mmcblk0p2' in conf.read())

    test_uboot_set_boot_to_controller.local_testable = True
