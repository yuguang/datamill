from unittest import TestCase
import subprocess
import worker_common.worker_info
import re

class WorkerInfoTest(TestCase):

    def setUp(self):

        self.worker_info = worker_common.worker_info

    def tearDown(self):
        pass

    def test_get_hostname(self):
        commandline_hostname = subprocess.check_output("hostname", stderr=subprocess.STDOUT, shell=True).strip()
        self.assertEqual(commandline_hostname, self.worker_info.get_hostname())
    test_get_hostname.local_testable = True

    def test_get_ip_address(self):

        # Does it look like an IP?
        self.assertTrue(re.match(r"\d+\.\d+\.\d+.\d+", self.worker_info.get_ip_address()))

    test_get_ip_address.local_testable = True

    def test_get_cpuinfo_field(self):

        with open("/proc/cpuinfo", 'r') as f:
            proc_cpuinfo = f.read()

        first_line = proc_cpuinfo.split("\n")[0]
        first_field = re.search(r"^(.*)\s*:\s+", first_line).group(1)
        data = re.search(r":\s*(.*)$", first_line).group(1).strip()
        self.assertEqual(data, self.worker_info.get_cpuinfo_field(first_field, first_line).group(1).strip())

    test_get_cpuinfo_field.local_testable = True

    def test_get_raminfo(self):
        worker_info = self.worker_info.get_worker_info()
        self.assertTrue(isinstance(worker_info['ram_mhz'], int))
        output = subprocess.Popen('dmidecode -s system-product-name', shell=True, stdout=subprocess.PIPE).stdout.read()
        self.assertTrue(worker_info['ram_type'].startswith('DDR') or output.find('VirtualBox') > -1)

    test_get_raminfo.local_testable = True

    def test_get_arm_alternative_cpu_name(self):
        match = self.worker_info.get_cpuinfo_field("model name", 'model name      : ARMv7 Processor rev 2 (v7l)')
        model = match.group(1)
        self.assertEqual(model, 'ARMv7 Processor rev 2 (v7l)')

    test_get_arm_alternative_cpu_name.local_testable = True

    def test_get_worker_info(self):

        self.assertEqual( dict, type(self.worker_info.get_worker_info()))

    test_get_worker_info.local_testable = True

    def test_worker_info_matches_ideal(self):
        # Ideal case, should pass
        saved_dict = {'uuid': 'who cares',
                      'ip': '192.168.0.160',
                      'hostname': 'tux',
                      'cpu_mhz': 10,
                      'something': 10,
                      'whatever': 20,
                      'dont care': "whut",
                      }

        curr_dict = {'ip': '192.168.0.160', 
                     'hostname': 'tux',
                     'something': 10,
                     'cpu_mhz': 10,
                     'whatever': 20,
                     'dont care': "whut",
                     }

        self.assertTrue(self.worker_info.worker_info_matches(saved_dict, curr_dict))

    test_worker_info_matches_ideal.local_testable = True

    def test_worker_info_matches_unequal(self):
        # Not the same, something changed, return false
        saved_dict = {'uuid': 'who cares',
                      'ip': '192.168.0.160',
                      'hostname': 'tux',
                      'cpu_mhz': 10,
                      'something': 10,
                      'whatever': 20,
                      'dont care': "whut",
                      }

        curr_dict = {'ip': '192.168.0.160', 
                     'hostname': 'tux',
                     'something': 10,
                     'cpu_mhz': 10,
                     'whatever': 50,
                     'dont care': "whut",
                     }

        self.assertFalse(self.worker_info.worker_info_matches(saved_dict, curr_dict))

    test_worker_info_matches_unequal.local_testable = True

    def test_worker_info_matches_malformed(self):
        # Malformed (missing uuid/ip/hostname) saved dict that loads, should report false.
        saved_dict = {'ip': '192.168.0.160',
                     'hostname': 'tux',
                     'something': 10,
                     'cpu_mhz': 10,
                     'dont care': "whut",
                     }

        curr_dict = {'ip': '192.168.0.160',
                 'hostname': 'tux',
                 'something': 10,
                 'cpu_mhz': 10,
                 'whatever': 20,
                 'dont care': "whut",
                 }

        self.assertFalse(self.worker_info.worker_info_matches(saved_dict, curr_dict))

    test_worker_info_matches_malformed.local_testable = True

    def test_worker_info_matches_threshold(self):

        # Small difference between cpu_mhz

        saved_dict = {'uuid': 'who cares',
                      'ip': '192.168.0.160',
                      'hostname': 'tux',
                      'cpu_mhz': 1000,
                      'something': 10,
                      'whatever': 20,
                      'dont care': "whut",
                      }

        curr_dict = {'ip': '192.168.0.160',
                     'hostname': 'tux',
                     'cpu_mhz': 1001,
                     'something': 10,
                     'whatever': 20,
                     'dont care': "whut",
                     }

        self.assertTrue(self.worker_info.worker_info_matches(saved_dict, curr_dict))

        # Less small difference in cpu_mhz (delta > 5%)
        curr_dict['cpu_mhz'] = 1051

        self.assertFalse(self.worker_info.worker_info_matches(saved_dict, curr_dict))

    test_worker_info_matches_threshold.local_testable = True

    def test_worker_info_matches_corner_cases(self):
        # Differing ips
        saved_dict = {'uuid': 'who cares',
                      'ip': '192.168.0.160',
                      'hostname': 'tux',
                      'cpu_mhz': 10,
                      'something': 10,
                      'whatever': 20,
                      'dont care': "whut",
                      }

        curr_dict = {'something': 10,
                     'ip': '192.168.0.161',
                     'hostname': 'tux',
                     'cpu_mhz': 10,
                     'whatever': 20,
                     'dont care': "whut",
                     }

        self.assertTrue(self.worker_info.worker_info_matches(saved_dict, curr_dict))

        # Differing hostname
        saved_dict = {'uuid': 'who cares',
                      'ip': '192.168.0.160',
                      'hostname': "derp",
                      'cpu_mhz': 10,
                      'something': 10,
                      'whatever': 20,
                      'dont care': "whut",
                      }

        curr_dict = {'something': 10,
                      'ip': '192.168.0.160',
                     'hostname': "lurch",
                     'cpu_mhz': 10,
                     'whatever': 20,
                     'dont care': "whut",
                     }

        self.assertTrue(self.worker_info.worker_info_matches(saved_dict, curr_dict))


    test_worker_info_matches_corner_cases.local_testable = True
