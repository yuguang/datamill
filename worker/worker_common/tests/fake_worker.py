# fake_worker.py - utils for setting up fake workers.

import os
import worker_common.worker
import worker_common.filesystem
import controller.master

from worker_common import settings
from worker_common.tests import fake_emerge
from worker_common import worker
from worker_common.emerge import Emerge

REAL_EMERGE = Emerge
FAKE_EMERGE = fake_emerge.Emerge


def switch_to_fake_emerge():
    worker_common.worker.Emerge = FAKE_EMERGE
    worker_common.filesystem.Emerge = FAKE_EMERGE

def switch_to_real_emerge():
    worker_common.worker.Emerge = REAL_EMERGE
    worker_common.filesystem.Emerge = REAL_EMERGE

def make_worker(use_fake_emerge=True):

    if use_fake_emerge:
            switch_to_fake_emerge()

    # Make fstab link, needed for some tests
    fstab_link = os.path.join(settings.ROOT, 'etc', 'fstab')
    os.makedirs(os.path.dirname(fstab_link))
    os.symlink('/etc/fstab', fstab_link)

    # Make a portage config
    os.makedirs(os.path.join(settings.ROOT, settings.PORTAGE_KEYWORD_DIR))

    with open(os.path.join(settings.ROOT, 'etc', 'portage', 'make.conf'), 'w') as mc:
        mc.write("some stuff")

    # Create a worker object to play with.
    master = controller.master.Master()
    worker_status_file = os.path.join(settings.ROOT, settings.DATAMILL_DIR, settings.WORKER_STATUS_FILE)
    if not os.path.exists(os.path.dirname(worker_status_file)):
        os.makedirs(os.path.dirname(worker_status_file))
    worker_object = worker.Worker(master, force_uuid=worker.settings.UNITTEST_WORKER_UUID, status_file=worker_status_file)

    return worker_object
