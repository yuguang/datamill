import os
import shutil
import tarfile
import tempfile

from unittest import TestCase
from worker_common import worker
from worker_common import settings
from worker_common.tests import fake_worker

class FilesystemTests(TestCase):

    @classmethod
    def mock_file(self, file_path, content="some content"):

        try:
            os.makedirs(os.path.dirname(file_path))
        except OSError:
            pass

        with open(file_path, 'w') as f:
            f.write(content)

    def setUp(self):

        self.tempdir = tempfile.mkdtemp()
        settings.ROOT = self.tempdir

        try:
            os.makedirs(os.path.join(settings.ROOT, worker.settings.BENCHMARK_MOUNT))
        except:
            pass

        self.worker_object = fake_worker.make_worker()

    def tearDown(self):

        self.worker_object.status.close()
        self.worker_object.filesystem.unmount_benchmark_partition()
        self.worker_object = None
        shutil.rmtree(self.tempdir)

    def test_create_backup(self):
        if self.worker_object.filesystem.disk_usage() > settings.BACKUP_INSUFFICIENT_SPACE_PERCENT:
            return
        benchmark_backup = os.path.join(settings.ROOT, settings.BENCHMARK_BACKUP)
        fake_benchmark = os.path.join('worker_common', 'tests', 'fake_benchmark')

        try:
            os.makedirs(os.path.dirname(benchmark_backup))
            os.makedirs(fake_benchmark)
        except:
            pass

        self.worker_object.filesystem.create_backup(root_dir=fake_benchmark)

        self.assertTrue(os.path.exists(benchmark_backup))
        expected_listing = map(lambda path: os.path.normpath(os.path.join('/', path)),
                               ['bin/',
                                'bin/store_this',
                                'etc/',
                                'etc/store_this'])

        with tarfile.open(benchmark_backup, 'r') as tf:
            actual_listing = map(lambda path: '/' + path, tf.getnames())

        self.assertEqual(set(expected_listing), set(actual_listing))

    test_create_backup.local_testable = True

    def test_create_untarrable_files(self):

        self.worker_object.filesystem.create_untarrable_files()
        should_exist = [('dev', 'console'),
                        ('dev', 'null'),
                        ('dev', 'zero')]
        for filespec in should_exist:
            self.assertTrue(os.path.exists(os.path.join(settings.ROOT, worker.settings.BENCHMARK_MOUNT, *filespec)))

    def test_get_device_from_mountpoint(self):
        pass
