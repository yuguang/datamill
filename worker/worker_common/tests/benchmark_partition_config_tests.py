import os
import shutil
import tempfile

from unittest import TestCase
from worker_common import settings

from worker_common import worker
from worker_common.tests import fake_worker

class BenchmarkPartitionConfigTests(TestCase):

    @classmethod
    def mock_file(self, file_path, content="some content"):

        try:
            os.makedirs(os.path.dirname(file_path))
        except OSError:
            pass

        with open(file_path, 'w') as f:
            f.write(content)

    def setUp(self):

        self.tempdir = tempfile.mkdtemp()
        settings.ROOT = self.tempdir

        try:
            os.makedirs(os.path.join(settings.ROOT, worker.settings.BENCHMARK_MOUNT))
        except:
            pass

        self.worker_object = fake_worker.make_worker()

    def tearDown(self):
        self.worker_object.status.close()
        self.worker_object.filesystem.unmount_benchmark_partition()
        self.worker_object = None
        shutil.rmtree(self.tempdir)

    def test_configure_benchmark_fstab_ideal(self):

        fstab_file = os.path.join(self.tempdir, 'fstab')

        # Ideal case
        self.mock_file(fstab_file, content="""
# <fs>                  <mountpoint>    <type>          <opts>          <dump/pass>

# NOTE: If your BOOT partition is ReiserFS, add the notail option to opts.
/dev/sda1               /boot           ext2            noauto,noatime  1 2
/dev/sda3               /               ext4            noatime         0 1
/dev/sda2               none            swap            sw              0 0
/dev/cdrom              /mnt/cdrom      auto            noauto,ro       0 0
/dev/fd0                /mnt/floppy     auto            noauto          0 0
/dev/sda4               {BENCHMARK_MOUNT}  ext4            noauto,noatime          0 3
        """.format(BENCHMARK_MOUNT=os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT)))

        # with open(fstab_file, 'r') as f:
        #     resulting_lines = f.readlines()

        # self.assertEqual([], resulting_lines)

        self.worker_object.filesystem.benchmark_partition_config.configure_benchmark_fstab(fstab=fstab_file)

        with open(fstab_file, 'r') as f:
            resulting_lines = f.readlines()

        self.assertEqual(
            ['\n',
             '# <fs>                  <mountpoint>    <type>          <opts>          <dump/pass>\n',
             '\n',
             '# NOTE: If your BOOT partition is ReiserFS, add the notail option to opts.\n',
             '/dev/sda1               /boot           ext2            noauto,noatime  1 2\n',
             '/dev/sda2               none            swap            sw              0 0\n',
             '/dev/cdrom              /mnt/cdrom      auto            noauto,ro       0 0\n',
             '/dev/fd0                /mnt/floppy     auto            noauto          0 0\n',
             '/dev/sda4\t/\text4\tnoatime\t0\t1\t\n',
             '        '],
            resulting_lines)


    test_configure_benchmark_fstab_ideal.local_testable = True

    def test_configure_benchmark_fstab_malformed(self):
        # fstab malformed

        fstab_file = os.path.join(self.tempdir, 'fstab')

        try:
            os.remove(fstab_file)
        except:
            pass

        self.mock_file(fstab_file, content="""
# <fs>                  <mountpoint>    <type>          <opts>          <dump/pass>

# NOTE: If your BOOT partition is ReiserFS, add the notail option to opts.
/dev/sda1               /boot           ext2            noauto,noatime  1 2
/dev/sda3               /               ext4            noatime         0 1
/dev/sda2               none            swap            sw              0 0
/dev/cdrom              /mnt/cdrom      auto            noauto,ro       0 0
/dev/fd0                /mnt/floppy     auto            noauto          0 0
        """)

        self.assertRaises(OSError, self.worker_object.filesystem.benchmark_partition_config.configure_benchmark_fstab, fstab=fstab_file)

        try:
            os.remove(fstab_file)
        except:
            pass

        self.mock_file(fstab_file, content="""
# <fs>                  <mountpoint>    <type>          <opts>          <dump/pass>

# NOTE: If your BOOT partition is ReiserFS, add the notail option to opts.
/dev/sda1               /boot           ext2            noauto,noatime  1 2
/dev/sda2               none            swap            sw              0 0
/dev/cdrom              /mnt/cdrom      auto            noauto,ro       0 0
/dev/fd0                /mnt/floppy     auto            noauto          0 0
/dev/sda4               /mnt/benchmark  ext4            noauto,noatime          0 3
        """)

        self.assertRaises(OSError, self.worker_object.filesystem.benchmark_partition_config.configure_benchmark_fstab, fstab=fstab_file)


    test_configure_benchmark_fstab_malformed.local_testable = True

    def test_configure_benchmark_fstab_missing(self):

        fstab_file = os.path.join(self.tempdir, 'fstab')
        # fstab missing

        try:
            os.remove(fstab_file)
        except:
            pass

        self.assertRaises(IOError, self.worker_object.filesystem.benchmark_partition_config.configure_benchmark_fstab, fstab=fstab_file)

    test_configure_benchmark_fstab_missing.local_testable = True

    def test_configure_benchmark_profile(self):

        fprofile = os.path.join(self.tempdir, 'profile')

        # Ideal case
        self.mock_file(fprofile, content="""
blah
blah
blah
blah
PS1="(controller) ${PS1}"
        """)

        self.worker_object.filesystem.benchmark_partition_config.configure_benchmark_profile(fprofile=fprofile)


        with open(fprofile, 'r') as f:
            fprofile_lines = f.readlines()

        self.assertEqual(
            ['\n',
             'blah\n',
             'blah\n',
             'blah\n',
             'blah\n',
             'PS1="(benchmark) ${PS1}"\n',
             '        '],
            fprofile_lines)

        os.remove(fprofile)

        # No assertion necessary, it should just not raise an exception
        self.worker_object.filesystem.benchmark_partition_config.configure_benchmark_profile(fprofile=fprofile)
