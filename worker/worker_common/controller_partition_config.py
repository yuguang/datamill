import settings

from worker_common.portage_config import PortageConfig

class ControllerPartitionConfig:
    """This class represents the /etc directory of the controller
    partition.

    Any functions responsible for the configuration of the controller
    side can be found here.

    """

    def __init__(self, worker_status):
        """worker_status: the worker shelve object"""

        self.status = worker_status
        self.portage_config = PortageConfig(worker_status,
                                            root = settings.ROOT,
                                            portage_config = settings.CONTROLLER_PORTAGE_CONFIG)

    def configure_controller_side(self):
        """Main entry point of the configuration.

        """

        self.portage_config.configure_portage()
