import os
import re
import shutil
import subprocess
from worker_common import settings
from worker_common.filesystem import Filesystem
import socket
__metaclass__ = type

class Grub:

    def __init__(self, grub_config = None):
        hostname = socket.gethostname()
        if hostname.startswith('galileo'):
            self.grub_config = os.path.join(settings.ROOT, 'boot', 'boot', 'grub', 'grub.conf')
        elif grub_config is None:
            self.grub_config = os.path.join(settings.ROOT, 'boot', 'grub', 'grub.conf')



    def _edit_grub_config(self, default_boot):

        with open(self.grub_config, 'r') as f:
            grub_lines = f.readlines()

        for i in xrange(len(grub_lines)):
            if re.match(r'^\s*default\s+\d+', grub_lines[i]):
                grub_lines[i] = re.sub(r'^\s*default\s+\d+', 'default ' + str(default_boot), grub_lines[i])

        with open(self.grub_config, 'w') as f:
            f.writelines(grub_lines)

    def set_boot_to_benchmark(self):
        Filesystem.mount_boot_partition()
        self._edit_grub_config(1)

    def set_boot_to_controller(self):
        Filesystem.mount_boot_partition()
        self._edit_grub_config(0)


class Silo:

    def __init__(self):
        self.benchmark_config  = os.path.join(settings.ROOT, 'etc', 'silo.bench')
        self.controller_config = os.path.join(settings.ROOT, 'etc', 'silo.cont') # t is not a typo
        self.silo_config       = os.path.join(settings.ROOT, 'etc', 'silo.conf')

    def set_boot_to_benchmark(self):
        shutil.copy(self.benchmark_config, self.silo_config)
        subprocess.check_call('silo', shell=True)

    def set_boot_to_controller(self):
        shutil.copy(self.controller_config, self.silo_config)
        subprocess.check_call('silo', shell=True)

class UbootConfigCompiler:
    def _edit_uboot_config(self, default_boot, default_device=0):

        with open(self.uboot_config, 'r') as f:
            uboot_lines = f.readlines()

        for i in xrange(len(uboot_lines)):
            if re.search(r'mmcblk\dp\d+', uboot_lines[i]):
                uboot_lines[i] = re.sub(r'mmcblk\dp\d+', 'mmcblk' + str(default_device) + 'p' + str(default_boot), uboot_lines[i])

        with open(self.uboot_config, 'w') as f:
            f.writelines(uboot_lines)

    def compile_bootscript(self):
        subprocess.check_call('mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "boot script" -d ' + self.uboot_config + ' ' + self.uboot_image, shell=True)

class UbootConfigEditor:
    def __init__(self):
        self.uboot_config = os.path.join(settings.ROOT, 'boot', 'boot', 'uEnv.txt')

    def compile_bootscript(self):
        pass

    def _edit_uboot_config(self, default_boot, partition_string='mmcpart='):

        with open(self.uboot_config, 'r') as f:
            uboot_lines = f.readlines()

        for i in xrange(len(uboot_lines)):
            if re.search(partition_string, uboot_lines[i]):
                uboot_lines[i] = re.sub(r''+ re.escape(partition_string) + '\d', partition_string + str(default_boot), uboot_lines[i])

        with open(self.uboot_config, 'w') as f:
            f.writelines(uboot_lines)

    def edit_uboot_config(self, side):
        if side == 'benchmark':
            self._edit_uboot_config(4)
        elif side == 'controller':
            self._edit_uboot_config(3)

class SabreLite(UbootConfigCompiler):
    def __init__(self):
        self.uboot_config = os.path.join(settings.ROOT, 'boot', '6x_bootscript.txt')
        self.uboot_image = os.path.join(settings.ROOT, 'boot', '6x_bootscript')

    def edit_uboot_config(self, side):
        if side == 'benchmark':
            self._edit_uboot_config(1, 1)
        elif side == 'controller':
            self._edit_uboot_config(2, 0)

class CI20ConfigEditor:
    def __init__(self):
        self.side = 'controller'

    def edit_uboot_config(self, side):
        self.side = side

    def compile_bootscript(self):
        partition_map = {'controller': 'first', 'benchmark': 'second'}
        subprocess.check_call('{path} {disk} {partition}'.format(path=os.path.join(settings.ROOT, 'uboot', 'install.sh'), disk='/dev/mmcblk0', partition=partition_map[self.side]), shell=True)

class BeagleXM(UbootConfigEditor):
    def __init__(self):
        self.uboot_config = os.path.join(settings.ROOT, 'boot', 'bootcmd')

    def edit_uboot_config(self, side):
        if side == 'benchmark':
            self._edit_uboot_config(3, 'mmcblk0p')
        elif side == 'controller':
            self._edit_uboot_config(2, 'mmcblk0p')


class Uboot:
    def __init__(self):
        hostname = socket.gethostname()
        if hostname.startswith('beaglexm'):
            self.model = BeagleXM()
        elif hostname.startswith('sabre'):
            self.model = SabreLite()
        elif hostname.startswith('udooquad'):
            self.model = UbootConfigEditor()
        elif hostname.startswith('ci20'):
            self.model = CI20ConfigEditor()

    def set_boot_to_benchmark(self):
        Filesystem.mount_boot_partition()
        self.model.edit_uboot_config('benchmark')
        self.model.compile_bootscript()

    def set_boot_to_controller(self):
        Filesystem.mount_boot_partition()
        self.model.edit_uboot_config('controller')
        self.model.compile_bootscript()


def BootLoaderFactory(machine_arch):

    # Regex to 
    mapping = {
        r'sparc': Silo,
        r'arm':   Uboot,
        r'x86':   Grub,
        r'mips':  Uboot,
    }

    for arch, bootloader in mapping.iteritems():
        if re.match(arch, machine_arch):
            return bootloader()

    # Default to Grub
    return Grub()
