import json
import os
import subprocess
import shutil
import time
import tempfile

from status_shelve import shelve

from worker_common import settings
from worker_common.utilities import timed_method, get_worker_logger, date_json_handler
from worker_common import factor_factory
logger = get_worker_logger('job')

class Job:

    def __init__(self):
        # this will load (for example) /datamill/job_status, no matter which side ---
        # controller or benchmark --- it is
        self.status = shelve.open(os.path.join(settings.ROOT, settings.DATAMILL_DIR,
                             settings.JOB_STATUS_FILE))

        self.factor_objects = []

    @classmethod
    def from_package(cls, pkg_file, cfg_file, name=None):
        # use this constructor before running, to install, run hooks and run the actual job
        obj = cls()
        obj.package_file = pkg_file
        obj.configuration_file = cfg_file

        # clear the status dict
        obj.status.clear()

        if name is not None:
            obj.status['name'] = name

        with open(cfg_file) as cfg_fl:
            obj.configuration = json.load(cfg_fl)

        obj.initialize_factors()

        return obj

    @classmethod
    def from_results(cls, bench_status_file=None):

        # use this constructor after running, to upload results
        obj = cls()

        # this will load /mnt/benchmark/job_status, and therefore should
        # only be used from controller side

        # the shelf might not exist, since the controller side hooks
        # can fail or something. we fail the job in such a case
        if bench_status_file is None:
            bench_status_file = os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT,
                settings.DATAMILL_DIR, settings.JOB_STATUS_FILE)

        if os.path.exists(bench_status_file):
            bench_status = shelve.open(bench_status_file)

            # things defined in the bench-side dict clobber control-side dict:
            obj.status.merge(bench_status)
        else:
            obj.fail('failed to find job status file')

        return obj

    def name(self):
        return self.status['name']

    def initialize_factors(self):

        if 'factors' in self.configuration:
            factors_dict = self.configuration['factors']
        else:
            factors_dict = self.configuration # TODO this is back-compat, remove after all workers/jobs use the new dict

        for factor in factors_dict.keys():
            f = factor_factory.make_factor(factor, factors_dict[factor])
            if f:
                self.factor_objects.append(f)
            else:
                logger.warning("Factor {} not found".format(factor))

    def finish(self, success=True, message=None):
        self.status['finish_time'] = time.time()

        if message:
            self.status['finish_msg'] = message

        self.status['success'] = success

    def fail(self, msg):
        self.finish(success=False, message=msg)

    def results_file_dict(self):
        results_files = {}

        # ACTUAL USER RESULTS
        if self.status.has_key('job_do_collect_output'):

            split_collect = [ l.strip() for l in
                    self.status['job_do_collect_output'].split('\n')
                    if l != '' ]

            while len(split_collect) > 0 and 'user_results' not in results_files:
                last_line_of_collect = split_collect.pop()

                if os.path.isabs(last_line_of_collect):
                    last_line_of_collect = os.path.relpath(last_line_of_collect, '/')

                full_result_filename_candidates = [
                    # relative to root
                    os.path.join(settings.ROOT,
                    settings.BENCHMARK_MOUNT,
                    last_line_of_collect),
                    # relative to work dir
                    os.path.join(settings.ROOT,
                    settings.BENCHMARK_MOUNT,
                    settings.BENCHMARK_WORK_DIR,
                    last_line_of_collect) ]

                for c in full_result_filename_candidates:
                    if os.path.exists(c):
                        results_files['user_results'] = c
                        break

        # JOB SHELF (sync before actually sending!)
        tmpshelf_handle, tmpshelf_path = tempfile.mkstemp()

        with os.fdopen(tmpshelf_handle, 'w') as f:
            json.dump(dict(self.status), f, default=date_json_handler)

        results_files['job_status'] = tmpshelf_path

        return results_files

    def do_x_hooks(self, x):
        for factor in self.factor_objects:
            func = getattr(factor, x)
            timed_method(func())

    def do_prepopulate_hooks(self):
        self.do_x_hooks('prepopulate')

    def do_preboot_hooks(self):
        self.do_x_hooks('preboot')

    def do_presetup_hooks(self):
        self.do_x_hooks('presetup')

    def do_prerun_hooks(self):
        self.do_x_hooks('prerun')

    def do_precollect_hooks(self):
        self.do_x_hooks('precollect')

    @timed_method
    def verify_factor_levels(self):
        for factor in self.factor_objects:
            self.status['{}_verify'.format(factor.name)] = factor.verify()

    @timed_method
    def install(self):
        shutil.move(self.package_file,
                os.path.join(settings.ROOT,
                             settings.BENCHMARK_MOUNT,
                             settings.DATAMILL_DIR,
                             settings.STANDARD_PACKAGE_FILENAME))

        shutil.move(self.configuration_file,
                os.path.join(settings.ROOT,
                             settings.BENCHMARK_MOUNT,
                             settings.DATAMILL_DIR,
                             settings.STANDARD_CONFIGURATION_FILENAME))

    def kernel_version(self):
        return subprocess.check_output("cat /proc/version", shell=True)

    def installed_packages(self):
        return subprocess.check_output("ls -d /var/db/pkg/*/*| cut -f5- -d/", shell=True)

    @timed_method
    def unpack(self, dirname):
        cmd = 'tar xapf {} -C {}'.format(self.package_file, dirname)
        subprocess.check_call(cmd, shell=True)
        self.status['installed_packages'] = self.installed_packages()
        self.status['kernel_version'] = self.kernel_version()

    def do_x(self, x):

        working_dir = os.path.join(settings.ROOT, settings.BENCHMARK_WORK_DIR)
        tmp_str =  os.path.join(working_dir, '{}.sh'.format(x))
        success = False
        output = ""

        if os.path.exists(tmp_str):
            try:
                output = subprocess.check_output('bash {}'.format(tmp_str), cwd=working_dir, stderr=subprocess.STDOUT, shell=True)
                success = True
            except subprocess.CalledProcessError as e:
                output = e.output
                logger.warning("Subprocess exited with: {}".format(e))

        self.status['job_do_{}_success'.format(x)] = success
        self.status['job_do_{}_output'.format(x)] = unicode(output, errors='replace')

        return success

    @timed_method
    def do_setup(self):
        self.do_x('setup')

    @timed_method
    def do_run(self):
        self.do_x('run')

    @timed_method
    def do_collect(self):
        self.do_x('collect') # collect_output will be the user result file
