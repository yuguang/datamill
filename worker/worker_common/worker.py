import json
import os
from status_shelve import shelve
import subprocess
import sys
import time
import uuid
import tempfile

from worker_common import settings, worker_info
from worker_common.boot_loader import BootLoaderFactory
from worker_common.utilities import get_worker_logger, timed_method, date_json_handler
from worker_common.emerge import Emerge
logger = get_worker_logger('worker')

from worker_common.job import Job
from worker_common.filesystem import Filesystem

class Worker:

    def __init__(self, master,
                 status_file=os.path.join(settings.ROOT, settings.DATAMILL_DIR, settings.WORKER_STATUS_FILE),
                 force_uuid=None, fail_safe=False):

        self.master = master
        self.status = shelve.open(status_file)

        if not fail_safe:
            if 'skip_backup' not in self.status:
                self.status['skip_backup'] = False

            # TODO: this revision check is to support workers with old code
            # once all workers have migrated to using revisioned hello files, we can remove it
            if 'hardware_revision' not in self.status:
                self.status['hardware_revision'] = -1
                self.status['hello_version'] = 'pre-historic'

                if 'registered' in self.status:
                    self.create_uuid(self.status['uuid'])
            # END TODO

            # TODO: this revision fix is due to the revision starting at -1 below
            if self.status['hardware_revision'] == -1:
                self.status['hardware_revision'] = 0

            if 'registered' not in self.status: # first boot
                logger.info('first boot')
                self.status['registered'] = False
                self.status['pending_results'] = False
                self.status['skip_backup'] = False
                self.status['hardware_revision'] = 0
                self.status['uuid'] = self.create_uuid(force_uuid)
                self.make_hello_file()
            elif self.hardware_changed():
                logger.info('hardware changed, remaking hello file')
                self.status['registered'] = False
                self.status['hardware_revision'] = self.status['hardware_revision'] + 1
                self.make_hello_file()
            elif self.hello_file_needs_update():
                logger.info('hello file needs update, remaking hello file')
                self.status['registered'] = False
                self.make_hello_file()

        self.boot_loader = BootLoaderFactory(worker_info.get_architecture())
        self.job = None

        self.filesystem = Filesystem(self.status)

        self.filesystem.controller_partition_config.configure_controller_side()

    def status_file_dict(self):
        status_files = {}

        # logs
        status_files['benchmark_log'] = os.path.join(
            settings.ROOT, settings.BENCHMARK_MOUNT, settings.WORKER_LOG_DIR, 'benchmark.log'
        )

        status_files['watchdog_log'] = os.path.join(
            settings.ROOT, settings.BENCHMARK_MOUNT, settings.WORKER_LOG_DIR, 'watchdog.log'
        )

        status_files['benchmark_common_log'] = os.path.join(
            settings.ROOT, settings.BENCHMARK_MOUNT, settings.WORKER_LOG_DIR, 'datamill_common.log'
        )

        status_files['controller_log'] = os.path.join(
            settings.ROOT, settings.WORKER_LOG_DIR, 'controller.log'
        )

        status_files['controller_common_log'] = os.path.join(
            settings.ROOT, settings.WORKER_LOG_DIR, 'datamill_common.log'
        )

        # status shelf
        tmpshelf_handle, tmpshelf_path = tempfile.mkstemp()
        with os.fdopen(tmpshelf_handle, 'w') as f:
            json.dump(dict(self.status), f, default=date_json_handler)

        status_files['worker_status'] = tmpshelf_path

        return status_files

    def clear(self):
        # clears status and any files, makes as if booting for the first time
        # except for the uuid file, which must survive a clear
        self.boot_loader.set_boot_to_controller()

        if os.path.exists(self.hello_file()):
            os.remove(self.hello_file())

        self.status.clear()

    def uuid(self):
        return self.status['uuid']

    def hardware_revision(self):
        return self.status['hardware_revision']

    def hello_version(self):
        return self.status['hello_version']

    def create_uuid(self, force_uuid=None):

        # load / create
        my_uuid = None

        if force_uuid is not None:
            my_uuid = force_uuid
        elif os.path.exists(self.uuid_file()):
            with open(self.uuid_file(), 'r') as infile:
                my_uuid = json.load(infile)
        else:
            my_uuid = str(uuid.uuid4())

        # save
        with open(self.uuid_file(), 'w') as outfile:
            json.dump(my_uuid, outfile)

        return my_uuid

    def make_hello_file(self):
        self.status['hello_version'] = Emerge.installed_package_version('sys-apps/datamill-controller')
        info = worker_info.get_worker_info()

        info.update(worker_info.get_features())
        info['uuid'] = self.status['uuid']
        info['hardware_revision'] = self.status['hardware_revision']
        info['hello_version'] = self.status['hello_version']

        with open(self.hello_file(), 'w') as outfile:
            json.dump(info, outfile)

    def hello_file(self):
        return os.path.join(settings.ROOT, settings.HELLO_FILENAME)

    def uuid_file(self):
        return os.path.join(settings.ROOT, settings.UUID_FILENAME)

    def hardware_changed(self):
        if os.path.exists(self.hello_file()):
            with open(self.hello_file(), 'r') as infile:
                saved_info = json.load(infile)

            return not worker_info.worker_info_matches(saved_info,
                                                       worker_info.get_worker_info())
        else:
            # Can't decide if hardware changed, assume the worst, that it did
            return True

    def backup_needs_update(self):

        if 'backup_version' in self.status:
            return self.status['backup_version'] != \
                Emerge.installed_package_version('sys-apps/datamill-controller')
        else:
            return True

    def hello_file_needs_update(self):

        if 'hello_version' in self.status:
            return self.status['hello_version'] != \
                Emerge.installed_package_version('sys-apps/datamill-controller')
        else:
            return True

    def ip_file(self):
        return os.path.join(settings.ROOT, settings.IP_FILENAME)

    def get_previous_ip(self):
        if os.path.exists(self.ip_file()):
            with open(self.ip_file(), 'r') as infile:
                ip = json.load(infile)['ip']
        elif os.path.exists(self.hello_file()):
            with open(self.hello_file(), 'r') as infile:
                ip = json.load(infile)['ip']
        else:
            ip = '0.0.0.0'

        return ip

    def get_current_ip(self):
        info = worker_info.get_worker_info()
        return info['ip']

    def ip_changed(self):
        return self.get_previous_ip() != self.get_current_ip()

    def make_ip_file(self):
        with open(self.ip_file(), 'w') as outfile:
            json.dump({'ip': self.get_current_ip(), 'uuid': self.uuid()}, outfile)

    def update_ip(self):
        self.make_ip_file()
        #upload ip file to master
        self.master.update_ip(self)

    def is_registered(self):
        return self.status['registered']

    @timed_method
    def register(self):
        self.master.register(self)
        self.status['registered'] = True
        time.sleep(settings.DATAMILL_COLLECTOR_LOOP_SECS*10)

    def has_pending_results(self):
        return self.status['pending_results']

    @timed_method
    def upload_pending_results(self):
        if self.status['pending_results']:

            try:
                self.filesystem.mount_benchmark_partition() # should silently ignore already mounted partition
            except:
                logger.warning('Failed to mount benchmark partition when uploading results. This can happen after DataMill software updates or worker hardware changes.')

            self.job = None # clear possible job object from previous get_job
            self.job = Job.from_results()

            self.master.put_job(self, self.job)

            self.job = None
            self.status['pending_results'] = False

            return True

    def upload_job_status(self):
        self.job = None # clear possible job object from previous get_job
        self.job = Job.from_results()

        self.master.put_job(self, self.job)

        self.job = None

        return True

    @classmethod
    def reboot(cls):
        subprocess.check_call("reboot")
        sys.exit(0)

    @timed_method
    def get_job(self):
        self.job = None
        logger.info("getting job from master")
        self.job = self.master.get_job(self)

        if not self.job:
            return False

        self.job.status['get_time'] = time.time()

        logger.info("getting installed worker version")
        self.job.status['worker_version'] = Emerge.installed_package_version('sys-apps/datamill-controller')
        self.job.status['worker_hardware_revision'] = self.hardware_revision()

        self.status['pending_results'] = True

        logger.info("unmounting benchmark partition")
        self.filesystem.unmount_benchmark_partition()

        logger.info("running prepopulate hooks")
        self.job.do_prepopulate_hooks() # formats partition
        logger.info("mounting benchmark partition")
        self.filesystem.mount_benchmark_partition()
        logger.info("populating benchmark partition")
        self.filesystem.populate_benchmark_partition()
        logger.info("installing job")
        self.job.install()
        logger.info("running preboot hooks")
        self.job.do_preboot_hooks()
        logger.info("configuring bootloader")
        self.boot_loader.set_boot_to_benchmark()

        # Last step, tell the master everything went fine and we are
        # imminently beginning the job
        logger.info("notifying master that the job is proceeding")
        self.master.put_wip(self, self.job)
        return True
