from worker_common.factor import Factor
import os


class fpie(Factor):
    LEVELS = ['on', 'off']

    def presetup(self):
        os.environ['WRAPPER_POSITION_INDEPENDENT'] = self.level

    def system_level(self):
        if 'WRAPPER_POSITION_INDEPENDENT' in os.environ:
            return os.environ['WRAPPER_POSITION_INDEPENDENT']
        else:
            return 'unknown'

    def test_current_level(self):
        self.presetup()
        return self.verify()
