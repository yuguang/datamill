from worker_common.factor import Factor
import os

class env_padding(Factor):
    LEVELS = ['0', '10928', '16392', '5464']

    def prerun(self):
        os.environ['ENV_PADDING'] = 'x' * int(self.level)

    def system_level(self):
        if 'ENV_PADDING' in os.environ:
            return '{}'.format(len(os.environ['ENV_PADDING']))
        else:
            return 'unknown'

    def test_current_level(self):
        self.prerun()
        return self.verify()