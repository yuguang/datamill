from worker_common.factor import Factor
import os

class drop_caches(Factor):
    LEVELS = ['on', 'off']

    PROC_FILE = '/proc/sys/vm/drop_caches'

    def prerun(self):
        if self.level == 'on':
            if os.path.exists(self.PROC_FILE):
                with open(self.PROC_FILE, 'w') as f:
                    f.write('3')

    def system_level(self):

        if os.path.exists(self.PROC_FILE):
            return self.level # if the file's there, we can write to it
        else:
            return 'off' # if not, we can't force a cache drop, so the factor is locked to off

    def test_current_level(self):
        self.prerun()
        return self.verify()
