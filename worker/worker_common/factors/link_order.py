from worker_common.factor import Factor
import os

class link_order(Factor):
    LEVELS = ['alphabetical', 'reverse_alphabetical', 'default']

    def presetup(self):
        os.environ['WRAPPER_LINK_ORDER'] = self.level

    def system_level(self):

        if 'WRAPPER_LINK_ORDER'in os.environ:
            return os.environ['WRAPPER_LINK_ORDER']
        else:
            return 'unknown'

    def test_current_level(self):
        self.presetup()
        return self.verify()