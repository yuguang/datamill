from worker_common.factor import Factor
import os

class autogroup(Factor):
    LEVELS = ['on', 'off']

    PROC_FILE = '/proc/sys/kernel/sched_autogroup_enabled'

    def prerun(self):

        if os.path.exists(self.PROC_FILE):
            with open(self.PROC_FILE, 'w') as f:
                if self.level == 'off':
                    f.write('0')
                else:
                    f.write('1')

    def system_level(self):

        if os.path.exists(self.PROC_FILE):
            with open(self.PROC_FILE) as f:
                value = f.readline().strip()
                if value == '0':
                    return 'off'
                elif value == '1':
                    return 'on'
        else:
            return 'off'

    def test_current_level(self):
        self.prerun()
        return self.verify()
