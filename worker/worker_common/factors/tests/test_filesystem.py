import os, sys
from worker_common.factors import filesystem
import unittest
import tempfile
import shutil
from worker_common import settings

# These need to be imported here to make sure that they are imported
# first (before the filesystem.py factor) to resolve the circular
# dependency
import worker_common
import worker_common.worker

class TestFilesystem(unittest.TestCase):

    def setUp(self):
        self.tempdir = tempfile.mkdtemp()
        settings.ROOT = self.tempdir

    def tearDown(self):
        shutil.rmtree(self.tempdir)

    def test_kernel_supports_current_level(self):
        factor = filesystem.filesystem('ext2')
        self.assertFalse(factor.kernel_supports_current_level())
        factor1 = filesystem.filesystem('btrfs')
        factor2 = filesystem.filesystem('ext3')
        factor3 = filesystem.filesystem('ext4')
        self.assertTrue(any([factor1.kernel_supports_current_level(), factor2.kernel_supports_current_level(), factor3.kernel_supports_current_level()]))

    test_kernel_supports_current_level.local_testable = True

    def test_verify_test_factor(self):
        factor = filesystem.filesystem('exoticfs')
        self.assertFalse(factor.verify_test_factor())

    test_verify_test_factor.local_testable = True

    def test_prepopulate(self):
        controller_fstab = os.path.join(settings.ROOT, 'etc/fstab')
        os.makedirs(os.path.dirname(controller_fstab))

        with open(controller_fstab, 'w') as f:
            f.write("""
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
/dev/sda1 /boot               ext4   defaults,noatime 0       1
/dev/sda3 {root}           ext4   defaults,noatime        0       2
/dev/sda4 {benchmark_mount}           ext4   defaults,noatime        0       2
            """.format(root=settings.ROOT, benchmark_mount=os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT)))

        factor = filesystem.filesystem('ext3')
        factor.prepopulate()

        with open(controller_fstab, 'r') as f:
            self.assertEqual('\n# <file system> <mount point>   <type>  <options>       <dump>  <pass>\n/dev/sda1 /boot               ext4   defaults,noatime 0       1\n/dev/sda3 {root}           ext4   defaults,noatime        0       2\n/dev/sda4\t{benchmark_mount}\text3\tdefaults,noatime\t0\t2\t\n            '.format(root=settings.ROOT, benchmark_mount=os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT)),

            f.read())

    def test_preboot(self):
        benchmark_fstab = os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, 'etc/fstab')

        os.makedirs(os.path.dirname(benchmark_fstab))

        with open(benchmark_fstab, 'w') as f:
            f.write("""
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
/dev/sda1 /boot               ext4   defaults,noatime 0       1
/dev/sda4 {root}           ext4   defaults,noatime        0       2
            """.format(root=settings.ROOT))

        factor = filesystem.filesystem('btrfs')
        factor.preboot()

        with open(benchmark_fstab, 'r') as f:
            self.assertEqual('\n# <file system> <mount point>   <type>  <options>       <dump>  <pass>\n/dev/sda1 /boot               ext4   defaults,noatime 0       1\n/dev/sda4 {root}           ext4   defaults,noatime        0       2\n            '.format(root=settings.ROOT),
                             f.read())

    test_preboot.local_testable = True

if __name__ == '__main__':
    unittest.main()
