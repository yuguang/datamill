import os, sys
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
sys.path.append(parentdir)
from worker_common.factors import opt_flag, freq_scaling, address_randomization, autogroup, drop_caches, filesystem, swap, sys_time
import unittest, datetime
from worker_common import settings

class TestOptFlag(unittest.TestCase):
    def test_verify(self):
        opt = opt_flag.opt_flag('03')
        self.assertEqual(opt.verify(), False)
        opt = opt_flag.opt_flag('-O3')
        opt.presetup()
        self.assertEquals(os.environ['WRAPPER_OPT_FLAG'], '-O3')

class TestSwap(unittest.TestCase):
    def test_run(self):
        factor = swap.swap('off')
        factor.prerun()
        self.assertEquals(factor.system_level(), 'off')
#
# class TestFreqScaling(unittest.TestCase):
#     def test_verify(self):
#         factor = freq_scaling.freq_scaling('on')
#         factor.prerun()
#         self.assertEquals(factor.system_level(), 'on')

class TestAddressRandomization(unittest.TestCase):
    def test_verify(self):
        factor = address_randomization.address_randomization('on')
        factor.prerun()
        self.assertEquals(factor.system_level(), 'on')

class TestAutogroup(unittest.TestCase):
    def test_verify(self):
        factor = autogroup.autogroup('on')
        factor.prerun()
        self.assertEquals(factor.system_level(), 'on')

class TestDropCaches(unittest.TestCase):
    def test_verify(self):
        factor = drop_caches.drop_caches('on')
        factor.prerun()
        self.assertEquals(factor.system_level(), 'on')

class TestSysTime(unittest.TestCase):
    def test_verify(self):
        factor = sys_time.sys_time('on')
        factor.prerun()
        self.assertEquals(datetime.datetime.now().hour, 14)
        factor.precollect()
        self.assertNotEqual(datetime.datetime.now().hour, 14)

if __name__ == '__main__':
    unittest.main()