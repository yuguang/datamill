from worker_common.factor import Factor
import subprocess
import tempfile
import os

class compiler(Factor):
    LEVELS = ['llvm', 'gcc']

    def prerun(self):
        os.environ['WRAPPER_CC'] = self.level
        # Default to our gcc for sane makefiles and for emerge
        os.environ['CC'] = '/datamill/compiler_wrapper/bin/gcc'

        # Make our compiler wrappers appear first in the path, this
        # takes care of things that call gcc or clang directly.
        os.environ['PATH'] = '{compiler_wrapper_path}:{current_path}'.format(
            compiler_wrapper_path = '/datamill/compiler_wrapper/bin',
            current_path = os.environ['PATH']
        )

    def system_level(self):
        return os.environ.get('WRAPPER_CC', 'unknown')

    def test_current_level(self, optimization_level=None):
        fd, path = tempfile.mkstemp('.c')
        os.write(fd, """
                    #include <stdio.h>

                    int main() {
                        printf("hello, world");
                        return 0;
                    }
                    """)
        os.close(fd)
        compiler_map = {'gcc': 'gcc', 'llvm': 'clang'}
        subprocess.check_call(filter(None, [compiler_map[self.level], optimization_level, '-o', '/tmp/hello', path]))
        output = subprocess.check_output('/tmp/hello', shell=True)
        return output == "hello, world"
