from worker_common.factor import Factor
import subprocess
import os

from worker_common.utilities import get_worker_logger
logger = get_worker_logger('swap')

class swap(Factor):
    LEVELS = ['on', 'off']

    PROC_FILE = '/proc/swaps'

    def prerun(self):

        cmd = None
        if self.level == 'off':
            cmd = 'swapoff'
        elif self.level == 'on':
            cmd = 'swapon'

        if cmd is not None:
            try:
                subprocess.check_call('{} -a'.format(cmd), shell=True)
            except Exception, e:
                logger.error('could not set swap level, ignoring: {}'.format(e))

    def system_level(self):
        if os.path.exists(self.PROC_FILE):
            with open(self.PROC_FILE) as f:
                if len(list(f)) == 1:
                    return 'off'
                else:
                    return 'on'
        else:
            return 'off'

    def test_current_level(self):
        self.prerun()
        return self.verify()
