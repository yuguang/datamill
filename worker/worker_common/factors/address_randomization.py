from worker_common.factor import Factor
import os

class address_randomization(Factor):
    LEVELS = ['on', 'off']

    levels = ['on', 'off']

    # consider a third level http://cyruslab.net/2011/06/12/randomize_va_space-values/
    PROC_FILE = '/proc/sys/kernel/randomize_va_space'

    def prerun(self):
        if os.path.exists(self.PROC_FILE):
            with open(self.PROC_FILE, 'w') as f:
                if self.level == 'off':
                    f.write('0')
                else:
                    f.write('2')

    def system_level(self):

        if os.path.exists(self.PROC_FILE):
            with open(self.PROC_FILE) as f:
                value = f.readline().strip()
                if value == '0':
                    return 'off'
                elif value == '2':
                    return 'on'
        else:
            return 'off'

    def test_current_level(self):
        self.prerun()
        return self.verify()
