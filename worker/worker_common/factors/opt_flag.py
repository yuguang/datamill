import os
from worker_common.factors.compiler import compiler
from worker_common.factor import Factor

class opt_flag(Factor):
    LEVELS = ['-O1', '-O3', '-Os', '-O0', '-O2']

    def presetup(self):
        os.environ['WRAPPER_OPT_FLAG'] = self.level

    def system_level(self):

        if 'WRAPPER_OPT_FLAG' in os.environ:
            return os.environ['WRAPPER_OPT_FLAG']
        else:
            return 'unknown'

    def compile(self, compiler_level):
        try:
            return compiler(compiler_level).test_current_level(self.level)
        except:
            return False

    def test_current_level(self):
        return any(map(self.compile, compiler.LEVELS))