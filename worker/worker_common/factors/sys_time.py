from worker_common.factor import Factor
import datetime

def set_time(t):
    import ctypes
    import ctypes.util
    import time

    time_tuple = (
        t.year,
        t.month,
        t.day,
        t.hour,
        t.minute,
        t.second
    )

    # /usr/include/linux/time.h:
    #
    # define CLOCK_REALTIME                     0
    CLOCK_REALTIME = 0

    # /usr/include/time.h
    #
    # struct timespec
    #  {
    #    __time_t tv_sec;            /* Seconds.  */
    #    long int tv_nsec;           /* Nanoseconds.  */
    #  };
    class timespec(ctypes.Structure):
        _fields_ = [("tv_sec", ctypes.c_long),
                    ("tv_nsec", ctypes.c_long)]

    librt = ctypes.CDLL(ctypes.util.find_library("rt"))

    ts = timespec()
    ts.tv_sec = int( time.mktime( datetime.datetime( *time_tuple[:6]).timetuple() ) )

    # http://linux.die.net/man/3/clock_settime
    librt.clock_settime(CLOCK_REALTIME, ctypes.byref(ts))

class sys_time(Factor):
    LEVELS = ['on', 'off']

    def prerun(self):
        if self.level == 'off':
            return
        # save current time
        self.prerun_time = datetime.datetime.now()
        # set time to 2pm
        set_time(self.prerun_time.replace(hour=14))

    def precollect(self):
        if self.level == 'off':
            return
        # subtract current time from 2pm
        delta = datetime.datetime.now() - self.prerun_time.replace(hour=14)
        # add to saved time
        set_time(self.prerun_time + delta)

    def system_level(self):
        # this is an exception
        # ideally check the current time with a server and return on if it is off by more than a minute
        return self.level

    def test_current_level(self):
        if self.level == 'on':
            self.prerun()
            time = datetime.datetime.now()
            self.precollect()
            return time.hour == 14
        return 'off'