import os
import re

from worker_common import settings
from worker_common.factor import Factor
from worker_common.worker_info import get_architecture
from worker_common.utilities import get_worker_logger
from worker_common.benchmark_partition_config import BenchmarkPartitionConfig
import subprocess

logger = get_worker_logger('filesystem')

class filesystem(Factor):
    LEVELS = ['ext4', 'btrfs', 'ext3', 'reiserfs', 'jfs', 'xfs']
    def __init__(self, level):
        super(filesystem, self).__init__(level)
        self.benchmark_mount  = os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT)
        self.controller_fstab = os.path.join(settings.ROOT, 'etc/fstab')
        self.benchmark_fstab  = os.path.join(self.benchmark_mount, 'etc/fstab')

    def prepopulate(self):
        # TODO: move into worker get filesystem info

        architecture = get_architecture()

        # TODO: fix this
        # Hard code ARM
        extra_parm = ''
        if re.search('^arm', architecture) and self.level in ['ext3', 'ext4']:
            extra_parm = '-N 512256'

        if self.level in ['ext3', 'ext4']:
            extra_parm += ' -FF'
        elif self.level in ['btrfs', 'reiserfs', 'jfs', 'xfs']:
            extra_parm += ' -f'

        # Format
        benchmark_device = BenchmarkPartitionConfig.get_device_from_mountpoint(
            fstab      = self.controller_fstab,
            mountpoint = self.benchmark_mount
        )

        try:
            subprocess.check_call('mkfs.{} {} {}'.format(self.level, extra_parm, benchmark_device), shell=True)
        except Exception, e:
            logger.error('could not format partition, very bad: {}'.format(e))
            raise

        # Edit controller fstab
        with open(self.controller_fstab, 'r') as f:
            controller_fstab_contents = f.readlines()

        for i in xrange(len(controller_fstab_contents)):
            cols = re.split('\s+', controller_fstab_contents[i])
            if cols[1] == self.benchmark_mount:
                cols[2] = self.level # change benchmark partition filesystem on controller side
                controller_fstab_contents[i] = "\t".join(cols) + "\n"

        with open(self.controller_fstab, 'w') as f:
            f.writelines(controller_fstab_contents)

    def preboot(self):
        """This assumes that the fstabs as they stand point to the right
        places, they just need corrected filesystem specs."""

        # Edit benchmark fstab
        with open(self.benchmark_fstab, 'r') as f:
            benchmark_fstab_contents = f.readlines()

        for i in xrange(len(benchmark_fstab_contents)):
            cols = re.split('\s+', benchmark_fstab_contents[i])
            if cols[1] == '/':
                cols[2] = self.level # change benchmark partition filesystem on benchmark side
                benchmark_fstab_contents[i] = "\t".join(cols) + "\n"

        with open(self.benchmark_fstab, 'w') as f:
            f.writelines(benchmark_fstab_contents)

    def system_level(self):
        mount_info = subprocess.check_output('mount', shell=True, stderr=subprocess.STDOUT).decode('utf-8').split('\n')

        for line in mount_info:
            if len(line.strip()):
                mount_point = line.split(' ')[2]
                if mount_point == '/':
                    filesystem = line.split(' ')[4]
                    if filesystem != 'rootfs':
                        return filesystem

        return 'unknown'

    def verify_test_factor(self):
        PROC_KERNEL_CONFIG = '/proc/config.gz'
        BUILD_KERNEL_CONFIG = '/usr/src/linux/.config'
        output = ''
        if os.path.isfile(PROC_KERNEL_CONFIG):
            output = subprocess.check_output('zcat {}'.format(PROC_KERNEL_CONFIG), shell=True)
        elif os.path.isfile(BUILD_KERNEL_CONFIG):
            output = subprocess.check_output('cat {}'.format(BUILD_KERNEL_CONFIG), shell=True)
        if output.find('CONFIG_{}_FS=y'.format(self.level.upper())) == -1 and not self.kernel_supports_current_level():
            return False
        return self.test_file_operations()

    def kernel_supports_current_level(self):
        return self.system_level() == self.level

    def test_file_operations(self):
        benchmark_device = BenchmarkPartitionConfig.get_device_from_mountpoint(
            fstab      = self.controller_fstab,
            mountpoint = self.benchmark_mount
        )
        subprocess.check_call('mount {} {}'.format(benchmark_device, self.benchmark_mount), shell=True)
        subprocess.check_call('echo "firstfile" > {}/copyme'.format(self.benchmark_mount), shell=True)
        subprocess.check_call('cp {}/copyme {}/copiedme'.format(self.benchmark_mount, self.benchmark_mount), shell=True)
        output = subprocess.check_output('cat {}/copiedme'.format(self.benchmark_mount), shell=True)
        subprocess.check_call('umount {}'.format(benchmark_device), shell=True)
        if output == 'firstfile\n':
            return True

    def test_current_level(self):
        self.prepopulate()
        return self.verify_test_factor()
