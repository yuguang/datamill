from worker_common.factor import Factor
import subprocess
import os

from worker_common.utilities import get_worker_logger
logger = get_worker_logger('freq_scaling')

class freq_scaling(Factor):
    LEVELS = ['on', 'off']

    PROC_FILE = '/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor'

    def prerun(self):

        gov = 'performance'
        if self.level == 'on':
            gov = 'ondemand'
        elif self.level == 'off':
            gov = 'performance'

        try:
            if os.path.isfile('/sys/devices/system/cpu/cpu0') and self.level == 'off':
                subprocess.check_call('for c in $(ls -d /sys/devices/system/cpu/cpu[0-9]*); do echo {} >$c/cpufreq/scaling_governor; done'.format(gov), shell=True)
        except Exception, e:
            logger.error('could not set freq scaling, ignoring: {}'.format(e))

    def system_level(self):

        if os.path.exists(self.PROC_FILE):
            with open(self.PROC_FILE) as f:
                value = f.readline().strip()
                if value == 'performance':
                    return 'off'
                else: # interactive or ondemand
                    return 'on'
        else:
            return 'off' # if proc file doesn't exist, assume we don't support freq scaling, therefore it is off

    def test_current_level(self):
        self.prerun()
        return self.verify()
