
import shelve as old_shelve
import UserDict

class shelve(UserDict.DictMixin):

    def __init__(self, *args, **kwargs):
        self._shelve = old_shelve.open(*args, **kwargs)

    def __setitem__(self, index, value):
        self._shelve[index] = value
        self._shelve.sync()

    def __getitem__(self, index):
        return self._shelve[index]

    def __delitem__(self, index):
        del self._shelve[index]
        self._shelve.sync()

    def keys(self):
        return self._shelve.keys()

    def clear(self):
        self._shelve.clear()

    def merge(self, status_shelve, clobber=True):

        if clobber:
            temp_dict = dict(self._shelve.items() + status_shelve.items())
        else:
            temp_dict = dict(status_shelve.items() + self._shelve.items())

        for key, item in temp_dict.iteritems():
            self._shelve[key] = item

    def sync(self):
        self._shelve.sync()

    def close(self):
        self._shelve.close()

    @classmethod
    def open(cls, *args, **kwargs):
        return cls(*args, **kwargs)
