
# FACTOR UTILITIES

from worker_common import factors
from worker_common.factors import *

factor_list = lambda: factors.__all__

def make_factor(name, parameter):
    
    if name not in factor_list():
        return None

    inst_str = '{}.{}("{}")'.format(name, name, parameter)
    return eval(inst_str)

def factor_levels(name):
    inst_str = '{}.{}'.format(name, name)
    return eval(inst_str).LEVELS

def levels_supported(factor_name):
    valid_levels = []
    # loop through levels for a factor
    for level in get_levels(factor_name):
        # initialize factor with level
        factor = make_factor(factor_name, level)
        # call test function which returns true or false
        if factor.current_level_available():
            valid_levels.append(level)
    return valid_levels

def get_levels(factor_name):
    return factor_levels(factor_name)

def get_features():
    features = {}
    # for every factor name
    for factor_name in factor_list():
        features[factor_name] = levels_supported(factor_name)
    return features
