from worker_common.utilities import get_worker_logger
logger = get_worker_logger('factor')

__metaclass__ = type

class Factor:
    def __init__(self, level):
        self.level = level
        self.name = self.__class__.__name__

    def system_level(self):
        return None

    def verify(self):
        if self.system_level() == self.level:
            logger.info('%s factor verification succeed' % self.name.capitalize())
            return True
        else:
            logger.error('%s factor verification failed' % self.name.capitalize())
            return False

    def prepopulate(self):
        pass

    def preboot(self):
        pass

    # before compiling
    def presetup(self):
        pass

    # before running experiment
    def prerun(self):
        pass

    def precollect(self):
        pass

    def test_current_level(self):
        pass

    def current_level_available(self):
        try:
            # call test function which relegates to a pre* method
            return self.test_current_level()
        except:
            return False
