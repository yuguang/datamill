import os
import re
import stat
import shutil
import settings
import subprocess
from worker_common.emerge import Emerge
from worker_common.utilities import timed_method, get_worker_logger
from worker_common.benchmark_partition_config import BenchmarkPartitionConfig
from worker_common.controller_partition_config import ControllerPartitionConfig
from utilities import SingletonErrorLogFile

logger = get_worker_logger('worker')

class Filesystem:
    """
    This is the filesystem object, workers have filesystems.
    """

    def __init__(self, worker_status):
        """
        worker_status: the worker status_shelve object (self.status)
        """

        self.status = worker_status
        self.benchmark_partition_config = BenchmarkPartitionConfig(self.status)
        self.controller_partition_config = ControllerPartitionConfig(self.status)

    def disk_usage(self):
        disk_usage = 0
        for line in subprocess.check_output('df', shell=True).split('\n'):
            # get the percentage disk usage
            if len(line.strip()):
                mount_point = re.split('\s+', line)[5]
                if mount_point == '/':
                    disk_usage = int(re.split('\s+', line)[4].replace('%', ''))
        return disk_usage

    @timed_method
    def untar_backup(self):
        if self.status['skip_backup']:
            # copy over files from root partitions that don't have enough space
            copy_directories = 'bin boot etc home lib lib64 lib32 media opt root run sbin tmp var usr uboot'.split(' ')
            for d in copy_directories:
                source = os.path.join(settings.ROOT, d)
                if os.path.isdir(source):
                    subprocess.check_call(['cp', '-ar', source, os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, d)])
            shutil.rmtree(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, 'etc/local.d'))
            os.mkdir(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, 'etc/local.d'))
        else:
            # This uses the system's tar utility for two reasons:

            # 1. Speed, might as well use the fastest implementation we can.
            #
            # 2. Memory, the pure python version (tarfile) stores metadata
            #    for each file added/removed. When your tarball includes an
            #    entire operating system, this will result in a huge memory
            #    allocation. Since Python will never give the OS back the
            #    memory it allocates for the untarring process, forked
            #    processes are starved on small/old systems (like the
            #    raspberry pi or beagle board).

            backup_tarball = os.path.join(settings.ROOT, settings.BENCHMARK_BACKUP)
            logger.info("Untarring backup tarball {}".format(backup_tarball))
            lines = subprocess.check_output("tar xapf {tarball} -C {target_path}"
                                            .format(tarball=backup_tarball,
                                                    target_path=os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT)),
                                            stderr=SingletonErrorLogFile(), shell=True)
            logger.info("tar returned: {}".format(lines))

    @timed_method
    def create_untarrable_files(self):
        # Since the tarballs that we use as backups for the benchmark
        # side are created from a running system, there are a few
        # places t hat are not safe to archive, and we need to make
        # them after untarring our backup.
        untarrable_directories = ['proc', 'sys', 'dev']

        for d in untarrable_directories:
            os.makedirs(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, d))

        # Create device nodes not managed by the kernel
        os.mknod(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, 'dev', 'console'),
                (0666 | stat.S_IFCHR), os.makedev(0,0))
        os.mknod(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, 'dev', 'null'),
                (0666 | stat.S_IFCHR), os.makedev(1,3))
        os.mknod(os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, 'dev', 'zero'),
                (0666 | stat.S_IFCHR), os.makedev(1,5))

    @timed_method
    def create_backup(self, root_dir = None, blacklist = ['proc', 'sys', 'dev', 'datamill', 'etc/local.d', 'usr/portage/distfiles', 'usr/portage/packages']):

        if self.disk_usage() > settings.BACKUP_INSUFFICIENT_SPACE_PERCENT:
            self.status['skip_backup'] = True
            return

        # Do this here instead of in the function parameters because
        # otherwise it caches settings.ROOT before we change it in
        # tests.
        if root_dir is None:
            root_dir = settings.ROOT

        # The temporary tarball must be somewhere in the blacklist,
        # otherwise tar will try and tar the tarball that it's currently
        # tarring.
        tar_temp_dir = os.path.dirname(os.path.join(settings.ROOT, settings.BENCHMARK_BACKUP))
        tar_temp = os.path.join(tar_temp_dir, 'unfinished.tar.gz')
        backup_tarball = os.path.join(settings.ROOT, settings.BENCHMARK_BACKUP)

        # Explicitly exclude the tarball we're creating, remove potential leading /
        if tar_temp.startswith('/'):
            tar_temp_exclude = tar_temp[1:]
        else:
            tar_temp_exclude = tar_temp

        excludes = " ".join(map(lambda pattern: "--exclude='{pattern}'"\
                                .format(pattern=pattern),
                                blacklist)) \
            + " --exclude='{pattern}'".format(pattern=tar_temp_exclude)

        tar_script="""
        tar caf {tarball} * --anchored {excludes}
        """.format(
            tarball=tar_temp,
            excludes=excludes
        )

        if not os.path.exists(tar_temp_dir):
            os.makedirs(tar_temp_dir)

        subprocess.check_call(tar_script, cwd=root_dir, shell=True)

        shutil.move(tar_temp, backup_tarball)

        self.status['backup_version'] = Emerge.installed_package_version('sys-apps/datamill-controller')

    @timed_method
    def populate_benchmark_partition(self):
        """This should untar the benchmark backup and install datamill to the
        disk.

        THIS FUNCTION ASSUMES THE DISK HAS BEEN FORMATTED

        Raises exception on failure

        """

        self.untar_backup()
        self.create_untarrable_files()
        self.benchmark_partition_config.configure_benchmark_side()
        Emerge.update_benchmark_package()

    @classmethod
    def mount_benchmark_partition(cls):

        mtab = subprocess.check_output("mount",
                                        stderr=SingletonErrorLogFile(), shell=True)

        if re.search(settings.BENCHMARK_MOUNT, mtab, re.MULTILINE):
            return
        else:
            subprocess.check_call("mount {}".format(os.path.join(settings.ROOT,
                settings.BENCHMARK_MOUNT)), stderr=SingletonErrorLogFile(), shell=True)

    @classmethod
    def mount_boot_partition(cls):
        """Mount the boot partition, should be robust to it already being mounted"""

        mtab = subprocess.check_output("mount",
                                        stderr=SingletonErrorLogFile(), shell=True)

        if '/boot' in  mtab:
            return
        else:
            subprocess.check_call("mount /boot",
                                  stderr=SingletonErrorLogFile(), shell=True)

    @classmethod
    def unmount_benchmark_partition(cls):

        benchmark_device = None
        with open('/etc/mtab', 'r') as mtab:
            for line in mtab:
                if re.search(r'\S+{}'.format(settings.BENCHMARK_MOUNT), line):
                    cols = re.split(r'\s+', line)
                    benchmark_device = cols[0]

        if not benchmark_device:
            # If we didn't find the device, nothing to do
            return
        else:
            subprocess.check_call("umount {device}".format(
                device = benchmark_device
            ), stderr=SingletonErrorLogFile(), shell=True)
