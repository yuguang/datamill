import os
import re
import settings

from worker_common.portage_config import PortageConfig
from worker_common.utilities import timed_method, get_worker_logger
logger = get_worker_logger('worker')

class BenchmarkPartitionConfig:
    """This class represents the /etc directory of the worker node,
    including the benchmark side's version of this directory.

    Anything to do with editing or querying the /etc directories is
    included in this class.

    """

    def __init__(self, worker_status):
        """
        worker_status: the worker status_shelve object (self.status)
        """

        self.status = worker_status
        self.portage_config = PortageConfig(worker_status,
                                            root = os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT),
                                            portage_config = settings.BENCHMARK_PORTAGE_CONFIG)

    @timed_method
    def configure_benchmark_fstab(self, fstab=None):
        """Change the fstab based on what is mounted at settings.BENCHMARK_MOUNT so
        that the benchmark partition will be mounted as root.

        Performs the following transformation:

           <controller-partition> / ...
           <benchmark-partition>  /mnt/benchmark ...

           --- becomes ---

           <benchmark-partition> / ...

        Raises exception on failure.

        """

        # do this every call so that it doesn't get hardcoded at import time
        if fstab is None:
            fstab = os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, 'etc', 'fstab')

        controller_partition = self.get_device_from_mountpoint(fstab=fstab, mountpoint='/')
        if not controller_partition:
            raise OSError("Misconfigured fstab, no root partition listed!")

        with open(fstab, 'r') as f:
            fstab_lines = f.readlines()

        # Remove controller root entry
        old_fstab_length = len(fstab_lines)
        fstab_lines = filter(lambda line: not re.match(controller_partition, line), fstab_lines)
        if not old_fstab_length == len(fstab_lines) + 1:
            raise OSError('fstab of controller side had no root entry??')

        # Edit benchmark entry to point to root
        found = False
        for i in xrange(len(fstab_lines)):
            cols = re.split(r'\s+', fstab_lines[i])
            if cols[1].strip() == os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT):
                found = True
                cols = re.split(r'\s+', fstab_lines[i])

                cols[1] = '/'
                cols[3] = 'noatime'
                cols[4] = '0'
                cols[5] = '1'
                fstab_lines[i] = "\t".join(cols) + "\n"

        if not found:
            raise OSError('fstab of controller side had no benchmark entry??')

        with open(fstab, 'w') as f:
            f.writelines(fstab_lines)

    @timed_method
    def configure_benchmark_profile(self, fprofile=None):
        """Cosmetic change for ease of debugging for worker maintainer.

        Changes prompt to indicate what partition is currently booted.

        Fails silently (on purpose).
        """

        # do this every call so that it doesn't get hardcoded at import time
        if not fprofile:
            fprofile = os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT, 'etc', 'profile')

        try:
            with open(fprofile, 'r') as f:
                profile_lines = f.readlines()

            with open(fprofile, 'w') as f:
                for line in profile_lines:
                    if re.match(r'.*controller.*', line):
                        line = re.sub(r'(.*)controller(.*)', r'\1benchmark\2', line)
                    f.write(line)
        except:
            pass

    @timed_method
    def configure_benchmark_side(self):
        """
        This function fixes the following files:

        1. /etc/fstab
        2. /etc/profile
        3. /etc/portage/package.keywords
        """
        self.configure_benchmark_fstab()
        self.configure_benchmark_profile()

        self.portage_config.configure_portage()

    @classmethod
    def get_device_from_mountpoint(self, fstab='/etc/fstab',
                                   mountpoint=os.path.join(settings.ROOT, settings.BENCHMARK_MOUNT)):

        with open(fstab, 'r') as f:
            fstab_lines = f.readlines()

        benchmark_device = None
        for line in fstab_lines:
            cols = re.split('\s+', line)
            if cols[1].strip() == mountpoint:
                benchmark_device = cols[0].strip()
                break

        if benchmark_device is None:
            raise OSError("{} not listed in /etc/fstab, there must be a mistake.".format(mountpoint))

        # Is the device UUID?
        if re.search('UUID', benchmark_device):
            benchmark_device = os.path.join('/dev/disk/by-uuid',
                                            benchmark_device.split("=")[1].strip())

        if not os.path.exists(benchmark_device):
            raise OSError("Device not found, {} does not exist!".format(benchmark_device))

        return benchmark_device
