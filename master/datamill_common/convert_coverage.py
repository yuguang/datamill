from datamill_website.models import *
from datamill_daemons.scheduler import Scheduler


experiment_list = Experiment.objects.all()

for experiment in experiment_list:
    try:
        cov = eval(experiment.coverage_spec)
        cons = eval(experiment.constraint_spec)
        scope = eval(experiment.scope_spec)
        cov = Scheduler.relative_to_absolute_coverage(cov, cons, scope)
        new_cov = {}
        for factor in cov:
            new_cov[factor] = {
                'count': cov.get(factor, 1),
                'min': 0,
                'max': 0
            }

        experiment.coverage_spec = str(new_cov)
        experiment.save()
    except:
        pass