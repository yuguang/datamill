from django.core.files import File
from datamill_website.models import Experiment, Worker, Job, Package
from django.conf import settings
from md5hash import md5hash
import os.path
import shutil
import logging

####################
# PACKAGE
####################

def create_package_file(p):
    # puts a valid package file where Package p thinks it should be
    package_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_results_file.tar.gz')
    target_package_file = os.path.join(settings.MEDIA_ROOT, p.file.name)

    if not os.path.exists(os.path.dirname(target_package_file)):
        os.makedirs(os.path.dirname(target_package_file))

    shutil.copy(package_file, target_package_file)

def delete_package_file(p):
    target_package_file = os.path.join(settings.MEDIA_ROOT, p.file.name)
    os.remove(target_package_file)

####################
# WORKER
####################

def create_worker_dir(w):
    if not os.path.exists(w.get_dir()):
        os.mkdir(w.get_dir())

def create_hello_file():
    if not os.path.exists(settings.DATAMILL_HELLO_ROOT):
        os.mkdir(settings.DATAMILL_HELLO_ROOT)

    hello_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_hello_file.json')
    target_hello_file = os.path.join(settings.DATAMILL_HELLO_ROOT, 'test_hello.json')
    shutil.copy(hello_file, target_hello_file)
    md5hash.generate_md5(target_hello_file)

def create_ip_file():
    if not os.path.exists(settings.DATAMILL_IP_ROOT):
        os.mkdir(settings.DATAMILL_IP_ROOT)

    ip_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_ip_file.json')
    target_ip_file = os.path.join(settings.DATAMILL_IP_ROOT, 'test_ip.json')
    shutil.copy(ip_file, target_ip_file)
    md5hash.generate_md5(target_ip_file)

def create_reregistration_hello_file():
    if not os.path.exists(settings.DATAMILL_HELLO_ROOT):
        os.mkdir(settings.DATAMILL_HELLO_ROOT)

    hello_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_reregistration_hello_file.json')
    target_hello_file = os.path.join(settings.DATAMILL_HELLO_ROOT, 'test_rereg_hello.json')
    shutil.copy(hello_file, target_hello_file)
    md5hash.generate_md5(target_hello_file)

####################
# JOB
####################

def create_all_job_files():
    for j in Job.objects.all():
        create_package_file(j.package)
        create_worker_dir(j.worker)
        j.create_outgoing_files()

def create_job_outgoing_files(j):
    # puts valid pkg and config files where Job j thinks they should be

    if not os.path.exists(j.worker.get_dir()):
        os.mkdir(j.worker.get_dir())

    pkg_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_package.tar.gz')
    job_cfg_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_job_config.json')

    shutil.copy(pkg_file, j.outgoing_pkg_filename())
    shutil.copy(job_cfg_file, j.outgoing_config_filename())
    md5hash.generate_md5(j.outgoing_pkg_filename())
    md5hash.generate_md5(j.outgoing_config_filename())

def create_job_incoming_wip_file(j):
    if not os.path.exists(j.worker.get_dir()):
        os.mkdir(j.worker.get_dir())

    open(j.incoming_wip_filename(), 'w').close()

def create_job_incoming_results_files(j):
    # puts valid results files where Job j thinks they should be

    if not os.path.exists(j.worker.get_dir()):
        os.mkdir(j.worker.get_dir())

    done_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_done_file')
    results_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_results_file.tar.gz')
    job_status_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_job_status_file.json')
    worker_status_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_worker_status_file.json')

    shutil.copy(done_file, j.incoming_done_filename())
    shutil.copy(results_file, j.incoming_user_results_filename())
    shutil.copy(job_status_file, j.incoming_job_status_filename())
    shutil.copy(worker_status_file, j.incoming_worker_status_filename())
    md5hash.generate_md5(j.incoming_done_filename())
    md5hash.generate_md5(j.incoming_user_results_filename())
    md5hash.generate_md5(j.incoming_job_status_filename())
    md5hash.generate_md5(j.incoming_worker_status_filename())

def create_job_permanent_results_files(j):
    # puts valid results files where Job j thinks they should be, makes the job be done

    results_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_results_file.tar.gz')
    job_status_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_job_status_file.json')
    worker_status_file = os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_worker_status_file.json')

    shutil.copy(results_file, j.permanent_user_results_filename())
    shutil.copy(job_status_file, j.permanent_job_status_filename())
    shutil.copy(worker_status_file, j.permanent_worker_status_filename())

    j.results_file = j.permanent_user_results_filename()
    j.job_status_file = j.permanent_job_status_filename()
    j.worker_status_file = j.permanent_worker_status_filename()

    j.status = Job.DONE
    j.save()

def delete_job_permanent_results_files(j):
    target_results_file = j.permanent_user_results_filename()
    os.remove(target_results_file)

####################
# GENERAL
####################

def populate_db():

    # DO NOT USE THIS METHOD DIRECTLY!
    # --------------------------------
    # there's a fixture in datamill_website/fixtures/datamill_test_fixture
    # that contains exactly what this method generates (except for putting package files in their locations)
    # what you _should_ do is use the fixture then call the create_[modelname]_files() methods to make 'real'
    # files as needed (it will be unecessary for the majority of tests).

    e1 = Experiment(name='Experiment 1', constraint_spec='{}', coverage_spec='{ "cpu_arch":100, "cpu_count":100 }')
    e2 = Experiment(name='Experiment 2', constraint_spec='{ "cpu_arch":["arm"] }', coverage_spec='{ "cpu_arch":100, "cpu_count":100 }')

    w1 = Worker(uuid='45645013-93dc-4470-9681-7d7066c709ca', ip='192.168.0.1',   hostname='t1', cpu_count=2, cpu_arch='arm', cpu_model='beagle', cpu_flags='MMX', cpu_mhz=1000, ram=100, page_sz=4000)
    w2 = Worker(uuid='569ff803-1a6d-40af-bb7a-ec18684fb6a2', ip='192.168.0.10',  hostname='t2', cpu_count=4, cpu_arch='arm', cpu_model='rasp pi', cpu_flags='SSE MMX', cpu_mhz=2000, ram=200, page_sz=8000)
    w3 = Worker(uuid='75e51308-257d-4c84-bba1-67aaafafa89c', ip='192.168.0.100', hostname='t3', cpu_count=4, cpu_arch='x86', cpu_model='core i7', cpu_flags='SSE', cpu_mhz=3000, ram=1000, page_sz=4000)

    for obj in [ e1, e2, w1, w2, w3 ]:
        obj.save()

    p1 = Package(experiment=e1, file=File(open(os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_package.tar.gz'), 'r'), 'test_package.tar.gz'))
    p2 = Package(experiment=e2, file=File(open(os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_package.tar.gz'), 'r'), 'test_package_2.tar.gz'))

    for obj in [ p1, p2 ]:
        obj.save()

    j1_1 = Job(experiment=e1, package=p1, worker=w1, config_id=1, config='{}', replicate_no=1)
    j1_2 = Job(experiment=e1, package=p1, worker=w1, config_id=1, config='{}', replicate_no=2)
    j1_3 = Job(experiment=e1, package=p1, worker=w1, config_id=1, config='{}', replicate_no=3)

    j2_1 = Job(experiment=e2, package=p2, worker=w2, config_id=1, config='{}', replicate_no=1)
    j2_2 = Job(experiment=e2, package=p2, worker=w3, config_id=2, config='{}', replicate_no=1)

    for obj in [ j1_1, j1_2, j1_3, j2_1, j2_2 ]:
        obj.save()

def delete_all_datamill_files():

    # use this in tearDowns for tests that make files!
    # it'll nuke EVERYTHING that may exist file-wise

    for p in [ settings.DATAMILL_FTP_ROOT,
                settings.DATAMILL_MEDIA_ROOT,
                settings.DATAMILL_RESULTS_ROOT ]:
        try:
            shutil.rmtree(p)
        except OSError:
            pass
        os.makedirs(p) # remake them for the next round
