#!/usr/bin/env bash

VERSION=15.0.2
VIRTUALENV="virtualenv-${VERSION}.tar.gz"

read -r -d '' HELP <<EOF
Usage: setup-virtualenv.sh [options]
Setup a local python environment with virtualenv, and install all
DataMill dependencies. Optionally install only the dependencies with
the existing virtualenv.

    Options:
    -h,--help         Print this help documentation.
    -d,--only-deps    Only install dependencies, not virtualenv.
EOF

# Execute getopt
ARGS=`getopt -o "dh" -l "only-deps,help" \
      -n "setup-virtualenv.sh" -- "$@"`
 
#Bad arguments
if [ $? -ne 0 ];
then
  exit 1
fi
 
eval set -- "$ARGS"
 
while true;
do
  case "$1" in
    -h|--help)
      echo "$HELP"
      exit 0
      shift;;
 
    -d|--only-deps)
      echo "Only installing deps, assuming working virtualenv"
      ONLYDEPS=true
      shift;;

    --)
      shift
      break;;
  esac
done

# Non PYPI install functions
function install_pyglpk() {

    local VERSION=0.3
    local PYGLPK="pyglpk-${VERSION}.tar.bz2"
    
    wget -c "http://tfinley.net/software/pyglpk/"${PYGLPK} -O $PYGLPK

    tar xavf "$PYGLPK"
    pushd "${PYGLPK%%.tar.bz2}" > /dev/null
    if echo "$(uname -m)" | grep "64" > /dev/null; then
        echo "64 bit machine detected, updating ${PYGLPK%%.tar.bz2}/setup.py"
        perl -pi -e "s/-m32/-m64/g" setup.py
    fi
    ../dev-python/bin/python setup.py install
    popd > /dev/null
    rm -r "${PYGLPK%%.tar.bz2}"
    rm "$PYGLPK"
}

# Install Virtualenv in local directory

if [ -z "$ONLYDEPS" ]; then

    echo "Installing virtualenv in $(pwd)"

    curl -O https://pypi.python.org/packages/source/v/virtualenv/"$VIRTUALENV"
    tar xavf "$VIRTUALENV"
    pushd "${VIRTUALENV%%.tar.gz}" > /dev/null
    python2 virtualenv.py --no-site-packages ../dev-python
    popd > /dev/null

    rm -r "${VIRTUALENV%%.tar.gz}"
    rm "${VIRTUALENV}"

fi

# Install all pypi dependencies
echo "Installing PYPI packages"

if ! dev-python/bin/pip install -r dependencies.pip; then
    echo "Pip command failed." 1>&2
    exit 1
fi

# Install dependencies not in pypi
echo "Installing packages not in PYPI"

install_pyglpk;
