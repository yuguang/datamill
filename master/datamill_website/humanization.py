from datamill_website.models import *
from humanization import *
import numpy

from django.utils.translation import ugettext as _

from datetime import datetime, timedelta


def get_unit_name(index):
    times = ["s", "m", "h"]
    return times[index]


def find_time_unit(sec):
    if sec < 60:
        unit_index = 0
    elif sec < 3600:
        unit_index = 1
    else:
        unit_index = 2
    return unit_index


def humanize_worker_times(worker_times, time_field):
    def calculate_percentiles(job, unit_index):
        times = Job.objects.filter(worker__hostname=job['worker__hostname'], experiment__id=job['experiment__id']).values_list(time_field, flat=True)
        times = [time for time in times if time is not None]
        if not len(times):
            return []
        return [humanize_time(min(times), unit_index),
                humanize_time(numpy.percentile(times, 25),unit_index),
                humanize_time(numpy.percentile(times, 50), unit_index),
                humanize_time(numpy.percentile(times, 75), unit_index),
                humanize_time(max(times), unit_index)]
    unit_index = 0
    for time in worker_times:
        index = find_time_unit(time["average"])
        if index > unit_index:
            unit_index = index
    for time in worker_times:
        percentiles = calculate_percentiles(time, unit_index)
        time['percentiles'] = percentiles
        if len(percentiles) > 0 :
            time["unit"] = get_unit_name(unit_index)
            time["average"] = humanize_time(time["average"], unit_index)
    return worker_times


def humanize_time(sec, unit_index):
    if unit_index == 0:
        result = sec
    elif unit_index == 1:
        result = sec/60
    elif unit_index == 2:
        result = sec/3600
    return round(result, 3)

def get_humanized_time(sec):
        if sec < 60:
            sec = round(sec, 3)
            return "{sec}s".format(sec=sec)
        sec = round(sec, 0)
        time = str(timedelta(seconds=sec)).split(":")
        return "{hour}h {min}m {sec}s".format(hour=time[0], min=time[1], sec=time[2])

def humanize_size(bytes):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    if bytes == 0:
        return '0 B'
    i = 0
    while bytes >= 1024 and i < len(suffixes)-1:
        bytes /= 1024.
        i += 1
    f = ('%.2f' % bytes).rstrip('0').rstrip('.')
    return '%s%s' % (f, suffixes[i])

