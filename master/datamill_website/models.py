from django.conf import settings
from django.db import models
from django.db import connection
from django.db.models import Max, Min
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db.models import Q
from datetime import datetime, timedelta
from taggit.managers import TaggableManager
import sys
import os
import os.path
import shutil
import csv
import subprocess
import json
import reversion
from pwd import getpwnam
from grp import getgrnam
import stat
import math
from django.core.files.storage import FileSystemStorage

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User, Group
from django.contrib.sites.models import Site
from django.db import transaction #TODO will commit_on_success will become atomic() in 1.6
from guardian.shortcuts import assign_perm, remove_perm, get_users_with_perms, get_perms_for_model, get_objects_for_group

from md5hash import md5hash

import logging, glob
import pwd
import grp
from django.db.models import signals
from django.core.files import File
from reversion.models import Version
if len(sys.argv) > 1 and sys.argv[1] != None:
    logger = logging.getLogger('{0}.models'.format(sys.argv[1]))
else:
    logger = logging.getLogger('datamill_models')

###########################################
# DATAMILL MODELS:
###########################################

class WorkerManager(models.Manager):
    def available(self):
        return self.exclude(status=Worker.DEAD)

def change_owner_to_apache(filename):
    try:
        apache_uid = pwd.getpwnam('apache').pw_uid
    except KeyError:
        apache_uid = os.getuid() # current process when running without apache

    return os.chown(filename, apache_uid, -1) # leave group id alone

class News(models.Model):
    title = models.CharField(default='Enter title here', max_length=120)
    body = models.TextField(default='')
    day = models.DateField(auto_now_add=True)
    class Meta:
        verbose_name_plural = 'news'

class Worker(models.Model):
    IDLE = 'ID'
    WORKING = 'WR'
    DEAD = 'DE'
    WORKER_STATUS_CHOICES = (
           (IDLE, 'Idle'),
           (WORKING, 'Working'),
           (DEAD, 'Dead'),
           )

    status = models.CharField('status', max_length=2, choices=WORKER_STATUS_CHOICES,
            default=IDLE)
    uuid = models.CharField('UUID', max_length=36, unique=True)
    hello_version = models.CharField('software version', max_length=30)

    last_update = models.DateTimeField('last update to worker information', auto_now_add=True)
    user = models.ForeignKey(User, default=settings.DEFAULT_WORKER_OWNER_ID)

    # non-critical metadata from here on
    ip = models.IPAddressField('IP')
    hostname = models.CharField('hostname', max_length=30)
    # these are not considered factors
    cpu_model = models.CharField('CPU architecture', max_length=128)
    cpu_flags = models.TextField('CPU flags')
    gpu_model = models.CharField('GPU architecture', max_length=128, default='N/A')
    gpu_flags = models.TextField('GPU flags', default='N/A')
    ram_type = models.CharField('RAM type', max_length=128, default='N/A')
    objects = WorkerManager()

    def __unicode__(self):
        return '{} - {} - {}'.format(self.hostname, self.ip, self.uuid)

    # datamill methods
    def current_experiment(self):
        if self.job_set.filter(status=Job.IN_PROGRESS).count() > 1:
            return self.job_set.filter(status=Job.IN_PROGRESS).order_by('-id')[0].experiment
        else:
            return self.job_set.get(status=Job.IN_PROGRESS).experiment

    def job_queue(self):
        return self.job_set.filter(status__in=[ Job.IN_PROGRESS, Job.QUEUED ])

    def get_hardware_level(self, factor_internal_name, date=None):
        if not date:
            return self.workerfeature_set.get(level__factor=Factor.objects.get(internal_name=factor_internal_name)).level.internal_name
        else:
            # fields in the worker model are not hardware factors
            for field in self._meta.fields:
                if factor_internal_name == field.name:
                    raise Exception('{} is not a factor'.format(factor_internal_name))
            if Factor.objects.filter(internal_name=factor_internal_name).exists():
                worker_feature = self.workerfeature_set.get(level__factor=Factor.objects.get(internal_name=factor_internal_name))
                try:
                    # Find the most recent version for a given date:
                    version = reversion.get_for_date(worker_feature, date)
                    # Access the model data stored within the version:
                    version_data = version.field_dict
                    return Level.objects.get(id=version_data['level']).internal_name
                except Version.DoesNotExist:
                    return worker_feature.level.internal_name

    def get_dir(self):
        return os.path.join(settings.DATAMILL_FTP_ROOT, 'worker-{0}'.format(self.uuid))

    def make_dead(self):
        self.status = Worker.DEAD
        pending_jobs = self.job_queue()

        for j in pending_jobs:
            j.fail()

        self.save()
        if os.path.isdir(self.get_dir()):
            shutil.rmtree(self.get_dir())

    def update_status(self):

        if not self.status == Worker.DEAD: # can't come back from the dead

            if self.job_set.filter(status=Job.IN_PROGRESS).exists():
                self.status = Worker.WORKING
            else:
                self.status = Worker.IDLE

            # the following query gets the max update time between this worker's jobs
            # it may return None if the worker hasn't done anything yet
            # in that case, we maintain the existing last_update (i.e., worker creation time)

            candidate_last_update = self.job_set.filter(
                    status__in=[ Job.IN_PROGRESS, Job.DONE, Job.FAILED ]).aggregate(
                            Max('last_update'))['last_update__max']

            if candidate_last_update:
                # in case of rereg, self.last_update will > candidate_last_update and should be retained
                self.last_update = max(self.last_update, candidate_last_update)

            # get the oldest still-queued job (most stale)
            # may return none if there's no pending jobs
            oldest_queue_time = self.job_set.filter(status=Job.QUEUED).aggregate(
                            Min('last_update'))['last_update__min']

            # if I'm expecting to hear from the worker (i.e., jobs are waiting)
            #   if he's not said anything in a long time
            #       then it's dead

            if self.job_set.filter(status__in=[Job.IN_PROGRESS, Job.QUEUED]).exists():
                if not oldest_queue_time: oldest_queue_time = datetime.min # cover when only in_prog exists

                effective_last_update = max(oldest_queue_time, self.last_update)

                if (datetime.now() - effective_last_update).days > settings.DATAMILL_DAYS_UNTIL_WORKER_DEAD:
                    self.make_dead()
                    return

            self.save()

    @reversion.create_revision()
    def fill_from_dict(self, dict):
        # attention: now calls self.save()

        # this method fills in as much as it can, ignoring missing data.
        # if any missing field turns out to be important or if any
        # of the values filled turn out to be bogus we catch it when we
        # call save() on this worker.

        # extraneous fields in the dict are ignored.

        try:
            with transaction.commit_on_success():
                for field in self._meta.fields:
                    if dict.has_key(field.name):
                        setattr(self, field.name, dict[field.name])

                self.save()

                for factor in Factor.objects.all():
                    if dict.has_key(factor.internal_name):
                        if isinstance(dict[factor.internal_name], list):
                            WorkerFeature.objects.filter(worker=self, level__factor__internal_name=factor.internal_name).delete()
                            for level_name in dict[factor.internal_name]:
                                level, created = Level.objects.get_or_create(factor=factor, internal_name=str(level_name))
                                feature, created = WorkerFeature.objects.get_or_create(worker=self, level=level)
                        else:
                            level, created = Level.objects.get_or_create(factor=factor, internal_name=str(dict[factor.internal_name]))
                            # assumes there is only one level per worker and factor pair
                            if WorkerFeature.objects.filter(worker=self, level__factor=level.factor).exists():
                                feature = WorkerFeature.objects.get(worker=self, level__factor=level.factor)
                                feature.level = level
                                feature.save()
                            else:
                                WorkerFeature.objects.create(worker=self, level=level)
                    else:
                        raise Exception('factor is missing in dictionary: {}'.format(factor.internal_name))

        except Exception:
            if Worker.objects.filter(uuid=self.uuid).exists(): # if I was in the db
                self.make_dead()
            raise

    def create_dir(self):
        if not os.path.exists(self.get_dir()):
            os.mkdir(self.get_dir())

            try:
                uid = getpwnam('apache')[2]
                gid = getgrnam('ftp')[2]
                os.chown(self.get_dir(), uid, gid)
                os.chmod(self.get_dir(),
                                stat.S_IRUSR |
                                stat.S_IWUSR |
                                stat.S_IXUSR |
                                stat.S_IRGRP |
                                stat.S_IWGRP |
                                stat.S_IXGRP |
                                stat.S_IXOTH |
                                stat.S_IROTH)
            except KeyError:
                pass # ignore missing ftp user/group for testing
            except OSError:
                pass # ignore missing permissions for testing

    class Meta:
        permissions = (
                ("view_workers", "Can list workers"),
                )

def before_save(sender, instance, *args, **kwargs):
    hostname_count = Worker.objects.filter(hostname=instance.hostname, status__in=[ Worker.WORKING, Worker.IDLE ]).exclude(uuid=instance.uuid).count()
    if hostname_count:
        instance.hostname = "{0} ({1})".format(instance.hostname, str(hostname_count))

signals.pre_save.connect(before_save, sender=Worker)

class Experiment(models.Model):
    CREATED = 'CR'
    SCHEDULING = 'SG'
    SCHEDULED = 'SD'
    IN_PROGRESS = 'PR'
    DONE = 'DN'
    IMPOSSIBLE = 'IM'
    EXP_STATUS_CHOICES = (
           (CREATED, 'Created'),
           (SCHEDULING, 'Scheduling'),
           (SCHEDULED, 'Scheduled'),
           (IN_PROGRESS, 'In progress'),
           (DONE, 'Done'),
           (IMPOSSIBLE, 'Unschedulable'),
           )

    name = models.CharField('name', max_length=30)
    description = models.CharField('description', max_length=750, default='', blank=True)
    submission_date = models.DateTimeField('creation date', auto_now_add=True)
    completion_date = models.DateTimeField('completion date', blank=True, null=True)
    status = models.CharField('status', max_length=2, choices=EXP_STATUS_CHOICES,
            default=CREATED)
    num_software_factors = models.IntegerField(default=0)
    num_hardware_factors = models.IntegerField(default=0)
    replicate_count = models.PositiveSmallIntegerField('number of replicates per job')
    send_confirmation_alert = models.BooleanField('a confirmation alert for this experiment should be sent', default=True)
    deleted = models.BooleanField(default=False)
    run_on_all_workers = models.BooleanField("this experiment's jobs should run on all workers", default=False)
    finished_results_file = models.FileField(upload_to='results', default='settings.MEDIA_ROOT/new_file')
    results_file = models.FilePathField(path=settings.DATAMILL_RESULTS_ROOT,
            match='.*\.tar$', null=True, blank=True)
    tags = TaggableManager()

    def __unicode__(self):
        return self.name

    def get_cpu_architectures(self):
        return set([job.level.internal_name for job in JobConfiguration.objects.filter(job__in=self.job_set.all(), level__internal_name='cpu_arch')])

    def get_time_elapsed(self):
        delta = None
        if self.completion_date:
            delta = self.completion_date - self.submission_date
        else:
            delta = datetime.now() - self.submission_date

        return '{}'.format(delta).rpartition('.')[0]

    @classmethod
    def calculate_priority(cls, jobs_per_worker):

        # MAX_WEEKS = 2
        # MINUTES_PER_JOB = 15
        # MAX_MINUTES = MAX_WEEKS * 7 * 24 * 60
        # MAX_JOBS = MAX_MINUTES / MINUTES_PER_JOB
        # MAX_JOBS = 1344

        prio = int(math.ceil( 0.8 * ( jobs_per_worker ** (1.0/1.5) )))
        return min(99, prio)

    def get_priority(self):
        job_c = self.get_job_count()
        worker_c = self.job_set.distinct('worker').count()

        if worker_c:
            return Experiment.calculate_priority(job_c / worker_c)
        else:
            return None

    def get_job_count(self):
        return self.job_set.count()

    def get_done_job_count(self):
        return self.job_set.filter(status__in=[ Job.DONE ]).count()

    def get_queued_job_count(self):
        return self.job_set.filter(status__in=[ Job.QUEUED ]).count()

    def get_in_progress_job_count(self):
        return self.job_set.filter(status__in=[ Job.IN_PROGRESS ]).count()

    def get_failed_job_count(self):
        return self.job_set.filter(status__in=[ Job.FAILED ]).count()

    def get_cancelled_job_count(self):
        return self.job_set.filter(status__in=[ Job.CANCELLED ]).count()

    def get_completed_job_count(self):
        # completed = done, cancelled or failed
        return self.job_set.filter(status__in=[ Job.DONE, Job.CANCELLED, Job.FAILED ]).count()

    def get_completion_percentage(self):
        job_no = self.job_set.count()

        if job_no == 0:
            return 0.0

        return round(float(self.get_completed_job_count()) / float(job_no) * 100.0, 2)

    def update_status(self):

        if not self.status in [ Experiment.CREATED,
                Experiment.SCHEDULING,
                Experiment.IMPOSSIBLE ]:
            if self.job_set.filter(status__in=[ Job.IN_PROGRESS, Job.QUEUED ]).exists():
                self.status = Experiment.IN_PROGRESS
                self.save()
            else:
                self.status = Experiment.DONE # uploading to google storage depends on experiment being done
                self.make_results_package()
                self.completion_date = datetime.now()
                self.notify_creators_of_completion()
                self.save()

    def notify_creators_of_completion(self):

        if self.send_confirmation_alert: # send alerts only once
            self.send_confirmation_alert = False
            self.save()

            users_with_perms = get_users_with_perms(self, with_group_users=False)
            link_to_experiment = 'http://{}{}'.format(Site.objects.get_current().domain,
                    self.get_absolute_url())

            for u in users_with_perms:
                send_mail('Your DataMill experiment is done! (Experiment ID: {})'.format(self.id),
                '''Hello {},

Looks like your DataMill experiment has no more pending jobs! Go to {} to download your results, retry any failed jobs, or publish your experiment.

Thanks,
The DataMill Team'''.format(u, link_to_experiment),
                        settings.DATAMILL_EMAIL,
                        [ u.email ])

    def get_absolute_url(self):
        return reverse('experiment_detail', args=[self.id])

    def make_results_package(self):
        results_file_list = []
        csv_data = []
        first_iteration = True

        header_array = ['experiment_id', 'job_id', 'results_file', 'package_id',
                        'package_name', 'worker_id', 'config_id', 'replicate_no',
                        'setup_time', 'run_time', 'collect_time']

        hw_factor_names = [f.internal_name for f in Factor.objects.filter(fac_type=Factor.HARDWARE)
                           .order_by('internal_name')]
        sw_factors = Factor.objects.filter(fac_type=Factor.SOFTWARE).order_by('internal_name')
        done_jobs = self.job_set.filter(status=Job.DONE)
        worker_ids = [j.worker.id for j in Job.objects.filter(experiment_id = self.id).order_by('worker__id').distinct('worker__id')]
        workers_in_question = Worker.objects.filter(id__in = worker_ids)

        worker_factor_cache = {}

        for f in hw_factor_names:
            header_array.append('hw_{}'.format(f))

        for w in workers_in_question:
            hw_levels = []
            for f in hw_factor_names:
                hw_levels.append(w.get_hardware_level(f, self.submission_date))

            worker_factor_cache[w.id] = hw_levels

        for j in done_jobs:

            # meta info
            user_results = j.permanent_user_results_filename()
            job_results = j.permanent_job_status_filename()
            results_file_list.append(user_results)
            results_file_list.append(job_results)

            #            exp_id
            tmp_array = [self.id, j.id,
                         os.path.basename(user_results), j.package.id,
                         os.path.basename(j.package.file.name), j.worker.id, j.config_id,
                         j.replicate_no, j.setup_time, j.run_time, j.collect_time]

            # hw info
            tmp_array.extend(worker_factor_cache[j.worker.id])

            # sw info
            # -------
            # this section picks default positions for factors that are
            # not explicitely in the job config dictionary. we do this
            # because factors may be added to datamill between this experiment
            # being scheduled and the results package being generated

            job_config = j.get_configuration()

            for f in sw_factors:

                if f.internal_name in job_config['factors']:
                    tmp_array.append(job_config['factors'][f.internal_name])
                else:
                    tmp_array.append(f.get_default())

                if first_iteration:
                    header_array.append('sw_{}'.format(f.internal_name))

            if first_iteration:
                csv_data.append(header_array)
                first_iteration = False

            csv_data.append(tmp_array)

        index_filename = os.path.join(settings.DATAMILL_RESULTS_ROOT,
                '{}{}'.format(self.id, '_results_index.csv'))

        with open(index_filename, 'w') as results_index:
            my_csvwriter = csv.writer(results_index)
            my_csvwriter.writerows(csv_data)
        change_owner_to_apache(index_filename)

        full_results_filename = os.path.join(settings.DATAMILL_RESULTS_ROOT,
                '{}{}{}'.format('experiment_', self.id, '_results.tar'))

        folder_in_tar_file = 'experiment_{}/'.format(self.id)

        results_dir = os.path.join(settings.DATAMILL_RESULTS_ROOT, folder_in_tar_file)
        if not os.path.isdir(results_dir):
            os.makedirs(results_dir)
        if os.path.isfile(index_filename):
            shutil.move(index_filename, os.path.join(results_dir, os.path.basename(index_filename)))
        for r in [f for f in results_file_list if os.path.isfile(f)]:
            shutil.move(r, os.path.join(results_dir, os.path.basename(r)))

        cwd = os.getcwd()
        os.chdir(settings.DATAMILL_RESULTS_ROOT)
        lines = subprocess.check_output("tar -cf {tarball} {target_path}".format(tarball=full_results_filename,target_path=folder_in_tar_file), shell=True)
        try:
            os.chdir(cwd)
        except OSError:
            logger.warn("could not change directory, possible due to permissions")
        logger.info("tar returned: {}".format(lines))

        change_owner_to_apache(full_results_filename)
        self.results_file = full_results_filename
        self.save()
        if self.status == Experiment.DONE:
            file_path = self.results_file
            filename = os.path.basename(file_path)
            self.finished_results_file.save(filename, File(open(file_path)))

    def cancel(self):
        # don't send a dumb e-mail: user knows it'll be done immediately
        self.send_confirmation_alert = False
        self.save()

        pending_jobs = self.job_set.filter(status__in=[Job.QUEUED, Job.IN_PROGRESS])
        for j in pending_jobs:
            j.cancel()

        self.update_status()

        return pending_jobs.count()

    def has_failed_jobs(self):
        return self.job_set.filter(status=Job.FAILED).exists()

    def retry_failed_jobs(self):
        jobs_to_retry = self.job_set.filter(status=Job.FAILED)
        tried_to_retry = jobs_to_retry.count()

        actually_retried = 0

        for j in jobs_to_retry:
            if j.retry():
                actually_retried += 1

        if actually_retried:
            self.update_status()

        return (actually_retried, tried_to_retry - actually_retried)

    def publish(self):
        g = Group.objects.get(name='everyone')

        view_job_perm = "datamill_website.view_job"

        for j in self.job_set.all():
            assign_perm(view_job_perm, g, j)

        view_pkg_perm = "datamill_website.view_package"

        for j in self.package_set.all():
            assign_perm(view_pkg_perm, g, j)

        view_exp_perm = "datamill_website.view_experiment"
        assign_perm(view_exp_perm, g, self)

    def is_public(self):
        g = Group.objects.get(name='everyone')
        exps = get_objects_for_group(g, 'datamill_website.view_experiment')

        return self in exps

    def make_private(self):
        g = Group.objects.get(name='everyone')

        view_job_perm = "datamill_website.view_job"

        for j in self.job_set.all():
            remove_perm(view_job_perm, g, j)

        view_pkg_perm = "datamill_website.view_package"

        for j in self.package_set.all():
            remove_perm(view_pkg_perm, g, j)

        view_exp_perm = "datamill_website.view_experiment"
        remove_perm(view_exp_perm, g, self)

    class Meta:
        permissions = (
                ("view_experiment", "Can view experiment"),
                ("unlimited_experiment", "Can submit unlimited experiment")
                )

class Job(models.Model):
    QUEUED = 'QU'
    IN_PROGRESS = 'PR'
    DONE = 'DN'
    FAILED = 'FA'
    CANCELLED = 'CA'
    JOB_STATUS_CHOICES = (
           (QUEUED, 'Queued'),
           (IN_PROGRESS, 'In progress'),
           (DONE, 'Done'),
           (FAILED, 'Failed'),
           (CANCELLED, 'Cancelled'),
           )

    experiment = models.ForeignKey('Experiment', verbose_name='related experiment')
    package = models.ForeignKey('Package', verbose_name='related package')
    priority = models.PositiveSmallIntegerField('priority', default=1)
    worker = models.ForeignKey('Worker', verbose_name='related worker')
    config_id = models.PositiveSmallIntegerField('configuration ID')
    replicate_no = models.PositiveSmallIntegerField('replicate')
    status = models.CharField('status of the job', max_length=2, choices=JOB_STATUS_CHOICES,
            default=QUEUED)
    deleted = models.BooleanField(default=False)

    last_update = models.DateTimeField('last update to job information', auto_now_add=True)

    results_file = models.FilePathField(path=settings.DATAMILL_RESULTS_ROOT,
            match='.*{}$'.format(settings.DATAMILL_RESULTS_EXTENSION), null=True, blank=True)
    job_status_file = models.FilePathField(path=settings.DATAMILL_RESULTS_ROOT,
            match='.*{}$'.format(settings.DATAMILL_JOB_STATUS_EXTENSION), null=True, blank=True)
    worker_status_file = models.FilePathField(path=settings.DATAMILL_RESULTS_ROOT,
            match='.*{}$'.format(settings.DATAMILL_WORKER_STATUS_EXTENSION), null=True, blank=True)

    # stats
    setup_time = models.FloatField('setup time (s)', null=True, blank=True)
    run_time = models.FloatField('run time (s)', null=True, blank=True)
    collect_time = models.FloatField('collect time (s)', null=True, blank=True)

    def __unicode__(self):
        return 'Job ID: {0}'.format(self.id)

    class Meta:
        ordering = ('-last_update',)
        permissions = (
                ("view_job", "Can view job"),
                )

    # methods
    def get_configuration(self):
        cursor = connection.cursor()

        # Might as well do it raw.
        cursor.execute("""
                       SELECT F.internal_name, L.internal_name
                       FROM datamill_website_experiment E, datamill_website_job J,
                            datamill_website_jobconfiguration JC, datamill_website_level L,
                            datamill_website_factor F
                       WHERE J.id = %s
                         AND E.id = J.experiment_id
                         AND J.id = JC.job_id
                         AND L.id = JC.level_id
                         AND L.factor_id = F.id
        """, [self.id])

        configuration = {}
        configuration['factors'] = {row[0]: row[1] for row in cursor.fetchall()}
        configuration['name'] = self.filename_root()
        configuration['replicate_no'] = self.replicate_no
        return configuration

    def set_configuration(self, config_dict):
        for factor_name, level_name in config_dict.items():
            factors = Factor.objects.filter(internal_name=factor_name)
            if factors.exists():
                JobConfiguration.objects.create(level=Level.objects.get(internal_name=level_name, factor=factors[0]), job=self)

    def filename_root(self):
        priority = str(self.priority).rjust(2, '0')
        exp_id = str(self.experiment.id).rjust(4, '0')
        pkg_id = str(self.package.id).rjust(4, '0')
        config_id = str(self.config_id).rjust(4, '0')
        replicate = str(self.replicate_no).rjust(4, '0')
        job_id = str(self.id).rjust(4, '0')

        package_name = self.package.filename_without_extension()

        return '-'.join([ priority, exp_id, pkg_id, config_id, replicate, job_id, package_name ])

    @classmethod
    def decode_filename(cls, filename):
        keys = ['priority', 'exp_id', 'pkg_id', 'config_id',
                'repetition', 'job_id', 'package_name']

        tokens = os.path.basename(filename).split('-', len(keys) - 1)
        pkg_name = tokens[-1].partition('.')[0] # dump extension from first dot (dots are illegal in name)
        def int_or_string(x):
            if x.isdigit():
                return int(x)
            else:
                raise ValueError
        tokens = map(int_or_string, tokens[:len(keys) - 1])
        tokens.append(pkg_name)
        return dict(zip(keys, tokens))


    def outgoing_pkg_filename(self):
        return os.path.join(self.worker.get_dir(),
                '{}{}'.format(self.filename_root(), settings.DATAMILL_PACKAGE_EXTENSION))

    def outgoing_config_filename(self):
        return os.path.join(self.worker.get_dir(),
                '{}{}'.format(self.filename_root(), settings.DATAMILL_CONFIG_EXTENSION))

    def incoming_wip_filename(self):
        return os.path.join(self.worker.get_dir(),
                '{}{}'.format(self.filename_root(), settings.DATAMILL_WIP_EXTENSION))

    def incoming_done_filename(self):
        return os.path.join(self.worker.get_dir(),
                '{}.done'.format(self.filename_root()))

    def incoming_user_results_filename(self):
        return os.path.join(self.worker.get_dir(),
                '{}.user_results'.format(self.filename_root()))

    def incoming_job_status_filename(self):
        return os.path.join(self.worker.get_dir(),
                '{}.job_status'.format(self.filename_root()))

    def incoming_worker_status_filename(self):
        return os.path.join(self.worker.get_dir(),
                '{}.worker_status'.format(self.filename_root()))

    def permanent_user_results_filename(self):
        return os.path.join(settings.DATAMILL_RESULTS_ROOT,
                '{}.user_results{}'.format(self.id, settings.DATAMILL_RESULTS_EXTENSION))

    def permanent_job_status_filename(self):
        return os.path.join(settings.DATAMILL_RESULTS_ROOT,
                '{}.job_status{}'.format(self.id, settings.DATAMILL_JOB_STATUS_EXTENSION))

    def permanent_worker_status_filename(self):
        return os.path.join(settings.DATAMILL_RESULTS_ROOT,
                '{}.worker_status{}'.format(self.id, settings.DATAMILL_WORKER_STATUS_EXTENSION))

    def logs(self):
        # returns all logs that are not empty as a dictionary
        logs = {}
        for file_name in glob.glob(os.path.join(settings.DATAMILL_RESULTS_ROOT, self.filename_root() + '*_log')):
            with open(file_name, 'r') as log:
                base_name = os.path.basename(file_name)
                contents = log.read()
                if contents:
                    logs[base_name.replace('_log', '')] = contents
        return logs

    def update_status(self):
        # only try to update myself if I can become done
        if self.status in [ Job.IN_PROGRESS, Job.QUEUED ]:
            if os.path.exists(self.incoming_done_filename()):
                self.status = Job.DONE
                self.last_update = datetime.fromtimestamp(
                    os.path.getmtime(self.incoming_done_filename()))

                permanent_file_map = {self.incoming_user_results_filename():
                                          self.permanent_user_results_filename(),
                                      self.incoming_job_status_filename():
                                          self.permanent_job_status_filename(),
                                      self.incoming_worker_status_filename():
                                          self.permanent_worker_status_filename()}

                for file in [self.incoming_user_results_filename(), self.incoming_job_status_filename(), self.incoming_worker_status_filename()]:
                    if not os.path.isfile(file):
                        logger.warning('file does not exist: {0}'.format(file))
                        self.status = Job.FAILED
                        continue
                    try:
                        md5hash.check_md5(file)
                    except Exception, e:
                        logger.warning('results failed md5 check, job failed: {0}'.format(e))
                        self.status = Job.FAILED

                    shutil.move(file, permanent_file_map[file])

                    try:
                        os.chmod(permanent_file_map[file],
                                stat.S_IRUSR |
                                stat.S_IWUSR |
                                stat.S_IRGRP |
                                stat.S_IROTH)
                        change_owner_to_apache(permanent_file_map[file])

                    except OSError, e:
                        logging.warning('could not chmod/chown, ignoring: {}'.format(e))

                if os.path.isfile(self.permanent_user_results_filename()):
                    self.results_file = self.permanent_user_results_filename()
                if os.path.isfile(self.permanent_job_status_filename()):
                    self.job_status_file = self.permanent_job_status_filename()
                    with open(self.permanent_job_status_filename()) as f:
                        job_status = json.load(f)
                        if 'worker_common.job_do_setup_time' in job_status:
                            self.setup_time = job_status['worker_common.job_do_setup_time']
                        if 'worker_common.job_do_run_time' in job_status:
                            self.run_time = job_status['worker_common.job_do_run_time']
                        if 'worker_common.job_do_collect_time' in job_status:
                            self.collect_time = job_status['worker_common.job_do_collect_time']

                if os.path.isfile(self.permanent_worker_status_filename()):
                    self.worker_status_file = self.permanent_worker_status_filename()

                self.delete_incoming_and_outgoing_files()
                self.save()

            elif os.path.exists(self.incoming_wip_filename()):
                self.status = Job.IN_PROGRESS
                self.last_update = datetime.fromtimestamp(
                    os.path.getmtime(self.incoming_wip_filename()))
                self.save()

    def cancel(self):
        self.delete_incoming_and_outgoing_files()
        self.status = Job.CANCELLED
        self.last_update = datetime.now()
        self.save()

    def fail(self):
        self.delete_incoming_and_outgoing_files()
        self.status = Job.FAILED
        self.last_update = datetime.now()
        self.save()

    def retry(self):
        if self.status == Job.FAILED:
            if not self.worker.status == Worker.DEAD and os.path.exists(self.worker.get_dir()):
                self.create_outgoing_files()
                self.status = Job.QUEUED
                self.last_update = datetime.now()
                self.save()
                return True

        return False

    def delete_incoming_and_outgoing_files(self):
        # this keeps anything that may have been
        # saved to DATAMILL_RESULTS_ROOT by update_status

        worker_dir = self.worker.get_dir()
        all_worker_files = os.listdir(worker_dir)
        files_to_delete = [ os.path.join(worker_dir, f)
                for f in all_worker_files
                if os.path.basename(f).startswith(self.filename_root()) ]

        for f in files_to_delete:
            logger.debug('rming {0}'.format(f))
            try:
                os.remove(f)
            except OSError, e:
                logger.warning('error rming file. may have never existed: {0}'.format(e))

    def create_outgoing_files(self):
        # create link to package and md5
        if not os.path.exists(self.package.outgoing_filename()):
            self.package.create_outgoing_file()

        os.link(self.package.outgoing_filename(), self.outgoing_pkg_filename())
        os.link('{}{}'.format(self.package.outgoing_filename(),
            settings.DATAMILL_INTEG_EXTENSION),
            '{}{}'.format(self.outgoing_pkg_filename(), settings.DATAMILL_INTEG_EXTENSION))

        # create config and its md5
        with file(self.outgoing_config_filename(), 'w') as f:
            json.dump(self.get_configuration(), f)

        md5hash.generate_md5(self.outgoing_config_filename())

        # since this is called from the scheduler, everything will be created w/ root.root
        # user.group. we make them world-writable here so that delete_in_and_outgoing_files
        # works when called by apache. vsftpd does not allow overwriting files even with
        # these permissions, so this should be fine until we move to a view-based approach

        files_to_chmod = [ self.outgoing_pkg_filename(), self.outgoing_config_filename() ]
        files_to_chmod.extend([ '{}{}'.format(f, settings.DATAMILL_INTEG_EXTENSION)
                                                for f in files_to_chmod ])

        try:
            apache_uid = pwd.getpwnam('apache').pw_uid
        except KeyError:
            apache_uid = os.getuid() # current process when running without apache

        try:
            apache_gid = grp.getgrnam('apache').gr_gid
        except KeyError:
            apache_gid = os.getgid()

        for filename in files_to_chmod:
            try:
                os.chmod(filename,
                        stat.S_IRUSR |
                        stat.S_IWUSR |
                        stat.S_IRGRP |
                        stat.S_IWOTH |
                        stat.S_IROTH)
                os.chown(filename, apache_uid, apache_gid)
            except OSError, e:
                logging.warning('could not chmod/chown, ignoring: {}'.format(e))

        #publish job to redis channel
        # job_queue = PubSub('jobs')
        # job_queue.publish({'uuid': self.worker.uuid})

class Package(models.Model):
    file = models.FileField(upload_to='worker-package-store', storage=FileSystemStorage()) # relative to MEDIA_ROOT
    experiment = models.ForeignKey('Experiment',
            verbose_name='the experiment this package belongs to')

    def __unicode__(self):
        return os.path.basename(self.file.name)

    def filename(self):
        return os.path.basename(self.file.name)

    def filename_without_extension(self):
        # package_name dumps extension! dots are illegal in name!
        return os.path.basename(self.file.name).partition('.')[0]

    def exists(self):
        return os.path.isfile(self.uploaded_filename())

    def uploaded_filename(self):
        return os.path.join(settings.MEDIA_ROOT, self.file.name)

    def outgoing_filename(self):
        return(os.path.join(settings.DATAMILL_FTP_ROOT,
            'worker-package-store',
            os.path.basename(self.file.name)))

    def create_outgoing_file(self):

        if not os.path.exists(os.path.dirname(self.outgoing_filename())):
            os.makedirs(os.path.dirname(self.outgoing_filename()))

        shutil.copyfile(self.uploaded_filename(), self.outgoing_filename())

        # make file accessible to workers
        os.chmod(self.outgoing_filename(),
                                 stat.S_IRUSR |
                                 stat.S_IWUSR |
                                 stat.S_IRGRP |
                                 stat.S_IROTH)

        md5hash.generate_md5(self.outgoing_filename())

    class Meta:
        permissions = (
                ("view_package", "Can view package"),
                )


class Factor(models.Model):
    HARDWARE = 'HW'
    SOFTWARE = 'SW'
    FAC_TYPE_CHOICES = (
           (HARDWARE, 'Hardware'),
           (SOFTWARE, 'Software'),
           )

    DISCRETE = 'CB'
    BOOLEAN = 'SH'
    CONTINUOUS = 'RA'
    # these string constants are used to determine the display
    # modifying them will break experiment_add view
    DATA_CHOICES = (
           (BOOLEAN, 'Switch'),
           (DISCRETE, 'Checkbox'),
           (CONTINUOUS, 'Range'),
           )

    #name = models.CharField('factor name', max_length=30, unique=True)
    internal_name = models.CharField('internal factor name', max_length=30, unique=True)
    description = models.TextField('factor description')
    fac_type = models.CharField('factor type', max_length=2,
            choices=FAC_TYPE_CHOICES, default=HARDWARE)
    data_category = models.CharField('display type', max_length=2,
            choices=DATA_CHOICES, default=BOOLEAN)

    def get_levels(self, workers=None):
        if workers:
            levels = self.level_set.distinct()
            return WorkerFeature.objects.filter(reduce(lambda q, f: q | Q(worker=f), workers, Q()),
                                          reduce(lambda q, f: q | Q(level=f), levels, Q()))\
                                        .distinct().values_list('level__internal_name', flat=True)
        else:
            return list(self.level_set.distinct().values_list('internal_name', flat=True))

    def get_max_level(self):
        return max(self.get_levels())

    def get_min_level(self):
        return min(self.get_levels())

    def get_default(self):
        if self.fac_type == self.SOFTWARE:
            return self.level_set.get(default=True).internal_name
        else:
            return None

    @classmethod
    def get_full_scope(cls):
        scope = {}
        for f in Factor.objects.all():
            scope[f.internal_name] = f.get_levels()

        return scope

    def __unicode__(self):
        return self.internal_name

class Level(models.Model):
    internal_name = models.CharField(max_length=100)
    factor = models.ForeignKey(Factor)
    default = models.BooleanField(default=False)
    obsolete = models.BooleanField(default=False)

    class Meta:
        unique_together = ('internal_name', 'factor',)

    def __unicode__(self):
        return '{0}: {1}'.format(self.factor.internal_name, self.internal_name)

class Scope(models.Model):
    experiment = models.ForeignKey(Experiment)
    level = models.ForeignKey(Level)

class Constraint(models.Model):
    experiment = models.ForeignKey(Experiment)
    level = models.ForeignKey(Level)

class JobConfiguration(models.Model):
    level = models.ForeignKey(Level)
    job = models.ForeignKey(Job)

class FeatureManager(models.Manager):
    def available(self):
        return self.exclude(worker__status=Worker.DEAD).distinct('level__internal_name', 'level__factor__internal_name')

    def get_workers_by_level(self, level_name, level_value):
        factor = Factor.objects.get(internal_name=level_name)
        level = Level.objects.get(factor=factor, internal_name=level_value)
        return [feature.worker for feature in self.filter(level=level)]

class WorkerFeature(models.Model):
    worker = models.ForeignKey(Worker)
    level = models.ForeignKey(Level)
    objects = FeatureManager()

reversion.register(Worker, follow=["workerfeature_set"])
reversion.register(WorkerFeature)

###########
# SIGNALS #
###########

# all new users go to everyone
@receiver(post_save, sender=User)
def everyone_group_handler(sender, instance, created, **kwargs):
    if created:
        try:
            g = Group.objects.get(name='everyone')
        except:
            g = Group(name='everyone')
            g.save()

        g.user_set.add(instance)

# all new jobs go to experiment owners
@receiver(post_save, sender=Job)
def job_permission_handler(sender, instance, created, **kwargs):
    if created:
        users_with_perms = get_users_with_perms(instance.experiment, with_group_users=False)
        job_perms = [ p.codename for p in get_perms_for_model(Job) ]

        for user in users_with_perms:
            for perm in job_perms:
                assign_perm(perm, user, instance)

@receiver(post_save, sender=Package)
def package_permission_handler(sender, instance, created, **kwargs):
    if created:
        users_with_perms = get_users_with_perms(instance.experiment, with_group_users=False)
        pkg_perms = [ p.codename for p in get_perms_for_model(Package) ]

        for user in users_with_perms:
            for perm in pkg_perms:
                assign_perm(perm, user, instance)
