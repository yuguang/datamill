from functools import wraps
from django.http import HttpResponseRedirect
from django.conf import settings

def is_superuser(f):
    @wraps(f)
    def wrapper(request, *args, **kwds):
        if request.user.is_superuser:
            return f(request, *args, **kwds)
        else:
            return HttpResponseRedirect(settings.LOGIN_URL)
    return wrapper