#!/bin/sh

NAS_DM_BACKUP_PATH=/media/esg-backup
LOCAL_DM_BACKUP_PATH=/datamill/backup

#make dir for local datamill backup
mkdir $LOCAL_DM_BACKUP_PATH

#make dir for /home/ftp THEN rsync
mkdir -p $LOCAL_DM_BACKUP_PATH/home/ftp-tree/ && rsync -aHA --del --force /home/ftp/ $LOCAL_DM_BACKUP_PATH/home/ftp-tree/

#make dir for /usr/portage/distfiles THEN rsync
mkdir -p $LOCAL_DM_BACKUP_PATH/usr/portage/distfiles && rsync -aHA --del --force /usr/portage/distfiles/ $LOCAL_DM_BACKUP_PATH/usr/portage/distfiles/

#make dir for /var/datamill/ THEN rsync
mkdir -p $LOCAL_DM_BACKUP_PATH/var/datamill && rsync -aHA --del --force /var/datamill/ $LOCAL_DM_BACKUP_PATH/var/datamill/

#create directory for database backup THEN dumpall
mkdir -p $LOCAL_DM_BACKUP_PATH/database && /usr/bin/pg_dumpall -c > $LOCAL_DM_BACKUP_PATH/database/datamill_db.sql

cd $LOCAL_DM_BACKUP_PATH
#compress backup THEN copy backup file to esg-nas THEN remove backup file from local
tar -cpzf datamill-backup-$(date +"%m-%d-%y").tar.gz * && 
cp datamill-backup-$(date +"%m-%d-%y").tar.gz $NAS_DM_BACKUP_PATH &&
rm datamill-backup-$(date +"%m-%d-%y").tar.gz
