import subprocess
from django.core.management.base import BaseCommand
from datamill_website.management.deploy_utils import prep_master_tarball, install_master_software


class Command(BaseCommand):

    help = 'Datamill complete backup (including files and database)'
    
    def handle(self, *args, **options):
        subprocess.check_call("./datamill_website/management/backup.sh", shell=True)
