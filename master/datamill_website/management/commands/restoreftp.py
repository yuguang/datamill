from django.core.management.base import BaseCommand, CommandError
from datamill_website.models import Worker
import os
from datamill_django_project.settings import *


class Command(BaseCommand):

    def handle(self, *args, **options):
        for worker in Worker.objects.available():
            worker.create_dir()
            for job in worker.job_set.all():
                job.create_outgoing_files()
        if not os.path.exists(DATAMILL_HELLO_ROOT):
            os.makedirs(DATAMILL_HELLO_ROOT)
            # TODO package store links and wip files
