
from django.core.management.base import BaseCommand

# from datamill_website.management.deploy_utils import prep_master_tarball, install_master_software
import subprocess

class Command(BaseCommand):

    help = """Clones the master's software/database to the testmaster"""
    
    def handle(self, *args, **options):

        user = "root"
        address = "datamill.uwaterloo.ca"
        master_port = "22"
        testmaster_port = "2222"
        tarball = "/tmp/datamill_clone.tar.xz"

        # 1. compress master software, database contents, and
        #    optionally tarfiles.

        subprocess.check_call("ssh -p {port} {user}@{address} '{copy_script}'".format(
            user=user,
            address=address,
            port=master_port,
            copy_script=copy_master_script
        ), shell=True)

        # 2. Copy these to the testmaster

        subprocess.check_call("scp -P {port} {user}@{address}:{tarball} /tmp".format(
            user=user,
            address=address,
            port=master_port,
            tarball=tarball
        ), shell=True)

        subprocess.check_call("scp -P {port} {tarball} {user}@{address}:{tarball}".format(
            user=user,
            address=address,
            port=testmaster_port,
            tarball=tarball
        ), shell=True)

        # 3. Turn off currently running daemons.
        # 4. Delete current master software.
        # 5. Untar current master software
        # 6. Run psql -f <dbbackup>
        # 7. Run the following commands:
        # ./setup-virtualenv.sh
        # ./manage.py migrate datamill_website
        # ./manage.py syncdb --all
        # ./manage.py collectstatic --noinput
        # env-update && source /etc/profile
        # 8. Spin up the daemons

        subprocess.check_call("ssh -p {port} {user}@{address} '{clone_script}'".format(
            user=user,
            address=address,
            port=testmaster_port,
            clone_script=clone_master_from_tarball_script
        ), shell=True)


# This should use the same lock as the update master scripts

copy_master_script = """
if [[ -f /var/lock/datamill_master_update ]]; then exit 1; fi
touch /var/lock/datamill_master_update

DATAMILL_CLONE=/tmp/datamill_clone


rm -rf ${DATAMILL_CLONE} || exit 1
rm -rf ${DATAMILL_CLONE}.tar.xz || exit 1

mkdir ${DATAMILL_CLONE}
cp -a /var/www/localhost/wsgi/eval-lab ${DATAMILL_CLONE} || exit 1

su - postgres -c "pg_dumpall --clean" > ${DATAMILL_CLONE}/datamill-clone-backup.sql

cd /tmp
tar caf ${DATAMILL_CLONE##*/}.tar.xz ${DATAMILL_CLONE##*/} || exit 1

rm /var/lock/datamill_master_update
"""

clone_master_from_tarball_script = """
if [[ -f /var/lock/datamill_master_update ]]; then exit 1; fi
touch /var/lock/datamill_master_update

/etc/init.d/apache2 stop
/etc/init.d/collector stop
/etc/init.d/registrar stop
/etc/init.d/ip-updater stop
/etc/init.d/scheduler stop

/etc/init.d/postgresql-9.2 restart

DATAMILL_MASTER_CLONE=/tmp/datamill_clone.tar.xz

cd /tmp || exit 1
tar xaf ${DATAMILL_MASTER_CLONE} || exit 1
su - postgres -c "psql -f "${DATAMILL_MASTER_CLONE%%.*}/datamill-clone-backup.sql

cd /var/www/localhost/wsgi/ || exit 1
rm -rf eval-lab || exit 1
cd ${DATAMILL_MASTER_CLONE%%.*} || exit 1
cp -a eval-lab /var/www/localhost/wsgi || exit 1

cd /var/www/localhost/wsgi/eval-lab/master || exit 1
rm -rf dev-python
./setup-virtualenv.sh
./manage.py migrate datamill_website --noinput
./manage.py syncdb --all
./manage.py collectstatic --noinput -c

env-update && source /etc/profile

/etc/init.d/apache2 restart
/etc/init.d/collector restart
/etc/init.d/registrar restart
/etc/init.d/ip-updater restart
/etc/init.d/scheduler restart

rm /var/lock/datamill_master_update
"""
