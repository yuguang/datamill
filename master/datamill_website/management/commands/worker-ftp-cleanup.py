from django.core.management.base import BaseCommand, CommandError
from datamill_website.models import Worker
import shutil, os


class Command(BaseCommand):

    def handle(self, *args, **options):
        workers = Worker.objects.filter(status=Worker.DEAD)
        for worker in workers:
            if os.path.isdir(worker.get_dir()):
                shutil.rmtree(worker.get_dir(), ignore_errors=True)
