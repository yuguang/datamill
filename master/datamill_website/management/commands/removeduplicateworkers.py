from django.core.management.base import BaseCommand, CommandError
from datamill_website.models import Worker
from django.db.models import Count


class Command(BaseCommand):

    def handle(self, *args, **options):
        workers_by_hostname = Worker.objects.values('hostname').annotate(count=Count('hostname')).order_by().filter(count__gt=1)
        for worker_by_hostname in workers_by_hostname:
            workers = Worker.objects.filter(hostname=worker_by_hostname['hostname']).order_by('last_update')
            workers = workers[0:len(workers)-1]
            for worker in workers:
                worker.make_dead()
