from django.core.management.base import BaseCommand, CommandError
from datamill_website.models import Worker
from django.core.mail import send_mail
from string import Template
from datamill_django_project import settings


class Command(BaseCommand):

    def handle(self, *args, **options):
        admin_emails = []
        for admin in settings.ADMINS:
            admin_emails.append(admin[1])
        workers = Worker.objects.filter(status=Worker.DEAD)
        message_template = Template('You have received this message because your DataMill worker appears to be down. The machine at $ip with name $hostname has not reported to DataMill for an extended period of time. Please check the machine or reboot it. ')
        for worker in workers:
            message = message_template.substitute(ip=worker.ip, hostname=worker.hostname)
            send_mail('DataMill Worker Needs Reboot', message, 'datamill@uwaterloo.ca',
    [worker.user.email], admin_emails)
