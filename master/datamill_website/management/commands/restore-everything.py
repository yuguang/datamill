import subprocess
from django.core.management.base import BaseCommand
from datamill_website.management.deploy_utils import prep_master_tarball, install_master_software


class Command(BaseCommand):

    help = 'Datamill complete backup (including files and database)'
    
    def handle(self, *args, **options):
		if len(args) > 0 and args[0].endswith(".tar.gz") == True:
			subprocess.check_call("./datamill_website/management/restore.sh {backup_file}".format(backup_file=args[0]), shell=True)
		else:
			print "You have to specify a backup file (*.tar.gz)"
