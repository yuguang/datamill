
from django.core.management.base import BaseCommand

from datamill_website.management.deploy_utils import prep_master_tarball, install_master_software

class Command(BaseCommand):

    help = 'Deploys current code to the master node.'
    
    def handle(self, *args, **options):
        prep_master_tarball()
        install_master_software("master-software.tar.gz", "root", "datamill.uwaterloo.ca", 22)
