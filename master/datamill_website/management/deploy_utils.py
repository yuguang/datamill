
import os
import sys
import shutil
import tempfile
import subprocess
import time

def prep_master_tarball():
    # Update static file version
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../datamill_django_project/static_media_version.py'), 'w') as version_file:
        version_file.write('MEDIA_VERSION = {}\n'.format(int(time.time())))

    # Excludes
    exclude_patterns = ['*.pyc']
    exclude_dirs = ['dev-python']
    exclude_files = ['datamill_django_project/local_settings.py',
                     'datamill_django_project/example_local_settings.py']

    try:
        subprocess.check_call('./manage.py test datamill_website', shell=True)
    except:
        print >> sys.stderr, "Website tests failed... Halting"
        raise

    try:
        subprocess.check_call('./manage.py test datamill_daemons', shell=True)
    except:
        print >> sys.stderr, "Daemon tests failed... Halting"
        raise

    print >> sys.stderr, "All tests passed... deploying"

    # Prep staging dir
    staging_basename = 'master-software'
    staging_temp_dir = tempfile.mkdtemp()
    staging_dir = os.path.join(staging_temp_dir, staging_basename)

    print >> sys.stderr, "Staging in: {}".format(staging_dir)

    # Generate excludes portion of tar command
    excludes = map(lambda path: os.path.join(staging_basename, path),
                   exclude_dirs + exclude_files) + exclude_patterns
    excludes = ' '.join(map(lambda path: '--exclude={pattern}'.format(pattern=path),
                            excludes))

    print >> sys.stderr, "Using excludes: {}".format(excludes)

    try:
        # Populate staging dir
        shutil.copytree('.', staging_dir)

        tar_command = "tar cavf {staging_basename}.tar.gz {staging_basename} {excludes}".format(
            staging_basename=staging_basename,
            excludes=excludes
        )

        subprocess.check_call(tar_command, cwd=staging_temp_dir, shell=True)
        shutil.move(staging_dir + '.tar.gz', '.')

    finally:
        try:
            print >> sys.stderr, "Cleaning staging dir: rm -f {}".format(staging_dir)
            shutil.rmtree(staging_temp_dir)
        except:
            pass

# Deploy tarball to master
def install_master_software(tarball, user, address, port):

    port = str(port)

    subprocess.check_call('ssh -p {port} {user}@{address} "rm -f /tmp/master-software.tar.gz"'.format(
        user=user,
        address=address,
        port=port
    ), shell=True)

    subprocess.check_call("scp -P {port} {tarball} {user}@{address}:/tmp/".format(
        tarball=tarball,
        user=user,
        address=address,
        port=port
    ), shell=True)

    try:
        subprocess.check_call("ssh -p {port} {user}@{address} '{install_script}'".format(
            port=port,
            user=user,
            address=address,
            install_script=install_master_software_script
        ), shell=True)

    except Exception as e:
        print >> sys.stderr, "Update to master software failed with " + str(e)
        print >> sys.stderr, "Attempting to recover"
        subprocess.check_call("ssh -p {port} {user}@{address} '{recovery_script}'".format(
            port=port,
            user=user,
            address=address,
            recovery_script=recover_from_failed_install
        ), shell=True)
        raise

    finally:
        try:
            os.remove(tarball)
        except:
            pass

install_master_software_script = """
if [[ -f /var/lock/datamill_master_update ]]; then exit 1; fi
touch /var/lock/datamill_master_update

/etc/init.d/apache2 stop
/etc/init.d/collector stop
/etc/init.d/registrar stop
/etc/init.d/ip-updater stop
/etc/init.d/scheduler stop

rm -rf /tmp/datamill-master-bak || exit 1
cp -a /var/www/localhost/wsgi/eval-lab /tmp/datamill-master-bak || exit 1
rm -rf /tmp/datamill-master-tmp
mkdir /tmp/datamill-master-tmp
tar xavf /tmp/master-software.tar.gz -C /tmp/datamill-master-tmp || exit 1

cd /var/www/localhost/wsgi/ || exit 1
rm -rf eval-lab || exit 1
mkdir eval-lab || exit 1
cp -a /tmp/datamill-master-tmp/master-software eval-lab/master || exit 1
cd eval-lab/master || exit 1

./setup-virtualenv.sh
./manage.py migrate datamill_website --noinput
./manage.py syncdb --all
./manage.py collectstatic --noinput -c

env-update && source /etc/profile

/etc/init.d/apache2 restart
/etc/init.d/collector restart
/etc/init.d/registrar restart
/etc/init.d/ip-updater restart
/etc/init.d/scheduler restart

rm /var/lock/datamill_master_update
"""

recover_from_failed_install = """
if [[ -f /var/lock/datamill_master_update ]]; then exit 1; fi
touch /var/lock/datamill_master_update

mkdir -p /var/www/localhost/wsgi/eval-lab
cd /var/www/localhost/wsgi/eval-lab || exit 1
cp -r /tmp/datamill-master-bak/*  . || exit 1

/etc/init.d/apache2 restart
/etc/init.d/collector restart
/etc/init.d/registrar restart
/etc/init.d/ip-updater restart
/etc/init.d/scheduler restart

rm /var/lock/datamill_master_update
"""
