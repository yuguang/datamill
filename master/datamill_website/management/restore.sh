#!/bin/sh

if [[ ${1: -7} != ".tar.gz" ]]
	then
		echo "You have to specify a backup file (*.tar.gz)"
		exit 1;
	else
		BACKUP_FILE_PATH=$1
fi

BACKUP_FILE=$(basename $BACKUP_FILE_PATH)
RESTORE_TMP_PATH=/tmp/datamill-restore

#make restore dir THEN copy backup file into restore dir
mkdir -p $RESTORE_TMP_PATH && cp $BACKUP_FILE_PATH $RESTORE_TMP_PATH

#extract backup file on local
cd $RESTORE_TMP_PATH
tar xpzf $BACKUP_FILE

#stop apache and datamill deamons
/etc/init.d/apache2 stop
/etc/init.d/collector stop
/etc/init.d/registrar stop
/etc/init.d/ip-updater stop
/etc/init.d/scheduler stop

#restore everything
rsync -aHA --del --force $RESTORE_TMP_PATH/home/ftp-tree/ /home/ftp/
rsync -aHA --del --force $RESTORE_TMP_PATH/usr/portage/distfiles/ /usr/portage/distfiles/
rsync -aHA --del --force $RESTORE_TMP_PATH/var/datamill/ /var/datamill/

#restore database dump
su - postgres -c 'psql -f '$RESTORE_TMP_PATH'/database/datamill_db.sql'

#remove restore temporary path
rm -rf $RESTORE_TMP_PATH

#start apache and datamill deamons
/etc/init.d/apache2 start
/etc/init.d/collector start
/etc/init.d/registrar start
/etc/init.d/ip-updater start
/etc/init.d/scheduler start
