from django import template
from django.template.defaultfilters import stringfilter
import string

register = template.Library()

@register.filter
@stringfilter
def humanize(s):
    return string.capwords(s, '_').replace('_', ' ')
