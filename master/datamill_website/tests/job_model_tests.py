from django.test import TestCase
from datamill_website.models import *
from datamill_common import datamill_db_helpers
from md5hash import md5hash
import logging
import os
import grp
import pwd
import os.path

class JobTests(TestCase):

    fixtures = [ 'initial_data', 'datamill_test_fixture' ]

    def setUp(self):
        pass

    def tearDown(self):
        datamill_db_helpers.delete_all_datamill_files()

    def test_filename_root(self):
        experiment = Experiment.objects.all()[0]
        package = Package.objects.all()[0]
        worker = Worker.objects.all()[0]
        j = Job()
        j.priority = 1
        j.experiment = experiment
        j.package = package
        j.worker = worker
        j.config_id = 1
        j.replicate_no = 10
        filename_root = '-'.join([ '01', str(j.experiment.id).rjust(4, '0'), str(j.package.id).rjust(4, '0'), '0001', '0010', str(j.id).rjust(4, '0'), package.filename_without_extension() ])
        self.assertEquals(j.filename_root(), filename_root)

    def test_decode_filename(self):
        d = Job.decode_filename('01-0002-0003-0003-0005-1241-eval-lab_WWkgXh.tar.gz')
        self.assertEquals(d['priority'], 1)
        self.assertEquals(d['exp_id'], 2)
        self.assertEquals(d['pkg_id'], 3)
        self.assertEquals(d['config_id'], 3)
        self.assertEquals(d['repetition'], 5)
        self.assertEquals(d['job_id'], 1241)
        self.assertEquals(d['package_name'], 'eval-lab_WWkgXh')

    def test_decode_filename_malformed(self):
        self.assertRaises(ValueError, Job.decode_filename, '01-0002-0003-0003-0005-abcd-eval-lab_WWkgXh.tar.gz')

    def test_priority(self):
        j = Job()
        for num_jobs in [1,1000,2001]:
            priority = Experiment.calculate_priority(num_jobs)
            self.assertGreaterEqual(priority, 0)
            self.assertLess(priority, 100)

    def test_retry(self):
        j = Job.objects.all()[0]
        success = True
        try:
            j.retry()
            j.retry()
        except:
            success = False
        self.assertTrue(success)

    def test_filename_helpers(self):
        j = Job.objects.all()[0]
        f = j.incoming_wip_filename()
        self.assertEquals(Job.decode_filename(f)['repetition'], j.replicate_no)
        f = j.incoming_user_results_filename()
        self.assertEquals(Job.decode_filename(f)['repetition'], j.replicate_no)
        f = j.permanent_user_results_filename()
        self.assertEquals(os.path.basename(f), '{}.user_results.tar.gz'.format(j.id))

    def test_update_status(self):
        j = Job.objects.all()[0]
        j.update_status()
        self.assertEquals(j.status, Job.QUEUED)
        datamill_db_helpers.create_job_incoming_wip_file(j)
        j.update_status()
        self.assertEquals(j.status, Job.IN_PROGRESS)
        self.assertTrue(os.path.exists(j.incoming_wip_filename()))
        datamill_db_helpers.create_job_incoming_results_files(j)
        j.update_status()
        self.assertFalse(os.path.exists(j.incoming_done_filename()))
        self.assertTrue(os.path.exists(j.permanent_user_results_filename()))
        self.assertEquals(j.status, Job.DONE)
        j.update_status() # do twice to see if update_status on a done job is harmless
        self.assertEquals(j.status, Job.DONE)

    def test_fail_and_delete_files(self):
        j = Job.objects.all()[0]
        datamill_db_helpers.create_job_outgoing_files(j)
        datamill_db_helpers.create_job_incoming_wip_file(j)
        datamill_db_helpers.create_job_incoming_results_files(j)

        jobfiles = [ f for f in os.listdir(j.worker.get_dir()) if f.startswith(j.filename_root()) ]
        for f in jobfiles:
            self.assertTrue(os.path.exists(os.path.join(j.worker.get_dir(), f)))

        j.fail() # calls delete_incoming_files internally
        self.assertEquals(j.status, Job.FAILED)

        for f in jobfiles:
            self.assertFalse(os.path.exists(os.path.join(j.worker.get_dir(), f)))

    def test_delete_files(self):
        j = Job.objects.all()[0]
        datamill_db_helpers.create_package_file(j.package)
        j.package.create_outgoing_file()
        datamill_db_helpers.create_worker_dir(j.worker)
        j.create_outgoing_files()
        worker_dir = j.worker.get_dir()
        all_worker_files = os.listdir(worker_dir)

        try:
            apache_uid = pwd.getpwnam('apache').pw_uid

            # Testing proper permissions on the created files only
            # really works if we are root.
            uid = os.getuid()
            if uid != 0:
                apache_uid = uid

        except KeyError:
            logging.warning('apache user does not exist')
            apache_uid = os.getuid()

        try:
            apache_gid = grp.getgrnam('apache').gr_gid

            gid = os.getgid()
            if gid != 0:
                apache_gid = gid

        except KeyError:
            logging.warning('apache group does not exist')
            apache_gid = os.getgid()

        for f in all_worker_files:
            if os.path.basename(f).startswith(j.filename_root()):
                stat_info = os.stat(os.path.join(worker_dir, f))
                uid = stat_info.st_uid
                gid = stat_info.st_gid
                self.assertEquals(apache_uid, uid)
                self.assertEquals(apache_gid, gid)

        j.delete_incoming_and_outgoing_files()

        files = [ f for f in os.listdir(j.worker.get_dir()) if os.path.basename(f).startswith(j.filename_root()) ]
        self.assertEquals(len(files), 0)

    def test_create_outgoing_files(self):
        j = Job.objects.all()[0]
        datamill_db_helpers.create_package_file(j.package)
        j.package.create_outgoing_file()
        datamill_db_helpers.create_worker_dir(j.worker)
        j.create_outgoing_files()
        self.assertTrue(os.path.exists(j.outgoing_pkg_filename()))
        self.assertTrue(os.path.exists(j.outgoing_config_filename()))
        md5hash.check_md5(j.outgoing_pkg_filename())
        md5hash.check_md5(j.outgoing_config_filename())

        # Ensure that the configuration is a dictionary, not anything
        # else
        with open(j.outgoing_config_filename(), "r") as f:
            config_dictionary = json.load(f)
            self.assertEquals(dict, type(config_dictionary))

