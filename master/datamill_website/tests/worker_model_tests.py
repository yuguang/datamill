from django.test import TestCase
from datamill_website.models import Worker, Job, Factor, Level, WorkerFeature
from datetime import datetime, timedelta
from django.conf import settings
from datamill_common import datamill_db_helpers
import json
import os
import os.path
import shutil

class WorkerTests(TestCase):

    fixtures = [ 'initial_data', 'datamill_test_fixture' ]

    def setUp(self):
        pass

    def tearDown(self):
        datamill_db_helpers.delete_all_datamill_files()

    def test_update_status_and_make_dead(self):
        w = Worker.objects.all()[0]
        jobs = w.job_set.all()

        w.last_update = datetime.min # the worker regged aeons ago
        w.save()

        for j in jobs:
            datamill_db_helpers.create_job_incoming_results_files(j)
             # make sure the test doesn't fail because the fixture is old
            j.last_update = datetime.now()
            j.save()

        w.update_status()
        self.assertEquals(w.status, Worker.IDLE)

        jz = jobs[0]
        jz.status = Job.IN_PROGRESS
        jz.save()

        w.update_status()
        self.assertEquals(w.status, Worker.WORKING)

        for j in jobs:
            j.status = Job.DONE
            j.save()

        w.update_status()
        self.assertEquals(w.status, Worker.IDLE)

        # make jobs finish long in the past, but all are done
        # so last update is a while ago, but we can't tell if the worker is dead
        delta = timedelta(days=3 * settings.DATAMILL_DAYS_UNTIL_WORKER_DEAD)
        jobs.update(last_update=(datetime.now() - delta)) # use update to get around autonow

        w.update_status()
        self.assertEquals(w.status, Worker.IDLE)

        # make one of the jobs queued
        # so last update is a while ago, and worker is supposed to be working
        # so now he's for sure dead

        w.last_update = datetime.min # the worker regged aeons ago
        w.save()

        jz = jobs[0]
        jz.status = Job.QUEUED
        jz.save()
        jobs.update(last_update=(datetime.now() - delta)) # use update to get around autonow

        w.update_status() # will call make_dead internally
        self.assertEquals(w.status, Worker.DEAD)

        # make sure make_dead did its job
        self.assertFalse(w.job_set.filter(status__in=[Job.QUEUED, Job.IN_PROGRESS]).exists())

    def test_fill_from_dict_twice(self):
        w = Worker()
        with open(os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_hello_file.json')) as f:
            w_dict = json.load(f)

        w.fill_from_dict(w_dict)
        self.assertEquals(w.get_hardware_level('cpu_arch'), 'x86_64')
        tmp_lvl = Level.objects.get(factor=Factor.objects.get(internal_name='cpu_arch'), internal_name='x86_64')

        w_dict['cpu_arch'] = 'something else'
        w.fill_from_dict(w_dict)
        self.assertEquals(w.get_hardware_level('cpu_arch'), 'something else')
        self.assertEquals(WorkerFeature.objects.filter(worker=w, level=tmp_lvl).count(), 0)
        w_dict['opt_flag'] = ['-O2', '-O3']
        w.fill_from_dict(w_dict)
        self.assertSetEqual(set([feature.level.internal_name for feature in list(WorkerFeature.objects.filter(worker=w, level__factor__internal_name='opt_flag'))]), set(['-O3', '-O2']))
        self.assertEquals(w.gpu_flags, 'msi pm vga_controller bus_master cap_list rom')
        self.assertEquals(w.get_hardware_level('gpu_mhz'), '33')
        tmp_lvl = Level.objects.get(factor=Factor.objects.get(internal_name='cpu_arch'), internal_name='something else')
        self.assertEquals(WorkerFeature.objects.filter(worker=w, level=tmp_lvl).count(), 1)

    def test_fill_from_dict(self):
        w = Worker()
        with open(os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_hello_file.json')) as f:
            w_dict = json.load(f)

        w.fill_from_dict(w_dict)
        w.save()
        self.assertEquals(w.get_hardware_level('cpu_arch'), 'x86_64')

    def test_fill_from_dict_corrupt(self):
        with open(os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_corrupt_file.json')) as f:
            self.assertRaises(ValueError, json.load, f)

    def test_fill_from_dict_incomplete(self):
        w = Worker()

        with open(os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_incomplete_file.json')) as f:
            w_dict = json.load(f)

        self.assertRaises(Exception, w.fill_from_dict, w_dict)

    def test_create_dir(self):
        w = Worker.objects.all()[0]

        try:
            w.create_dir()
        except OSError:
            # the chown is very likely to fail, since we
            #   (1) don't run tests as root and
            #   (2) don't have ftp.ftp user/group in our dev boxes
            pass

        self.assertTrue(os.path.exists(w.get_dir()))
        #self.assertThrowsException(w.create_dir())


    def test_create_dir_no_ftp(self):
        w = Worker.objects.all()[0]

        if os.path.exists(settings.DATAMILL_FTP_ROOT):
            shutil.rmtree(settings.DATAMILL_FTP_ROOT)

        self.assertRaises(OSError, w.create_dir)
