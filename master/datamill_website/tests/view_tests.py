from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
from django.template import Template, Context

class ViewTests(TestCase):

    fixtures = [ 'initial_data', 'datamill_test_fixture' ]

    def assertQSEqual(self, a, b):
        """
        Takes 2 lists/querysets/iterables, sorts them by pk, and checks for
        equality
        """
        self.assertEqual(sorted(list(a), key=lambda x: x.pk),
                         sorted(list(b), key=lambda x: x.pk))

    def test_views(self):
        c = Client()
        c.login(username='test', password='123')
        response = c.get('/worker/1/')
        self.assertEqual(response.status_code, 200)
        response = c.get('/jobs/')
        self.assertEqual(response.status_code, 200)
        response = c.get('/experiments/')
        self.assertEqual(response.status_code, 200)

    def test_experiment_save(self):
        experiment_add = reverse('experiment_add')
        self.assertEqual(experiment_add, '/experiment/add/')
        resp = self.client.get(experiment_add)
        self.client.login(username='test', password='123')
        payload = {u'factorSettings': [u'{"sliders":{"hardware":[{"name":"Core Count","values":["8","1","2","4","0","16"],"min":0,"max":16,"ranges":[0,16],"selected":true,"levelCount":6,"count":6},{"name":"RAM Size","values":["1553164","1024400","2072520","1813636","513528","1033684","2073932","252200","1000544","1033092","514076","1033604","2072552","489292","2073904","254044","8152440","8096712","1024016","773036","32965280","1000436","1024280"],"min":252200,"max":32965280,"ranges":[252200,32965280],"selected":false,"levelCount":23,"count":23},{"name":"Page Size","values":["4096","8192"],"min":4096,"max":8192,"ranges":[4096,8192],"selected":false,"levelCount":2,"count":2},{"name":"Clock Speed","values":["3401","1733","1800","3000","3292","2003","3200","2400","1496","295993453","1495","2411","2392","2200","2992","3291","663","295","296","2993","3412","1595"],"min":295,"max":295993453,"ranges":[295,295993453],"selected":false,"levelCount":22,"count":22}],"software":[{"name":"Memory Padding","values":["0","10928","16392","5464"],"min":0,"max":16392,"ranges":[0,16392],"selected":false,"levelCount":4,"count":4}]},"switches":{"software":[{"name":"Address Randomization"},{"name":"Cache Dropping"},{"name":"Autogroup"},{"name":"Frequency Scaling"},{"name":"Swap Partition"}]},"switchesSelected":["Cache Dropping"],"options":{"cpu_arch":["i686","armv7l","x86_64","sparc64"],"opt_flag":["-O2"],"fpie":["off"],"sys_time":["off"],"link_order":["default"]}}'], u'name': [u'abc'], u'packages-TOTAL_FORMS': [u'1'], u'packages-INITIAL_FORMS': [u'1'], u'replicate_count': [u'1'], u'packages-MAX_NUM_FORMS': [u'1000'], u'csrfmiddlewaretoken': [u'yJHtUhcxRyTY2jK36qyYBokBxQz0QlKf'], u'check': [u'Core Count', u'Cache Dropping']}
        resp = self.client.post(experiment_add, payload)
        self.assertEqual(resp.status_code, 200)
