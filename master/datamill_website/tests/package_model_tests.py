from django.test import TestCase
from datamill_website.models import Package
from datamill_common import datamill_db_helpers
from django.conf import settings
from md5hash import md5hash
import logging
import os.path

class PackageTests(TestCase):

    fixtures = [ 'initial_data', 'datamill_test_fixture' ]

    def setUp(self):
        pass

    def tearDown(self):
        datamill_db_helpers.delete_all_datamill_files()

    def test_uploaded_filename(self):
        p = Package.objects.all()[0]
        f = p.uploaded_filename()
        upstore = os.path.join(settings.MEDIA_ROOT, 'worker-package-store')
        self.assertEquals(upstore, os.path.dirname(f))

    def test_outgoing_filename(self):
        p = Package.objects.all()[0]
        f = p.outgoing_filename()
        outstore = os.path.join(settings.DATAMILL_FTP_ROOT, 'worker-package-store')
        self.assertEquals(outstore, os.path.dirname(f))

    def test_create_outgoing_file(self):
        p = Package.objects.all()[0]
        datamill_db_helpers.create_package_file(p)
        p.create_outgoing_file()
        self.assertTrue(os.path.exists(p.outgoing_filename()))
        md5hash.check_md5(p.outgoing_filename())

