from django.test import TestCase
from datamill_website.models import *
import datetime, reversion
from django.core.management import call_command

class FactorTests(TestCase):
    fixtures = [ 'initial_data', 'datamill_test_fixture' ]

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_levels_for_sw_factors(self):
        # TODO make dummy factors for this test to make them less brittle
        # discrete
        f = Factor.objects.get(internal_name="opt_flag")
        lvls = f.get_levels()
        self.assertEquals(set(lvls), set([ '-Os', '-O0', '-O1', '-O2', '-O3' ]))


        f = Factor.objects.get(internal_name="fpie")
        lvls = f.get_levels()
        self.assertEquals(set(lvls), set([ 'on', 'off' ]))


        # continuous
        f = Factor.objects.get(internal_name="env_padding")
        lvls = f.get_levels()
        self.assertEquals(set(lvls), set([ '0', '5464', '10928', '16392']))

    def test_get_levels_for_hw_factors(self):
        # TODO make dummy workers for this test to make them less brittle
        f = Factor.objects.get(internal_name="cpu_arch")
        lvls = f.get_levels()
        self.assertEquals(set(lvls), set(['arm', 'x86', 'x86_64']))

    def test_get_default(self):
        f = Factor.objects.get(internal_name="cpu_arch")
        self.assertEquals(f.get_default(), None)

        f = Factor.objects.get(internal_name="env_padding")
        self.assertEquals(f.get_default(), '0')

        f = Factor.objects.get(internal_name="fpie")
        self.assertEquals(f.get_default(), 'off')

        f = Factor.objects.get(internal_name="link_order")
        self.assertEquals(f.get_default(), 'default')

    def test_get_hardware_factor_level_with_revision(self):
        worker = Worker.objects.get(id=1)
        factor_name = 'ram_sz'
        with open(os.path.join(settings.DATAMILL_GIT_ROOT, 'master/datamill_common/files/example_hello_file.json')) as f:
            w_dict = json.load(f)
        w_dict['ram_sz'] = '1000'
        worker.fill_from_dict(w_dict)
        date = datetime.datetime.now()
        w_dict['ram_sz'] = '200'
        worker.fill_from_dict(w_dict)
        previous_level = worker.get_hardware_level(factor_name, date)
        current_level = worker.get_hardware_level(factor_name)
        self.assertEqual(previous_level, "1000")
        self.assertEqual(current_level, "200")
        self.assertNotEqual(previous_level, current_level)
