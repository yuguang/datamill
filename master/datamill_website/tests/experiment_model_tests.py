from guardian.shortcuts import assign_perm
from django.test import TestCase
from datamill_website.models import Experiment, Job, User
from datamill_common import datamill_db_helpers
import os
import tarfile
from boto.exception import NoAuthHandlerFound

class ExperimentTests(TestCase):
    fixtures = ['initial_data', 'datamill_test_fixture']

    def setUp(self):
        pass

    def tearDown(self):
        datamill_db_helpers.delete_all_datamill_files()

    def test_update_status(self):
        try:
            e = Experiment.objects.all()[0]

            e.update_status() # status moves to inprog
            self.assertEquals(e.status, Experiment.IN_PROGRESS)

            my_j = e.job_set.all()[0]
            my_j.status = Job.IN_PROGRESS
            my_j.save()

            e.update_status() # status stays in_prog
            self.assertEquals(e.status, Experiment.IN_PROGRESS)

            for j in e.job_set.all():
                # make results files since e.update_status() will tarball 'em up
                datamill_db_helpers.create_job_permanent_results_files(j)
                j.save()

            e.update_status() # status changes, all is done
            self.assertEquals(e.status, Experiment.DONE)
        except NoAuthHandlerFound:
            return

    def test_notify_creators(self):
        e = Experiment.objects.all()[0]
        u = User.objects.all()[0]
        assign_perm("view_experiment", u, e)
        success = True
        try:
            e.notify_creators_of_completion()
        except:
            success = False
        self.assertTrue(success)

    def test_make_results_package(self):
        e = Experiment.objects.all()[0]
        expected_file_list = ['{0}_results_index.csv'.format(e.id)]
        foldername = 'experiment_{}'.format(e.id)
        expected_file_list.append(foldername)

        jobs = e.job_set.all()

        for j in jobs:
            datamill_db_helpers.create_job_permanent_results_files(j)
            expected_file_list.append(os.path.basename(j.results_file))
            expected_file_list.append(os.path.basename(j.job_status_file))
            j.save()

        e.make_results_package()
        full_res_file = e.results_file

        self.assertTrue(os.path.exists(full_res_file))
        self.assertTrue(tarfile.is_tarfile(full_res_file))

        with tarfile.open(full_res_file, "r") as resfile:
            archive_files = resfile.getnames()
            archive_list = [os.path.basename(fnm) for fnm in archive_files]
            archive_index_file = filter(lambda p: 'results_index' in p, archive_files)[0]
            resfile.extract(archive_index_file)

            with open(archive_index_file, "r") as indexfile:
                indexfile_contents = indexfile.read()
                self.assertEquals(indexfile_contents,
'experiment_id,job_id,results_file,package_id,package_name,worker_id,config_id,replicate_no,setup_time,run_time,collect_time,hw_cpu_arch,hw_cpu_mhz,hw_gpu_mhz,hw_num_cpus,hw_page_sz,hw_ram_sz,sw_address_randomization,sw_autogroup,sw_compiler,sw_drop_caches,sw_env_padding,sw_filesystem,sw_fpie,sw_freq_scaling,sw_link_order,sw_opt_flag,sw_swap\r\n1,3,3.user_results.tar.gz,1,test_package.tar.gz,1,1,3,,,,arm,1000,100,2,4000,100,on,on,llvm,off,0,ext4,off,off,default,-O2,on\r\n1,2,2.user_results.tar.gz,1,test_package.tar.gz,1,1,2,,,,arm,1000,100,2,4000,100,on,on,llvm,off,0,ext4,off,off,default,-O2,on\r\n1,1,1.user_results.tar.gz,1,test_package.tar.gz,1,1,1,,,,arm,1000,100,2,4000,100,on,on,llvm,off,0,ext4,off,off,default,-O2,on\r\n')

            os.remove(archive_index_file)

        self.assertEqual(set(expected_file_list), set(archive_list))
