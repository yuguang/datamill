from datamill_website.models import Experiment, Package

from django.forms import ModelForm, Textarea
from django import forms
from django.contrib.auth.models import User

class PackageForm(forms.ModelForm):
    class Meta:
        model = Package
        exclude = ('experiment',)

class NumberInput(forms.widgets.TextInput):
    input_type = 'number'

class ExperimentForm(ModelForm):
    description = forms.CharField(widget=forms.Textarea, required=False)
    class Meta:
        model = Experiment
        fields = ['name', 'replicate_count', 'run_on_all_workers', 'tags']

    replicate_count = forms.IntegerField(
            min_value = 1,
            widget = NumberInput(attrs={'data-bind': 'value: numReplicates', 'class': 'input-mini'})
    )

class RegistrationForm(ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password']
        widgets = {
            'password': forms.PasswordInput(),
        }