
// Accordian 

// perform JavaScript after the document is scriptable.
$(function() {

	// Accordion 1
	$("#accordion1 > li > a.expanded + ul").slideToggle("medium");
	$("#accordion1 > li > a").click(function() {
	    $("#accordion1 > li > a.expanded").not(this).toggleClass("expanded").toggleClass("collapsed").parent().find('> ul').slideToggle("medium");
	    $(this).toggleClass("expanded").toggleClass("collapsed").parent().find('> ul').slideToggle("medium");
		temp = $(this).attr('rel');
		$('.constraints .pane').css('display','none');
		$('#'+temp).css('display','block');
		$('#accordion1 li').removeClass('active');
		$(this).parent().addClass('active');
		return false;
	});
	
	$("#accordion1 .secondtabs > li > a").click(function() {
		temp = $(this).attr('rel');
		$('.constraints .pane').css('display','none');
		$('#'+temp).css('display','block');
		$('#accordion1 li').removeClass('active');
		$(this).parent().addClass('active');
		return false;
	});
	
	// Accordion 2
	$("#accordion2 > li > a.expanded + ul").slideToggle("medium");
	$("#accordion2 > li > a").click(function() {
	    $("#accordion2 > li > a.expanded").not(this).toggleClass("expanded").toggleClass("collapsed").parent().find('> ul').slideToggle("medium");
	    $(this).toggleClass("expanded").toggleClass("collapsed").parent().find('> ul').slideToggle("medium");
		temp = $(this).attr('rel');
		$('.factors .pane').css('display','none');
		$('#'+temp).css('display','block');
		$('#accordion2 li').removeClass('active');
		$(this).parent().addClass('active');
		return false;
	});
	
	$("#accordion2 .secondtabs > li > a").click(function() {
		temp = $(this).attr('rel');
		$('.factors .pane').css('display','none');
		$('#'+temp).css('display','block');
		$('#accordion2 li').removeClass('active');
		$(this).parent().addClass('active');
		return false;
	});	  
	
	$('#A1pane1').css('display','block');
	$('#A2pane1').css('display','block');
});


function selectvisible(id){
	//alert(id);
}