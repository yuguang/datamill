
// Slider: This is all the JS that makes up the sliders

// This function handles all the slider aspects related to the necessary html

var Slider = (function ($) {
    
    var myslider = {};

    function updateDoubleSliderTags(name, newMin, newMax, sliderMin, sliderMax) {
        // Update the position of the slider tags
	    var minPos = (newMin) / (sliderMax - sliderMin) * 100; //percentage is (position - min) / range * 100
  	    var maxPos = (newMax) / (sliderMax - sliderMin) * 100; //percentage is (position - min) / range * 100
	    $( ".slider-count-min.slider-" + name).css('left', minPos +'%');
	    $( ".slider-count-max.slider-" + name).css('left', maxPos +'%');				
    }

    // This function updates double sided sliders
    function updateDoubleSlider( name, event, ui, sliderMin, sliderMax ) {
	    // Set form input values
	    $( "#amount-" + name + "-first" ).val( ui.values[ 0 ] );
	    $( "#amount-" + name + "-second").val( ui.values[ 1 ] );
	    
	    // set markers to have correct values in right place
	    $( ".slider-count-min.slider-" + name).html( ui.values[ 0 ] );
	    $( ".slider-count-max.slider-" + name).html( ui.values[ 1 ] );
        
        updateDoubleSliderTags(name, ui.values[0] -0, ui.values[1] -0, sliderMin, sliderMax);
    }


    function setupTwoSidedSlider(name, sliderMin, sliderMax, defaultMin, defaultMax) {
        
        var amountName = "amount-" + name;
        var amountFirst = amountName + "-first";
        var amountSecond = amountName + "-second";

        // TODO: There has to be a better way of adding static html like this.
        $( "#" + name ).first()
            .before('<input type="text" '
                    + 'id="' + amountFirst + '" '
                    + 'name="' + amountFirst + '" '
                    + 'style="border:0; color:#f6931f; font-weight:bold; display:none;" />')
            .before('<input type="text" '
                    + 'id="' + amountSecond + '" '
                    + 'name="' + amountSecond + '" '
                    + 'style="border:0; color:#f6931f; font-weight:bold; display:none;" />')
            .before('<div class="slide-min"><p>' + sliderMin + '</p>MIN</div>')
            .after('<div class="clearfloat"></div>')
            .after('<div class="slide-max"><p>' + sliderMax + '</p>MAX</div>')
            .prepend('<div class="slider-count-min slider-' + name + '" >' + defaultMin + '</div>')
            .prepend('<div class="slider-count-max slider-' + name + '" >' + defaultMax + '</div>');
        
        
	    $( "#amount-" + name + "-first" ).val( defaultMin );
	    $( "#amount-" + name + "-second").val( defaultMax );

        updateDoubleSliderTags(name, defaultMin, defaultMax, sliderMin, sliderMax);
    }

    // This function adds a slider to the document given the values provided.
    function addTwoSidedSlider(name, sliderMin, sliderMax, defaultMin, defaultMax) {
        $(function () {

            // Setup the necessary html 
            setupTwoSidedSlider(name, sliderMin, sliderMax, defaultMin, defaultMax);

            // Get object with the id name and add a slider
	        $( "#" + name ).slider({
                
                // The following is an object that holds all the data relevant
                // to the slider
		        range: true,
		        min: sliderMin,
		        max: sliderMax,
                sliderValue1: defaultMin,
                sliderValue2: defaultMax,
		        values: [ defaultMin, defaultMax ],
		        slide: function( event, ui ) {

                    // Update the slider
                    updateDoubleSlider( name, event, ui, sliderMin, sliderMax );
                    
                    // Update the slider values of this object according to
                    // what has been set by the slider ui
	                this.sliderValue1 = ui.values[ 0 ];
	                this.sliderValue2 = ui.values[ 1 ];
	                updateStats();
                }
	        });
        });
    }
    
    function updateSingleSliderTag(name, newPos, sliderMin, sliderMax) {
        var position = (newPos) / (sliderMax - sliderMin) * 100;
        $(".slider-count-min.slider-" + name).css('left', position + '%');
    }

    function updateSingleSlider(name, event, ui, sliderMin, sliderMax) {
        $("#amount-" + name).val( ui.value );
        $( ".slider-count-min.slider-" + name).html(ui.value);
        updateSingleSliderTag(name, ui.value -0, sliderMin, sliderMax);
    }
    
    function setupOneSidedSlider(name, sliderMin, sliderMax, defaultValue) {
        var amountName = "amount-" + name;
        $("#" + name).first()
            .before('<input type="text" '
                    + 'id="' + amountName +'" '
                    + 'name="' + name +'" '
                    + ' style="border:0; color:#f6931f; font-weight:bold; display:none;" /></p>')
            .before('<div class="slide-min"><p>' + sliderMin + '</p>MIN</div>')
            .after('<div class="clearfloat"></div>')
            .after('<div class="slide-max"><p>' + sliderMax + '</p>MAX</div>')
            .append('<div class="slider-count-min slider-' + name + '" >' + defaultValue + '</div>');

        $("#amount-" + name).val(defaultValue);
        updateSingleSliderTag(name, defaultValue, sliderMin, sliderMax);
    }

    function addOneSidedSlider(name, sliderMin, sliderMax, defaultValue) {
        $(function() {
            // Setup one sided slider's html
            setupOneSidedSlider(name, sliderMin, sliderMax, defaultValue);

	        $( "#" + name ).slider({
		        min: sliderMin,
		        max: sliderMax,
		        value: defaultValue,
		        slide: function( event, ui ) {
                    
                    updateSingleSlider(name, event, ui, sliderMin, sliderMax);
			        this.value = ui.value;
			        updateStats();
		        }
	        });
        });
    }

    $(function(){
	    
	    // initialize slide counters to right locations (not the right
	    // values, just location)
	    $( ".slider-count-min").css('left','15%');
	    $( ".slider-count-max").css('left','60%');				
	    

        $("#SignupForm").formToWizard({ submitButton: 'SaveAccount' });
	    
    });

    //this get's called when page is fully loaded
    // add slide counter
    myslider.addTwoSidedSlider = addTwoSidedSlider;
    myslider.addOneSidedSlider = addOneSidedSlider;
    
    return myslider;
    
}(jQuery));

