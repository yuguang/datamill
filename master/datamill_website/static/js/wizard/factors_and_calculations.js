
//TODO: This is where you calculate the estimated number of complete
// jobs from slider values.  define 4 global variables for the slider
// values

// TODO: Get rid of these globals, use the values in the document
// instead. They should be accessible through jquery like so:
// $(',slider-count-min') and then they should be mappable or whatever


// this function gets called to update the stats at the bottom of the page
function updateStats() {
    
    // 	<div class="slider-count-max slider-slider-cpu_arch" style="left: 60%;">300</div>
    // <div class="slider-count-min slider-slider-cpu_arch" style="left: 23.6%;">118</div>
    
    var getvalue = function () { return $(this).html(); };
    
    var mins = $('.slider-count-min').map(getvalue); 
    mins = mins.map(function () { return this -0; });

    var bestcase = 1;
    for(i=0,len=mins.length;i<len;i++) {
       bestcase += mins[i]; 
    }

    var worstcase = 1;
    for(i=0,len=mins.length;i<len;i++) {
       worstcase *= mins[i]; 
    }

    worstcase = worstcase / 1 ;
    
	// updates first number with new calculation
	$('#step1num1').html(Math.floor(bestcase));
	
	// updates second number with new calculation
	$('#step1num2').html(Math.floor(worstcase));	
	
};




// This is how to add two sided sliders.

// TODO: Still need to generalize the one sided slider but it should
// be easy.

