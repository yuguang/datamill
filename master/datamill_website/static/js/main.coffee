# constants
highchartsDefaults =
  colors: ["#da4b29", "#666"]
  credits:
    enabled: false
  legend:
    enabled: false

wijmoDefaults =
  captionButtons:
    refresh:
      visible: false

    pin:
      visible: false

    toggle:
      visible: false

    minimize:
      visible: false

    maximize:
      visible: false

initalize_slider_labels = ->
  $('.slider').each( ->
    $(this).parent().siblings('.min').eq(0).html($(this).wijslider("values", 0)).position
      my: "center top"
      at: "center bottom"
      of: $(this).find('a').eq(0)
      offset: "0, 10"

    $(this).parent().siblings('.max').eq(0).html($(this).wijslider("values", 1)).position
      my: "center top"
      at: "center bottom"
      of: $(this).find('a').eq(1)
      offset: "0, 10"
  )

prepare_experiment_packages = ->
  # Add experiment packages
  cloneMore = (selector, type) ->
    newElement = $(selector).clone(true)
    total = $("#id_" + type + "-TOTAL_FORMS").val()
    newElement.find(":input").each ->
      name = $(this).attr("name").replace("-" + (total - 1) + "-", "-" + total + "-")
      id = "id_" + name
      $(this).attr(
        name: name
        id: id
      ).val("").removeAttr "checked"

    newElement.find("label").each ->
      newFor = $(this).attr("for").replace("-" + (total - 1) + "-", "-" + total + "-")
      $(this).attr "for", newFor

    total++
    $("#id_" + type + "-TOTAL_FORMS").val total
    $(selector).after newElement

  $("#add_more").click ->
    cloneMore "div.add-file:last", "packages"

  $btnAddFile = $("#btn-add-file").click(->
    cloneMore "div.add-file:last", "packages"
    $btnRemoveFile.removeClass "disabled"
  )
  $btnRemoveFile = $("#btn-remove-file").click(->
    $("div.add-file:last").remove()  if $("div.add-file").length > 1
    $btnRemoveFile.addClass "disabled"  if $("div.add-file").length <= 1
  )
  $btnRemoveFile.addClass "disabled"  if $("div.add-file").length <= 1

check_architectures_for_checkboxes = ->
  $('.software input[type="checkbox"]').click( ->
    hostnames = _.filter viewModel.machines.find_workers($(this).data('factor'), [$(this).val()]), (hostname) -> _.isString(hostname)
    _.each viewModel.machines.selected_cpu_arch(hostnames), (hostname) ->
      viewModel.options.cpu_arch.push(hostname)
  )

experiment_add = ->
  require ["lib/knockout.wijmo", "lib/wijmo", "lib/popover", "lib/humansize", "lib/tipsy"], ->
    clone = document.URL.has 'clone'
    ContinuousFactor = (@name, @internal_name, values, coverage, @description) ->
      @values = values
      @min = ko.observable(Math.min.apply(null, values))
      @max = ko.observable(Math.max.apply(null, values))
      @selected = true
      if clone
        @ranges = ko.observableArray([coverage.min, coverage.max])
      else
        @ranges = ko.observableArray([@min(), @max()])
      @selectedValues = ko.computed( =>
        _.chain(@values).filter((num) =>
          num >= @ranges()[0] and num <= @ranges()[1]
        ).map((value) ->
          value.toString()
        ).value()
      )
      @slide = (event, ui) ->
        delay = ->
          handleIndex = $(ui.handle).data("index")
          label = (if handleIndex is 0 then ".min" else ".max")
          $(ui.handle).parent().parent().siblings(label).eq(0).html(ui.value).position
            my: "center top"
            at: "center bottom"
            of: ui.handle
            offset: "0, 10"
        setTimeout delay, 5
      @

    findSelectedLevels = (factor) -> _.difference factor.levels, factor.constraints

    findCoverage = (factor) ->
      selectedLevels = _.map(findSelectedLevels(factor), (level) -> parseInt(level))
      min: Math.min.apply(null, selectedLevels)
      max: Math.max.apply(null, selectedLevels)
      count: selectedLevels.length

    Machines = (features, model) ->
      @find_workers = (factor_name, levels) ->
        _.chain(levels)
        .map((level) ->
          features[factor_name][level]
        )
        .flatten()
        .uniq()
        .value()
      @selected_cpu_arch = (hostnames) ->
        _.chain(hostnames)
        .map((hostname) ->
          workers[hostname].cpu_arch
        )
        .uniq()
        .value()
      findWorkersAnyLevel = (factors) =>
        _.map factors, (factor) =>
          @find_workers(factor.internal_name, factor.selectedValues())
      findWorkersAllLevels = (factors) ->
        _.map factors, (factor) ->
          _.intersection.apply(null,
            _.map factor.selectedValues(), (level) =>
              features[factor.internal_name][level]
          )
      findWorkersByOptions = (options) ->
        _.map options, (valueAccessor, factor_internal_name) ->
          _.chain(valueAccessor())
            .map((level) =>
              features[factor_internal_name][level]
            )
            .flatten()
            .uniq()
            .value()
      @selected = ko.computed =>
        _.intersection.apply(null,
           findWorkersAnyLevel(model.sliders.hardware)
#            .concat(findWorkersAnyLevel model.sliders.software)
#            .concat(findWorkersAllLevels model.switches.software)
            .concat(findWorkersByOptions model.options)
        )
      @all = _.map(workers, (worker, hostname) -> hostname)
      @details = (hostname) ->
        workerId = workers[hostname].id
        url = "/worker/#{ workerId }"
        window.open(url, '_blank').focus()
      @

    ViewModel = ->
      @sliders = {
          hardware: _.chain(factors).filter((factor) -> factor.type is 'HW')
            .filter((factor) ->
              factor.data_category is constants.Range
            )
            .map((factor) ->
              new ContinuousFactor(factor.name, factor.internal_name, factor.levels, findCoverage(factor), factor.description)
            )
            .value(),
          software: _.chain(factors).filter((factor) -> factor.type is 'SW')
            .filter((factor) ->
              factor.data_category is constants.Range
            )
            .map((factor) ->
              new ContinuousFactor(factor.name, factor.internal_name, factor.levels, findCoverage(factor), factor.description)
            )
            .value()
      }
      @switches = {
        software: _.chain(factors).filter((factor) -> factor.type is 'SW')
          .filter((factor) ->
            factor.data_category is constants.Switch
          )
          .map((factor) =>
            name: factor.internal_name,
            selectedValues: =>
              if factor.internal_name in @switchesSelected()
                ['on', 'off']
              else
                [factor.default]
          )
          .value()
      }
      @switchesSelected = ko.observableArray(_.chain(factors)
        .filter((factor) -> factor.type is 'SW' and factor.data_category is constants.Switch and factor.constraints.length is 0)
        .map((factor) ->
          factor.internal_name
        )
        .value())
      @save = ->
        if $('.error').length
          $( "#dialog" ).wijdialog(captionButtons: wijmoDefaults.captionButtons)
        else
          $('#id_tags').val($('#id_tags').val().toLowerCase())
          $('form').submit()
      @options = {}
      _.chain(factors)
      .filter((factor) ->
        factor.data_category is constants.Checkbox
      )
      .each((factor) =>
        @options[factor.internal_name] = ko.observableArray(findSelectedLevels factor)
      )
      .value()
      @machines = new Machines(features, @)
      @architectures_text = ko.computed =>
        text_segments = []
        architectures = @machines.selected_cpu_arch(@machines.selected())
        for architecture, index in architectures
          text_segments.push architecture
          if index is architectures.length - 2
            text_segments.push ', and '
          else if index is architectures.length - 1
            # last item
          else
            text_segments.push ', '
        text_segments.join('')

      @numReplicates = ko.observable($('#id_replicate_count').val() || 2)
      @numJobs = ko.computed =>
        _.chain(@options)
          .filter (valueAccessor, factor_name) ->
            factors[factor_name].type is 'SW'
          .map (valueAccessor, factor_name) ->
            valueAccessor().length
          .reduce (memo, num) ->
            memo * num
          .value() * @numReplicates()
      @jobsStatus = ko.computed =>
        jobs = @numJobs()
        if jobs < 5
          'badge-success'
        else if jobs < 10
          'badge-warning'
        else
          'badge-important'
      @jobStatusText = ko.computed =>
        statusText =
          'badge-success': 'low'
          'badge-warning': 'medium'
          'badge-important': 'high'
        statusText[@jobsStatus()]
      @

    window.viewModel = new ViewModel()
    ko.applyBindings viewModel

    initalize_slider_labels()
    prepare_experiment_packages()
    tag_completion = new Snippets.TagCompletion()
    tag_completion.bind_listener('#id_tags')
    $(".icon-info-sign").tipsy()

job_plot_options = (failed, completed, labels) ->
  options =
      chart:
        type: "column"

      title:
        text: ""

      xAxis:
        min: 0
        title:
          text: "Date"

        categories: labels

      yAxis:
        min: 0
        title:
          text: null

        stackLabels:
          enabled: true
          style:
            fontWeight: "bold"
            color: (Highcharts.theme and Highcharts.theme.textColor) or "gray"

      legend:
        align: "right"
        x: -100
        verticalAlign: "top"
        y: 20
        floating: true
        backgroundColor: (Highcharts.theme and Highcharts.theme.legendBackgroundColorSolid) or "white"
        borderColor: "#CCC"
        borderWidth: 1
        shadow: false

      tooltip:
        formatter: ->
          "<b>" + @x + "</b><br/>" + @series.name + ": " + @y + "<br/>" + "Total: " + @point.stackTotal

      plotOptions:
        column:
          stacking: "normal"
          dataLabels:
            enabled: true
            color: (Highcharts.theme and Highcharts.theme.dataLabelsColor) or "white"

      series: [
        name: "Completed"
        data: completed
      ,
        name: "Failed"
        data: failed
      ]
  options

worker_manage = ->
  require ["lib/highcharts"], ->
    $("#container-job-status").highcharts _.defaults job_plot_options(jobs.failed, jobs.completed, categories), highchartsDefaults

experiment_detail = ->
  require ["lib/tipsy"], ->
    $(".icon-info-sign").tipsy()

  require ["lib/highcharts"], ->
    if categories.length
      $("#container-job-status").highcharts _.defaults job_plot_options(jobs.failed, jobs.completed, categories), highchartsDefaults
    else
      $("#container-job-status").append($('<div/>', class: 'center', html: 'No data available'))

    time_graph = (process_name) ->
      process_times = _.filter(window.times[process_name], (time) -> time.average)
      if process_times.length > 0
        unit = process_times[0].unit
      if window.times.chart_type is 'boxplot'
        mean_of_means = process_times.average('average')
        series = [
          name: "Time"
          data: _.map(process_times, (time) ->
                  time.percentiles
                )
        ]

        options =
          chart:
            type: "boxplot"

          title:
            text: ""

          tooltip: {
            backgroundColor: "#dbdbdb",
            formatter: ->
                "<span style=\"font-weight:bold\">Time</span><br/>Max: "+this.point.high+unit+
                "<br/>Upper Quartile: "+this.point.q3+unit+
                "<br/>Median: "+this.point.median+unit+
                "<br/>Lower Quartile: "+this.point.q1+unit+
                "<br/>Min: "+this.point.low+unit
          }

          plotOptions: {
            boxplot: {
                fillColor: '#dbdbdb',
                lineWidth: 1,
                medianColor: '#616161',
                medianWidth: 1,
                stemDashStyle: 'solid',
                stemWidth: 1,
                whiskerLength: '50%',
                whiskerWidth: 1,
                whiskerColor: 'red'
            }
          }

          marker: {
            fillColor: 'white'
            lineWidth: 1
            lineColor: Highcharts.getOptions().colors[0]
            }

          xAxis: [
            categories: _.map(process_times, (time) ->
              time.worker__hostname
            )
          ]
          yAxis:
            title:
              text: "Time ("+unit+")"
            plotLines: [
              value: mean_of_means
              color: "black"
              width: 1
            ]

          legend:
            enabled: false

          series: series

      else

        series = [
          name: "Time ("+unit+")"
          data: _.map(process_times, (time) ->
                  time.average
                )
        ]

        options =
          chart:
            type: "column"

          title:
            text: ""

          tooltip: {
            backgroundColor: "#dbdbdb",
            formatter: ->
                "<b>Time: </b>"+this.point.y+unit
          }

          xAxis: [categories: _.map(process_times, (time) ->
              time.worker__hostname
            )]
          yAxis: [ # Primary yAxis
            title:
              text: "Time ("+unit+")"
          ]

          legend:
            enabled: false

          series: series

      if process_times.length > 8
         options.xAxis[0]['labels'] =
            rotation: -45
            align: 'right'
      if process_times.length
        $("#container-#{process_name}-time").highcharts _.defaults options, highchartsDefaults
      else
        $("#container-#{process_name}-time").append($('<div/>', class: 'center', html: 'No data available'))

    for process_name in ['setup', 'run', 'collect']
      time_graph(process_name)

worker_times = ->
  require ["lib/highcharts"], ->
    round = (number) ->
      number.round(3) or number.metric()
    make_histogram = (histogram_data, name) ->
      histogram = histogram_data[0]
      bin_edges = histogram_data[1]
      times = bin_edges["times"]
      unit = bin_edges["unit"]
      _.defaults(
          title:
            text: ''

          chart:
            renderTo: "container"
            type: "column"
            width: 732

          xAxis:
            categories: _.map(times, round)
            title:
              text: "Time ("+unit+")"

            labels:
              align: 'left'
              x: -48

          yAxis:
            title:
              text: ''

          plotOptions:
            column:
              groupPadding: 0
              pointPadding: 0
              borderWidth: 0

          series:[
              name: 'frequency'
              data: histogram
            ]
        ,
          highchartsDefaults
      )

    _.each(histograms, (histogram_data, name) ->
      $('#'+name).highcharts make_histogram(histogram_data, name)
    )

job_detail = ->
  $('td').each( ->
    if $(this).html().length is 0
      $(this).parent().remove()
  )
  $('.icon-').parent().parent().remove()
  $('table').removeClass('hidden')

experiment_list = ->
  require ["lib/wijmo"], ->
    $(".experimentDialog").wijdialog
      autoOpen: false
      width: "auto"
      captionButtons: wijmoDefaults.captionButtons

      buttons:
        Ok: ->
          $form = $(this).find("form")
          model = $form.serializeArray()
          $.post $form.attr("action"), model, _.bind((response) ->
            if response.success
              $("#experimentName" + $(this).data("id")).text $form.find("[name=name]").val()
              $(this).wijdialog "close"
          , this), "json"

    $(".edit-experiment").click ->
      $("#dialog" + $(this).data("id")).wijdialog "open"

change_password = ->
  $('<div/>', html: 'Generate password', class: 'btn', id: 'generate-password').appendTo($('#id_new_password1').parent())
  $('#generate-password').click( ->
    $.getJSON('/random_password/', (json) =>
      $(this).parent().append($('<span/>', html: json.password))
      $(this).remove()
      $('#id_new_password1').val(json.password)
      $('#id_new_password2').val(json.password)
    )
  )

mapping =
  'experiments\/': experiment_list
  'experiment\/add\/': experiment_add
  'experiment\/clone\/.*\/': experiment_add
  'experiment\/.*\/': experiment_detail
  'workers\/manage\/': worker_manage
  'user\/password\/change\/': change_password
  'worker\/.*\/': worker_times
  'job\/.*\/': job_detail

for url_pattern, action of mapping
  if (new RegExp url_pattern).test(document.URL)
    do action
    break