from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.models import Group
from guardian.shortcuts import get_objects_for_user, get_objects_for_group, assign_perm, get_perms_for_model
from guardian.decorators import permission_required_or_403
from django.core.servers.basehttp import FileWrapper
from django.template import RequestContext
from django.shortcuts import redirect
from datamill_website.forms import *
from django.forms.formsets import formset_factory
from django.contrib import messages
import string, time, re, graphs
from random_password import generate_pass
from humanization import *
from django.core.mail import send_mail

import os.path
import mimetypes
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.views import login
from django.http import HttpResponseForbidden
from jsonresponse import JsonResponse
import json
from django.db.models import Avg
from django.core import serializers
import numpy
from datamill_django_project.settings import *

from django.utils.translation import ugettext as _
from django.db.models import Count
from sendfile import sendfile
# LANDING, ABOUT, ETC
def home(request):
    everyone = Group.objects.filter(name='everyone').get()
    exps = get_objects_for_group(everyone, 'datamill_website.view_experiment', klass=Experiment).order_by('-submission_date').filter(deleted=False)
    return login(request, 'home.html', extra_context={ 'public_experiments' : exps, 'news': News.objects.order_by('-day') })

def about(request):
    return render_to_response('about.html', context_instance=RequestContext(request))

def sponsors(request):
    return render_to_response('sponsors.html', context_instance=RequestContext(request))

def institutions(request):
    return render_to_response('institutions.html', context_instance=RequestContext(request))

def random_password(request):
    return JsonResponse({'password': generate_pass()})

def paginate(request, query_set, limit=25):
    paginator = Paginator(query_set, limit)

    page = request.GET.get('page')
    try:
        query_set = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        query_set = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        query_set = paginator.page(paginator.num_pages)
    return query_set

# LISTS
def job_list(request):
    order_by = request.GET.get('order_by', '-experiment')
    jobs = paginate(request, get_objects_for_user(request.user, 'datamill_website.change_job', klass=Job).filter(deleted=False).order_by(order_by))
    return render_to_response('datamill_website/job_list.html', {'job_list': jobs, 'order_by': order_by} , context_instance=RequestContext(request))

def experiment_list(request, warning=''):
    order_by = request.GET.get('order_by', '-submission_date')
    experiments = paginate(request, get_objects_for_user(request.user, 'datamill_website.change_experiment', klass=Experiment).filter(deleted=False).order_by(order_by))
    return render_to_response('datamill_website/experiment_list.html', { 'experiment_list' : experiments, 'order_by': order_by, 'warning': warning }, context_instance=RequestContext(request))

def worker_monitor(request):
    recently_dead_workers = Worker.objects.filter(status=Worker.DEAD).order_by('-last_update')[:10]
    recently_failed_jobs = Job.objects.filter(status=Job.FAILED).order_by('-last_update')[:200]

    failed_jobs_per_day = []
    completed_jobs_per_day = []
    dates = []
    today = datetime.now()
    for day in range(30, 0, -1):
        end_date = today - timedelta(day)
        start_date = today - timedelta(day + 1)
        num_failed = Job.objects.filter(last_update__range=(start_date, end_date), status=Job.FAILED).count()
        num_completed = Job.objects.filter(last_update__range=(start_date, end_date), status=Job.DONE).count()
        dates.append(start_date.strftime("%m-%d"))
        failed_jobs_per_day.append(num_failed)
        completed_jobs_per_day.append(num_completed)
    jobs = {
        'failed': failed_jobs_per_day,
        'completed': completed_jobs_per_day,
        'labels': dates
    }

    return render_to_response('datamill_website/worker_manage.html', { 'worker_list' : recently_dead_workers, 'job_list': recently_failed_jobs, 'jobs': jobs }, context_instance=RequestContext(request))

def worker_list(request, warning=''):
    order_by = request.GET.get('order_by', 'hostname')
    workers = paginate(request, Worker.objects.available().order_by(order_by))
    return render_to_response('datamill_website/worker_list.html', {'worker_list': workers, 'order_by': order_by, 'warning': warning} , context_instance=RequestContext(request))

def register_worker(request):
    worker_ids = request.POST.getlist('workers')
    for worker_id in worker_ids:
        worker = Worker.objects.get(id=int(worker_id))
        worker.user = request.user
        worker.save()
    return redirect('workers')

def new_worker_list(request):
    if Worker.objects.filter(user__id=DEFAULT_WORKER_OWNER_ID).count() == 0:
        return worker_list(request, NO_NEW_WORKER_WARNING)
    order_by = request.GET.get('order_by', '-last_update')
    workers = paginate(request, Worker.objects.available().filter(user__id=DEFAULT_WORKER_OWNER_ID).order_by(order_by))
    return render_to_response('datamill_website/worker_list.html', {'worker_list': workers, 'order_by': order_by, 'worker_claim': True} , context_instance=RequestContext(request))

# DETAILS
@permission_required_or_403('datamill_website.view_experiment', (Experiment, 'pk', 'pk'))
def experiment_detail(request, pk):
    experiment = get_object_or_404(Experiment, pk=pk)
    if experiment.deleted:
        raise Http404
    failed_jobs_per_day = []
    completed_jobs_per_day = []
    dates = []
    day = 0
    today = datetime.now()
    while True:
        end_date = experiment.submission_date + timedelta(day + 1)
        start_date = experiment.submission_date + timedelta(day)
        num_failed = experiment.job_set.filter(last_update__range=(start_date, end_date), status=Job.FAILED).count()
        num_completed = experiment.job_set.filter(last_update__range=(start_date, end_date), status=Job.DONE).count()
        day = day + 1
        if not num_failed + num_completed == 0:
            dates.append(start_date.strftime("%m-%d"))
            failed_jobs_per_day.append(num_failed)
            completed_jobs_per_day.append(num_completed)
        if end_date > today or (experiment.completion_date is not None and end_date > experiment.completion_date):
            break

    jobs = {
        'failed': failed_jobs_per_day,
        'completed': completed_jobs_per_day,
        'labels': dates
    }
    def worker_time(time_field):
        worker_times = experiment.job_set.values('worker__hostname', 'experiment__id').order_by('worker__hostname').annotate(average=Avg(time_field))
        worker_times = humanize_worker_times(worker_times, time_field)
        return list(worker_times)
    times = {}
    for process_name in ['setup', 'run', 'collect']:
        times[process_name] = worker_time(process_name + '_time')
    if experiment.replicate_count > 2:
        chart_type = 'boxplot'
    else:
        chart_type = 'column'

    times['chart_type'] = chart_type
    # print times
    return render_to_response('datamill_website/experiment_detail.html', { 'experiment' : experiment, 'request': request, 'done': Experiment.DONE, 'jobs': jobs, 'times': json.dumps(times), 'job_list': paginate(request, experiment.job_set.all()) }, context_instance=RequestContext(request))

def commonsuffix(string_list):
    string_list = map(lambda x: x[::-1], string_list)
    return os.path.commonprefix(string_list)[::-1]

@permission_required_or_403('datamill_website.view_job', (Job, 'pk', 'pk'))
def job_detail(request, pk):
    job = get_object_or_404(Job, pk=pk)
    output_job_status = {}
    status_file = job.permanent_job_status_filename()
    if os.path.isfile(status_file):
        f = open(status_file, 'r')
        input_job_status = json.load(f)
        f.close()
        output_job_status = {
            'verify': {},
            'general': {},
            'output': {},
            'worker_common': {},
            'success': {}
        }
        job_configuration = job.get_configuration().items()[2][1].items()
        for key, value in input_job_status.iteritems():
            if key in ['get_time', 'finish_time']:
                value = datetime.fromtimestamp(value)
            elif key.endswith("_time"):
                #beautifying the times
                value = get_humanized_time(value)
            if key.endswith('_verify') and Factor.objects.filter(internal_name=key.replace('_verify', '')).exists():
                category = 'verify'
                try:
                    tempindex = [y[0] for y in job_configuration].index(key.partition('_verify')[0])
                    value = (value, job_configuration[tempindex][1])
                except:
                    value = (value, u'Not Found')
                key = _(Factor.objects.get(internal_name=key.replace('_verify', '')).internal_name)
            elif key.startswith('worker_common'):
                category = 'worker_common'
            elif key.endswith('_output'):
                category = 'output'
            elif key.endswith('_success'):
                category = 'success'
            else:
                category = 'general'
            # worker_common times
            key = re.sub(r'(do_|_factor_levels)', '', key)
            output_job_status[category][key] = value
        for category, dictionary in output_job_status.iteritems():
            dictionary = rename_keys(dictionary)

    return render_to_response('datamill_website/job_detail.html', { 'job' : job, 'job_status': output_job_status }, context_instance=RequestContext(request))

def job_status_log(request, pk, log_name):
    job = get_object_or_404(Job, pk=pk)
    status_file = job.permanent_job_status_filename()
    if os.path.isfile(status_file):
        f = open(status_file, 'r')
        input_job_status = json.load(f)
        f.close()
        response = HttpResponse(input_job_status["job_do_{}_output".format(log_name)], content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename={}.log'.format(log_name)
    else:
        response = HttpResponse('<h1>Not Found</h1><p>The requested file was not found on this server.</p>')
    return response

def rename_keys(dictionary):
    prefix = os.path.commonprefix(dictionary.keys())
    suffix = commonsuffix(dictionary.keys())
    for key, value in dictionary.iteritems():
        del dictionary[key]
        dictionary[key.replace(prefix, '').replace(suffix, '')] = value
    return dictionary

def cancel_worker_jobs(request, pk):
    worker = get_object_or_404(Worker, pk=pk)
    for job in worker.job_set.filter(status__in=[ Job.IN_PROGRESS, Job.QUEUED ]):
        job.cancel()
    return worker_detail(request, pk)

def create_account(request):
    success = False
    if not request.method == 'POST':
        form = RegistrationForm()
    else:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            success = True
            user, created = User.objects.get_or_create(first_name=form.cleaned_data['first_name'], last_name=form.cleaned_data['last_name'], email=form.cleaned_data['email'], username=form.cleaned_data['email'])
            user.is_active = False
            user.set_password(form.cleaned_data['password'])
            admin_emails = [user_tuple[1] for user_tuple in settings.ADMINS]
            send_mail('New Datamill User Registration', """Dear admin,

        {} {} from {} has signed up for a new Datamill account with email {}. Please login to https://datamill.uwaterloo.ca/admin/ to create an account for the user.
        """.format(form.cleaned_data['first_name'], form.cleaned_data['last_name'], form.cleaned_data['inst'], form.cleaned_data['email']),
                    settings.DATAMILL_EMAIL,
                    admin_emails)
    return render_to_response('registration/create_account.html', {'form': form, 'success': success}, context_instance=RequestContext(request))

def worker_detail(request, pk):
    worker = get_object_or_404(Worker, pk=pk)
    worker_settings = {}
    for factor in ['num_cpus', 'cpu_mhz', 'ram_sz', 'page_sz', 'cpu_arch']:
        value = worker.get_hardware_level(factor)
        if factor == "ram_sz":
            value = humanize_size(int(value)*1024)
        elif factor == "page_sz":
            value = humanize_size(int(value))
        worker_settings[factor] = value
    jobs = get_objects_for_user(request.user, 'datamill_website.view_job', klass=Job).filter(worker=worker)
    histograms = rename_keys(graphs.worker_time_histograms(worker))
    return render_to_response('datamill_website/worker_detail.html', { 'worker' : worker, 'job_list' : paginate(request, jobs, 10), 'worker_settings': worker_settings, 'histograms' : json.dumps(histograms), 'histogram_dict': histograms, 'admin': request.user.is_superuser, }, context_instance=RequestContext(request))

def save_scope(experiment):
    for setting in WorkerFeature.objects.available():
        scope = Scope(experiment=experiment, level=setting.level)
        scope.save()

def set_default_level(factor, experiment):
    for level in factor.level_set.filter(default=True):
        Constraint(experiment=experiment, level=level).save()

def save_factors(experiment, settings):
    save_scope(experiment)
    slider_factors = settings['sliders']['hardware'] + settings['sliders']['software']
    for factor in slider_factors:
        db_factor = Factor.objects.get(internal_name=factor['internal_name'])
        # save levels not selected or outside of range to constraints
        for level in db_factor.level_set.all():
            # if level is greater than the max or less than the min
            if not factor['selected'] or int(level.internal_name) < factor['ranges'][0] or int(level.internal_name) > factor['ranges'][1]:
                constraint = Constraint(experiment=experiment, level=level)
                constraint.save()
    # loop through software switch factors, which only have "on, off"
    switch_factors = settings['switches']['software']
    for factor in switch_factors:
        db_factor = Factor.objects.get(internal_name=factor['internal_name'])
        if not db_factor.internal_name in settings['switchesSelected']:
            set_default_level(db_factor, experiment)
    for name, levels in settings['options'].iteritems():
        db_factor = Factor.objects.get(internal_name=name)
        if 'default' in levels:
            levels = levels + [db_factor.get_default()]
        # find the difference between the levels for the factor in the scope and the selected levels and save in constraints
        constraint_levels = list(set(db_factor.get_levels()) - set(levels))
        for level_value in constraint_levels:
            constraint = Constraint(experiment=experiment, level=Level.objects.get(internal_name=level_value, factor=db_factor))
            constraint.save()

def cloning_previous_packages(request):
    return request.POST.getlist('previous_packages')

def is_user_experiment_limit_exceeded(request):
        #exceptions: superusers & users who have permission to submit unlimited experiments
        if (request.user.is_superuser and request.user.has_perm("datamill_website.unlimited_experiment")) == False:
            if get_objects_for_user(request.user, 'datamill_website.change_experiment', klass=Experiment)\
                .filter(deleted=False, status__in=[ Experiment.SCHEDULED, Experiment.CREATED ]).count()\
                    > Worker.objects.filter(user=request.user).exclude(status=Worker.DEAD).count() \
                            * EXPERIMENTS_ALLOWED_PER_WORKER_CONTRIBUTED:
                return True
        return False

def construct_packageset(experiment):
    PackageFormSet = formset_factory(PackageForm, extra=0)
    packageset = PackageFormSet(prefix='packages', initial=[{'file': ''}])
    previous_packages = [ package for package in experiment.package_set.all() ]
    return packageset, previous_packages

# EXPERIMENT WIZARD
@transaction.commit_on_success
def experiment_add(request, pk=None):

    #raise error if user experiment limit exceeded
    if is_user_experiment_limit_exceeded(request):
        return experiment_list(request, EXPERIMENT_EXCEED_WARNING)

    PackageFormSet = formset_factory(PackageForm, extra=0)
    previous_packages = []

    if request.method == 'POST':
        experiment = Experiment()
        experiment_form = ExperimentForm(instance=experiment, data=request.POST)

        PackageFormSet = formset_factory(PackageForm, extra=0)
        packageset = PackageFormSet(request.POST, request.FILES, prefix='packages') # A form bound to the POST data

        factor_settings = request.POST.get('factorSettings', None)
        if not factor_settings:
            return experiment_list(request, INVALID_EXPERIMENT_SUBMISSION)
        if experiment_form.is_valid() and (packageset.is_valid() or cloning_previous_packages(request)):
            experiment_form.save()
            save_factors(experiment, json.loads(factor_settings))

            # make the user have every permission on the new experiment
            exp_perms = [ p.codename for p in get_perms_for_model(Experiment) ]

            for perm in exp_perms:
                assign_perm(perm, request.user, experiment)
            # Iterate over submitted packages
            for package_form in packageset.forms:
                package_form.clean()
                package_file = package_form.cleaned_data.get('file')
                if package_file:
                    new_package = Package.objects.create(
                        file = package_file,
                        experiment = experiment
                    )

                    new_package.save()

            # Clone previous packages
            if pk is not None:
                package_ids = request.POST.getlist('previous_packages')
                for id in package_ids:
                    new_package = Package.objects.create(
                        file = Package.objects.get(id=id).file,
                        experiment = experiment
                    )
                    new_package.save()

            return HttpResponseRedirect('/experiment/{}'.format(experiment.id)) # Redirect to the newly created experiment
        elif pk is not None:
            previous_experiment = get_object_or_404(Experiment, pk=pk)
            packageset, previous_packages = construct_packageset(previous_experiment)

    else:

        # Clone previous package
        if pk is not None:
            experiment = get_object_or_404(Experiment, pk=pk)
            experiment.name = "Clone of "+experiment.name
            experiment_form = ExperimentForm(instance=experiment)
            packageset, previous_packages = construct_packageset(experiment)
        else:
            # make unbound forms
            experiment_form = ExperimentForm()
            packageset = PackageFormSet(prefix='packages', initial=[{'file': ''}])

    factors = {}
    software_choices = {
        'known': Factor.objects.filter(fac_type=Factor.SOFTWARE, data_category=Factor.DISCRETE, internal_name__in=['compiler', 'opt_flag', 'freq_scaling']),
        'hidden': Factor.objects.filter(fac_type=Factor.SOFTWARE, data_category=Factor.DISCRETE).exclude(internal_name__in=['compiler', 'opt_flag', 'freq_scaling'])
    }
    hardware_choices = Factor.objects.filter(fac_type=Factor.HARDWARE, data_category=Factor.DISCRETE)
    for db_factor in Factor.objects.all():
        factor = factors[db_factor.internal_name] = json.loads(serializers.serialize('json', (db_factor,)))[0]['fields']
        factor['name'] = _(db_factor.internal_name)
        factor['default'] = db_factor.get_default()
        factor['levels'] = db_factor.get_levels()
        factor['constraints'] = form_constraints(db_factor)
        factor['type'] = db_factor.fac_type
        if pk is not None:
            experiment = get_object_or_404(Experiment, pk=pk)
            factor['constraints'] = [ constraint.level.internal_name for constraint in Constraint.objects.filter(experiment=experiment, level__factor=db_factor) ]

    features = {}
    for feature in WorkerFeature.objects.exclude(worker__status=Worker.DEAD):
        level = feature.level
        factor_name = level.factor.internal_name
        if factor_name in features:
            if level.internal_name in features[factor_name]:
                features[factor_name][level.internal_name].append(feature.worker.hostname)
            else:
                features[factor_name][level.internal_name] = [feature.worker.hostname]
        else:
            features[factor_name] = {level.internal_name: [feature.worker.hostname]}

    return render_to_response('datamill_website/experiment_add.html', {
        'experiment_form': experiment_form,
        'packageset': packageset,
        'previous_packages': previous_packages,
        'factors': json.dumps(factors),
        'software_choices': software_choices,
        'hardware_choices': hardware_choices,
        'constants': json.dumps(dict((v,k) for k, v in dict(Factor.DATA_CHOICES).iteritems())),
        'features': json.dumps(features),
        'workers': serialize_workers(Worker.objects.available()),
        'admin': request.user.is_superuser,
    }, context_instance=RequestContext(request))

def tag_hint(request):
    q = request.GET.get('q', '')
    results = []
    if len(q) > 0:
        tag_qs = Experiment.tags.filter(name__startswith=q)
        annotated_qs = tag_qs.annotate(count=Count('taggit_taggeditem_items__id'))

        for obj in annotated_qs.order_by('-count', 'slug')[:10]:
            results.append({
                'tag': obj.slug,
                'count': obj.count,
            })

    return HttpResponse(json.dumps(results), mimetype='application/json')

def serialize_workers(workers):
    worker_dict = {}
    for worker in workers:
        worker_dict[worker.hostname] = {'id': worker.id }
        for prop in ['cpu_arch', 'cpu_mhz', 'ram_sz', 'num_cpus']:
            worker_dict[worker.hostname][prop] = worker.get_hardware_level(prop)
    return json.dumps(worker_dict)

def form_constraints(factor):
        if factor.internal_name == 'cpu_arch':
            return []
        elif factor.data_category == Factor.BOOLEAN:
            return ['on', 'off']
        else:
            return list(factor.level_set.filter(default=False).values_list('internal_name', flat=True))

def experiment_save(request, pk=None):
    experiment = get_object_or_404(Experiment, pk=pk)
    if not request.is_ajax():
        return HttpResponseForbidden()
    form = ExperimentForm(instance=experiment, data=request.POST)
    if form.is_valid():
        form.save()
    return JsonResponse({'success': True})


# DOWNLOADS

def FileResponse(file_path):
    if os.path.isfile(file_path):
        filename = os.path.basename(file_path)
        response = HttpResponse(FileWrapper(open(file_path)),
                                          content_type=mimetypes.guess_type(file_path)[0])

        response['Content-Length'] = os.path.getsize(file_path)
        response['Content-Disposition'] = "attachment; filename=%s" % filename
    else:
        response = HttpResponse('<h1>Not Found</h1><p>The requested file was not found on this server.</p>')
    return response

@permission_required_or_403('datamill_website.view_experiment', (Experiment, 'pk', 'pk'))
def experiment_results(request, pk):
    my_exp = get_object_or_404(Experiment, pk=pk)

    if my_exp.status != Experiment.DONE: # when done we make a final pkg and never remake
        my_exp.make_results_package()
        fullpath = my_exp.results_file
        return sendfile(request, fullpath, attachment=True, attachment_filename=os.path.basename(fullpath))

    location = my_exp.finished_results_file.url
    response = HttpResponse(location, status=302)
    response['Location'] = location
    return response

@permission_required_or_403('datamill_website.view_job', (Job, 'pk', 'pk'))
def job_results(request, pk):
    my_job = get_object_or_404(Job, pk=pk)
    fullpath = my_job.results_file

    if my_job.status != Job.DONE or not os.path.exists(fullpath):
        raise Http404

    return FileResponse(fullpath)

@permission_required_or_403('datamill_website.view_job', (Job, 'pk', 'pk'))
def job_status(request, pk):
    my_job = get_object_or_404(Job, pk=pk)
    fullpath = my_job.job_status_file

    if my_job.status != Job.DONE or not os.path.exists(fullpath):
        raise Http404

    return FileResponse(fullpath)

@permission_required_or_403('datamill_website.view_package', (Package, 'pk', 'pk'))
def package(request, pk):
    my_package = get_object_or_404(Package, pk=pk)

    return FileResponse(my_package.uploaded_filename())

# ACTIONS
@permission_required_or_403('datamill_website.change_experiment', (Experiment, 'pk', 'pk'))
def experiment_publish(request, pk):
    my_exp = get_object_or_404(Experiment, pk=pk)
    my_exp.publish()

    messages.info(request, 'This experiment is now public.')

    return HttpResponseRedirect('/experiment/{}'.format(my_exp.id))

@permission_required_or_403('datamill_website.change_experiment', (Experiment, 'pk', 'pk'))
def experiment_make_private(request, pk):
    my_exp = get_object_or_404(Experiment, pk=pk)
    my_exp.make_private()

    messages.info(request, 'This experiment is now private.')

    return HttpResponseRedirect('/experiment/{}'.format(my_exp.id))

@permission_required_or_403('datamill_website.change_experiment', (Experiment, 'pk', 'pk'))
def experiment_cancel(request, pk):
    my_exp = get_object_or_404(Experiment, pk=pk)
    num_cancelled = my_exp.cancel()

    messages.info(request, 'Cancelled {} jobs.'.format(num_cancelled))

    return HttpResponseRedirect('/experiment/{}'.format(my_exp.id))

@permission_required_or_403('datamill_website.change_experiment', (Experiment, 'pk', 'pk'))
def experiment_delete(request, pk):
    my_exp = get_object_or_404(Experiment, pk=pk)
    my_exp.deleted = True
    my_exp.save()
    for job in my_exp.job_set.all():
        job.deleted = True
        job.save()
    my_exp.cancel()
    return experiment_list(request)

@permission_required_or_403('datamill_website.change_experiment', (Experiment, 'pk', 'pk'))
def experiment_retry(request, pk):
    my_exp = get_object_or_404(Experiment, pk=pk)
    num_retried, num_failed_to_retry = my_exp.retry_failed_jobs()

    if num_failed_to_retry:
        messages.info(request,
                'Retrying {} out of {} failed jobs. {} jobs could not be retried because their associated workers are unavailable.'.format(num_retried, num_retried + num_failed_to_retry, num_failed_to_retry))
    else:
        messages.info(request, 'Retrying {} failed jobs.'.format(num_retried))

    return HttpResponseRedirect('/experiment/{}'.format(my_exp.id))
