import os, numpy
from humanization import *

def array_to_list(tuples):
    return map(lambda t: list(t), tuples)

def int64_to_int(list):
    return map(lambda x: int(x), list)

def worker_time_histograms(worker):

    def humanize_time_list(time_list):
        times = ["s", "min", "h"]
        unit_index = 0
        for time in time_list:
            index = find_time_unit(time)
            if index > unit_index:
                unit_index = index
        for index,time in enumerate(time_list):
            time_list[index] = humanize_time(time, unit_index)
        return {"times": time_list, "unit": get_unit_name(unit_index)}

    status_time_keys = [u'worker_common.worker_configure_benchmark_side_time',
 u'worker_common.worker_untar_backup_time',
 u'worker_common.worker_fix_benchmark_fstab_time',
 u'worker_common.worker_get_job_time',
 u'worker_common.worker_register_time',
 u'worker_common.worker_create_backup_time',
 u'worker_common.worker_upload_pending_results_time',
 u'worker_common.worker_create_untarrable_files_time',
 u'worker_common.worker_fix_etc_portage_time',
 u'worker_common.worker_fix_etc_profile_time',
 u'worker_common.worker_populate_benchmark_partition_time']
    # get the most recent 15 jobs for worker
    jobs = worker.job_set.order_by('last_update')[:15]
    times = {}
    # loop through job worker status jsons
    for worker_status in [json.load(open(job.permanent_worker_status_filename(), 'r')) for job in jobs if os.path.isfile(job.permanent_worker_status_filename())]:
        for name, measurement in worker_status.iteritems():
            # make a dictionary of lists with the time name as key
            if name.endswith('_time') and name in status_time_keys:
                times[name] = times.get(name, [])
                # gather entries that end with _time into lists
                times[name].append(measurement)
    # find the histograms
    histograms = {}
    for name, measurements in times.items():
        histograms[name] = array_to_list(numpy.histogram(measurements))
        histograms[name][0] = int64_to_int(histograms[name][0])
        histograms[name][1] = humanize_time_list(histograms[name][1])
    return histograms