from django.contrib import admin
from datamill_website.models import Experiment, Job, Worker, Package, Factor, WorkerFeature, News, Level
from guardian.admin import GuardedModelAdmin
import reversion

class ExperimentAdmin(GuardedModelAdmin):
    pass
admin.site.register(Experiment, ExperimentAdmin)

class JobAdmin(GuardedModelAdmin):
    pass
admin.site.register(Job, JobAdmin)

class WorkerAdmin(reversion.VersionAdmin, GuardedModelAdmin):
    pass
admin.site.register(Worker, WorkerAdmin)

class PackageAdmin(GuardedModelAdmin):
    pass
admin.site.register(Package, PackageAdmin)

class FactorAdmin(GuardedModelAdmin):
    pass
admin.site.register(Factor, FactorAdmin)

class WorkerFeatureAdmin(reversion.VersionAdmin, GuardedModelAdmin):
    pass
admin.site.register(WorkerFeature, WorkerFeatureAdmin)

class LevelAdmin(GuardedModelAdmin):
    pass
admin.site.register(Level, LevelAdmin)

class NewsAdmin(GuardedModelAdmin):
    pass
admin.site.register(News, NewsAdmin)


