# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Level'
        db.create_table(u'datamill_website_level', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('factor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Factor'])),
            ('default', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('obsolete', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'datamill_website', ['Level'])

        # Adding unique constraint on 'Level', fields ['name', 'factor']
        db.create_unique(u'datamill_website_level', ['name', 'factor_id'])

        # Adding model 'Coverage'
        db.create_table(u'datamill_website_coverage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('experiment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Experiment'])),
            ('factor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Factor'])),
            ('count', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('min', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('max', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'datamill_website', ['Coverage'])

        # Adding model 'Scope'
        db.create_table(u'datamill_website_scope', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('experiment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Experiment'])),
            ('level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Level'])),
        ))
        db.send_create_signal(u'datamill_website', ['Scope'])

        # Adding model 'Constraint'
        db.create_table(u'datamill_website_constraint', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('experiment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Experiment'])),
            ('level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Level'])),
        ))
        db.send_create_signal(u'datamill_website', ['Constraint'])

        # Adding model 'Registry'
        db.create_table(u'datamill_website_registry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('worker', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Worker'])),
            ('factor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Factor'])),
            ('level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Level'])),
        ))
        db.send_create_signal(u'datamill_website', ['Registry'])


        # Changing field 'Job.last_update'
        db.alter_column(u'datamill_website_job', 'last_update', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))
        # Deleting field 'Factor.dim_type'
        db.delete_column(u'datamill_website_factor', 'dim_type')

        # Adding field 'Factor.display'
        db.add_column(u'datamill_website_factor', 'display',
                      self.gf('django.db.models.fields.CharField')(default='SH', max_length=2),
                      keep_default=False)


    def backwards(self, orm):
        # Removing unique constraint on 'Level', fields ['name', 'factor']
        db.delete_unique(u'datamill_website_level', ['name', 'factor_id'])

        # Deleting model 'Level'
        db.delete_table(u'datamill_website_level')

        # Deleting model 'Coverage'
        db.delete_table(u'datamill_website_coverage')

        # Deleting model 'Scope'
        db.delete_table(u'datamill_website_scope')

        # Deleting model 'Constraint'
        db.delete_table(u'datamill_website_constraint')

        # Deleting model 'Registry'
        db.delete_table(u'datamill_website_registry')


        # Changing field 'Job.last_update'
        db.alter_column(u'datamill_website_job', 'last_update', self.gf('django.db.models.fields.DateTimeField')(auto_now=True))
        # Adding field 'Factor.dim_type'
        db.add_column(u'datamill_website_factor', 'dim_type',
                      self.gf('django.db.models.fields.CharField')(default='DI', max_length=2),
                      keep_default=False)

        # Deleting field 'Factor.display'
        db.delete_column(u'datamill_website_factor', 'display')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'datamill_website.constraint': {
            'Meta': {'object_name': 'Constraint'},
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Level']"})
        },
        u'datamill_website.continuous_factor_range': {
            'Meta': {'object_name': 'Continuous_Factor_Range'},
            'default': ('django.db.models.fields.IntegerField', [], {}),
            'factor': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['datamill_website.Factor']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.IntegerField', [], {}),
            'min': ('django.db.models.fields.IntegerField', [], {}),
            'steps': ('django.db.models.fields.IntegerField', [], {})
        },
        u'datamill_website.coverage': {
            'Meta': {'object_name': 'Coverage'},
            'count': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'min': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'datamill_website.discrete_factor_level': {
            'Meta': {'object_name': 'Discrete_Factor_Level'},
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'datamill_website.experiment': {
            'Meta': {'object_name': 'Experiment'},
            'completion_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'constraint_spec': ('django.db.models.fields.TextField', [], {}),
            'coverage_spec': ('django.db.models.fields.TextField', [], {}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '750', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'num_hardware_factors': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'num_software_factors': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'replicate_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'results_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/yuguang/PycharmProjects/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*\\\\.tar$'", 'blank': 'True'}),
            'scope_spec': ('django.db.models.fields.TextField', [], {}),
            'send_confirmation_alert': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'CR'", 'max_length': '2'}),
            'submission_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'datamill_website.factor': {
            'Meta': {'object_name': 'Factor'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'display': ('django.db.models.fields.CharField', [], {'default': "'SH'", 'max_length': '2'}),
            'fac_type': ('django.db.models.fields.CharField', [], {'default': "'HW'", 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'datamill_website.job': {
            'Meta': {'object_name': 'Job'},
            'config': ('django.db.models.fields.TextField', [], {}),
            'config_id': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Package']"}),
            'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'replicate_no': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'results_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/yuguang/PycharmProjects/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*\\\\.tar\\\\.gz$'", 'blank': 'True'}),
            'stats_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/yuguang/PycharmProjects/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*\\\\.csv$'", 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'QU'", 'max_length': '2'}),
            'worker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Worker']"})
        },
        u'datamill_website.level': {
            'Meta': {'unique_together': "(('name', 'factor'),)", 'object_name': 'Level'},
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'obsolete': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'datamill_website.package': {
            'Meta': {'object_name': 'Package'},
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'datamill_website.registry': {
            'Meta': {'object_name': 'Registry'},
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Level']"}),
            'worker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Worker']"})
        },
        u'datamill_website.scope': {
            'Meta': {'object_name': 'Scope'},
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Level']"})
        },
        u'datamill_website.worker': {
            'Meta': {'object_name': 'Worker'},
            'cpu_arch': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'cpu_flags': ('django.db.models.fields.TextField', [], {}),
            'cpu_mhz': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'cpu_model': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'num_cpus': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'page_sz': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ram_sz': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'ID'", 'max_length': '2'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['auth.User']"}),
            'uuid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '36'})
        }
    }

    complete_apps = ['datamill_website']