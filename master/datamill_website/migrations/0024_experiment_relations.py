# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models
from datamill_website.models import *
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

class Migration(DataMigration):

    def forwards(self, orm):
        if not orm.Experiment.objects.count():
            return
        for worker in orm.Worker.objects.all():
            factor= orm.Factor.objects.get(internal_name='num_cpus')
            level, created = orm.Level.objects.get_or_create(name=str(worker.num_cpus), factor=factor, default=False, obsolete=False)
            entry = orm.Registry(worker=worker, factor=factor, level=level)
            entry.save()

            factor= orm.Factor.objects.get(internal_name='cpu_mhz')
            level, created = orm.Level.objects.get_or_create(name=str(worker.cpu_mhz), factor=factor, default=False, obsolete=False)
            entry = orm.Registry(worker=worker, factor=factor, level=level)
            entry.save()

            factor= orm.Factor.objects.get(internal_name='ram_sz')
            level, created = orm.Level.objects.get_or_create(name=str(worker.ram_sz), factor=factor, default=False, obsolete=False)
            entry = orm.Registry(worker=worker, factor=factor, level=level)
            entry.save()

            factor= orm.Factor.objects.get(internal_name='page_sz')
            level, created = orm.Level.objects.get_or_create(name=str(worker.page_sz), factor=factor, default=False, obsolete=False)
            entry = orm.Registry(worker=worker, factor=factor, level=level)
            entry.save()

            factor= orm.Factor.objects.get(internal_name='cpu_arch')
            level, created = orm.Level.objects.get_or_create(name=worker.cpu_arch, factor=factor, default=False, obsolete=False)
            entry = orm.Registry(worker=worker, factor=factor, level=level)
            entry.save()

        for experiment in orm.Experiment.objects.all():
            coverage = eval(experiment.coverage_spec)
            for factor in coverage:
                try:
                    orm.Coverage(experiment=experiment, factor= orm.Factor.objects.get(internal_name=factor), min=coverage[factor]['min'], max=coverage[factor]['max'], count=coverage[factor]['count']).save()
                except TypeError:
                    orm.Coverage(experiment=experiment, factor= orm.Factor.objects.get(internal_name=factor)).save()
            constraint = eval(experiment.constraint_spec)
            for factor in constraint:
                for level in constraint[factor]:
                    # skip default level for compiler
                    if str(level) == 'default' and factor == 'compiler':
                        continue
                    level, created = orm.Level.objects.get_or_create(name=str(level), factor= orm.Factor.objects.get(internal_name=factor), default=False, obsolete=False)
                    orm.Constraint(experiment=experiment, level=level).save()
            scope = eval(experiment.scope_spec)
            for factor in scope:
                for level in scope[factor]:
                    # skip default level for compiler
                    if str(level) == 'default' and factor == 'compiler':
                        continue
                    level, created = orm.Level.objects.get_or_create(name=str(level), factor= orm.Factor.objects.get(internal_name=factor), default=False, obsolete=False)
                    orm.Scope(experiment=experiment, level=level).save()

        levels = [orm.Level.objects.get(name='-O2', factor= orm.Factor.objects.get(internal_name='opt_flag')),
                  orm.Level.objects.get(name='default', factor= orm.Factor.objects.get(internal_name='link_order')),
                  orm.Level.objects.get(name='on', factor= orm.Factor.objects.get(internal_name='address_randomization')),
                  orm.Level.objects.get(name=orm.Level.objects.filter(factor= orm.Factor.objects.get(internal_name='env_padding'))[0].name, factor= orm.Factor.objects.get(internal_name='env_padding')),
                  orm.Level.objects.get(name='off', factor= orm.Factor.objects.get(internal_name='drop_caches')),
                  orm.Level.objects.get(name='on', factor= orm.Factor.objects.get(internal_name='autogroup')),
                  orm.Level.objects.get(name='ext4', factor= orm.Factor.objects.get(internal_name='filesystem')),
                  orm.Level.objects.get(name='gcc', factor= orm.Factor.objects.get(internal_name='compiler')),
                  orm.Level.objects.get(name='off', factor= orm.Factor.objects.get(internal_name='freq_scaling')),
                  orm.Level.objects.get(name='on', factor= orm.Factor.objects.get(internal_name='swap'))]
        for level in levels:
            level.default = True
            level.save()

        for worker in orm.Worker.objects.exclude(status=Worker.DEAD):
            for factor in orm.Factor.objects.filter(fac_type=Factor.SOFTWARE):
                for level in factor.level_set.distinct():
                    if level.name in ['llvm']:
                        continue
                    entry = orm.Registry(worker=worker, factor=factor, level=level)
                    entry.save()

        orm.Level.objects.get(name='295993453').delete()

    def backwards(self, orm):
        raise RuntimeError("Cannot reverse this migration.")
    
    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'datamill_website.constraint': {
            'Meta': {'object_name': 'Constraint'},
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Level']"})
        },
        u'datamill_website.continuous_factor_range': {
            'Meta': {'object_name': 'Continuous_Factor_Range'},
            'default': ('django.db.models.fields.IntegerField', [], {}),
            'factor': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['datamill_website.Factor']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.IntegerField', [], {}),
            'min': ('django.db.models.fields.IntegerField', [], {}),
            'steps': ('django.db.models.fields.IntegerField', [], {})
        },
        u'datamill_website.coverage': {
            'Meta': {'object_name': 'Coverage'},
            'count': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'min': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'datamill_website.discrete_factor_level': {
            'Meta': {'object_name': 'Discrete_Factor_Level'},
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'datamill_website.experiment': {
            'Meta': {'object_name': 'Experiment'},
            'completion_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'constraint_spec': ('django.db.models.fields.TextField', [], {}),
            'coverage_spec': ('django.db.models.fields.TextField', [], {}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '750', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'num_hardware_factors': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'num_software_factors': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'replicate_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'results_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/yuguang/PycharmProjects/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*\\\\.tar$'", 'blank': 'True'}),
            'scope_spec': ('django.db.models.fields.TextField', [], {}),
            'send_confirmation_alert': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'CR'", 'max_length': '2'}),
            'submission_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'datamill_website.factor': {
            'Meta': {'object_name': 'Factor'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'display': ('django.db.models.fields.CharField', [], {'default': "'SH'", 'max_length': '2'}),
            'fac_type': ('django.db.models.fields.CharField', [], {'default': "'HW'", 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'datamill_website.job': {
            'Meta': {'object_name': 'Job'},
            'config': ('django.db.models.fields.TextField', [], {}),
            'config_id': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Package']"}),
            'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'replicate_no': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'results_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/yuguang/PycharmProjects/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*\\\\.tar\\\\.gz$'", 'blank': 'True'}),
            'stats_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/yuguang/PycharmProjects/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*\\\\.csv$'", 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'QU'", 'max_length': '2'}),
            'worker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Worker']"})
        },
        u'datamill_website.level': {
            'Meta': {'unique_together': "(('name', 'factor'),)", 'object_name': 'Level'},
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'obsolete': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'datamill_website.package': {
            'Meta': {'object_name': 'Package'},
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'datamill_website.registry': {
            'Meta': {'object_name': 'Registry'},
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Level']"}),
            'worker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Worker']"})
        },
        u'datamill_website.scope': {
            'Meta': {'object_name': 'Scope'},
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Level']"})
        },
        u'datamill_website.worker': {
            'Meta': {'object_name': 'Worker'},
            'cpu_arch': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'cpu_flags': ('django.db.models.fields.TextField', [], {}),
            'cpu_mhz': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'cpu_model': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'num_cpus': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'page_sz': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ram_sz': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'ID'", 'max_length': '2'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['auth.User']"}),
            'uuid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '36'})
        }
    }

    complete_apps = ['datamill_website']
    symmetrical = True
