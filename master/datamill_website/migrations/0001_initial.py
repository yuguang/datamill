# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Worker'
        db.create_table(u'datamill_website_worker', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='ID', max_length=2)),
            ('uuid', self.gf('django.db.models.fields.CharField')(max_length=36)),
            ('last_update', self.gf('django.db.models.fields.DateTimeField')()),
            ('ip', self.gf('django.db.models.fields.IPAddressField')(max_length=15)),
            ('hostname', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('cpu_count', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('cpu_arch', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('cpu_model', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('cpu_flags', self.gf('django.db.models.fields.TextField')()),
            ('cpu_mhz', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('ram', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('page_sz', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'datamill_website', ['Worker'])

        # Adding model 'Experiment'
        db.create_table(u'datamill_website_experiment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('submission_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('completion_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('status', self.gf('django.db.models.fields.CharField')(default='CR', max_length=2)),
            ('constraint_spec', self.gf('django.db.models.fields.TextField')()),
            ('coverage_spec', self.gf('django.db.models.fields.TextField')()),
            ('result_file', self.gf('django.db.models.fields.FilePathField')(path='/tmp/', max_length=100, match='.*\\.tar$')),
        ))
        db.send_create_signal(u'datamill_website', ['Experiment'])

        # Adding model 'Job'
        db.create_table(u'datamill_website_job', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('experiment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Experiment'])),
            ('package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Package'])),
            ('priority', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('worker', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Worker'])),
            ('config_id', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('config', self.gf('django.db.models.fields.TextField')()),
            ('replicate_no', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('status', self.gf('django.db.models.fields.CharField')(default='QU', max_length=2)),
            ('result_file', self.gf('django.db.models.fields.FilePathField')(path='/tmp/', max_length=100, match='.*\\.tar\\.gz$')),
            ('stats_file', self.gf('django.db.models.fields.FilePathField')(path='/tmp/', max_length=100, match='.*\\.csv$')),
        ))
        db.send_create_signal(u'datamill_website', ['Job'])

        # Adding model 'Package'
        db.create_table(u'datamill_website_package', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('experiment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Experiment'])),
        ))
        db.send_create_signal(u'datamill_website', ['Package'])

        # Adding model 'Factor'
        db.create_table(u'datamill_website_factor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('internal_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('fac_type', self.gf('django.db.models.fields.CharField')(default='HW', max_length=2)),
            ('dim_type', self.gf('django.db.models.fields.CharField')(default='DI', max_length=2)),
        ))
        db.send_create_signal(u'datamill_website', ['Factor'])

        # Adding model 'Discrete_Factor_Level'
        db.create_table(u'datamill_website_discrete_factor_level', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('factor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamill_website.Factor'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'datamill_website', ['Discrete_Factor_Level'])

        # Adding model 'Continuous_Factor_Range'
        db.create_table(u'datamill_website_continuous_factor_range', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('factor', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['datamill_website.Factor'], unique=True)),
            ('min', self.gf('django.db.models.fields.IntegerField')()),
            ('max', self.gf('django.db.models.fields.IntegerField')()),
            ('steps', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'datamill_website', ['Continuous_Factor_Range'])


    def backwards(self, orm):
        # Deleting model 'Worker'
        db.delete_table(u'datamill_website_worker')

        # Deleting model 'Experiment'
        db.delete_table(u'datamill_website_experiment')

        # Deleting model 'Job'
        db.delete_table(u'datamill_website_job')

        # Deleting model 'Package'
        db.delete_table(u'datamill_website_package')

        # Deleting model 'Factor'
        db.delete_table(u'datamill_website_factor')

        # Deleting model 'Discrete_Factor_Level'
        db.delete_table(u'datamill_website_discrete_factor_level')

        # Deleting model 'Continuous_Factor_Range'
        db.delete_table(u'datamill_website_continuous_factor_range')


    models = {
        u'datamill_website.continuous_factor_range': {
            'Meta': {'object_name': 'Continuous_Factor_Range'},
            'factor': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['datamill_website.Factor']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.IntegerField', [], {}),
            'min': ('django.db.models.fields.IntegerField', [], {}),
            'steps': ('django.db.models.fields.IntegerField', [], {})
        },
        u'datamill_website.discrete_factor_level': {
            'Meta': {'object_name': 'Discrete_Factor_Level'},
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'datamill_website.experiment': {
            'Meta': {'object_name': 'Experiment'},
            'completion_date': ('django.db.models.fields.DateTimeField', [], {}),
            'constraint_spec': ('django.db.models.fields.TextField', [], {}),
            'coverage_spec': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'result_file': ('django.db.models.fields.FilePathField', [], {'path': "'/tmp/'", 'max_length': '100', 'match': "'.*\\\\.tar$'"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'CR'", 'max_length': '2'}),
            'submission_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'datamill_website.factor': {
            'Meta': {'object_name': 'Factor'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'dim_type': ('django.db.models.fields.CharField', [], {'default': "'DI'", 'max_length': '2'}),
            'fac_type': ('django.db.models.fields.CharField', [], {'default': "'HW'", 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'datamill_website.job': {
            'Meta': {'object_name': 'Job'},
            'config': ('django.db.models.fields.TextField', [], {}),
            'config_id': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Package']"}),
            'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'replicate_no': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'result_file': ('django.db.models.fields.FilePathField', [], {'path': "'/tmp/'", 'max_length': '100', 'match': "'.*\\\\.tar\\\\.gz$'"}),
            'stats_file': ('django.db.models.fields.FilePathField', [], {'path': "'/tmp/'", 'max_length': '100', 'match': "'.*\\\\.csv$'"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'QU'", 'max_length': '2'}),
            'worker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Worker']"})
        },
        u'datamill_website.package': {
            'Meta': {'object_name': 'Package'},
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'datamill_website.worker': {
            'Meta': {'object_name': 'Worker'},
            'cpu_arch': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'cpu_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'cpu_flags': ('django.db.models.fields.TextField', [], {}),
            'cpu_mhz': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'cpu_model': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'page_sz': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'ram': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'ID'", 'max_length': '2'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        }
    }

    complete_apps = ['datamill_website']