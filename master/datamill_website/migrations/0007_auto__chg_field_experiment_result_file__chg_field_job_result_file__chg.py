# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Experiment.result_file'
        db.alter_column(u'datamill_website_experiment', 'result_file', self.gf('django.db.models.fields.FilePathField')(max_length=100, path='/tmp/', null=True, match='.*\\.tar$'))

        # Changing field 'Job.result_file'
        db.alter_column(u'datamill_website_job', 'result_file', self.gf('django.db.models.fields.FilePathField')(max_length=100, path='/tmp/', null=True, match='.*\\.tar\\.gz$'))

        # Changing field 'Job.stats_file'
        db.alter_column(u'datamill_website_job', 'stats_file', self.gf('django.db.models.fields.FilePathField')(max_length=100, path='/tmp/', null=True, match='.*\\.csv$'))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Experiment.result_file'
        raise RuntimeError("Cannot reverse this migration. 'Experiment.result_file' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Job.result_file'
        raise RuntimeError("Cannot reverse this migration. 'Job.result_file' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Job.stats_file'
        raise RuntimeError("Cannot reverse this migration. 'Job.stats_file' and its values cannot be restored.")

    models = {
        u'datamill_website.continuous_factor_range': {
            'Meta': {'object_name': 'Continuous_Factor_Range'},
            'default': ('django.db.models.fields.IntegerField', [], {}),
            'factor': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['datamill_website.Factor']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.IntegerField', [], {}),
            'min': ('django.db.models.fields.IntegerField', [], {}),
            'steps': ('django.db.models.fields.IntegerField', [], {})
        },
        u'datamill_website.discrete_factor_level': {
            'Meta': {'object_name': 'Discrete_Factor_Level'},
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'datamill_website.experiment': {
            'Meta': {'object_name': 'Experiment'},
            'completion_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'constraint_spec': ('django.db.models.fields.TextField', [], {}),
            'coverage_spec': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'result_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/tmp/'", 'null': 'True', 'match': "'.*\\\\.tar$'", 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'CR'", 'max_length': '2'}),
            'submission_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'datamill_website.factor': {
            'Meta': {'object_name': 'Factor'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'dim_type': ('django.db.models.fields.CharField', [], {'default': "'DI'", 'max_length': '2'}),
            'fac_type': ('django.db.models.fields.CharField', [], {'default': "'HW'", 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'datamill_website.job': {
            'Meta': {'object_name': 'Job'},
            'config': ('django.db.models.fields.TextField', [], {}),
            'config_id': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Package']"}),
            'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'replicate_no': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'result_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/tmp/'", 'null': 'True', 'match': "'.*\\\\.tar\\\\.gz$'", 'blank': 'True'}),
            'stats_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/tmp/'", 'null': 'True', 'match': "'.*\\\\.csv$'", 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'QU'", 'max_length': '2'}),
            'worker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Worker']"})
        },
        u'datamill_website.package': {
            'Meta': {'object_name': 'Package'},
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'datamill_website.worker': {
            'Meta': {'object_name': 'Worker'},
            'cpu_arch': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'cpu_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'cpu_flags': ('django.db.models.fields.TextField', [], {}),
            'cpu_mhz': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'cpu_model': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'page_sz': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'ram': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'ID'", 'max_length': '2'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        }
    }

    complete_apps = ['datamill_website']