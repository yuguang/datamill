# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Experiment.results_file'
        db.alter_column(u'datamill_website_experiment', 'results_file', self.gf('django.db.models.fields.FilePathField')(max_length=100, path='/home/augusto/work/eval-lab/master/local/local_results', null=True, match='.*\\.tar$')) # ok
        # Deleting field 'Job.stats_file'
        db.delete_column(u'datamill_website_job', 'stats_file') # ok

        # Adding field 'Job.job_status_file'
        db.add_column(u'datamill_website_job', 'job_status_file',
                      self.gf('django.db.models.fields.FilePathField')(max_length=100, path='/home/augusto/work/eval-lab/master/local/local_results', null=True, match='.*.json$', blank=True),
                      keep_default=False) # ok

        # Adding field 'Job.worker_status_file'
        db.add_column(u'datamill_website_job', 'worker_status_file',
                      self.gf('django.db.models.fields.FilePathField')(max_length=100, path='/home/augusto/work/eval-lab/master/local/local_results', null=True, match='.*.json$', blank=True),
                      keep_default=False) # ok

        # Adding field 'Job.setup_time'
        db.add_column(u'datamill_website_job', 'setup_time',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False) # ok

        # Adding field 'Job.run_time'
        db.add_column(u'datamill_website_job', 'run_time',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False) # ok 

        # Adding field 'Job.collect_time'
        db.add_column(u'datamill_website_job', 'collect_time',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False) # ok


        # Changing field 'Job.results_file'
        db.alter_column(u'datamill_website_job', 'results_file', self.gf('django.db.models.fields.FilePathField')(max_length=100, path='/home/augusto/work/eval-lab/master/local/local_results', null=True, match='.*.tar.gz$')) # ok

    def backwards(self, orm):

        # Changing field 'Experiment.results_file'
        db.alter_column(u'datamill_website_experiment', 'results_file', self.gf('django.db.models.fields.FilePathField')(path='/home/augusto/work/eval-lab/django/datamill_master/local/local_results', max_length=100, null=True, match='.*\\.tar$'))
        # Adding field 'Job.stats_file'
        db.add_column(u'datamill_website_job', 'stats_file',
                      self.gf('django.db.models.fields.FilePathField')(path='/home/augusto/work/eval-lab/django/datamill_master/local/local_results', max_length=100, null=True, match='.*\\.csv$', blank=True),
                      keep_default=False)

        # Deleting field 'Job.job_status_file'
        db.delete_column(u'datamill_website_job', 'job_status_file')

        # Deleting field 'Job.worker_status_file'
        db.delete_column(u'datamill_website_job', 'worker_status_file')

        # Deleting field 'Job.setup_time'
        db.delete_column(u'datamill_website_job', 'setup_time')

        # Deleting field 'Job.run_time'
        db.delete_column(u'datamill_website_job', 'run_time')

        # Deleting field 'Job.collect_time'
        db.delete_column(u'datamill_website_job', 'collect_time')


        # Changing field 'Job.results_file'
        db.alter_column(u'datamill_website_job', 'results_file', self.gf('django.db.models.fields.FilePathField')(path='/home/augusto/work/eval-lab/django/datamill_master/local/local_results', max_length=100, null=True, match='.*\\.tar\\.gz$'))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'datamill_website.continuous_factor_range': {
            'Meta': {'object_name': 'Continuous_Factor_Range'},
            'default': ('django.db.models.fields.IntegerField', [], {}),
            'factor': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['datamill_website.Factor']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.IntegerField', [], {}),
            'min': ('django.db.models.fields.IntegerField', [], {}),
            'steps': ('django.db.models.fields.IntegerField', [], {})
        },
        u'datamill_website.discrete_factor_level': {
            'Meta': {'object_name': 'Discrete_Factor_Level'},
            'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'datamill_website.experiment': {
            'Meta': {'object_name': 'Experiment'},
            'completion_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'constraint_spec': ('django.db.models.fields.TextField', [], {}),
            'coverage_spec': ('django.db.models.fields.TextField', [], {}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '750'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'num_hardware_factors': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'num_software_factors': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'replicate_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'results_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/augusto/work/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*\\\\.tar$'", 'blank': 'True'}),
            'scope_spec': ('django.db.models.fields.TextField', [], {}),
            'send_confirmation_alert': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'CR'", 'max_length': '2'}),
            'submission_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'datamill_website.factor': {
            'Meta': {'object_name': 'Factor'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'dim_type': ('django.db.models.fields.CharField', [], {'default': "'DI'", 'max_length': '2'}),
            'fac_type': ('django.db.models.fields.CharField', [], {'default': "'HW'", 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'datamill_website.job': {
            'Meta': {'object_name': 'Job'},
            'config': ('django.db.models.fields.TextField', [], {}),
            'config_id': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            'job_status_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/augusto/work/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*.json$'", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'worker_status_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/augusto/work/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*.json$'", 'blank': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Package']"}),
            'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'replicate_no': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'results_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/augusto/work/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*.tar.gz$'", 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'QU'", 'max_length': '2'}),
            'collect_time': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'setup_time': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'run_time': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'worker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Worker']"})
        },
        u'datamill_website.package': {
            'Meta': {'object_name': 'Package'},
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'datamill_website.worker': {
            'Meta': {'object_name': 'Worker'},
            'cpu_arch': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'cpu_flags': ('django.db.models.fields.TextField', [], {}),
            'cpu_mhz': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'cpu_model': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'num_cpus': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'page_sz': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ram_sz': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'ID'", 'max_length': '2'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['auth.User']"}),
            'uuid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '36'})
        }
    }

#   NEWWORKER_models = {
#       u'datamill_website.continuous_factor_range': {
#           'Meta': {'object_name': 'Continuous_Factor_Range'},
#           'default': ('django.db.models.fields.IntegerField', [], {}),
#           'factor': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['datamill_website.Factor']", 'unique': 'True'}),
#           u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
#           'max': ('django.db.models.fields.IntegerField', [], {}),
#           'min': ('django.db.models.fields.IntegerField', [], {}),
#           'steps': ('django.db.models.fields.IntegerField', [], {})
#       },
#       u'datamill_website.discrete_factor_level': {
#           'Meta': {'object_name': 'Discrete_Factor_Level'},
#           'factor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Factor']"}),
#           u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
#           'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
#           'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
#       },
#       u'datamill_website.experiment': {
#           'Meta': {'object_name': 'Experiment'},
#           'completion_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
#           'constraint_spec': ('django.db.models.fields.TextField', [], {}),
#           'coverage_spec': ('django.db.models.fields.TextField', [], {}),
#           u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
#           'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
#           'replicate_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
#           'results_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/augusto/work/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*\\\\.tar$'", 'blank': 'True'}),
#           'scope_spec': ('django.db.models.fields.TextField', [], {}),
#           'send_confirmation_alert': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
#           'status': ('django.db.models.fields.CharField', [], {'default': "'CR'", 'max_length': '2'}),
#           'submission_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
#       },
#       u'datamill_website.factor': {
#           'Meta': {'object_name': 'Factor'},
#           'description': ('django.db.models.fields.TextField', [], {}),
#           'dim_type': ('django.db.models.fields.CharField', [], {'default': "'DI'", 'max_length': '2'}),
#           'fac_type': ('django.db.models.fields.CharField', [], {'default': "'HW'", 'max_length': '2'}),
#           u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
#           'internal_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
#           'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
#       },
#       u'datamill_website.job': {
#           'Meta': {'object_name': 'Job'},
#           'collect_time': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
#           'config': ('django.db.models.fields.TextField', [], {}),
#           'config_id': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
#           'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
#           u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
#           'job_status_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/augusto/work/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*.json$'", 'blank': 'True'}),
#           'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
#           'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Package']"}),
#           'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
#           'replicate_no': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
#           'results_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/augusto/work/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*.tar.gz$'", 'blank': 'True'}),
#           'run_time': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
#           'setup_time': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
#           'status': ('django.db.models.fields.CharField', [], {'default': "'QU'", 'max_length': '2'}),
#           'worker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Worker']"}),
#           'worker_status_file': ('django.db.models.fields.FilePathField', [], {'max_length': '100', 'path': "'/home/augusto/work/eval-lab/master/local/local_results'", 'null': 'True', 'match': "'.*.json$'", 'blank': 'True'})
#       },
#       u'datamill_website.package': {
#           'Meta': {'object_name': 'Package'},
#           'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamill_website.Experiment']"}),
#           'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
#           u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
#       },
#       u'datamill_website.worker': {
#           'Meta': {'object_name': 'Worker'},
#           'cpu_arch': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
#           'cpu_flags': ('django.db.models.fields.TextField', [], {}),
#           'cpu_mhz': ('django.db.models.fields.PositiveIntegerField', [], {}),
#           'cpu_model': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
#           'hostname': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
#           u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
#           'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
#           'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
#           'num_cpus': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
#           'page_sz': ('django.db.models.fields.PositiveIntegerField', [], {}),
#           'ram_sz': ('django.db.models.fields.PositiveIntegerField', [], {}),
#           'status': ('django.db.models.fields.CharField', [], {'default': "'ID'", 'max_length': '2'}),
#           'uuid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '36'})
#       }
#   }

    complete_apps = ['datamill_website']
