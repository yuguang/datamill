from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from django.contrib import admin

from datamill_website import views
from decorators import *

admin.autodiscover()

urlpatterns = patterns(
    '',

    # landing, about, etc 
    url(r'^$', 'datamill_website.views.home', name='home'),
    url(r'about/$', 'datamill_website.views.about', name='about'),
    url(r'sponsors/$', 'datamill_website.views.sponsors', name='sponsors'),
    url(r'institutions/$', 'datamill_website.views.institutions', name='institutions'),

    # wizard
    url(r'^experiment/add/$', login_required(views.experiment_add), name='experiment_add'),
    url(r'^experiment/tag-hint/$', login_required(views.tag_hint), name='tag_hint'),
    # edit title and description
    url(r'^experiment/save/(?P<pk>\d+)/$', login_required(views.experiment_save), name='experiment_save'),
    # clone
    url(r'^experiment/clone/(?P<pk>\d+)/$', login_required(views.experiment_add), name='experiment_clone'),
    # delete
    url(r'^experiment/delete/(?P<pk>\d+)/$', login_required(views.experiment_delete), name='experiment_delete'),

    # individual detail views
    url(r'^job/(?P<pk>\d+)/$', login_required(views.job_detail), name='job_detail'),
    url(r'^worker/(?P<pk>\d+)/$', login_required(views.worker_detail), name='worker_detail'),
    url(r'^experiment/(?P<pk>\d+)/$', login_required(views.experiment_detail), name='experiment_detail'),

    # log file downloads
    url(r'job/(?P<pk>\d+)/log/(?P<log_name>\w+)/$', login_required(views.job_status_log), name='job_status_log'),

    # list views
    url(r'^jobs/$', login_required(views.job_list), name='jobs'),
    url(r'^workers/new/$', login_required(views.new_worker_list), name='new_workers'),
    url(r'^workers/register/$', login_required(views.register_worker), name='register_worker'),
    url(r'^workers/$', login_required(views.worker_list), name='workers'),
    url(r'^experiments/$', login_required(views.experiment_list), name='experiments'),

    # file serving
    url(r'^package/(?P<pk>\d+)/$', login_required(views.package), name='package'),
    url(r'^job_results/(?P<pk>\d+)/$', login_required(views.job_results), name='job_results'),
    url(r'^job_status/(?P<pk>\d+)/$', login_required(views.job_status), name='job_status'),
    url(r'^experiment_results/(?P<pk>\d+)/$', login_required(views.experiment_results), name='experiment_results'),

    # account mgmt
    url(r'^login/$', 'datamill_website.views.home', name='home'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'^register/$', 'datamill_website.views.create_account', name="create_account"),
    url(r'^user/password/change/$', 'django.contrib.auth.views.password_change', name='password_change'),
    url(r'random_password/$', 'datamill_website.views.random_password', name='random_password'),
    url(r'^user/password/change/done/$', 'django.contrib.auth.views.password_change_done',),
    url(r'^user/password/reset/$',
        'django.contrib.auth.views.password_reset',
        {'post_reset_redirect' : '/user/password/reset/done/', 'from_email': settings.DATAMILL_EMAIL},
        name="password_reset"),
    url(r'^user/password/reset/done/$',
        'django.contrib.auth.views.password_reset_done'),
    url(r'^user/password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        'django.contrib.auth.views.password_reset_confirm',
        {'post_reset_redirect' : '/user/password/done/'}),
    url(r'^user/password/reset/complete/$',
        'django.contrib.auth.views.password_reset_complete'),

    # actions
    url(r'^experiment_cancel/(?P<pk>\d+)/$', login_required(views.experiment_cancel), name='experiment_cancel'),
    url(r'^experiment_publish/(?P<pk>\d+)/$', login_required(views.experiment_publish), name='experiment_publish'),
    url(r'^experiment_make_private/(?P<pk>\d+)/$', login_required(views.experiment_make_private), name='experiment_make_private'),
    url(r'^experiment_retry/(?P<pk>\d+)/$', login_required(views.experiment_retry), name='experiment_retry'),

    # admin
    url(r'^workers/manage/$', is_superuser(views.worker_monitor), name='worker_manage'),
    url(r'^worker/(?P<pk>\d+)/jobs/cancel/$', is_superuser(views.cancel_worker_jobs), name='cancel_worker_jobs'),
)