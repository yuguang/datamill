#!/bin/sh

echo "Starting DataMill Tests" 
#Sync with the DataMill portage overlay
layman -S
emerge datamill-controller

#Run all unittests, logging to a file
cd /datamill
./setup-virtualenv.sh
#./manage.py destructive-test
#./manage.py destructive-integration-test
