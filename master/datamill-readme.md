Installation
============

A short installation synopsis (incomplete):

    # Install Non-Python dependencies
    emerge postgresql-server glpk          # or equivalent
    emerge --config postgresql-server      # or equivalent 

    # Setup Postgres
    su postgres                            # or whatever user your distro defines
    createuser -S -d -P datamill-dbuser # set password to datamill-password
    createdb datamilldb -O datamill-dbuser
    exit                                   # to get back to root or whatever user
    /etc/init.d/postgresql* start

    # Get the DataMill Source
    git clone git@bitbucket.org:yuguang/datamill.git && cd eval-lab/master
    
    # Setup virtualenv and install all python deps into master/dev-python
    ./setup-virtualenv.sh
    
    # Setup logging directory (or change the location of the logging directory)
    mkdir -p /var/datamill/log
    chown -R <webserver-user>:<webserver-user> /var/datamill

    # Add DataMill Schema to our database
    ./manage.py syncdb        # create admin/password for website when prompted
    ./manage.py migrate
    ./manage.py createinitialrevisions
    ./manage.py runserver

    # Optional for local development
    ./manage.py syncdb --all
    ./manage.py loaddata  datamill_test_fixture

    # Optional for loading data from a .sql file
    su postgres                            # or whatever user your distro defines
    cd
    exit
    cp /path/to/file.sql ./
    su postgres                            # or whatever user your distro defines
    psql -f file.sql
    ./manage.py migrate datamill_website


Additional Considerations for the Master Node (Incomplete)
---------------------------------------------

When creating a master node from a fresh machine, there are a few
extra steps that must be performed, and a few things that should be
done a little differently.

We use a few more packages for a production setup, namely apache and
an ftp daemon:

    # Install additional production deps
    
    emerge vsftpd apache mod_wsgi
    
We should place our code somewhere apache can get to it, so clone the
repository somewhere appropriate:

    # Get the DataMill Source
    mkdir -p /var/www/localhost/wsgi
    cd /var/www/localhost/wsgi
    git clone git@bitbucket.org:sfischme/eval-lab.git && cd eval-lab/master
    
We need to change the permissions of the logging directory so that
apache started processes have access to it.
    
    # Change owner of logging dir for apache
    mkdir -p /var/datamill/log
    chown -R apache:apache /var/datamill
    
    
We use `mod_wsgi` for running the python web portions of the
project. `mod_wsgi` is a very fast and stable apache extension for the
`wsgi` python web application api.
    
To use `mod_wsgi`, add the apache module to the apache daemon
configuration in `/etc/conf.d/apache2`:

    APACHE2_OPTS= ... -D WSGI -D XSENDFILE
    
The main configuration location for our web app will be placed in the
default virtual-host config,
`/etc/apache2/vhosts.d/default_vhost.include`. Add a directory
directive for our wsgi directory that we created earlier, and an alias
for our wsgi-script.

    <Directory /var/www/localhost/wsgi>
    Order allow,deny
    Allow from all
    </Directory>  
    
    WSGIScriptAlias / /var/www/localhost/wsgi/eval-lab/master/datamill_django_project/wsgi.py

We also want apache to serve static files since it is substantially
faster than having django do it. The default configuration of our
django app puts static files in `/static` so lets add an alias in
apache for this location that points to apache's default static file
folder. This should go in the same vhosts file
(`/etc/apache2/vhosts.d/default_vhost.include`).

    Alias /static /var/www/localhost/htdocs
    
The last apache config that we need to change is the httpd.conf
file. We need WSGI to use the right python path, and to use our
virtualenv as well. The following should go into your httpd.conf
(`/etc/apache2/httpd.conf`'):

    WSGIPythonPath /var/www/localhost/wsgi/eval-lab/master:/var/www/localhost/wsgi/eval-lab/master/dev-python/lib/python2.7/site-packages

Modify /etc/hosts to contain the hostname of your machine,for example:

    127.0.0.1       datamill.uwaterloo.ca cube localhost
    ::1             datamill.uwaterloo.ca cube localhost

Now we should put all our static files in place, and run apache:

    cd eval-lab/master
    ./manage.py collectstatic
    /etc/init.d/apache2 start

We need to put the passwords for the database and the datamill email
host into a place that our django app can get to. Currently that's in
an environment variable. Put the following entries into an env.d file
(`/etc/env.d/99datamill`):

    DATAMILL_DATABASE_PASSWORD="<password>"
    DATAMILL_EMAIL_HOST_PASSWORD="<password2>"

Set up SSL by following the steps "Configuring Apache to use your Certificate/Key Pair" and "Enabling mod\_ssl" in the [gentoo wiki](http://www.gentoo-wiki.info/Apache_Modules_mod_ssl).
For the automatic redirect, use the following settings instead:
	
	# Redirect to SSL
	RewriteEngine On
	RewriteCond %{HTTPS} !on
	RewriteRule ^/(.*) https://datamill.uwaterloo.ca%{REQUEST_URI} [L,R=301]

Set ftp folder permissions and give apache permissions to create files in worker ftp folders:

    usermod apache --append --groups ftp
    cd /home/ftp
    chown apache:ftp worker-*
    chmod g+w worker-* #add permissions to create files in the directory to ftp group

Create symlinks for the services:

    cd /etc/init.d
    ln -s /var/www/localhost/wsgi/eval-lab/master/init_scripts/collector collector
    ln -s /var/www/localhost/wsgi/eval-lab/master/init_scripts/registrar registrar
    ln -s /var/www/localhost/wsgi/eval-lab/master/init_scripts/scheduler scheduler
    ln -s /var/www/localhost/wsgi/eval-lab/master/init_scripts/ip-updater ip-updater

Add services to the default runlevel:

    rc-update add apache2 default
    rc-update add vsftpd default
    rc-update add postgresql-[version] default
    rc-update add collector default
    rc-update add registrar default
    rc-update add scheduler default
    rc-update add ip-updater default

Additional Considerations for Test Master nodes
-----------------------------------------------

When setting up a test master there are a few additional steps that
are necessary, and a few steps that need to be treated differently.

The first thing that we can omit is the ssl redirection. The
testmaster is not expected to be secure so we can leave it unsecure
for simplicity.

Several different ports need to be configured differently. The port
mappings are as follows:

    80  -> 8080
    22  -> 2222
    21  -> 2121
    
Configuring apache to listen to 8080 all takes place in the
`/etc/apache2/vhosts.d/00_default_vhost.conf` file. All references to
port 80 in here should be changed to 8080.

SSHD is configured to listen to 2222 by adding the line:

    Port 2222

to `/etc/ssh/sshd_config`. While you're in there, ensure that UsePAM
is set to "no" at the very least.

Lastly we need to configure vsftpd to listen to 2121. Add the
following line to `/etc/vsftpd/vsftpd.conf`:

    listen_port=2121
    
You should also add local_settings.py to the django project
configurations setting `DATAMILL_DEBUG` to `True`.

Development
===========

If you need something more specific than the instructions in
[Installation](#Installation) then read this section.

Local Settings
--------------

Take a look at the default settings in the global configuration file
to get an idea of what's possible:

    datamill_django_project/settings.py

If you need to adjust any settings for your local dev machine and
avoid changing the configuration on the actual master node, copy the
example settings file to a special file called `local_settings.py`:

    cp datamill_django_project/example_local_settings.py \
       datamill_django_project/local_settings.py

And edit the `local_settings.py` file as necessary. Any configurations
set in `local_settings.py` will take precedence over their counterpart
in the global configuration file.

Once you are satisfied with your settings, make sure that the database
you point to in your configuration exists.

Hacking On DataMill
-------------------

All django web related code is roughly organized as follows:

- models are in `datamill_website/models.py`
- templates are in `datamill_website/templates`
- views are in `datamill_website/views.py` (only make new views if the django generic ones don't suffice)

Read up on south (http://south.aeracode.org/) before doing DB stuff

    cd eval-lab/master
    ./manage.py runserver                               # to start the server
    ./manage.py test                                    # to run all tests
    ./manage.py schemamigration datamill_website --auto # to generate migrations (schema changes) when you change the DB structure in models.py
    ./manage.py migrate datamill_website                # to apply pending migrations on the database
    ./manage.py syncdb --all                            # to update Meta information (such as perms)

When the server is up and running, you can visit the local DataMill
page at port 8000 with any webclient:

    ./manage.py runserver
    curl localhost:8000/experiment/1/ goes to the expermient detail view
    curl localhost:8000/admin goes to admin

For access to the admin interface, use the password you set when you ran syncdb

Running Local Worker Tests
--------------------------
Worker tests need to be run as root from `eval-lab/worker`. `lshw`, `dmidecode`, and `btrfs-progs` are assumed to be installed.

    ./dev-python/bin/python ./manage.py test
    
Running Virtual Worker Tests
----------------------------

To test worker code changes, change to the worker folder and deploy a package with the testing flag

    cd datamill/worker/
    ./manage.py deploy-test-package

Start virtual worker in VirtualBox, make a snapshot of the virtual machine, and run

    datamill-virtual-worker-tests.sh
    
Revert back to the snapshot, then run

    layman -S && emerge datamill-controller && cd /datamill && ./setup-virtualenv.sh
    ./manage.py destructive-integration-test
    
Test output is in `/tmp/datamill_test.log`

DO NOT TAKE WORKER UPDATES LIGHTLY
----------------------------------

Mistakes made when updating workers have the potential to be
cripplingly expensive timewise, as opposed to master updates. Update
with caution!

Updating the Portage Mirror
---------------------------
After updating the mirror

1. update the tarballs downloaded in wget_stage3_tarball in worker/install/install_gentoo.sh
2. upgrade all machines in a manner similar to the following

    killall python2
    screen
    perl-cleaner --all
    emerge portage
    emerge -uDN world && emerge --depclean && revdep-rebuild -i

    gcc-config -l
    gcc-config 1
    env-update && source /etc/profile

    /etc/local.d/00_controller.start
