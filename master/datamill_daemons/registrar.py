# standard modules
import sys
from time import sleep
import signal
import json
import os
import os.path
from datetime import datetime

# logging
import logging
logger = logging.getLogger(sys.argv[1])

# datamill modules
from django.conf import settings
from datamill_website.models import Worker
from md5hash import md5hash

shutdown_requested = False

class FileWatcher(object):
    # placeholder and defaults
    folder = settings.DATAMILL_HELLO_ROOT
    extension = settings.DATAMILL_HELLO_EXTENSION

    def __init__(self, create_only = False):
        logger.debug("{} up!".format(self.__class__.__name__))
        self.file_name = ''
        if not create_only:
            while True and not shutdown_requested:
                self.run()
                sleep(settings.DATAMILL_REGISTRAR_LOOP_SECS)

            logger.info("turning off by request")

    def run(self):

        for h in self.list_files():

            logger.info('doing {1}: {0}'.format(h, self.file_name))
            try:
                md5hash.check_md5(h)
            except md5hash.FileCorrupted as e:
                logger.warning('corrupted {1} file: {0}'.format(e, self.file_name))
                self.delete_files(h)
                continue

            try:
                with open(h) as f:
                    worker_dict = json.load(f)
            except Exception, e:
                logger.warning('json reading failed: {0}'.format(e))
                self.delete_files(h)
                continue

            try:
                try:
                    w = Worker.objects.get(uuid=worker_dict['uuid'])
                except Worker.DoesNotExist:
                    w = Worker()

                self.update_worker(w, worker_dict)

                if w.status == Worker.DEAD:
                    w.status = Worker.IDLE

                w.last_update = datetime.now()

            except Exception, e:
                logger.warning('failed to make worker from dict: {0}'.format(e))
                self.delete_files(h)
                continue

            try:
                w.save()
            except Exception, e:
                logger.warning('error saving worker (missing/malformed fields?): {0}'.format(e))
                self.delete_files(h)
                continue

            try:
                w.create_dir()
            except Exception, e:
                logger.warning('failed create worker dir, bad but ignoring: {0}'.format(e))
                w.delete()
                self.delete_files(h)
                continue

            try:
                self.delete_files(h)
            except Exception,e :
                logger.warning('failed to delete {1}, bad but ignoring: {0}'.format(e, self.file_name))
                continue

    @classmethod
    def list_files(cls):
        # list .md5s, but return only .json
        # since the workers upload .json first, .md5 last

        if not os.path.exists(cls.folder):
            os.mkdir(cls.folder)

        hello_files = [ os.path.join(cls.folder, os.path.splitext(f)[0])
                for f in os.listdir(cls.folder)
                if f.endswith('{0}{1}'.format(
                    cls.extension, settings.DATAMILL_INTEG_EXTENSION))]

        return hello_files

    @classmethod
    def delete_files(cls, f):
        try:
            os.remove(f)
        except Exception, e:
            logger.warning('failed to remove json file: {0}'.format(e))

        try:
            os.remove('{0}{1}'.format(f, settings.DATAMILL_INTEG_EXTENSION))
        except Exception, e:
            logger.warning('failed to remove md5 file: {0}'.format(e))

    def update_worker(self, w, worker_dict):
        raise NotImplementedError

class Registrar(FileWatcher):
    folder = settings.DATAMILL_HELLO_ROOT
    extension = settings.DATAMILL_HELLO_EXTENSION

    def __init__(self, create_only = False):
        super(Registrar, self).__init__(create_only)
        self.file_name = 'hello'

    def update_worker(self, w, worker_dict):
        w.fill_from_dict(worker_dict)

def shutdown_daemon(signum, frame):
    global shutdown_requested
    shutdown_requested = True

signal.signal(signal.SIGINT, shutdown_daemon)
signal.signal(signal.SIGTERM, shutdown_daemon)

if __name__ == "__main__":
    try:
        Registrar()
    except Exception, e:
        logger.error('Exploded: %s', e)
        raise SystemExit(-1)
