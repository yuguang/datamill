from registrar import *
shutdown_requested = False

class IPUpdater(FileWatcher):
    folder = settings.DATAMILL_IP_ROOT
    extension = settings.DATAMILL_IP_EXTENSION

    def __init__(self, create_only = False):
        super(IPUpdater, self).__init__(create_only)
        self.file_name = 'ip'

    def update_worker(self, w, worker_dict):
        w.uuid = worker_dict['uuid']
        w.ip = worker_dict['ip']

signal.signal(signal.SIGINT, shutdown_daemon)
signal.signal(signal.SIGTERM, shutdown_daemon)

if __name__ == "__main__":
    try:
        IPUpdater()
    except Exception, e:
        logger.error('Exploded: %s', e)
        raise SystemExit(-1)
