from django.test import TestCase
import random
import logging

from datamill_daemons.collector import Collector
from datamill_website.models import Job, Worker, Experiment
from datamill_common import datamill_db_helpers

class CollectorTests(TestCase):

    fixtures = [ 'initial_data', 'datamill_test_fixture' ]

    def setUp(self):
        pass

    def tearDown(self):
        datamill_db_helpers.delete_all_datamill_files()

    def test_run(self):

        for w in Worker.objects.all():
            datamill_db_helpers.create_worker_dir(w)

        c = Collector(create_only=True)

        js = random.sample(Job.objects.all(), 2)

        # make an uncollected done job
        js[0].status = Job.IN_PROGRESS
        datamill_db_helpers.create_job_incoming_wip_file(js[0])
        datamill_db_helpers.create_job_incoming_results_files(js[0])
        js[0].save()

        # make a wip
        datamill_db_helpers.create_job_incoming_wip_file(js[1])

        c.run()

        self.assertTrue(Job.objects.filter(status=Job.DONE).exists())
        self.assertTrue(Job.objects.filter(status=Job.IN_PROGRESS).exists())
        self.assertTrue(Worker.objects.filter(status=Worker.WORKING).exists())
        self.assertTrue(Experiment.objects.filter(status=Experiment.IN_PROGRESS).exists())
