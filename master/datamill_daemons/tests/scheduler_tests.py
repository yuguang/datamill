from django.test import TestCase
from datamill_common import datamill_db_helpers
from datamill_daemons.scheduler import Scheduler
from datamill_website.models import *

class SchedulerTests(TestCase):

    fixtures = [ 'initial_data', 'datamill_test_fixture' ]

    def setUp(self):
        pass

    def tearDown(self):
        datamill_db_helpers.delete_all_datamill_files()

    def test_select_workers_empty(self):
        scope = Factor.get_full_scope()
        const = {u'cpu_arch': ['arm', 'x86'], u'address_randomization': [], u'drop_caches': [], u'num_cpus': [], u'opt_flag': [], u'ram_sz': [], u'page_sz': [], u'autogroup': [], u'freq_scaling': [], u'swap': [], u'cpu_mhz': [], u'filesystem': ["ext3", "btrfs", "ext2"], u'compiler': ["llvm"], u'env_padding': [], u'fpie': [], u'sys_time': [], u'link_order': [], 'gpu_mhz': []}

        self.assertRaises(ValueError, Scheduler.select_workers, const, scope)

    def test_select_workers(self):
        scope = Factor.get_full_scope()
        const = {u'cpu_arch': [], u'address_randomization': [], u'drop_caches': [], u'num_cpus': [], u'opt_flag': [], u'ram_sz': [], u'page_sz': [], u'autogroup': [], u'freq_scaling': [], u'swap': [], u'cpu_mhz': [], u'filesystem': ["ext3", "btrfs", "ext2"], u'compiler': ["llvm"], u'env_padding': [], u'fpie' : [],  u'link_order': [], 'gpu_mhz': []}

        w, hw_factors = Scheduler.select_workers(const, scope)
        self.assertEquals(len(w), Worker.objects.count())

    def test_create_jobs_for_multipackage_exp(self):
        e = Experiment.objects.all()[0]

        for p in Package.objects.all():
            p.experiment = e
            p.save()

        e.job_set.all().delete()

        ws = Worker.objects.all()

        const = {u'cpu_arch': ['arm', 'x86_64'], u'address_randomization': ['off'], u'drop_caches': ['off'], u'num_cpus': ['2'], u'opt_flag': [], u'ram_sz': ["100", "1000", "200"], u'page_sz': ["4000", "4096"], u'autogroup': ['off'], u'freq_scaling': ['off'], u'swap': ['off'], u'cpu_mhz': ["1000", "2000", "3000"], u'filesystem': ["ext3", "btrfs", "ext2"], u'compiler': ["llvm"], u'env_padding': ["0", "10928", "16392"], u'fpie': ['off'], u'link_order': ["alphabetical", "reverse_alphabetical"] }
        scope = Factor.get_full_scope()
        js, sw_factors = Scheduler.create_jobs(e, ws, const, scope)

        opt_flag = Factor.objects.filter(internal_name='opt_flag').get()
        self.assertEquals(len(js), e.package_set.count() * e.replicate_count * len(ws) * len(opt_flag.get_levels()))
        self.assertGreater(e.package_set.count(), 1)

    def test_create_no_jobs(self):
        e = Experiment.objects.all()[0]
        e.job_set.all().delete()

        ws = []

        const = {u'cpu_arch': [], u'address_randomization': [], u'drop_caches': [], u'num_cpus': [], u'opt_flag': [], u'ram_sz': [], u'page_sz': [], u'autogroup': [], u'freq_scaling': [], u'swap': [], u'cpu_mhz': [], u'filesystem': ["ext3", "btrfs", "ext2"], u'compiler': ["llvm"], u'env_padding': [], u'fpie': [], u'sys_time': [], u'link_order': []}
        scope = Factor.get_full_scope()

        js, sw_factors = Scheduler.create_jobs(e, ws, const, scope)

        self.assertEquals(len(js), 0)

    def test_create_jobs(self):
        e = Experiment.objects.all()[0]
        e.job_set.all().delete()

        ws = Worker.objects.all()

        const = {u'cpu_arch': ['arm', 'x86_64'], u'address_randomization': ['off'], u'drop_caches': ['off'], u'num_cpus': ['2'], u'opt_flag': [], u'ram_sz': ["100", "1000", "200"], u'page_sz': ["4000", "4096"], u'autogroup': ['off'], u'freq_scaling': ['off'], u'swap': ['off'], u'cpu_mhz': ["1000", "2000", "3000"], u'filesystem': ["ext3", "btrfs", "ext2"], u'compiler': ["llvm"], u'env_padding': ["0", "10928", "16392"], u'fpie': ['off'], u'link_order': ["alphabetical", "reverse_alphabetical"] }
        scope = Factor.get_full_scope()

        js, sw_factors = Scheduler.create_jobs(e, ws, const, scope)

        opt_flag = Factor.objects.filter(internal_name='opt_flag').get()
        self.assertEquals(len(js), e.replicate_count * len(ws) * len(opt_flag.get_levels()))

    def test_create_jobs_priorities(self):
        e = Experiment.objects.all()[0]
        e.job_set.all().delete()

        ws = Worker.objects.all()

        const = {u'cpu_arch': ['arm', 'x86_64'], u'address_randomization': ['off'], u'drop_caches': ['off'], u'num_cpus': ['2'], u'opt_flag': [], u'ram_sz': ["100", "1000", "200"], u'page_sz': ["4000", "4096"], u'autogroup': ['off'], u'freq_scaling': ['off'], u'swap': ['off'], u'cpu_mhz': ["1000", "2000", "3000"], u'filesystem': ["ext3", "btrfs", "ext2"], u'compiler': ["llvm"], u'env_padding': ["0", "10928", "16392"], u'fpie': ['off'], u'link_order': ["alphabetical", "reverse_alphabetical"] }
        scope = Factor.get_full_scope()

        js, sw_factors = Scheduler.create_jobs(e, ws, const, scope)

        for job in js:
            self.assertGreaterEqual(job.priority, 0)
            self.assertLess(job.priority, 100)

    def test_run_with_all_workers(self):
        for w in Worker.objects.all():
            datamill_db_helpers.create_worker_dir(w)

        for p in Package.objects.all():
            datamill_db_helpers.create_package_file(p)
            p.create_outgoing_file()

        s = Scheduler(create_only=True)

        Job.objects.all().delete()
        for e in Experiment.objects.all():
            e.status = Experiment.CREATED
            e.run_on_all_workers = True
            for setting in WorkerFeature.objects.available():
                Scope.objects.get_or_create(experiment=e, level=setting.level)
                if setting.level.default == True:
                    Constraint.objects.filter(experiment=e, level=setting.level).delete()
            e.save()

        s.run()

        # experiment (from the fixture) 1 is feasible
        # experiment 2 is impossible, but run_on_all ignores worker constraints
        # therefore, job tally should be above rep count for exp 1 and 2 times workers
        min_job_count = Experiment.objects.get(pk=1).replicate_count * Worker.objects.count()
        self.assertGreaterEqual(Job.objects.all().count(), min_job_count)

        for w in Worker.objects.all():
            self.assertGreaterEqual(w.job_set.count(),
                    Experiment.objects.get(pk=1).replicate_count)

    def test_run(self):
        for w in Worker.objects.all():
            datamill_db_helpers.create_worker_dir(w)

        for p in Package.objects.all():
            datamill_db_helpers.create_package_file(p)
            p.create_outgoing_file()

        s = Scheduler(create_only=True)

        Job.objects.all().delete()
        for e in Experiment.objects.all():
            e.status = Experiment.CREATED
            for setting in WorkerFeature.objects.available():
                Scope.objects.get_or_create(experiment=e, level=setting.level)
                if setting.level.default == True:
                    Constraint.objects.filter(experiment=e, level=setting.level).delete()
            e.save()

        s.run()

        # experiment (from the fixture) 1 is feasible
        # experiment 2 is impossible
        # therefore, job tally should be above rep count for exp 1
        min_job_count = Experiment.objects.get(pk=1).replicate_count

        self.assertGreaterEqual(Job.objects.all().count(), min_job_count)

    def test_multiple_packages_priority(self):

        e = Experiment.objects.all()[0]

        for p in Package.objects.all():
            p.experiment = e
            p.save()

        e.job_set.all().delete()
        ws = Worker.objects.all()

        const = {u'cpu_arch': ['arm', 'x86_64'], u'address_randomization': ['off'], u'drop_caches': ['off'], u'num_cpus': ['2'], u'opt_flag': [], u'ram_sz': ["100", "1000", "200"], u'page_sz': ["4000", "4096"], u'autogroup': ['off'], u'freq_scaling': ['off'], u'swap': ['off'], u'cpu_mhz': ["1000", "2000", "3000"], u'filesystem': ["ext3", "btrfs", "ext2"], u'compiler': ["llvm"], u'env_padding': ["0", "10928", "16392"], u'fpie': ['off'], u'link_order': ["alphabetical", "reverse_alphabetical"] }
        scope = Factor.get_full_scope()

        js, sw_factors = Scheduler.create_jobs(e, ws, const, scope)

        sum = 0
        for job in js:
            sum = sum + job.priority

        e.job_set.all().delete()
        Package.objects.get(id=1).delete()
        ws = Worker.objects.all()

        const = {u'cpu_arch': ['arm', 'x86_64'], u'address_randomization': ['off'], u'drop_caches': ['off'], u'num_cpus': ['2'], u'opt_flag': [], u'ram_sz': ["100", "1000", "200"], u'page_sz': ["4000", "4096"], u'autogroup': ['off'], u'freq_scaling': ['off'], u'swap': ['off'], u'cpu_mhz': ["1000", "2000", "3000"], u'filesystem': ["ext3", "btrfs", "ext2"], u'compiler': ["llvm"], u'env_padding': ["0", "10928", "16392"], u'fpie': ['off'], u'link_order': ["alphabetical", "reverse_alphabetical"] }
        scope = Factor.get_full_scope()

        js, sw_factors = Scheduler.create_jobs(e, ws, const, scope)

        sum2 = 0
        for job in js:
            sum2 = sum2 + job.priority
        self.assertGreater(sum, sum2)

