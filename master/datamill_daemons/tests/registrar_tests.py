from django.test import TestCase
from django.conf import settings
import os
import os.path

from datamill_common import datamill_db_helpers
from datamill_daemons.registrar import Registrar
from datamill_daemons.updater import IPUpdater
from datamill_website.models import Worker, Level, Factor

class RegistrarTests(TestCase):

    fixtures = [ 'initial_data', 'datamill_test_fixture' ]

    def setUp(self):
        pass

    def tearDown(self):
        datamill_db_helpers.delete_all_datamill_files()

    def test_list_hello_files(self):

        hello_files = [ 'a', 'b', 'c' ]
        hello_jsons = [ '{0}{1}'.format(name, settings.DATAMILL_HELLO_EXTENSION)
                for name in hello_files ]
        hello_jsons_and_md5s = hello_jsons + [ '{0}{1}'.format(name,
            settings.DATAMILL_INTEG_EXTENSION) for name in hello_jsons ]

        if not os.path.exists(settings.DATAMILL_HELLO_ROOT):
            os.mkdir(settings.DATAMILL_HELLO_ROOT)

        for f in hello_jsons_and_md5s:
            open(os.path.join(settings.DATAMILL_HELLO_ROOT, f), 'w').close()

        returned_jsons = [ os.path.basename(f) for f in Registrar.list_files() ]

        self.assertEquals(set(hello_jsons), set(returned_jsons))

    def test_delete_hello_files(self):
        hello_basenames = [ 'a.json', 'a.json.md5' ]
        hello_files = [ os.path.join(settings.DATAMILL_HELLO_ROOT, f) for f in hello_basenames ]

        if not os.path.exists(settings.DATAMILL_HELLO_ROOT):
            os.mkdir(settings.DATAMILL_HELLO_ROOT)

        for f in hello_files:
            open(f, 'w').close()

        for f in hello_files:
            self.assertTrue(os.path.exists(f))

        Registrar.delete_files(hello_files[0])

        for f in hello_files:
            self.assertFalse(os.path.exists(f))

    def test_run(self):
        r = Registrar(create_only=True)

        datamill_db_helpers.create_hello_file()
        r.run()

        self.assertTrue(Worker.objects.filter(uuid='0e5bec3e-e9b7-4fe7-a482-6f576a4b0843').exists())
        self.assertTrue(os.path.exists(Worker.objects.filter(uuid='0e5bec3e-e9b7-4fe7-a482-6f576a4b0843').get().get_dir()))
        self.assertEquals(Registrar.list_files(), [])
        self.assertEquals(Worker.objects.get(uuid='0e5bec3e-e9b7-4fe7-a482-6f576a4b0843').get_hardware_level('ram_sz'), '3789344')

        orig_count = Level.objects.filter(factor=Factor.objects.get(internal_name='ram_sz')).count()
        ### rereg

        datamill_db_helpers.create_reregistration_hello_file()
        r.run()

        self.assertTrue(Worker.objects.filter(uuid='0e5bec3e-e9b7-4fe7-a482-6f576a4b0843').exists())
        self.assertTrue(os.path.exists(Worker.objects.filter(uuid='0e5bec3e-e9b7-4fe7-a482-6f576a4b0843').get().get_dir()))
        self.assertEquals(Registrar.list_files(), [])
        self.assertEquals(Level.objects.filter(factor=Factor.objects.get(internal_name='ram_sz')).count(), orig_count + 1)
        self.assertEquals(Worker.objects.get(uuid='0e5bec3e-e9b7-4fe7-a482-6f576a4b0843').get_hardware_level('ram_sz'), '13789344')

class IPUpdaterTests(TestCase):

    fixtures = [ 'initial_data', 'datamill_test_fixture' ]

    def setUp(self):
        pass

    def tearDown(self):
        datamill_db_helpers.delete_all_datamill_files()

    def test_list_hello_files(self):

        hello_files = [ 'a', 'b', 'c' ]
        hello_jsons = [ '{0}{1}'.format(name, settings.DATAMILL_IP_EXTENSION)
                for name in hello_files ]
        hello_jsons_and_md5s = hello_jsons + [ '{0}{1}'.format(name,
            settings.DATAMILL_INTEG_EXTENSION) for name in hello_jsons ]

        if not os.path.exists(settings.DATAMILL_IP_ROOT):
            os.mkdir(settings.DATAMILL_IP_ROOT)

        for f in hello_jsons_and_md5s:
            open(os.path.join(settings.DATAMILL_IP_ROOT, f), 'w').close()

        returned_jsons = [ os.path.basename(f) for f in IPUpdater.list_files() ]

        self.assertEquals(set(hello_jsons), set(returned_jsons))

    def test_delete_hello_files(self):
        hello_basenames = [ '0e5bec3e-e9b7-4fe7-a482-6f576a4b0843.json', '0e5bec3e-e9b7-4fe7-a482-6f576a4b0843.json.md5' ]
        hello_files = [ os.path.join(settings.DATAMILL_IP_ROOT, f) for f in hello_basenames ]

        if not os.path.exists(settings.DATAMILL_IP_ROOT):
            os.mkdir(settings.DATAMILL_IP_ROOT)

        for f in hello_files:
            open(f, 'w').close()

        for f in hello_files:
            self.assertTrue(os.path.exists(f))

        IPUpdater.delete_files(hello_files[0])

        for f in hello_files:
            self.assertFalse(os.path.exists(f))

    def test_run(self):
        r = Registrar(create_only=True)
        datamill_db_helpers.create_hello_file()
        r.run()
        self.assertTrue(Worker.objects.filter(uuid='0e5bec3e-e9b7-4fe7-a482-6f576a4b0843').exists())

        m = IPUpdater(create_only=True)
        datamill_db_helpers.create_ip_file()
        m.run()
        self.assertEquals(Worker.objects.get(uuid='0e5bec3e-e9b7-4fe7-a482-6f576a4b0843').ip, '192.168.161.89')
