# standard modules
import sys
from time import sleep
import signal
from django.core.mail import send_mail

# logging
import logging
logger = logging.getLogger(sys.argv[1])

# datamill modules
from django.conf import settings
from datamill_website.models import Job, Worker, Experiment

shutdown_requested = False

class Collector:

    def __init__(self, create_only = False):
        logger.debug("Collector up!")
        self.worker_count = Worker.objects.filter(status__in=[ Worker.IDLE, Worker.WORKING ]).count()
        self.admins_notified = False
        if not create_only:
            while True and not shutdown_requested:
                self.run()
                sleep(settings.DATAMILL_COLLECTOR_LOOP_SECS)

            logger.info("turning off by request")

    def run(self):

        # check jobs that can progress (i.e., not dead, cancelled or failed)
        jobs_to_check = Job.objects.filter(status__in=[ Job.IN_PROGRESS, Job.QUEUED ])

        # we check idle workers too, to catch dead ones
        workers_to_check = Worker.objects.filter(status__in=[ Worker.IDLE, Worker.WORKING ])

        # we check all experiments because workers dying may make experiments finish
        experiments_to_check = Experiment.objects.filter(status__in=[ Experiment.SCHEDULED, Experiment.IN_PROGRESS ])

        current_worker_count = workers_to_check.count()
        if current_worker_count < self.worker_count and current_worker_count < settings.DATAMILL_LOW_WORKERS and not self.admins_notified:
            admin_emails = [user_tuple[1] for user_tuple in settings.ADMINS]
            send_mail('Datamill Worker Count Low', '', settings.DATAMILL_EMAIL, admin_emails)
            self.admins_notified = True
        self.worker_count = current_worker_count

        for j in jobs_to_check:
            try:
                j.update_status()
            except Exception, e:
                logger.warning('failed to update job status, ignoring: {}'.format(e))

        for w in workers_to_check:
            try:
                w.update_status()
            except Exception, e:
                logger.warning('failed to update worker status, ignoring: {}'.format(e))

        for e in experiments_to_check:
            try:
                e.update_status()
            except Exception, e:
                logger.warning('failed to update experiment status, ignoring: {}'.format(e))

def shutdown_daemon(signum, frame):
    global shutdown_requested
    shutdown_requested = True

signal.signal(signal.SIGINT, shutdown_daemon)
signal.signal(signal.SIGTERM, shutdown_daemon)

if __name__ == "__main__":
    try:
        Collector()
    except Exception, e:
        logger.error('Exploded: %s', e)
        raise SystemExit(-1)
