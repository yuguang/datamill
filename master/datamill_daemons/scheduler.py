# standard modules
import sys
from time import sleep
import signal

# logging
import logging
logger = logging.getLogger(sys.argv[1])

# datamill modules
from django.conf import settings
from datamill_website.models import Job, Worker, Experiment, Factor, Constraint, Scope

import glpk
import itertools

shutdown_requested = False

class Scheduler:

    def __init__(self, create_only = False):
        logger.debug('Scheduler up!')
        if not create_only:
            while True and not shutdown_requested:
                self.run()
                sleep(settings.DATAMILL_SCHEDULER_LOOP_SECS)

            logger.info("turning off by request")

    def run(self):

        experiments_to_schedule = Experiment.objects.filter(status=Experiment.CREATED)

        for e in experiments_to_schedule:

            logger.info('working on exp {}'.format(e))
            e.status = Experiment.SCHEDULING
            e.save()

            try:
                # TODO: this dictionary building is redundant with the new schema.
                scope = {}
                constraints = {}
                for factor in Factor.objects.all():
                    scope[factor.internal_name] = [ item.level.internal_name for item in Scope.objects.filter(experiment=e, level__factor=factor) ]
                    constraints[factor.internal_name] = [ item.level.internal_name for item in Constraint.objects.filter(experiment=e, level__factor=factor) ]
            except Exception, exc:
                e.status = Experiment.IMPOSSIBLE
                e.save()
                logger.warning('failed to process specs for exp {}: {}'.format(e, exc))
                continue

            try:
                if e.run_on_all_workers:

                    worker_list = Worker.objects.available()
                    num_hw_factors = 0

                    for f in Factor.objects.filter(fac_type=Factor.HARDWARE):
                        if len(f.get_levels(worker_list)) > 1:
                            num_hw_factors += 1 # increment number of factors explored

                    e.num_hardware_factors = num_hw_factors

                else:
                    worker_list, e.num_hardware_factors = Scheduler.select_workers(constraints, scope)
                    logger.info('selected {} workers for exp'.format(len(worker_list)))

            except ValueError:
                e.status = Experiment.IMPOSSIBLE
                e.save()
                logger.warning('exp {} is impossible, no workers selected'.format(e))
                continue
            except Exception, exc:
                e.status = Experiment.IMPOSSIBLE
                e.save()
                logger.warning('failed to select workers for exp {}: {}'.format(e, exc))
                continue

            try:
                jobs, e.num_software_factors = Scheduler.create_jobs(e, worker_list, constraints, scope)

                if len(jobs) == 0:
                    e.status = Experiment.IMPOSSIBLE
                    e.save()
                    logger.warning('exp {} is impossible, no jobs created'.format(e))
                    continue

                logger.info('created {} jobs for exp {}'.format(len(jobs), e))

            except Exception, exc:
                e.status = Experiment.IMPOSSIBLE
                e.save()
                logger.warning('failed to create jobs for exp {}: {}'.format(e, exc))
                continue

            try:
                for j in jobs:
                    j.save()
                    j.create_outgoing_files()
            except Exception, exc:
                e.status = Experiment.IMPOSSIBLE
                e.save()
                logger.warning('failed to commit jobs for exp {}: {}'.format(e, exc))
                continue

            e.status = Experiment.SCHEDULED
            e.save()
            logger.info('done with exp {}'.format(e))

    @classmethod
    def select_workers(cls, constraints, scope):

        #####################################################################
        # QUERY/CLEANUP WORKERS
        #####################################################################
        # ATTENTION: workers will come and go as this executes.
        # we need to query workers _ONCE_ and work off that set.
        # when getting factor levels, restrict your query to the
        # initial, pruned set of workers defined here.
        #####################################################################

        workers = list(Worker.objects.available())
                                             # this is all workers we'll work with.
                                             # we call list to force a hit on the db

        logger.info('selection starting with {} workers'.format(len(workers)))
        workers_to_rm = []

        hw_factors = Factor.objects.filter(fac_type=Factor.HARDWARE)

        for f in hw_factors:
            fname = f.internal_name
            for w in workers:
                # check if fname is in scope in case we add more hw factors dynamically
                if fname in scope:
                    # e.g., if this worker's arch is explicitely forbidden or not in scope
                    #           remove current worker from eligible workers
                    level = w.get_hardware_level(fname)
                    if (level in constraints[fname]) or (level not in scope[fname]):
                        workers_to_rm.append(w)

        workers = list(set(workers) - set(workers_to_rm))

        if len(workers) is 0:
            logger.warning('cleanup of worker pool left scheduler with no workers')
            raise ValueError

        logger.info('cleanup of worker pool left selection with {} workers'.format(len(workers)))

        ####################################################################
        # FACTOR DICTIONARY
        ####################################################################

        total_level_count = 0 # running count of levels between _all_ hw factors

        factors = {} # dictionary of hw factors constrained to eligible workers
                     # keys: factor internal names, vals: list of distinct levels
        num_hw_factors = 0

        for f in hw_factors:
            fname = f.internal_name
            factors[fname] = f.get_levels(workers)
            total_level_count += len(factors[fname])
            if len(factors[fname]) > 1:
                num_hw_factors += 1 # increment number of factors explored

        ####################
        # OBJECTIVE FUNCTION
        ####################

        lp = glpk.LPX()
        lp.cols.add(len(workers) + total_level_count) # as many decision vars as there are machines and dimension _levels_
                                                # (i.e., 'i686' and 'sparc' get their own decision vars)

        for w in range(0, len(workers)):
            lp.obj[w] = workers[w].job_set.filter(status__in=[Job.QUEUED, Job.IN_PROGRESS]).count() + 1 # workers matter for obj func. proportional to job queue length

        for d in range(len(workers), total_level_count):
            lp.obj[d] = 0 # factors do not matter

        lp.obj.maximize = False # aka minimize

        for col in lp.cols: # vars must be true or false
            col.bounds = 0.0, 1.0

        ####################
        # CONSTRAINTS
        ####################

        level_idx = len(workers) # indexes factor level decision vars in the problem
                                 # it's a "flat" count over all levels of _all_ dims
                                 # starts at len(workers), indexing the decision vars
                                 # past the machines

        for f in factors: # iterates over 'cpu_arch', 'num_cpus', etc

            levels_at_factor = [] # what decision vars belong to this factor

            for l in factors[f]: # iterates over '686', 'sparc', etc
                #logger.info("f: {0} l: {1}".format(f, l))

                worker_idx = 0 # indexes worker decision vars in the problem
                workers_at_level = [] # what machines are of this level in this dimension

                for worker in workers:
                    worker_level = worker.get_hardware_level(f)
                    #logging.info("        worker {0}: {1}:{2}".format(worker_idx, f, worker_level))

                    if(worker_level == l): # i.e., the current dimension is cpu_arch, the current level is i686. are you an i686?
                        # add a row per machine, guaranteeing that selecting the machine causes the selection of the level
                        lp.rows.add(1)
                        lp.rows[-1].matrix = [ (level_idx, 1.0), (worker_idx, -1.0) ]
                        lp.rows[-1].bounds = 0.0, None # level - worker >= 0
                        workers_at_level.append(worker_idx)
                        #logger.info("                     ...added it")

                    worker_idx += 1

                # add a row per factor level, guaranteeing that selecting the level causes at least one machine to be selected
                lp.rows.add(1)
                constraint = [ (level_idx, -1.0) ]

                for w_idx in workers_at_level:
                    #logger.info("    worker {0} added to {1} constraint {2}".format(w_idx, f, level_idx))
                    constraint.append( (w_idx, 1.0) )

                lp.rows[-1].matrix = constraint
                lp.rows[-1].bounds = 0.0, None # sum(workers_at_level) - level >= 0

                levels_at_factor.append(level_idx)
                level_idx += 1

            # add a row per factor, forcing the min number of levels requested by the user
            lp.rows.add(1)

            constraint = []
            for l in levels_at_factor:
                constraint.append( (l, 1.0) )

            lp.rows[-1].matrix = constraint
            lp.rows[-1].bounds = min(settings.DATAMILL_SCHEDULER_MAX_WORKERS_PER_EXPERIMENT,
                    len(factors[f])), None # sum(selected_levels_of_dimension) >= level count target

        ####################
        # SOLVE
        ####################

        #for i in range(len(lp.rows)):
        #    logger.debug(lp.rows[i].matrix)

        solution_status = lp.simplex()
        logger.info("relaxed status: {0} obj val: {1}".format(lp.status, lp.obj.value))

        if solution_status is not None or lp.status != 'opt':
            logger.warning('relaxed solution yielded no or sub-optimal result')
            raise ValueError

        for col in lp.cols:
            col.kind = int

        solution_status = lp.integer()
        logger.info("integer status: {0} obj val: {1}".format(lp.status, lp.obj.value))

        if solution_status is not None or lp.status != 'opt':
            logger.warning('integer solution yielded no or sub-optimal result')
            raise ValueError

        workers_in_solution = []

        for i in range(len(workers)):
            if(lp.cols[i].value > 0.99): # don't do equals 1 as per http://tfinley.net/software/pyglpk/ex_sat.html
                workers_in_solution.append(workers[i])

        logging.info(workers_in_solution)

        if len(workers_in_solution) is 0:
            logger.error('no workers selected in optimal result? should never happen')
            raise ValueError

        return workers_in_solution, num_hw_factors

        # TODO OPTIMIZATION:
        # ------------------
        # the nested loop over workers inside the factor loop
        # appears to be sub-optimal, and may be a bottleneck
        # once (a) the number of workers increases significantly
        # and (b) the amount of new experiments being submitted
        # increases significantly.
        # there should be three base-level for loops here:
        # (1) for w in workers
        #         for f in factor
        #             add a constraint for that worker-factor combo
        #
        # (2) for l in flat_factor_levels
        #         add a constraint for factor level that 
        # 
        # (3) for f in factor
        #         add a constraint for that factor

    @classmethod
    def create_jobs(cls, experiment, worker_list, constraints, scope):
        # ATTENTION: does NOT save jobs, just returns them in a list

        sw_factors = Factor.objects.filter(fac_type=Factor.SOFTWARE)
        num_sw_factors = 0
        selected_levels = {}

        jobs_per_worker = experiment.replicate_count
        for f in sw_factors:
            fname = f.internal_name

            # fname might not be in scope if the factor was created between
            # experiment submission and scheduling. in that case, just use the default
            if fname not in scope:
                selected_levels[fname] = [ f.get_default() ]
            else:
                selected_levels[fname] = list(set(scope[fname]) - set(constraints[fname]))

            if len(selected_levels[fname]) > 1:
                num_sw_factors += 1 # increment number of factors explored
            jobs_per_worker = jobs_per_worker * len(selected_levels[fname])

        exhaustive_combinations = itertools.product(*selected_levels.values())

        packages = experiment.package_set.all()
        generated_jobs = []

        jobs_per_worker = jobs_per_worker * experiment.package_set.count()
        exp_priority = Experiment.calculate_priority(jobs_per_worker)

        # outer loop must be combinations since we're consuming the itertools.product
        for c_no, c in enumerate(exhaustive_combinations):
            for w in worker_list:
                for p in packages:
                    for r in range(experiment.replicate_count):

                        j = Job()
                        j.experiment = experiment
                        j.package = p
                        j.priority = exp_priority
                        j.worker = w
                        j.config_id = c_no
                        j.replicate_no = r

                        config_dict = {}

                        for k_no, k in enumerate(selected_levels.keys()):
                            config_dict[k] = c[k_no]

                        j.save()
                        j.set_configuration(config_dict)
                        generated_jobs.append(j)

        return generated_jobs, num_sw_factors

def shutdown_daemon(signum, frame):
    global shutdown_requested
    shutdown_requested = True

signal.signal(signal.SIGINT, shutdown_daemon)
signal.signal(signal.SIGTERM, shutdown_daemon)

if __name__ == "__main__":
    try:
        Scheduler()
    except Exception, e:
        logger.error('Exploded: %s', e)
        raise SystemExit(-1)
