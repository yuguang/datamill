# Django settings for datamill project.
import os.path
import os

################################
#      DATAMILL SETTINGS       #
#------------------------------#
#  make local settings in your #
#   local_settings.py file!    #
################################

DATAMILL_DEBUG=False

DATAMILL_GIT_ROOT='/var/www/localhost/wsgi/eval-lab'
DATAMILL_LOG_ROOT='/var/datamill/log/'
DATAMILL_FTP_ROOT='/home/ftp/'
DATAMILL_RESULTS_ROOT='/var/datamill/results/'
DATAMILL_MEDIA_ROOT='/var/datamill/media/'
DATAMILL_STATIC_ROOT='/var/www/localhost/htdocs'

DATAMILL_DATABASE_PASSWORD=os.environ.get("DATAMILL_DATABASE_PASSWORD", '')
DATAMILL_EMAIL='datamill@uwaterloo.ca'
DATAMILL_EMAIL_HOST_PASSWORD=os.environ.get("DATAMILL_EMAIL_HOST_PASSWORD", '')
GS_ACCESS_KEY_ID = os.environ.get("GS_ACCESS_KEY_ID", '')
GS_SECRET_ACCESS_KEY = os.environ.get("GS_SECRET_ACCESS_KEY", '')

DATAMILL_PACKAGE_EXTENSION = '.tar.gz'
DATAMILL_RESULTS_EXTENSION = '.tar.gz'
DATAMILL_CONFIG_EXTENSION  = '.json'
DATAMILL_HELLO_EXTENSION   = '.json'
DATAMILL_IP_EXTENSION   = '.json'
DATAMILL_WIP_EXTENSION     = '.wip'
DATAMILL_DONE_EXTENSION    = '.done'
DATAMILL_JOB_STATUS_EXTENSION   = '.json'
DATAMILL_WORKER_STATUS_EXTENSION   = '.json'
DATAMILL_INTEG_EXTENSION   = '.md5'

# collector settings
DATAMILL_COLLECTOR_LOOP_SECS=60
DATAMILL_DAYS_UNTIL_WORKER_DEAD = 2
DATAMILL_LOW_WORKERS = 10

# scheduler settings
DATAMILL_SCHEDULER_MAX_WORKERS_PER_EXPERIMENT=3
DATAMILL_SCHEDULER_LOOP_SECS=60

# registrar settings
DATAMILL_REGISTRAR_LOOP_SECS=60

EXPERIMENTS_ALLOWED_PER_WORKER_CONTRIBUTED = 5
DEFAULT_WORKER_OWNER_ID = 11

MEDIA_VERSION = 1

SESSION_COOKIE_SECURE=True
CSRF_COOKIE_SECURE=True

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

try:
    from local_settings import *
except ImportError:
    pass

try:
    from static_media_version import *
except ImportError:
    pass

DATAMILL_HELLO_ROOT= os.path.join(DATAMILL_FTP_ROOT, 'worker-new')
DATAMILL_IP_ROOT= os.path.join(DATAMILL_FTP_ROOT, 'worker-ip')

##############################
#  END DATAMILL SETTINGS END
##############################

DEBUG = DATAMILL_DEBUG
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Augusto Oliveira', 'a3olivei@uwaterloo.ca'),
    ('Jean-Christophe Petkovich', 'j2petkov@uwaterloo.ca'),
    ('Yuguang Zhang', 'y279zhan@uwaterloo.ca'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'USER': 'datamill-dbuser',
        'PASSWORD': DATAMILL_DATABASE_PASSWORD,
        'HOST': 'localhost',             # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
        'NAME': 'datamilldb',

        'OPTIONS': {
            'autocommit': True,
        },
    },
}

REDIS_CONFIG = {
    'host': 'mini.resl.uwaterloo.ca',
    'port': 8000,
    'db': 0,
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = [ 'datamill.uwaterloo.ca' ]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Toronto'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = DATAMILL_MEDIA_ROOT

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = 'http://mini.resl.uwaterloo.ca/data/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = DATAMILL_STATIC_ROOT

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'tgycn+=*nan-3^-umdx)bcw+3mx7qa*7z!ko($hogtd)nvddux'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware'
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

LOCALE_PATHS = (
    os.path.join(DATAMILL_GIT_ROOT, 'master/locale'),
)

ROOT_URLCONF = 'datamill_django_project.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'datamill_django_project.wsgi.application'

SENDFILE_BACKEND = 'sendfile.backends.xsendfile'

TEMPLATE_DIRS = (
    os.path.join(DATAMILL_GIT_ROOT, 'master/datamill_website/templates'),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

TEMPLATE_CONTEXT_PROCESSORS = (
        "django.contrib.auth.context_processors.auth",
        "django.core.context_processors.request",
        "django.core.context_processors.debug",
        "django.core.context_processors.i18n",
        "django.core.context_processors.media",
        "django.core.context_processors.static",
        "django.core.context_processors.tz",
        "django.contrib.messages.context_processors.messages",
        'sekizai.context_processors.sekizai',
        'datamill_website.context_processors.media',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'datamill_website',
    'datamill_daemons',
    'django_extensions',
    'reversion',
    'south',
    'guardian',
    'taggit',
    'sendfile',
    # wiki from here
    'django.contrib.humanize',
    'django_notify',
    'mptt',
    'sekizai',
    'sorl.thumbnail',
    'wiki',
    'wiki.plugins.attachments',
    'wiki.plugins.notifications',
    'wiki.plugins.images',
    'wiki.plugins.macros',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s:%(levelname)s:%(filename)s:%(lineno)d:%(funcName)s:%(message)s',
            'datefmt':'%m/%d/%Y %I-%M-%S%p',
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
            },
        'datamill_registrar_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(DATAMILL_LOG_ROOT, 'registrar.log'),
            'formatter':'verbose',
        },
        'datamill_collector_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(DATAMILL_LOG_ROOT, 'collector.log'),
            'formatter':'verbose',
        },
        'datamill_scheduler_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(DATAMILL_LOG_ROOT, 'scheduler.log'),
            'formatter':'verbose',
        },
        'datamill_general_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(DATAMILL_LOG_ROOT, 'datamill_general.log'),
            'formatter':'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
            },
        },
    'loggers': {
        'datamill_registrar': {
            'handlers': ['mail_admins', 'datamill_registrar_logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'datamill_collector': {
            'handlers': ['mail_admins', 'datamill_collector_logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'datamill_scheduler': {
            'handlers': ['mail_admins', 'datamill_scheduler_logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'datamill_models': {
            'handlers': ['mail_admins', 'datamill_general_logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'datamill_common': {
            'handlers': ['mail_admins', 'datamill_general_logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'test': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# AUTHENTICATION
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
)

SOUTH_MIGRATION_MODULES = {
    'taggit': 'taggit.south_migrations',
}

# AUTH SETTINGS
LOGIN_URL='/login/'
LOGOUT_URL='/logout'
LOGIN_REDIRECT_URL='/' # send users back home after login
ANONYMOUS_USER_ID = -1

# EMAIL SETTINGS
EMAIL_HOST='mailservices.uwaterloo.ca'
EMAIL_PORT=587
EMAIL_HOST_USER='datamill'
EMAIL_BACKEND ='django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST_PASSWORD=DATAMILL_EMAIL_HOST_PASSWORD
EMAIL_USE_TLS=True
DEFAULT_FROM_EMAIL = DATAMILL_EMAIL
SERVER_EMAIL= DATAMILL_EMAIL

# Messages
EXPERIMENT_EXCEED_WARNING = 'You\'ve exceeded your maximum number of allowed experiments. Install more workers or wait for your experiments to finish. '
NO_NEW_WORKER_WARNING = 'Currently, there are no new workers to register. If you\'ve just installed a worker, wait an hour before checking again. '
INVALID_EXPERIMENT_SUBMISSION = 'Sorry, your experiment was not submitted due to errors in the form. '

# WIKI SETTINGS
WIKI_ACCOUNT_HANDLING=False

# SEND EMAILS FROM LOCAL DEVELOPMENT SERVER
# python -m smtpd -n -c DebuggingServer localhost:1025
if DEBUG:
    EMAIL_HOST = 'localhost'
    EMAIL_PORT = 1025
    EMAIL_HOST_USER = ''
    EMAIL_HOST_PASSWORD = ''
    EMAIL_USE_TLS = False
    DEFAULT_FROM_EMAIL = 'testing@example.com'

DEFAULT_FILE_STORAGE = 'django-google-storage.storage.GoogleStorage'

GS_STORAGE_BUCKET_NAME = 'datamill'

