import os.path

# these are sane defaults you shouldn't have to change:
DATAMILL_DJANGO_PROJECT_ROOT=os.path.dirname(os.path.realpath(__file__))
DATAMILL_MASTER_ROOT=os.path.dirname(DATAMILL_DJANGO_PROJECT_ROOT)
DATAMILL_GIT_ROOT=os.path.dirname(DATAMILL_MASTER_ROOT)

# make sure to create the following paths:
DATAMILL_LOG_ROOT=os.path.join(DATAMILL_MASTER_ROOT, 'local', 'local_logs')
DATAMILL_FTP_ROOT=os.path.join(DATAMILL_MASTER_ROOT, 'local', 'local_ftp')
DATAMILL_RESULTS_ROOT=os.path.join(DATAMILL_MASTER_ROOT, 'local', 'local_results')
DATAMILL_MEDIA_ROOT=os.path.join(DATAMILL_MASTER_ROOT, 'local', 'local_media')

DATAMILL_DATABASE_PASSWORD='datamill-password'
DATAMILL_EMAIL_HOST_PASSWORD='Fastplane1@'

SESSION_COOKIE_SECURE=False
CSRF_COOKIE_SECURE=False

DATAMILL_DEBUG=True

SESSION_COOKIE_SECURE=False
CSRF_COOKIE_SECURE=False

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

for d in [DATAMILL_LOG_ROOT, DATAMILL_FTP_ROOT, DATAMILL_RESULTS_ROOT, DATAMILL_MEDIA_ROOT]:
    try:
        os.makedirs(d)
    except OSError:
        pass
