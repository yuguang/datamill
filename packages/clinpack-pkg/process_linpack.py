import tarfile
import sys
import os
import re
import tempfile
import shutil
import csv

in_filename = sys.argv[1]
out_filename = sys.argv[2]

EXTRACT_PATH = tempfile.mkdtemp()
OUTPUT_FILENAME =  'linpack-output.txt'

metric_patterns = {}

metric_patterns['trmo'] = r"times are reported for matrices of order   (\d+)"
metric_patterns['tald'] = r"times for array with leading dimension of (\d+)"
metric_patterns['rdp'] = r"Rolled Double  Precision (.+)"

tar = tarfile.open(in_filename)
tar.extractall(path=EXTRACT_PATH)
tar.close()

with open(os.path.join(EXTRACT_PATH, OUTPUT_FILENAME)) as of:
    lines = of.read()

results = {}


for m, p in metric_patterns.iteritems():
    results[m] = re.search(p, lines).group(1)

with open(out_filename,'w') as cf:
    cw = csv.DictWriter(cf, fieldnames=results.keys())
    cw.writeheader()
    cw.writerow(results)

shutil.rmtree(EXTRACT_PATH)
