#!/bin/sh

output=kernel-build.txt

echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: starting defconfig' > $output
make -C linux-3.10.12 defconfig
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: starting make' >> $output
make -C linux-3.10.12
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: finished make' >> $output
