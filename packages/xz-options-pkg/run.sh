#!/usr/bin/env bash

# VERBOSE=true

function call_xz() {
    local config_name=$1

    local config_number="$(echo ${config_name##*/} | cut -d'_' -f 1)"

    local expname="${config_number}!${CHECK_TYPE}!${PRESET_LEVEL}!${EXTREME}!${SPARSE}!${MEMORY_USAGE}" 

    # Pre cleanup, make no assumptions
    rm -f maximum_compression.tar.xz > /dev/null 2>&1

    if [[ ! -f maximum_compression.tar ]]; then
        tar caf maximum_compression.tar maximum_compression
    fi
    # memory and with zero
    if [[ -n ${MEMORY_USAGE} ]]; then
        MEM_FLAG="-M ${MEMORY_USAGE}"
    else
        MEM_FLAG=""
    fi

    if [[ -n ${CHECK_TYPE} ]]; then
        CHECK_FLAG="-C ${CHECK_TYPE}"
    else
        CHECK_FLAG=""
    fi
    
    if [[ -n $VERBOSE ]]; then
        echo "Test config: "
        echo "CHECK_FLAG: ${CHECK_FLAG}"
        echo "PRESET_LEVEL: ${PRESET_LEVEL}"
        echo "EXTREME: ${EXTREME}"
        echo "SPARSE: ${SPARSE}"
        echo "MEM_FLAG: ${MEM_FLAG}"
        echo "expname: ${expname}.timing"
    fi 
        
    /usr/bin/time xz --keep ${CHECK_FLAG} ${PRESET_LEVEL} ${EXTREME} ${SPARSE} ${MEM_FLAG} maximum_compression.tar > "${expname}.timing" 2>&1

    du -s maximum_compression.tar.xz > "${expname}.size" 

    # Cleanup
    rm maximum_compression.tar.xz
}

for configuration in configurations/*; do
    [[ -n $VERBOSE ]] && echo "Sourcing $configuration"
    source "$configuration"
    call_xz "$configuration"
done 
