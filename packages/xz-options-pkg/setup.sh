#!/usr/bin/env sh

# All we need is xz
emerge --quiet --noreplace xz-utils
emerge --quiet --noreplace sys-process/time
