#!/usr/bin/env python

import os
import csv
import shutil
import subprocess

dirlist = os.listdir('.')

def get_index_file(paths):
    return filter(lambda path: path.endswith('results_index.csv'), dirlist)[0]

def get_resultsdir(filespec):
    return filespec.rpartition('.tar.gz')[0]

def extract_results(filespec):

    resultsdir = get_resultsdir(filespec)
    subprocess.check_call('tar xavf {tarball} -C {resultsdir}'\
                          .format(
                              tarball=filespec,
                              resultsdir=resultsdir
                          ), shell=True)

def preprocess(filespec):

    resultsdir = get_resultsdir(filespec)

    dirlist = map(lambda path: os.path.join(resultsdir, path), os.listdir(resultsdir))

    timingfiles = filter(lambda path: path.endswith('.timing'), dirlist)
    sizefiles = filter(lambda path: path.endswith('.size'), dirlist)
    assert(len(timingfiles) == len(sizefiles))
    experiments = map(lambda path: path.rpartition('.timing')[0], timingfiles)

    for experiment in experiments:

        exp_elapsed = None
        exp_size = None

        with open("{experiment}.timing".format(experiment=experiment), 'r') as timing:
            elapsed = timing.readline().split()[2].rpartition('elapsed')[0]
            elapsed = map(float, elapsed.split(":"))
            exp_elapsed = elapsed[0] * 60 + elapsed[1]

        with open("{experiment}.size".format(experiment=experiment), 'r') as size:
            exp_size = size.readline().split()[0]

        with open("{experiment}.csv".format(experiment=experiment), 'w') as summary:
            summarywriter = csv.writer(summary)
            summarywriter.writerow([exp_size, exp_elapsed])


def consolidate(filespec, row, writer):

    resultsdir = get_resultsdir(filespec)
    dirlist = map(lambda path: os.path.join(resultsdir, path), os.listdir(resultsdir))
    summaryfile = filter(lambda path: path.endswith('summary.csv'), dirlist)[0]
    assert(os.path.exists(summaryfile))

    with open(summaryfile, 'r') as exp_results:
        exp_results_read = csv.reader(exp_results)
        final_rows = filter(lambda row: row != [], exp_results_read)
        print final_rows
        final_rows = map(lambda exp_row: row + exp_row, final_rows)
        map(writer.writerow, final_rows)
    
def cat_csv(resultsdir):

    subprocess.check_call('cd {resultsdir}; cat-csv -o summary.csv'\
                          .format(
                              resultsdir=resultsdir
                          ), shell=True)

def process_file(filespec, row, writer):
    print "Extracting"
    resultsdir = filespec.rpartition('.tar.gz')[0]

    if os.path.exists(resultsdir):
        shutil.rmtree(resultsdir)

    os.mkdir(resultsdir)

    extract_results(filespec)
    preprocess(filespec)
    cat_csv(resultsdir)
    consolidate(filespec, row, writer)


index_file = get_index_file(dirlist)

with open(index_file, "r") as f:
    csvreader = csv.reader(f)
    header = csvreader.next()
    file_col = header.index('results_file')

    with open("all_results.csv", "w") as outfile:
        writer = csv.writer(outfile)
        writer.writerow(header + ["config", "check", "preset", "extreme", "sparse", "memory", "size", "time"])
        
        for row in csvreader:
            process_file(row[file_col], row, writer)


