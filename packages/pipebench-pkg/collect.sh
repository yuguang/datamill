#!/bin/sh

results=pipebench-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results pipebench-output.txt >> /dev/null 2>&1

echo $results
