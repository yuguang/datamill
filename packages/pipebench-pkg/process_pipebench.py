import tarfile
import sys
import os
import re
import tempfile
import shutil
import csv

in_filename = sys.argv[1]
out_filename = sys.argv[2]

EXTRACT_PATH = tempfile.mkdtemp()
OUTPUT_FILENAME =  'pipebench-output.txt'

metric_patterns = {}

metric_patterns['result'] = r"Piped(.+)GB in(.+)   (.+)GB/second"

tar = tarfile.open(in_filename)
tar.extractall(path=EXTRACT_PATH)
tar.close()

with open(os.path.join(EXTRACT_PATH, OUTPUT_FILENAME)) as of:
    lines = of.read()

results = {}


for m, p in metric_patterns.iteritems():
	find = re.search(p, lines)
	for i in range(1,find.lastindex+1):
		results[m+str(i)] = find.group(i).strip()

with open(out_filename,'w') as cf:
    cw = csv.DictWriter(cf, fieldnames=results.keys())
    cw.writeheader()
    cw.writerow(results)

shutil.rmtree(EXTRACT_PATH)
