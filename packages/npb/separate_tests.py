import shutil
import os
import stat

if not os.path.exists('separated'):
    os.mkdir('separated')

clean_npbs = [ 'NPB3.3-OMP', 'NPB3.3-MPI', 'NPB3.3-SER' ]

for clean_npb in clean_npbs:

    stripped_npb='NPB3.3.1-stripped/{}'.format(clean_npb)
    full_npb='NPB3.3.1/{}'.format(clean_npb)

    setup_template = """#!/bin/bash

    {emergestring}
    make -C {npbroot} {app} CLASS={input} {extrasetupstring}

    """

    run_template = """#!/bin/bash

    {exportstring}
    ./{npbroot}/bin/{appbin} > {app}.out 2>&1

    """

    collect_template = """#!/bin/bash

    results={npbroot}-{app}-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
    tar czf $results {app}.out >> /dev/null 2>&1

    echo $results
    """

    with open('tests_{}'.format(clean_npb)) as f:
        test_lines = f.readlines()

    test_lines = [ c.strip() for c in test_lines if not c.startswith('#') ] # remove commented out tests/libs
    tests = [ c.split(',') for c in test_lines ] # remove \n

    extrasetupstring = ''
    exportstring=''
    emergetgts = []

    if clean_npb == 'NPB3.3-MPI':
        emergetgts.append('openmpi')
        extrasetupstring = 'NPROCS=4'

    if clean_npb == 'NPB3.3-OMP':
        exportstring="export OMP_NUM_THREADS=4"

    for t in tests:
        print('doing {}'.format(t[0]))

        tdir = 'separated/{}-{}-pkg'.format(clean_npb, t[0])
        os.mkdir(tdir)

        emergestring = ''
        if emergetgts:
            emergestring = 'emerge --noreplace {}'.format(' '.join(emergetgts))

        with open(os.path.join(tdir, 'setup.sh'), 'w') as setupsh:
            setupsh.write(setup_template.format(npbroot=clean_npb, app=t[0], input=t[1], emergestring=emergestring, extrasetupstring=extrasetupstring))

        with open(os.path.join(tdir, 'run.sh'), 'w') as runsh:
            runsh.write(run_template.format(npbroot=clean_npb, app=t[0], appbin=t[2], exportstring=exportstring))

        with open(os.path.join(tdir, 'collect.sh'), 'w') as collectsh:
            collectsh.write(collect_template.format(npbroot=clean_npb, app=t[0]))

        for f in [ 'setup.sh', 'run.sh', 'collect.sh' ]:
            full_file = os.path.join(tdir, f)
            os.chmod(full_file, os.stat(full_file).st_mode | stat.S_IEXEC)

        # cp no-input tree over
        shutil.copytree(stripped_npb, os.path.join(tdir, clean_npb))

        # copy bench over
        shutil.copytree(os.path.join(full_npb, t[0]), os.path.join(tdir, clean_npb, t[0]))
