#!/usr/bin/env python2

from __future__ import print_function
import os
import sys
import subprocess
import argparse

SRC_DIR = os.getcwd()
PKG_DIR = SRC_DIR

def make_package(func=None, src_path=None, pkg_dir=None):
    """Create a single package based on root argument."""

    print('packaging {}'.format(src_path))
    pkgname = os.path.basename(src_path)
    pkgfile = os.path.join(pkg_dir, '{}.tar.gz'.format(pkgname))
    subprocess.check_call('tar czf {pkgfile} -C {root} .'
                          .format(pkgfile=pkgfile, root=src_path),
                          shell=True)

def make_all_packages(func=None, src_path=None, pkg_dir=None):
    """Create all packages."""

    for root, dirs, files in os.walk(src_path):
        if root.endswith('-pkg'):
            make_package(src_path=root, pkg_dir=pkg_dir)

# ******************** manage.py app ********************
parser = argparse.ArgumentParser(description="Manage DataMill packages.")
subparsers = parser.add_subparsers(help='sub-command help')

# ==================== Package everything ====================
p_everything = subparsers.add_parser('all', help = make_all_packages.__doc__)
p_everything.add_argument('src_path', type=str, help="Path to the package sources, defaults to cwd.", default=SRC_DIR)
p_everything.add_argument('--pkg_dir', type=str, help="Location of the final packages, defaults to cwd.", default=PKG_DIR)
p_everything.set_defaults(func=make_all_packages)

# ==================== Package only one ====================
p_single = subparsers.add_parser('single', help = make_all_packages.__doc__)
p_single.add_argument('src_path', type=str, help="Path to the package source.")
p_single.add_argument('--pkg_dir', type=str, help="Location of the final package, defaults to cwd.", default=PKG_DIR)
p_single.set_defaults(func=make_package)

args = parser.parse_args(sys.argv[1:])
print(args)
args.func(**vars(args))
