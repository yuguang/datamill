import tarfile
import sys
import os
import re
import tempfile
import shutil
import csv

in_filename = sys.argv[1]
out_filename = sys.argv[2]

EXTRACT_PATH = tempfile.mkdtemp()
OUTPUT_FILENAME = 'pgbench.out'

number_pattern = r"[\d\.]+"

metric_patterns = {}

metric_patterns['transaction_type']        = r"transaction type:\s+(\S.*)$".format(number=number_pattern)
metric_patterns['scaling_factor']          = r"scaling factor:\s+({number})".format(number=number_pattern)
metric_patterns['query_mode']              = r"query mode:\s+(\S.*)$".format(number=number_pattern)
metric_patterns['number_of_clients']       = r"number of clients:\s+({number})".format(number=number_pattern)
metric_patterns['number_of_threads']       = r"number of threads:\s+({number})".format(number=number_pattern)
metric_patterns['duration']                = r"duration:\s+({number})".format(number=number_pattern)
metric_patterns['number_of_transactions']  = r"number of transactions actually processed:\s+({number})".format(number=number_pattern)
metric_patterns['tps_with_connections']    = r"tps =\s+({number})\s+\(including connections establishing\)".format(number=number_pattern)
metric_patterns['tps_without_connections'] = r"tps =\s+({number})\s+\(excluding connections establishing\)".format(number=number_pattern)

tar = tarfile.open(in_filename)
tar.extractall(path=EXTRACT_PATH)
tar.close()

with open(os.path.join(EXTRACT_PATH, OUTPUT_FILENAME)) as of:
    lines = of.read()

results = {}

for m, p in metric_patterns.iteritems():
    results[m] = re.search(p, lines, re.MULTILINE).group(1)

with open(out_filename, 'w') as cf:
    cw = csv.DictWriter(cf, fieldnames=results.keys())
    cw.writeheader()
    cw.writerow(results)

shutil.rmtree(EXTRACT_PATH)
