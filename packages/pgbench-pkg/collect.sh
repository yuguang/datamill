#!/bin/sh

results=pgbench-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results pgbench.out >> /dev/null 2>&1

echo $results
