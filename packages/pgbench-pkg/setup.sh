#!/bin/sh

if ! getent group postgres >/dev/null; then
    groupadd postgres
    useradd -c 'PostgreSQL user' -g postgres -d '/var/lib/postgres' -s /bin/bash postgres
fi

sudo=''
if [[ $EUID -ne 0 ]]; then
    sudo='sudo'
fi

bench_root='/pgbench'
${sudo} mkdir -m 777 -p $bench_root

# make pg
cd postgresql-9.3.1
./configure --prefix ${bench_root}/pg/
make
make install

# make pgbench
make -C contrib/pgbench
make -C contrib/pgbench install

export PGDATA=${bench_root}/pg/pg_data/
mkdir -m 700 -p $PGDATA

${sudo} chown -R postgres:postgres ${bench_root}/pg
${sudo} su - postgres -c "${bench_root}/pg/bin/initdb -D ${PGDATA}"
${sudo} su - postgres -c "${bench_root}/pg/bin/pg_ctl -D ${PGDATA} start -w"
${sudo} su - postgres -c "${bench_root}/pg/bin/createuser -s -h 127.0.0.1 pgbenchuser"
${sudo} su - postgres -c "${bench_root}/pg/bin/createdb -h 127.0.0.1 -U pgbenchuser pgbench"
${sudo} su - postgres -c "${bench_root}/pg/bin/pgbench -h 127.0.0.1 -U pgbenchuser -i -s 30 pgbench"
