#!/bin/sh

bench_root='/pgbench'

${bench_root}/pg/bin/pgbench -h 127.0.0.1 -U pgbenchuser -c 64 -j 4 -T 6 pgbench > pgbench.out 2>&1
