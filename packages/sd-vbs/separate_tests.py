import shutil
import os
import stat

if not os.path.exists('separated'):
    os.mkdir('separated')

setup_template = """#!/bin/bash

echo 'nothing to do'

"""

run_template = """#!/bin/bash

make -C sd-vbs/benchmarks/{app}/data/{dataset}/ > sd-vbs-{app}-output.txt 2>&1

"""

collect_template = """#!/bin/bash

results=sd-vbs-{app}-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results sd-vbs-{app}-output.txt >> /dev/null 2>&1

echo $results
"""

tests = []
with open('tests') as f:
    tests = f.readlines()

tests = [ c for c in tests if not c.startswith('#') ] # remove commented out tests/libs
tests = [ c.strip() for c in tests ] # remove \n

parm_dict = {
        'disparity':'fullhd',
        'localization':'vga',
        'mser':'fullhd',
        'multi_ncut':'fullhd',
        'sift':'fullhd',
        'stitch':'fullhd',
        'svm':'cif',
        'texture_synthesis':'fullhd',
        'tracking':'fullhd',
            }

for t in tests:
    tdir = 'separated/sd-vbs-{}-pkg'.format(t)
    os.mkdir(tdir)

    with open(os.path.join(tdir, 'setup.sh'), 'w') as setupsh:
        setupsh.write(setup_template.format(app=t))

    with open(os.path.join(tdir, 'run.sh'), 'w') as runsh:
        runsh.write(run_template.format(app=t, dataset=parm_dict[t]))

    with open(os.path.join(tdir, 'collect.sh'), 'w') as collectsh:
        collectsh.write(collect_template.format(app=t))

    for f in [ 'setup.sh', 'run.sh', 'collect.sh' ]:
        full_file = os.path.join(tdir, f)
        os.chmod(full_file, os.stat(full_file).st_mode | stat.S_IEXEC)

    shutil.copytree('stripped-sd-vbs', os.path.join(tdir, 'sd-vbs'))
    shutil.copytree(os.path.join('sd-vbs/benchmarks/', t), os.path.join(tdir, 'sd-vbs/benchmarks/', t))
    shutil.copyfile('process_results.py', os.path.join(tdir, 'process_results.py'))
