import tarfile
import sys
import os
import re
import tempfile
import shutil
import csv, glob

in_filename = sys.argv[1]
out_filename = sys.argv[2]

EXTRACT_PATH = tempfile.mkdtemp()

metric_patterns = {}

metric_patterns['cycles'] = r"Cycles elapsed\s+-\s+([\d\.]+)"

OUTPUT_FILENAME = glob.glob(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'UnixBench', '*results.txt'))[0]

with open(os.path.join(EXTRACT_PATH, OUTPUT_FILENAME)) as of:
    lines = of.readlines()

results = {}

def save_result(name, measure):
    if measure not in results.keys():
        results[name] = measure

record_next_line = False
for line in lines:
    if line.find('BASELINE       RESULT    INDEX') > -1:
        record_next_line = True
    elif record_next_line:
        segments = re.split('\s{2,}', line)
        save_result(segments[0], segments[-1])
        record_next_line = False
    elif re.search('^\w[-:\(\)\w\s]*\s{5,}', line):
        (name, measure) = re.split('\s{5,}', line)
        save_result(name, measure)

for m, p in metric_patterns.iteritems():
    results[m] = re.search(p, lines).group(1)

with open(out_filename,'w') as cf:
    cw = csv.DictWriter(cf, fieldnames=results.keys())
    cw.writeheader()
    cw.writerow(results)

shutil.rmtree(EXTRACT_PATH)
