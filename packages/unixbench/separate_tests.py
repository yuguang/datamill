import shutil
import os
import stat

if not os.path.exists('separated'):
    os.mkdir('separated')

setup_template = """#!/bin/sh

tar xaf UnixBench5.1.3.tgz
make -C UnixBench
"""

run_template = """#!/bin/sh

cd UnixBench
./Run {0} > unixbench-{0}-results.txt 2>&1
"""

collect_template = """#!/bin/sh

results=unixbench-{0}-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results UnixBench/unixbench-{0}-results.txt >> /dev/null 2>&1

echo $results
"""

tests = []

with open('tests') as f:
    tests = f.readlines()

tests = [ c.strip() for c in tests ] # remove \n

for t in tests:
    tdir = 'separated/unixbench-{}-pkg'.format(t)
    os.mkdir(tdir)

    with open(os.path.join(tdir, 'setup.sh'), 'w') as setupsh:
        setupsh.write(setup_template.format(t))

    with open(os.path.join(tdir, 'run.sh'), 'w') as runsh:
        runsh.write(run_template.format(t))

    with open(os.path.join(tdir, 'collect.sh'), 'w') as collectsh:
        collectsh.write(collect_template.format(t))

    for f in [ 'setup.sh', 'run.sh', 'collect.sh' ]:
        full_file = os.path.join(tdir, f)
        os.chmod(full_file, os.stat(full_file).st_mode | stat.S_IEXEC)

    shutil.copy('UnixBench5.1.3.tgz', tdir)
    shutil.copyfile('process_results.py', os.path.join(tdir, 'process_results.py'))
