#!/bin/sh

results=libmicro-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results libmicro_output.txt >> /dev/null 2>&1

echo $results
