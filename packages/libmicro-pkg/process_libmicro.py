import subprocess
import tarfile
import sys
import os
import re
import tempfile
import shutil
import csv

in_filename = sys.argv[1]
out_filename = sys.argv[2]

EXTRACT_PATH = tempfile.mkdtemp()
OUTPUT_FILENAME =  'libmicro_output.txt'


tar = tarfile.open(in_filename)
tar.extractall(path=EXTRACT_PATH)
tar.close()

ps = subprocess.Popen("./libmicrotocsv.pl "+os.path.join(EXTRACT_PATH, OUTPUT_FILENAME), shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
out = ps.communicate()[0].decode("UTF-8")
f = open(out_filename,'w')
f.write(out)

shutil.rmtree(EXTRACT_PATH)
