import shutil
import os
import stat

if not os.path.exists('separated'):
    os.mkdir('separated')

setup_template = """#!/bin/bash

emerge --sync
CONFIG_PROTECT='-*' emerge dev-java/icedtea-bin --autounmask-write
emerge dev-java/icedtea-bin

"""

run_template = """#!/bin/bash

cd specjvm2008
java -jar SPECjvm2008.jar -ikv -ict {app}
"""

collect_template = """#!/bin/bash

results=specjvm2008-{app}-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results specjvm2008/results/ >> /dev/null 2>&1

echo $results
"""

tests = []

with open('tests') as f:
    tests = f.readlines()

tests = [ c for c in tests if not c.startswith('#') ] # remove commented out tests/libs
tests = [ c.strip() for c in tests ] # remove \n

for t in tests:
    print('doing {}'.format(t))
    nice_t = t.replace('.', '-')
    tdir = 'separated/specjvm2008-{}-pkg'.format(nice_t)
    shutil.copytree('stripped-specjvm2008', os.path.join(tdir, 'specjvm2008'))

    with open(os.path.join(tdir, 'setup.sh'), 'w') as setupsh:
        setupsh.write(setup_template)

    with open(os.path.join(tdir, 'run.sh'), 'w') as runsh:
        runsh.write(run_template.format(app=t))

    with open(os.path.join(tdir, 'collect.sh'), 'w') as collectsh:
        collectsh.write(collect_template.format(app=nice_t))

    for f in [ 'setup.sh', 'run.sh', 'collect.sh' ]:
        full_file = os.path.join(tdir, f)
        os.chmod(full_file, os.stat(full_file).st_mode | stat.S_IEXEC)

    for f in os.listdir('clean-specjvm2008/resources'):
        if f in t: # if the dirname features in the benchname
            print('{} in {} so copying'.format(f, t))
            shutil.copytree(os.path.join('clean-specjvm2008/resources', f),
                os.path.join(tdir, 'specjvm2008/resources', f))

    for f in [ 'check' ]: # copy these two for all benches
        shutil.copytree(os.path.join('clean-specjvm2008/resources', f),
            os.path.join(tdir, 'specjvm2008/resources', f))
