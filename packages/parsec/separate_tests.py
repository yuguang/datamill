import shutil
import os
import stat
import subprocess

# !!!THIS WILL TAKE A LOT (20GB+) OF DISK SPACE!!!
# RUN ONLY FROM packages/parsec/

# notes: net{} benches are client-server
# splash2 only accept -i test
# splash2x are splash2 benches with larger inputs?

clean_parsec_dir = 'parsec-3.0'
no_inputs_parsec_dir = 'no-inputs-parsec-3.0'

if not os.path.exists(clean_parsec_dir):
    subprocess.check_call('wget ftp://datamill.uwaterloo.ca/internal/parsec-3.0.tar.gz', shell=True)
    subprocess.check_call('tar xzf parsec-3.0.tar.gz', shell=True)

if not os.path.exists('separated'):
    os.mkdir('separated')

setup_template = """#!/bin/bash

source parsec-3.0/env.sh
./parsec-3.0/bin/parsecmgmt -a build -c gcc -p {app}
"""

run_template = """#!/bin/bash

source parsec-3.0/env.sh
./parsec-3.0/bin/parsecmgmt -a run -c gcc -p {app} -i {input}
"""

collect_template = """#!/bin/bash

results={nice_t}-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results parsec-3.0/log/ >> /dev/null 2>&1

echo $results
"""

tests = []

with open('tests') as f:
    tests = f.readlines()

tests = [ c for c in tests if not c.startswith('#') ] # remove commented out tests/libs
tests = [ c.strip() for c in tests ] # remove \n

for t in tests:
    print('doing {}'.format(t))
    packname, junk, tname = t.partition('.')
    nice_t = t.replace('.', '-')
    tdir = 'separated/{}-pkg'.format(nice_t)
    os.mkdir(tdir)

    if tname in [ 'canneal', 'dedup' ]:
        input = 'native'
        input_filename = 'pkgs/apps/{}/inputs/input_native.tar'.format(tname)
    if t in [ 'splash2x.cholesky' ]:
        input = 'native'
        input_filename = 'ext/splash2x/kernels/{}/inputs/input_native.tar'.format(tname)
    elif packname == 'parsec':
        input = 'native'
        input_filename = 'pkgs/apps/{}/inputs/input_native.tar'.format(tname)
    elif packname == 'splash':
        input = 'test'
        input_filename = '/doesnotexist'
    elif packname == 'splash2x':
        input = 'native'
        input_filename = 'ext/splash2x/apps/{}/inputs/input_native.tar'.format(tname)

    with open(os.path.join(tdir, 'setup.sh'), 'w') as setupsh:
        setupsh.write(setup_template.format(app=t))

    with open(os.path.join(tdir, 'run.sh'), 'w') as runsh:
        runsh.write(run_template.format(app=t, input=input))

    with open(os.path.join(tdir, 'collect.sh'), 'w') as collectsh:
        collectsh.write(collect_template.format(nice_t=nice_t))

    for f in [ 'setup.sh', 'run.sh', 'collect.sh' ]:
        full_file = os.path.join(tdir, f)
        os.chmod(full_file, os.stat(full_file).st_mode | stat.S_IEXEC)

    # cp no-input tree over
    shutil.copytree(no_inputs_parsec_dir, os.path.join(tdir, no_inputs_parsec_dir))

    # cp input over
    if os.path.exists(os.path.join(clean_parsec_dir, input_filename)):
        shutil.copy(os.path.join(clean_parsec_dir, input_filename), os.path.join(tdir, no_inputs_parsec_dir, input_filename))

    # put in final place
    os.rename(os.path.join(tdir, no_inputs_parsec_dir), os.path.join(tdir, clean_parsec_dir))
