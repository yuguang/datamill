#!/bin/sh

results=javalinpack-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results java-linpack-output.txt >> /dev/null 2>&1

echo $results
