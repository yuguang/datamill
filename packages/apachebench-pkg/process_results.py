import tarfile
import sys
import os
import re
import tempfile
import shutil
import csv

in_filename = sys.argv[1]
out_filename = sys.argv[2]

EXTRACT_PATH = tempfile.mkdtemp()
OUTPUT_FILENAME = 'ab.out'

number_pattern = r"[\d\.]+"

metric_patterns = {}
metric_patterns['concurrent_connections']      = r"Concurrency Level:\s+({number})".format(number=number_pattern)
metric_patterns['total_time']                  = r"Time taken for tests:\s+({number})\s+seconds".format(number=number_pattern)
metric_patterns['complete_requests']           = r"Complete requests:\s+({number})".format(number=number_pattern)
metric_patterns['failed_requests']             = r"Failed requests:\s+({number})".format(number=number_pattern)
metric_patterns['write_errors']                = r"Write errors:\s+({number})".format(number=number_pattern)
metric_patterns['total_transferred']           = r"Total transferred:\s+({number}) bytes".format(number=number_pattern)
metric_patterns['html_transferred']            = r"HTML transferred:\s+({number}) bytes".format(number=number_pattern)
metric_patterns['requersts_per_second']        = r"Requests per second:\s+({number})".format(number=number_pattern)
metric_patterns['time_per_request_mean']       = r"Time per request:\s+({number})".format(number=number_pattern)
metric_patterns['time_per_request_concurrent'] = r"Time per request:\s+({number})".format(number=number_pattern)
metric_patterns['transfer_rate']               = r"Transfer rate:\s+({number})".format(number=number_pattern)


tar = tarfile.open(in_filename)
tar.extractall(path=EXTRACT_PATH)
tar.close()

with open(os.path.join(EXTRACT_PATH, OUTPUT_FILENAME)) as of:
    lines = of.read()

results = {}

for m, p in metric_patterns.iteritems():
    results[m] = re.search(p, lines).group(1)

with open(out_filename, 'w') as cf:
    cw = csv.DictWriter(cf, fieldnames=results.keys())
    cw.writeheader()
    cw.writerow(results)

shutil.rmtree(EXTRACT_PATH)
