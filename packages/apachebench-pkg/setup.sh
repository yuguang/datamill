#!/bin/sh

orig_pwd=$PWD

cd httpd-2.4.6
./configure --prefix ${orig_pwd}/httpd/ --with-included-apr
make
make install

cd ${orig_pwd}
cp -r htdocs/ httpd/
sed -i 's/Listen 80/Listen 8088/' httpd/conf/httpd.conf

./httpd/bin/apachectl -k start -f conf/httpd.conf
