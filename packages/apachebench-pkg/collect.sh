#!/bin/sh

results=apachebench-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results ab.out >> /dev/null 2>&1

echo $results
