import tarfile
import sys
import os
import re
import tempfile
import shutil
import csv

in_filename = sys.argv[1]
out_filename = sys.argv[2]

EXTRACT_PATH = tempfile.mkdtemp()
OUTPUT_FILENAME =  'coremark_v1.0/run1.log'

metric_patterns = {}

metric_patterns['cs'] = r"CoreMark Size    : (\d+)"
metric_patterns['ttck'] = r"Total ticks      : (\d+)"
metric_patterns['ttme'] = r"Total time \(secs\): (.+)"
metric_patterns['ips'] = r"Iterations/Sec   : (\d+)"
metric_patterns['itr'] = r"Iterations       : (\d+)"

tar = tarfile.open(in_filename)
tar.extractall(path=EXTRACT_PATH)
tar.close()

with open(os.path.join(EXTRACT_PATH, OUTPUT_FILENAME)) as of:
    lines = of.read()

results = {}


for m, p in metric_patterns.iteritems():
    results[m] = re.search(p, lines).group(1)

with open(out_filename,'w') as cf:
    cw = csv.DictWriter(cf, fieldnames=results.keys())
    cw.writeheader()
    cw.writerow(results)

shutil.rmtree(EXTRACT_PATH)
