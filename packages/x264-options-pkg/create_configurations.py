#!/usr/bin/env python

import sys
import csv

def write_config(filename, no_asm, no_8x8dct, no_cabac, no_deblock, no_fast_pskip, no_mbtree, no_mixed_refs, no_weightb, rc_lookahead, ref):
    config_template="""
NO_ASM="{no_asm}"
NO_8X8DCT="{no_8x8dct}"
NO_CABAC="{no_cabac}"
NO_DEBLOCK="{no_deblock}"
NO_FAST_PSKIP="{no_fast_pskip}"
NO_MBTREE="{no_mbtree}"
NO_MIXED_REFS="{no_mixed_refs}"
NO_WEIGHTB="{no_weightb}"
RC_LOOKAHEAD="{rc_lookahead}"
REF="{ref}"

""" .format(no_asm=no_asm,
            no_8x8dct=no_8x8dct,
            no_cabac=no_cabac,
            no_deblock=no_deblock,
            no_fast_pskip=no_fast_pskip,
            no_mbtree=no_mbtree,
            no_mixed_refs=no_mixed_refs,
            no_weightb=no_weightb,
            rc_lookahead=rc_lookahead,
            ref=ref)



    with open("configurations/" + filename, "w") as configuration:
        configuration.write(config_template)



def translate_line(line):
    
    line = line[:-1]
    config_key = [["--no-asm", ""],
                  ["--no-8x8dct", ""],
                  ["--no-cabac", ""],
                  ["--no-deblock", ""],
                  ["--no-fast-pskip", ""],
                  ["--no-mbtree", ""],
                  ["--no-mixed-refs", ""],
                  ["--no-weightb", ""],
                  ["--rc-lookahead 0", ""],
                  ["--rc-lookahead 20", ""],
                  ["--rc-lookahead 40", ""],
                  ["--rc-lookahead 60", ""],
                  ["--ref 0", ""],
                  ["--ref 1", ""],
                  ["--ref 5", ""],
                  ["--ref 9", ""]]

    def translate(cell):
        if cell == "Y":
            return 0
        elif cell == "N":
            return 1

    translated = map(translate, line)
    
    for cell_num in xrange(len(translated)):

        cell = translated[cell_num]
        translated[cell_num] = config_key[cell_num][cell]

    lookaheads = translated[8:12]
    refs = translated[12:]

    def firstMatching(f, l):
        x = None
        for i in reversed(xrange(len(l))):
            if f(l[i]):
                x = l[i]
                break
        return x

    lookahead = firstMatching(lambda item: item != '', lookaheads)
    ref = firstMatching(lambda item: item != '', refs)

    translated[8:12] = [lookahead]
    translated[9:] = [ref]

    return translated

def main():
    
    try:
        
        with open(sys.argv[1]) as experimentspec:
            specreader = csv.reader(experimentspec)
            header = specreader.next()

            specifications = map(translate_line, specreader)

            for num in xrange(len(specifications)):
                write_config(str(num) + "_configuration", *specifications[num])
    except IndexError:
        print """Usage: 
{} [csvfile] [options]""".format(sys.argv[0])
        

main()
