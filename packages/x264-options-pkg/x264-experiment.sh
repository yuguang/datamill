#!/usr/bin/env bash

# | Option          | Flag On           | Default | Description                                                               |
# |-----------------+-------------------+---------+---------------------------------------------------------------------------|
# | no_asm          | --no-asm          | not set | Disable cpu optimizations                                                 |
# | no_8x8dct       | --no-8x8dct       | not set | Disable intelligent adaptive use of 8x8 transforms                        |
# | no_cabac        | --no-cabac        | not set | Disable CABAC stream compression                                          |
# | no_deblock      | --no-deblock      | not set | Completely disable loop filter (not recommended)                          |
# | no_fast_pskip   | --no-fast-pskip   | not set | Disable early skip detection on P-frames.                                 |
# | no_mbtree       | --no-mbtree       | not set | Disable macroblock tree ratecontrol.                                      |
# | no_mixed_refs   | --no-mixed-refs   | not set | Disable mixed refs on per-8x8 partition rather than per-macroblock basis. |
# | no_weightb      | --no-weightb      | not set | Disable weighting of b-frames.                                            |
# | rc_lookahead    | --rc-lookahead 0  | 40      | Sets number of lookahead frames for mb-tree ratecontrol.                  |
# | rc_lookahead_20 | --rc-lookahead 20 | 40      | Sets number of lookahead frames for mb-tree ratecontrol.                  |
# | rc_lookahead_40 | --rc-lookahead 40 | 40      | Sets number of lookahead frames for mb-tree ratecontrol.                  |
# | rc_lookahead_60 | --rc-lookahead 60 | 40      | Sets number of lookahead frames for mb-tree ratecontrol.                  |
# | ref             | --ref 0           | 3       | Controls the size of the decoded picture buffer.                          |
# | ref_1           | --ref 1           | 3       | Controls the size of the decoded picture buffer.                          |
# | ref_5           | --ref 5           | 3       | Controls the size of the decoded picture buffer.                          |
# | ref_9           | --ref 9           | 3       | Controls the size of the decoded picture buffer.                          |


# Number of replications
NUM_REP=5
NUM_CONFIGURATIONS="$( ls configurations | wc -l )"
MAX_REPLICATE_NUM="$(( $NUM_REP * $NUM_CONFIGURATIONS - 1 ))"

function call_x264() {
    local config_name=$1

    local config_number="$(echo ${config_name##*/} | cut -d'_' -f 1)"

    local expname="${config_number}!${NO_ASM}!${NO_8X8DCT}!${NO_CABAC}!${NO_DEBLOCK}!${NO_FAST_PSKIP}!${NO_MBTREE}!${NO_MIXED_REFS}!${NO_WEIGHTB}!${RC_LOOKAHEAD}!${REF}"

    # We use spaces in our options, so get rid of them in the experiment name.
    expname=$(echo "$expname" | tr ' ' '-')

    # Pre cleanup
    rm -f sintel_trailer_2k_480p24.x264

    if [[ -n $VERBOSE ]]; then
        echo "NO_ASM: ${NO_ASM}"
        echo "NO_8X8DCT: ${NO_8X8DCT}"
        echo "NO_CABAC: ${NO_CABAC}"
        echo "NO_DEBLOCK: ${NO_DEBLOCK}"
        echo "NO_FAST_PSKIP: ${NO_FAST_PSKIP}"
        echo "NO_MBTREE: ${NO_MBTREE}"
        echo "NO_MIXED_REFS: ${NO_MIXED_REFS}"
        echo "NO_WEIGHTB: ${NO_WEIGHTB}"
        echo "RC_LOOKAHEAD: ${RC_LOOKAHEAD}"
        echo "REF: ${REF}"
        echo "expname: ${expname}.timing"
    fi 
    
    # /usr/bin/time xz --keep ${CHECK_FLAG} ${PRESET_LEVEL} ${EXTREME} ${SPARSE} ${MEM_FLAG} maximum_compression.tar > "${expname}.timing" 2>&1
    /usr/bin/time x264 --quiet --no-progress \
        ${NO_ASM} ${NO_8X8DCT} ${NO_CABAC} ${NO_DEBLOCK} ${NO_FAST_PSKIP} ${NO_MBTREE} ${NO_MIXED_REFS} ${NO_WEIGHTB} ${RC_LOOKAHEAD} ${REF} \
        -o sintel_trailer_2k_480p24.x264 shortest_sintel_trailer_2k_480p24.y4m > "${expname}.timing" 2>&1
    
    du -s sintel_trailer_2k_480p24.x264 > "${expname}.size" 2>&1

}

function get_config_file() {
    local replicate_num=$1

    echo configurations/"$(( $replicate_num / $NUM_REP ))"_configuration
}

# Datamill env variable with the 
# DATAMILL_REPLICATE_NO

configuration="$(get_config_file $DATAMILL_REPLICATE_NO)"

[[ -n $VERBOSE ]] && echo "Sourcing $configuration"
source "$configuration"
call_x264 "$configuration"


