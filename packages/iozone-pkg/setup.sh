#!/bin/sh

MY_ARCH=`uname -m`
MY_TARGET="linux"

if [ $MY_ARCH == "x86_64" ]
then
    MY_TARGET = "linux-AMD64"
elif [ "$(uname -m)" == "arm" ]
then
    MY_TARGET = "linux-arm"
elif [ "$(uname -m)" == "sparc64" ]
then
    MY_TARGET = "linux-sparc"
fi

make -C iozone3_420/src/current $MY_TARGET
