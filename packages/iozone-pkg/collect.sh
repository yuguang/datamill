#!/bin/sh

results=iozone-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results iozone_output.txt >> /dev/null 2>&1

echo $results
