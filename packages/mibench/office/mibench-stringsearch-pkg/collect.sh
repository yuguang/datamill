#!/bin/sh

results=mibench-stringsearch-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results stringsearch-output.txt >> /dev/null 2>&1

echo $results
