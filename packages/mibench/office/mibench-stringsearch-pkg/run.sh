#!/bin/sh

output=stringsearch-output.txt
repeats=40000

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
    stringsearch/search_large > /tmp/output_large.txt
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
