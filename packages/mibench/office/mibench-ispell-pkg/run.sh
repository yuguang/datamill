#!/bin/sh

output=aspell-output.txt
repeats=100

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./aspell-0.60.6.1/aspell --data-dir=${PWD}/aspell-0.60.6.1/data --dict-dir=${PWD}/aspell5-en-6.0-0 -d en_US -a < tests/large.txt > /tmp/output
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
