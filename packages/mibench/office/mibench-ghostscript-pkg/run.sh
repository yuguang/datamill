#!/bin/sh

output=gs-output.txt
repeats=300

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
ghostscript-9.10/bin/gs -sDEVICE=ppm -dNOPAUSE -q -sOutputFile=/tmp/output_large.ppm -- data/large.ps
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
