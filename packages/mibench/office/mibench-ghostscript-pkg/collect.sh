#!/bin/sh

results=mibench-gs-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results gs-output.txt >> /dev/null 2>&1

echo $results
