#!/bin/sh

output=lame-output.txt
repeats=300

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
    ./lame/lame3.70/lame lame/large.wav /tmp/output_large.mp3 >> /dev/null 2>&1
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
