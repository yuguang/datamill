#!/bin/sh

results=mibench-lame-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results lame-output.txt >> /dev/null 2>&1

echo $results
