#!/bin/sh

results=mibench-jpeg-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results jpeg-output.txt >> /dev/null 2>&1

echo $results
