#!/bin/sh

output=jpeg-output.txt
repeats=2000

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./jpeg/jpeg-6a/cjpeg -dct int -progressive -opt -outfile /tmp/output_large_encode.jpeg jpeg/input_large.ppm
./jpeg/jpeg-6a/djpeg -dct int -ppm -outfile /tmp/output_large_decode.ppm jpeg/input_large.jpg
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
