#!/bin/sh

results=mibench-mad-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results mad-output.txt >> /dev/null 2>&1

echo $results
