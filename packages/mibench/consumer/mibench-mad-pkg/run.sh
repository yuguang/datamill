#!/bin/sh

output=mad-output.txt
repeats=1000

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
    ./mad/madplay-0.15.2b/madplay --time=30 --output=wave:/tmp/output_large.wav -v mad/large.mp3 >> /dev/null 2>&1
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
