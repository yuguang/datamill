#!/bin/sh

results=mibench-tiff2rgba-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results tiff2rgba-output.txt >> /dev/null 2>&1

echo $results
