#!/bin/sh

output=tiff2rgba-output.txt
repeats=1000

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./tiff2rgba/tiff-4.0.3/tools/tiff2rgba -c none tiff2rgba/large.tif /tmp/output_largergba.tif
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
