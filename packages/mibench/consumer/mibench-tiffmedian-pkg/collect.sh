#!/bin/sh

results=mibench-tiffmedian-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results tiffmedian-output.txt >> /dev/null 2>&1

echo $results
