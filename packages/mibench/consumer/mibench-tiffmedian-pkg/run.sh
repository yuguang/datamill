#!/bin/sh

output=tiffmedian-output.txt
repeats=500

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./tiffmedian/tiff-4.0.3/tools/tiffmedian tiffmedian/large.tif /tmp/output_largemedian.tif
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
