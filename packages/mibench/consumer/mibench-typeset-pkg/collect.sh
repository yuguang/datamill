#!/bin/sh

results=mibench-typeset-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results typeset-output.txt >> /dev/null 2>&1

echo $results
