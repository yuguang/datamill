#!/bin/sh

output=typeset-output.txt
repeats=500

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./typeset/lout-3.24/lout -I typeset/lout-3.24/include -D typeset/lout-3.24/data -F typeset/lout-3.24/font -C typeset/lout-3.24/maps -H typeset/lout-3.24/hyph typeset/large.lout > /tmp/output_large.ps
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
