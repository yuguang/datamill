#!/bin/sh

output=tiffdither-output.txt
repeats=300

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./tiffdither/tiff-4.0.3/tools/tiffdither -c g4 tiffdither/largebw.tif /tmp/output_largedither.tif
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
