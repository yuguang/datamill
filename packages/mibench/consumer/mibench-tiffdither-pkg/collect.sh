#!/bin/sh

results=mibench-tiffdither-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results tiffdither-output.txt >> /dev/null 2>&1

echo $results
