#!/bin/sh

output=tiff2bw-output.txt
repeats=1500

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
    ./tiff2bw/tiff-4.0.3/tools/tiff2bw tiff2bw/large.tif /tmp/output_largebw.tif
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
