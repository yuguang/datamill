#!/bin/sh

results=mibench-tiff2bw-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results tiff2bw-output.txt >> /dev/null 2>&1

echo $results
