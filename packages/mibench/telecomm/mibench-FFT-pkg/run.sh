#!/bin/sh

output=fft-output.txt
repeats=500

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./FFT/fft 8 32768 > /tmp/output_large.txt
./FFT/fft 8 32768 -i > /tmp/output_large.inv.txt
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
