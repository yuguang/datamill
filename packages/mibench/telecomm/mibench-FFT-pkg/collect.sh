#!/bin/sh

results=mibench-fft-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results fft-output.txt >> /dev/null 2>&1

echo $results
