#!/bin/sh

results=mibench-crc-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results crc-output.txt >> /dev/null 2>&1

echo $results
