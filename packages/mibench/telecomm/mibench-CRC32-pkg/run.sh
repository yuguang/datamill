#!/bin/sh

output=crc-output.txt
repeats=250

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./CRC32/crc large.pcm > /tmp/output_large.txt
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
