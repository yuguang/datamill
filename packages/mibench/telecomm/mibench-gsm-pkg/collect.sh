#!/bin/sh

results=mibench-gsm-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results gsm-output.txt >> /dev/null 2>&1

echo $results
