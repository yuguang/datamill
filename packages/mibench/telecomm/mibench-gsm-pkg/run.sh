#!/bin/sh

output=gsm-output.txt
repeats=200

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
gsm/bin/toast -fps -c gsm/data/large.au > /tmp/output_large.encode.gsm
gsm/bin/untoast -fps -c gsm/data/large.au.run.gsm > /tmp/output_large.decode.run
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
