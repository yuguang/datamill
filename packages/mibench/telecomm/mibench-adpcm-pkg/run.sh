#!/bin/sh

output=adpcm-output.txt
repeats=100

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./adpcm/bin/rawcaudio < adpcm/data/large.pcm > /tmp/output_large.adpcm 2>>/dev/null
./adpcm/bin/rawdaudio < adpcm/data/large.adpcm > /tmp/output_large.pcm 2>>/dev/null
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
