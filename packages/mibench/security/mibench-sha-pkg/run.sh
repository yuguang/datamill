#!/bin/sh

output=sha-output.txt
repeats=3000

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
    ./sha/sha sha/input_large.asc > /tmp/output_large.txt
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
