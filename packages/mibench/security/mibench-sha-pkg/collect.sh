#!/bin/sh

results=mibench-sha-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results sha-output.txt >> /dev/null 2>&1

echo $results
