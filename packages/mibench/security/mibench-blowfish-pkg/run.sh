#!/bin/sh

output=blowfish-output.txt
repeats=250

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./blowfish/bf e blowfish/input_large.asc /tmp/output_large.enc 1234567890abcdeffedcba0987654321
./blowfish/bf d /tmp/output_large.enc /tmp/output_large.asc 1234567890abcdeffedcba0987654321
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
