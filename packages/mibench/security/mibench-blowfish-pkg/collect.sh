#!/bin/sh

results=mibench-blowfish-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results blowfish-output.txt >> /dev/null 2>&1

echo $results
