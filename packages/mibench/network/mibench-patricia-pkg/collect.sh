#!/bin/sh

results=mibench-patricia-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results patricia-output.txt >> /dev/null 2>&1

echo $results
