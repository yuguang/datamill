#!/bin/sh

results=mibench-dijkstra-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results dijkstra-output.txt >> /dev/null 2>&1

echo $results
