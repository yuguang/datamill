#!/bin/sh

output=dijkstra-output.txt
repeats=1500

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./dijkstra/dijkstra_large ./dijkstra/input.dat > /tmp/output_large.dat
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
