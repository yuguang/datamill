#!/bin/sh

results=mibench-basicmath-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results basicmath-output.txt >> /dev/null 2>&1

echo $results
