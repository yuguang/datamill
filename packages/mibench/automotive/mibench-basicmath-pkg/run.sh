#!/bin/bash

output=basicmath-output.txt
repeats=200

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output

for (( i=1; i<=$repeats; i++ ))
do
    ./basicmath/basicmath_large >> /dev/null
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
