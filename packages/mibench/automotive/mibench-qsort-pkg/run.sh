#!/bin/sh

output=qsort-output.txt
repeats=1000

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
    ./qsort/qsort_large qsort/input_large.dat >> /dev/null
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
