#!/bin/sh

results=mibench-qsort-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results qsort-output.txt >> /dev/null 2>&1

echo $results
