#!/bin/sh

results=mibench-bitcount-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results bitcount-output.txt >> /dev/null 2>&1

echo $results
