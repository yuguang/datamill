#!/bin/sh

output=susan-output.txt
repeats=1000

echo `date +%Y_%m_%d_%I_%M_%S_%N` "::: starting $repeats reps" > $output
for (( i=1; i<=$repeats; i++ ))
do
./susan/susan ./susan/input_large.pgm /tmp/output_large.smoothing.pgm -s
./susan/susan ./susan/input_large.pgm /tmp/output_large.edges.pgm -e
./susan/susan ./susan/input_large.pgm /tmp/output_large.corners.pgm -c
done
echo `date +%Y_%m_%d_%I_%M_%S_%N` '::: stop' >> $output
