#!/bin/sh

results=mibench-susan-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results susan-output.txt >> /dev/null 2>&1

echo $results
