#!/bin/sh

results=postmark-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results postmark.out >> /dev/null 2>&1

echo $results
