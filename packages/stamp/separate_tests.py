import shutil
import os
import stat

if not os.path.exists('separated'):
    os.mkdir('separated')

setup_template = """#!/bin/bash

make -C {app} -f Makefile.seq

"""

run_template = """#!/bin/bash

./{app}/{app} {parms} > {app}_output.txt 2>&1

"""

collect_template = """#!/bin/bash

results=stamp-{app}-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results {app}_output.txt >> /dev/null 2>&1

echo $results
"""

tests = []
with open('tests') as f:
    tests = f.readlines()

tests = [ c for c in tests if not c.startswith('#') ] # remove commented out tests/libs
tests = [ c.strip() for c in tests ] # remove \n

parm_dict = {
            'bayes':'-v32 -r4096 -n10 -p40 -i2 -e8 -s1',
            'genome':'-g16384 -s64 -n16777216',
            'intruder':'-a10 -l128 -n262144 -s1',
            'kmeans':'-m40 -n40 -t0.00001 -i kmeans/inputs/random-n65536-d32-c16.txt',
            'labyrinth':'-i labyrinth/inputs/random-x512-y512-z7-n512.txt',
            'ssca2':'-s20 -i1.0 -u1.0 -l3 -p3',
            'vacation':'-n2 -q90 -u98 -r1048576 -t4194304',
            'yada':'-a15 -i yada/inputs/ttimeu1000000.2',
            }

for t in tests:
    tdir = 'separated/stamp-{}-pkg'.format(t)
    os.mkdir(tdir)

    with open(os.path.join(tdir, 'setup.sh'), 'w') as setupsh:
        setupsh.write(setup_template.format(app=t))

    with open(os.path.join(tdir, 'run.sh'), 'w') as runsh:
        runsh.write(run_template.format(app=t, parms=parm_dict[t]))

    with open(os.path.join(tdir, 'collect.sh'), 'w') as collectsh:
        collectsh.write(collect_template.format(app=t))

    for f in [ 'setup.sh', 'run.sh', 'collect.sh' ]:
        full_file = os.path.join(tdir, f)
        os.chmod(full_file, os.stat(full_file).st_mode | stat.S_IEXEC)

    for stamp_dir in [ t, 'lib', 'common' ]:
       shutil.copytree(os.path.join('stamp-0.9.10', stamp_dir), os.path.join(tdir, stamp_dir))

