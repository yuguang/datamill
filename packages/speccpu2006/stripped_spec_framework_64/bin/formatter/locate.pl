#
# locate.pm
#
# Copyright 1999-2008 Standard Performance Evaluation Corporation
#  All Rights Reserved
#
# $Id: locate.pl 5976 2008-03-26 04:21:00Z cloyce $

use strict;
use IO::Dir;
use Time::HiRes qw(time);

require 'flagutils.pl';

my $version = '$LastChangedRevision: 5976 $ '; # Make emacs happier
$version =~ s/^\044LastChangedRevision: (\d+) \$ $/$1/;
$::tools_versions{'formatter_locate.pl'} = $version;

# This is the list of benchsets and benchmarks that will be used for the
# standalone result formatter at SPEC.  This should be updated whenever the
# *bset files change, and right before release.
my %bmarklist;
if ($::suite eq 'CPU2006') {
    %bmarklist = ('int' => { 'name' => 'int',
                             'topdir' => 'CPU2006',
                             'units' => 'SPECint',
                             'metric' => 'CINT2006',
                             'ref' => 'ref',
                             'train' => 'train',
                             'test' => 'test',
                             'benchmarklist' => [
                                 [ '400.perlbench',	'C'   ],
                                 [ '401.bzip2', 	'C'   ],
                                 [ '403.gcc',   	'C'   ],
                                 [ '429.mcf',   	'C'   ],
                                 [ '445.gobmk', 	'C'   ],
                                 [ '456.hmmer', 	'C'   ],
                                 [ '458.sjeng', 	'C'   ],
                                 [ '462.libquantum',	'C'   ],
                                 [ '464.h264ref',	'C'   ],
                                 [ '471.omnetpp',	'CXX' ],
                                 [ '473.astar', 	'CXX' ],
                                 [ '483.xalancbmk',	'CXX' ],
                                 [ '999.specrand',	'C'   ],
                             ],
                             'no_output' => { '999.specrand' => 1 },
			       },
		     'fp' => { 'name' => 'fp',
                               'topdir' => 'CPU2006',
			       'units' => 'SPECfp',
			       'metric' => 'CFP2006',
                               'ref' => 'ref',
                               'train' => 'train',
                               'test' => 'test',
			       'benchmarklist' => [
                                   [ '410.bwaves',	'F'     ],
                                   [ '416.gamess',	'F'     ],
                                   [ '433.milc',	'C'     ],
                                   [ '434.zeusmp',	'F'     ],
                                   [ '435.gromacs',	'F,C'   ],
                                   [ '436.cactusADM',	'F,C'   ],
                                   [ '437.leslie3d',	'F'     ],
                                   [ '444.namd',	'CXX'   ],
                                   [ '447.dealII',	'CXX'   ],
                                   [ '450.soplex',	'CXX'   ],
                                   [ '453.povray',	'CXX'   ],
                                   [ '454.calculix',	'F,C'   ],
                                   [ '459.GemsFDTD',	'F'     ],
                                   [ '465.tonto',	'F'     ],
                                   [ '470.lbm', 	'C'     ],
                                   [ '481.wrf', 	'F,C'   ],
                                   [ '482.sphinx3',	'C'     ],
                                   [ '998.specrand',	'C'     ],
			       ],
                               'no_output' => { '998.specrand' => 1 },
			      },
		     );
} elsif ($::suite eq 'MPI2007') {
    %bmarklist = ('medium' => { 'name' => 'medium',
                             'topdir' => 'MPI2007',
                             'units' => 'SPECmpiM',
                             'metric' => 'MPIM2007',
                             'ref' => 'mref',
                             'train' => 'train',
                             'test' => 'test',
                             'benchmarklist' => [
                                 [ '104.milc',          'C'   ],
                                 [ '107.leslie3d',      'F'   ],
                                 [ '113.GemsFDTD',      'F'   ],
                                 [ '115.fds4',          'F,C' ],
                                 [ '121.pop2',          'F,C' ],
                                 [ '122.tachyon',       'C'   ],
                                 [ '126.lammps',        'CXX' ],
                                 [ '127.wrf2',          'F,C' ],
                                 [ '128.GAPgeofem',     'F,C' ],
                                 [ '129.tera_tf',       'F'   ],
                                 [ '130.socorro',       'F,C' ],
                                 [ '132.zeusmp2',       'F,C' ],
                                 [ '137.lu',            'F'   ],
                             ],
                               },
                  'large' => { 'name' => 'large',
                             'topdir' => 'MPI2007',
                             'units' => 'SPECmpiL',
                             'metric' => 'MPIL2007',
                             'ref' => 'lref',
                             'train' => 'train',
                             'test' => 'test',
                             'benchmarklist' => [
                                 [ '104.milc',          'C'   ],
                                 [ '107.leslie3d',      'F'   ],
                                 [ '113.GemsFDTD',      'F'   ],
                                 [ '115.fds4',          'F,C' ],
                                 [ '121.pop2',          'F,C' ],
                                 [ '122.tachyon',       'C'   ],
                                 [ '126.lammps',        'CXX' ],
                                 [ '127.wrf2',          'F,C' ],
                                 [ '128.GAPgeofem',     'F,C' ],
                                 [ '129.tera_tf',       'F'   ],
                                 [ '130.socorro',       'F,C' ],
                                 [ '132.zeusmp2',       'F,C' ],
                                 [ '137.lu',            'F'   ],
                             ],
                             },
                     );
} else {
    %bmarklist = (${main::lcsuite} => { 'name' => ${main::lcsuite},
                                       'topdir' => ${main::suite},
				       'units' => 'SPECfoo_mark',
				       'metric' => ${main::suite},
                                       'ref' => 'ref',
                                       'train' => 'train',
                                       'test' => 'test',
				       'benchmarklist' => [qw()] }
		 );
}

sub locate_benchmarks {
    my ($config) = @_;

    $config->{'formats'} = {} if !exists $config->{'formats'};

    my $benchdir = jp($config->top, $config->benchdir);
    my %seen;
    # We're just formatting, so don't bother to look for the
    # benchmark directories; use the hardcoded list instead
    foreach my $bset (keys %bmarklist) {
        my $setclass = "Spec::Benchset::${bset}";
        eval "package $setclass; \@${setclass}::ISA = qw(Spec::Benchset);";
        my $setobj = $setclass->new($config, 'empty_ok' => 1);
        for my $var (keys %{$bmarklist{$bset}}) {
            next if ($var eq 'benchmarklist');
            $setobj->{$var} = $bmarklist{$bset}->{$var};
        }
        $config->{'benchsets'}->{$setobj->name} = $setobj;
        foreach my $bmarkref (@{$bmarklist{$bset}->{'benchmarklist'}}) {
            my ($bmark, $benchlang);
            if (isa($bmarkref, 'ARRAY')) {
              ($bmark, $benchlang) = @{$bmarkref};
            } else {
              $bmark = $bmarkref;
              $benchlang = '?';
            }
            $bmark =~ /(\d+).(.*)/o;
            my ($num, $name) = ($1, $2);
            my $bmarkclass = "Spec::Benchmark::${name}${num}";
            eval "
                  package $bmarkclass;
                  \@${bmarkclass}::ISA = qw(Spec::Benchmark);
                  \$${bmarkclass}::benchname = \'$name\';
                  \$${bmarkclass}::benchnum = $num;
                  \$${bmarkclass}::benchlang = \'$benchlang\';
                  \@${bmarkclass}::base_exe = (\'foo\');
                 ";
            if ($@) {
                Log(0, "\nError building benchmark class for '$bmark': $@\n");
                next;
            }
            my $bmobj = $bmarkclass->new("$num.$name", $config, $num, $name);
            push @{$setobj->{'benchmarklist'}}, $bmark;
            $config->{'benchmarks'}->{$bmark} = $bmobj;
            my $flags_file;
            if (defined($::website_formatter) && $::website_formatter) {
              $flags_file = jp($::flag_base, $bmark.'.flags.xml');
            } else {
              $flags_file = jp($benchdir, $setobj->{'topdir'}, $bmark, 'Spec', 'flags.xml');
            }
            if (-e $flags_file) {
                my $objectstarttime = Time::HiRes::time;
                (undef, $config->{'flaginfo'}->{$bmark}) =
                    main::get_flags_file($flags_file, $bmark);
                Log(90, sprintf("    Reading + parsing $flags_file took %8.9fs\n", Time::HiRes::time - $objectstarttime));
            }
            if (!-e $flags_file ||
                !defined($config->{'flaginfo'}->{$bmark})) {
                Log(0, "\nERROR: The flags file for $bmark ($flags_file) could not be parsed.\n");
                do_exit(1);
            }
        }
    }
    my $error = 0;
    for my $set (keys %{$config->{'benchsets'}}) {
	my $obj = $config->{'benchsets'}{$set};
	my $ref = {};
	$config->{'benchsets'}{$set}{'benchmarks'} = $ref;
	my @benchmarks = @{$obj->{'benchmarklist'}};
	for my $bench (@benchmarks) {
	    if (!exists $config->{'benchmarks'}{$bench}) {
		Log(0, "\nBenchmark Set '$set' calls for non-existant benchmark '$bench'\n");
		$obj->{'valid'} = 'X';
                $error++;
	    } else {
	        $ref->{$bench} = $config->{'benchmarks'}{$bench};
            }
	}
    }
    ::do_exit(1) if $error;
    $config->{'setobjs'} = [ map {$config->{'benchsets'}{$_}} keys %{$config->{'benchsets'}} ];
}

sub locate_formats {
    my ($config, $quiet) = @_;

    my @formats = list_files(jp($config->top, 'bin', 'formats'));
    @formats = grep (m|\/[^.][^/\\]*\.p[lm]$|o, @formats);

    my $logged = 0;
    for my $pm (@formats) { ## for each format .pl file
	my ($name) = $pm =~ m|([^/]+)\.p[lm]$|o;
        my $ok = 0;
        eval "\$ok = ::${name}_ok();";
	$ok = 1 if ($@ && $@ =~ /undefined subroutine/i);
        if (($@ && $@ !~ /undefined subroutine/i) || !$ok) {
            Log(2, ', ') if $logged && !$quiet;
            Log(2, "$name (DISABLED)") unless $quiet;
            Log(8, " [$@]");
            $logged++;
            next;
        }
	eval "package Spec::Format::${name}; \@Spec::Format::${name}::ISA = qw(Spec::Format); require '$pm';";
	if ($@) {
	    Log(102, "\nError requiring $pm for $name format: $@\nContinuing with output formats...");
            $logged = 0;
	    next;
	}

	my $class= "Spec::Format::${name}";

	if (!$class->can('new')) {
	    Log(12, "\nNo 'new' function for class '$class' in '$pm'\n");
	    next;
	}

	## run the creation method for the object, i.e., "new"
	my $obj = $class->new;
	if (!defined($obj) || !ref($obj)) {
	    Log(12, "\nError initializing format object\n");
	    next;
	}
	$config->{'formats'}{$name} = $obj;
        Log(2, ', ') if $logged && !$quiet;
	Log(2, $obj->{'name'}) unless $quiet;
        $logged++;
    }
    Log(2, "\n") unless $quiet;
}

1;
