#
# os.pm
#
# Copyright 1999-2008 Standard Performance Evaluation Corporation
#  All Rights Reserved
#
# $Id: os.pl 5784 2008-02-12 23:44:01Z cloyce $

use strict;

my $version = '$LastChangedRevision: 5784 $ '; # Make emacs happier
$version =~ s/^\044LastChangedRevision: (\d+) \$ $/$1/;
$::tools_versions{'os.pl'} = $version;

# It's really _all_ common
require 'os_common.pl';

1;
