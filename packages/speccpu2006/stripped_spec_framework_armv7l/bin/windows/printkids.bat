@echo off
rem
rem printkids.bat
rem
rem Copyright 2004-2008 Standard Performance Evaluation Corporation
rem  All Rights Reserved
rem
rem $Id: printkids.bat 5784 2008-02-12 23:44:01Z cloyce $
rem

specutil printkids.pl %*
