#
#  output_raw.pl - produces RAW output
#  Copyright 1995-2008 Standard Performance Evaluation Corporation
#   All Rights Reserved
#
#  Authors:  Christopher Chan-Nui
#            Cloyce D. Spradling
#
# $Id: output_raw.pl 5784 2008-02-12 23:44:01Z cloyce $

package Spec::Format::raw;

use strict;
use IO::File;
use UNIVERSAL qw(isa);

@Spec::Format::raw::ISA = qw(Spec::Format);

require 'format_raw.pl';

1;
