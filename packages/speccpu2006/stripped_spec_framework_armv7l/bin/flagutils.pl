#
# flagutils.pl
#
# Copyright 2005-2008 Standard Performance Evaluation Corporation
#  All Rights Reserved
#
# $Id: flagutils.pl 5784 2008-02-12 23:44:01Z cloyce $

use strict;

require 'util.pl';

my $version = '$LastChangedRevision: 5784 $ '; # Make emacs happier
$version =~ s/^\044LastChangedRevision: (\d+) \$ $/$1/;
$::tools_versions{'flagutils.pl'} = $version;

# runspec's requirements are actually quite modest
require 'flagutils_common.pl';

1;
