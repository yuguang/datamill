#
# log.pl
#
# Copyright 1999-2008 Standard Performance Evaluation Corporation
#  All Rights Reserved
#
# $Id: log.pl 5784 2008-02-12 23:44:01Z cloyce $

use strict;

my $version = '$LastChangedRevision: 5784 $ '; # Make emacs happier
$version =~ s/^\044LastChangedRevision: (\d+) \$ $/$1/;
$::tools_versions{'log.pl'} = $version;

$::log_handle = undef;

require 'log_common.pl';

1;
