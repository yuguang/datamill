#
# vars.pl
#
# Copyright 1995-2008 Standard Performance Evaluation Corporation
#  All Rights Reserved
#
# $Id: vars.pl 5784 2008-02-12 23:44:01Z cloyce $

##############################################################################
# Initialize Variables
##############################################################################

use strict;
use Config;
use Cwd;
use UNIVERSAL;

require 'vars_common.pl';

# List of SPEC(r) trademarked strings, for use by the formatters
# Values are "r" (registered trademark), "t" (trademark), and "s" (service mark)
%::trademarks = (
                  'SPEC'           => 'r',
                  'SPEC Logo'      => 'r',
                  'SPECapc'        => 's',
                  'SPECchem'       => 'r',
                  'SPECclimate'    => 'r',
                  'SPECenv'        => 'r',
                  'SPECfp'         => 'r',
                  'SPECglperf'     => 'r',
                  'SPEChpc'        => 'r',
                  'SPECint'        => 'r',
                  'SPECjAppServer' => 'r',
                  'SPECjbb'        => 'r',
                  'SPEC JMS'       => 't',
                  'SPECjvm'        => 'r',
                  'SPECmail'       => 'r',
                  'SPECmark'       => 'r',
                  'SPECmedia'      => 's',
                  'SPEC MPI'       => 't',
                  'SPECmpiM'       => 't',
                  'SPECmpiL'       => 't',
                  'SPEComp'        => 'r',
                  'SPECompM'       => 'r',
                  'SPECompL'       => 'r',
                  'SPECopc'        => 's',
                  'SPECrate'       => 'r',
                  'SPECseis'       => 'r',
                  'SPECsfs'        => 'r',
                  'SPECviewperf'   => 'r',
                  'SPECweb'        => 'r',
                );

my $version = '$LastChangedRevision: 5784 $ '; # Make emacs happier
$version =~ s/^\044LastChangedRevision: (\d+) \$ $/$1/;
$::tools_versions{'vars.pl'} = $version;

1;

__END__
