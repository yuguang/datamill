#
#  Formatter settings for website-side formatting.
#
#  Copyright 2006-2008 Standard Performance Evaluation Corporation
#   All Rights Reserved
#
#  Authors:  Christopher Chan-Nui
#            Cloyce D. Spradling
#
# $Id: formatter_vars.pl 5784 2008-02-12 23:44:01Z cloyce $
#

my $tmpversion = '$LastChangedRevision: 5784 $ '; # Make emacs happier
$tmpversion =~ s/^\044LastChangedRevision: (\d+) \$ $/$1/;
$tools_versions{'formatter_vars.pl'} = $tmpversion;

# This file intentionally left mostly blank

1;
