import shutil
import os
import stat

DESTDIR = '/home/arghods/tmp/separated4'

if not os.path.exists(DESTDIR):
    os.mkdir(DESTDIR)

setup_template = """#!/bin/bash
source /etc/profile
MY_CFG=""
ORIG_SPEC_ROOT="{orig_spec_root}"
if [ "$(uname -m)" == "x86_64" ]
then
    MY_CFG="datamill_x86_64.cfg"
    mv speccpu2006_64 speccpu2006
elif [ "$(uname -m)" == "armv7l" ]
then
	MY_CFG="datamill_armv7l.cfg"
	mv speccpu2006_armv7l speccpu2006
	ORIG_SPEC_ROOT="{orig_spec_root_arm}"
else
    MY_CFG="datamill_i686.cfg"
    mv speccpu2006_32 speccpu2006
fi

NEW_SPEC_ROOT="$PWD/speccpu2006/"
SCRIPT_LIST={script_list}

for script in ${{SCRIPT_LIST[@]}}
do
    sed -i s@$ORIG_SPEC_ROOT@$NEW_SPEC_ROOT@g $NEW_SPEC_ROOT/bin/$script
    sed -i 's@^[ ]*check_important_files@# check_important_files@' $NEW_SPEC_ROOT/bin/$script
done

mv CPU2006 speccpu2006/benchspec/
cd speccpu2006
. ./shrc
runspec --ignore_errors --config=$MY_CFG --action build {app}

"""

run_template = """#!/bin/bash
source /etc/profile
MY_CFG=""

if [ "$(uname -m)" == "x86_64" ]
then
    MY_CFG="datamill_x86_64.cfg"
elif [ "$(uname -m)" == "armv7l" ]
then
	MY_CFG="datamill_armv7l.cfg"
else
    MY_CFG="datamill_i686.cfg"
fi

cd speccpu2006
. ./shrc
runspec --config=$MY_CFG --noreportable --iterations=1 --size=ref {app} > {app}_output.txt 2>&1
"""

collect_template = """#!/bin/bash

results=speccpu2006-{app}-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results speccpu2006/result speccpu2006/{app}_output.txt >> /dev/null 2>&1

echo $results
"""

tests = []
script_list = "( specdiff makesrcalt flags_dump printpath.pl rawformat extract_config extract_raw specpp dumpsrcalt configpp toolsver printkids.pl flag_dump port_progress runspec extract_flags specperldoc convert_to_development verify_md5 )"
original_spec_root = '/home/augusto/work/eval-lab/packages/speccpu2006/clean_installed/'
original_spec_root_arm = '/root/test/speccpu/'
with open('tests') as f:
    tests = f.readlines()

tests = [ c for c in tests if not c.startswith('#') ] # remove commented out tests/libs
tests = [ c.strip() for c in tests ] # remove \n

stripped_spec_32 = 'stripped_spec_framework_32'
stripped_spec_64 = 'stripped_spec_framework_64'
stripped_spec_armv7l = 'stripped_spec_framework_armv7l'

for t in tests:
    nice_t = t.partition('.')[2]
    tdir = os.path.join(DESTDIR, 'speccpu2006-{}-pkg'.format(nice_t))
    os.mkdir(tdir)

    with open(os.path.join(tdir, 'setup.sh'), 'w') as setupsh:
        setupsh.write(setup_template.format(app=t, orig_spec_root=original_spec_root, orig_spec_root_arm=original_spec_root_arm, script_list=script_list))

    with open(os.path.join(tdir, 'run.sh'), 'w') as runsh:
        runsh.write(run_template.format(app=t))

    with open(os.path.join(tdir, 'collect.sh'), 'w') as collectsh:
        collectsh.write(collect_template.format(app=t))

    for f in [ 'setup.sh', 'run.sh', 'collect.sh' ]:
        full_file = os.path.join(tdir, f)
        os.chmod(full_file, os.stat(full_file).st_mode | stat.S_IEXEC)

    shutil.copytree(stripped_spec_32, os.path.join(tdir, 'speccpu2006_32'))
    shutil.copytree(stripped_spec_64, os.path.join(tdir, 'speccpu2006_64'))
    shutil.copytree(stripped_spec_armv7l, os.path.join(tdir, 'speccpu2006_armv7l'))

    for config in [ 'datamill_i686.cfg', 'datamill_x86_64.cfg' , 'datamill_armv7l.cfg' ]:
        for destination in [ os.path.join(tdir, 'speccpu2006_64'), os.path.join(tdir, 'speccpu2006_32'), os.path.join(tdir, 'speccpu2006_armv7l') ]:
            shutil.copy(config, os.path.join(destination, 'config'))

    shutil.copytree(os.path.join('benchmarks_and_inputs/benchspec/CPU2006/', t),
            os.path.join(tdir, 'CPU2006', t))
