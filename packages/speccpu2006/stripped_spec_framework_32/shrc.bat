@echo off
rem
rem shrc.bat
rem
rem Copyright (C) 1999-2007 Standard Performance Evaluation Corporation
rem  All Rights Reserved
rem
rem $Id: shrc.bat 6130 2008-04-30 21:17:35Z cloyce $
rem
rem    This file sets up your path and other environment variables for SPEC
rem    CPU2006.  It is intended to be customized as you may wish.  You will
rem    need to add a path to your compiler(s).  Look for lines that contain
rem    'BEGIN EDIT HERE'.
rem
rem    Usage: cd <specroot>
rem           shrc
rem
rem    Authors: J.Henning, Cloyce Spradling
rem    Last updated: 22 May 2007
rem    ---------------------------------------------------------------

rem XXXXXXXX BEGIN EDIT HERE XXXXXXXXXXX
rem
rem The SPEC CPU2006 benchmarks are supplied in source code form
rem (C, C++, Fortran).  In order to run the benchmarks, you need to
rem compile the benchmarks, or someone else will need to compile them
rem for you.  If you are compiling them, set your path appropriately,
rem using the examples below as a guideline.  Then, uncomment
rem the line that follows (just remove the word 'rem').
rem set SHRC_COMPILER_PATH_SET=yes
rem
rem If someone else has compiled the benchmarks for you, then the
rem only change you have to make to this file is to uncomment the
rem line that follows (just remove the word 'rem')
rem set SHRC_PRECOMPILED=yes
rem
rem XXXXXXXX END EDIT HERE XXXXXXXXXXX

if "%SHRC_COMPILER_PATH_SET%x"=="yesx" goto SHRC_compiler_path_set
if "%SHRC_PRECOMPILED%x"=="yesx"       goto SHRC_compiler_path_set
echo Please read and edit shrc.bat before attempting to execute it!
goto :EOF

:SHRC_compiler_path_set

rem    Now, add some compilers.  
rem    If the path to your compiler includes space characters, you
rem    may find that some utilities behave better if you define both
rem    the long name and the shorter, "8dot3" name.  You can discover
rem    the short name with the "dir /x" command.
rem
rem    Many compilers include a batch file which sets the path,  
rem    typically called XXXvars.bat.  The example lines below call the 
rem    batch files for Visual C++ version 6 and Digital Fortran version 6, 
rem    provided that both of these are installed on drive C.  You should 
rem    adjust for your compilers.

rem XXXXXXXX BEGIN EDIT HERE XXXXXXXXXXX
rem One way to set your path is to use the utilities provided by your
rem compiler.
rem *** This is the preferred way to set your compiler path ***
rem *** This is the preferred way to set your compiler path ***

rem call "c:\program files\microsoft visual studio 8\Common7\Tools\vsvars32.bat"
rem or maybe
rem call "c:\program files\microsoft visual studio .NET 2003\Vc7\Bin\vcvars32.bat"
rem or possibly even
rem call "c:\program files\Intel\Compiler\C++\9.1\IA32\Bin\iclvars.bat"

rem
rem -OR- (*ONLY* if your vendor does not provide you with a handy batch file)
rem

rem If you know where the executable is on your system, you can also
rem add the path using the set command

rem set PATH=%PATH%;"c:\program files\microsoft visual studio\vc98\bin"
rem set PATH=%PATH%;"c:\program files\microsoft visual studio\df98\bin"

rem The paths above are only examples; be sure to replace the paths with
rem correct ones for your compiler.

rem Note that setting the PATH manually is probably not sufficient for modern
rem compilers; in order for everything to work, you probably also need to
rem set LIB and INCLUDE.  It's really much better to use the vendor batch
rem file to get the environment set up.

rem XXXXXXXX END EDIT HERE XXXXXXXXXXX

rem set SPEC environment variable
rem

rem if the SPEC variable is set, and it points to something that looks
rem reasonable, then use that, otherwise fall through
if not "%SPEC%."=="." goto SPEC_env_not_defined
if exist %SPEC%\bin\runspec goto SPEC_env_defined
:SPEC_env_not_defined

rem we don't search the directory path, so you have to be in the top level
rem of the SPEC tree for this to work
if not exist bin\runspec (
    echo You are not in the top level of the SPEC directory!
    goto :EOF
)

rem go ahead and fetch the path, thanks to Diego.
CALL :set_short_path SPEC %CD%

:SPEC_env_defined
rem at this point SPEC should point to the correct stuff, so set up the
rem rest of the environment.
set SPECPERLLIB=%SPEC%\bin;%SPEC%\bin\lib;%SPEC%\bin\lib\site
echo "%PATH%" > %temp%\specpath.txt
CALL :add_path %SPEC%\bin %temp%\specpath.txt
CALL :add_path %SPEC%\bin\windows %temp%\specpath.txt
del /f /q %temp%\specpath.txt

if not "%SHRC_QUIET%."=="." goto :EOF

rem    Finally, let's print all this in a way that makes sense.  
rem    While we're at it, this is a good little test of whether 
rem    specperl is working for you!

specperl bin\printpath.pl

goto :EOF

:add_path
rem If the find from SFU is called, it'll spew some warnings, but the only
rem effect is that %1 will always be added to the PATH.  IOW, a fail safe.
find /I "%1" %2 > nul 2>&1
if errorlevel 1 set PATH=%1;%PATH%
goto :EOF

:set_short_path
rem Variable in %1, path in %2.  If only cmd would allow the ~ modifiers on
rem regular variables!
set %1=%~fs2
goto :EOF
