@echo off
rem
rem runspec.bat
rem
rem Copyright 1999-2008 Standard Performance Evaluation Corporation
rem  All Rights Reserved
rem
rem $Id: runspec.bat 5784 2008-02-12 23:44:01Z cloyce $
rem

specutil runspec %*
