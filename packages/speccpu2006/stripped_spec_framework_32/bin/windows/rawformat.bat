@echo off
rem
rem rawformat.bat
rem
rem Copyright 1999-2008 Standard Performance Evaluation Corporation
rem  All Rights Reserved
rem
rem $Id: rawformat.bat 5784 2008-02-12 23:44:01Z cloyce $
rem

specutil rawformat %*
