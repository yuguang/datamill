#
# Demand-load module list
#
package Encode::Config;
our $VERSION = do { my @r = (q$Revision: 2.1 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

use strict;

our %ExtModule = 
    (
      # Encode::Byte
      #iso-8859-1 is in Encode.pm itself
     'iso-8859-2'             => 'Encode::Byte',
     'iso-8859-3'             => 'Encode::Byte',
     'iso-8859-4'             => 'Encode::Byte',
     'iso-8859-5'             => 'Encode::Byte',
     'iso-8859-6'             => 'Encode::Byte',
     'iso-8859-7'             => 'Encode::Byte',
     'iso-8859-8'             => 'Encode::Byte',
     'iso-8859-9'             => 'Encode::Byte',
     'iso-8859-10'            => 'Encode::Byte',
     'iso-8859-11'            => 'Encode::Byte',
     'iso-8859-13'            => 'Encode::Byte',
     'iso-8859-14'            => 'Encode::Byte',
     'iso-8859-15'            => 'Encode::Byte',
     'iso-8859-16'            => 'Encode::Byte',
     'viscii'                 => 'Encode::Byte',
     'cp437'                  => 'Encode::Byte',
     'cp775'                  => 'Encode::Byte',
     'cp850'                  => 'Encode::Byte',
     'cp852'                  => 'Encode::Byte',
     'cp857'                  => 'Encode::Byte',
     'cp860'                  => 'Encode::Byte',
     'cp861'                  => 'Encode::Byte',
     'cp862'                  => 'Encode::Byte',
     'cp863'                  => 'Encode::Byte',
     'cp865'                  => 'Encode::Byte',
     'cp1250'                 => 'Encode::Byte',
     'cp1252'                 => 'Encode::Byte',
     'cp1254'                 => 'Encode::Byte',
     'cp1257'                 => 'Encode::Byte',
     'cp1258'                 => 'Encode::Byte',
     'AdobeStandardEncoding'  => 'Encode::Byte',
     'MacCentralEurRoman'     => 'Encode::Byte',
     'MacRoman'               => 'Encode::Byte',
     'MacSami'                => 'Encode::Byte',
     'MacUkrainian'           => 'Encode::Byte',
     'nextstep'               => 'Encode::Byte',
     'hp-roman8'              => 'Encode::Byte',
     'gsm0338'                => 'Encode::Byte',
     # Encode::EBCDIC
     'cp37'                   => 'Encode::EBCDIC',
     'cp500'                  => 'Encode::EBCDIC',
     'cp875'                  => 'Encode::EBCDIC',
     'cp1026'                 => 'Encode::EBCDIC',
     'cp1047'                 => 'Encode::EBCDIC',
     'posix-bc'               => 'Encode::EBCDIC',
     # Encode::Symbol
     'dingbats'               => 'Encode::Symbol',
     'symbol'                 => 'Encode::Symbol',
     'AdobeSymbol'            => 'Encode::Symbol',
     'AdobeZdingbat'          => 'Encode::Symbol',
     'MacDingbats'            => 'Encode::Symbol',
     'MacSymbol'              => 'Encode::Symbol',
     # Encode::Unicode
     'UCS-2BE'                => 'Encode::Unicode',
     'UCS-2LE'                => 'Encode::Unicode',
     'UTF-16'                 => 'Encode::Unicode',
     'UTF-16BE'               => 'Encode::Unicode',
     'UTF-16LE'               => 'Encode::Unicode',
     'UTF-32'                 => 'Encode::Unicode',
     'UTF-32BE'               => 'Encode::Unicode',
     'UTF-32LE'               => 'Encode::Unicode',
     'UTF-7'                  => 'Encode::Unicode::UTF7',
    );

unless (ord("A") == 193){
    %ExtModule =
	(
	 %ExtModule,





	 #'big5plus'           => 'Encode::HanExtra',
	 #'euc-tw'             => 'Encode::HanExtra',
	 #'gb18030'            => 'Encode::HanExtra',

	 'MIME-Header'        => 'Encode::MIME::Header',
	 'MIME-B'             => 'Encode::MIME::Header',
	 'MIME-Q'             => 'Encode::MIME::Header',

	 'MIME-Header-ISO_2022_JP' => 'Encode::MIME::Header::ISO_2022_JP',
	);
}

#
# Why not export ? to keep ConfigLocal Happy!
#
while (my ($enc,$mod) = each %ExtModule){
    $Encode::ExtModule{$enc} = $mod;
}

1;
__END__

=head1 NAME

Encode::Config -- internally used by Encode

=cut
