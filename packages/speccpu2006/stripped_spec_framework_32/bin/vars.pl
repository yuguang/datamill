#
# vars.pl
#
# Copyright 1995-2008 Standard Performance Evaluation Corporation
#  All Rights Reserved
#
# $Id: vars.pl 5784 2008-02-12 23:44:01Z cloyce $

##############################################################################
# Initialize Variables
##############################################################################

use strict;

require 'vars_common.pl';

1;

__END__
