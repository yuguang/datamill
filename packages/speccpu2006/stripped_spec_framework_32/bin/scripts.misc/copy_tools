#!/spec/cpu2006/bin/specperl
#!/spec/cpu2006/bin/specperl -d
#!/usr/bin/perl
#
#  copy_tools - a tool for copying CPU tools to other suites
#
#  Copyright 2007-2008 Standard Performance Evaluation Corporation
#   All Rights Reserved
#
# No support is provided for this script.
#
#  Author: Cloyce D. Spradling
#
# $Id$

use IO::Dir;
use IO::File;
use File::Copy;
use File::Basename;
use UNIVERSAL qw(isa);
use strict;
my $debug = 0;

die "Usage: $0 <CPU base> <target base> <target suite> <target year>\nStopped" if (@ARGV < 4);

my ($src, $base, $name, $year) = @ARGV;

die "CPU base dir \"$src\" has no bin/ subdir!\nStopped" unless (-d "$src/bin");
die "Target base dir \"$base\" has no bin/ subdir!\nStopped" unless (-d "$base/bin");
die "Target suite name is empty!\nStopped" unless ($name ne '');
die "Target suite year is empty!\nStopped" unless ($year ne '');
die "Target suite year is stupid!\nStopped" unless ($year > 2000 && $year < 2020);

my $suite = uc($name).$year;
my $lcsuite = lc($suite);

print "Copying tools files from CPU2006 to $suite...";

# First of all, copy all of the non-SVN files over
my @paths = read_files("$src/bin", 'bin/', [ qw(cpu2006.syntax index version.txt) ]);
unshift @paths, qw(install.sh install.bat shrc cshrc);

my $num_files = @paths+0;
print "$num_files files to copy\n";
my $copied = 0;
foreach my $srcpath (@paths) {
    my $destpath = $srcpath;
    $destpath =~ s/cpu2006/$lcsuite/go;
    $destpath =~ s/CPU2006/$suite/go;
    copy "$src/$srcpath", "$base/$destpath" or die "Copy of $srcpath to $destpath failed: $!\nStopped";
    system (qw(svn ps svn:executable *), "$base/$srcpath") if ( -x "$src/$srcpath" && -d "$base/.svn" );
    print "copy \"$src/$srcpath\", \"$base/$destpath\"\n" if $debug;
    $copied++;
    print "Copied $copied of $num_files\r";
}
print "\n\n";

print "Fixing up bang paths...";
foreach my $file (qw(configpp extract_config extract_flags extract_raw
                     flag_dump makesrcalt port_progress printkids.pl
                     printpath.pl rawformat runspec specdiff specpp
                     toolsver verify_md5 convert_to_development dumpsrcalt
                     scripts.misc/copy_tools
                     scripts.misc/extract_compopts scripts.misc/extract_misc)) {
    my @lines = read_file("$base/bin/$file");
    print " $file";
    # The bangpath to fix up is only in the first two lines
    $lines[0] =~ s/cpu2006/$lcsuite/go;
    $lines[1] =~ s/cpu2006/$lcsuite/go;
    write_file("$base/bin/$file", @lines);
}
print "\n\n";

print "Fixing up suite name/year:";
my @lines = read_file("$base/install.sh");
map { s#^SUITE=cpu2006#SUITE=$lcsuite#o; s#^UCSUITE=CPU2006#UCSUITE=$suite#o; } @lines;
write_file("$base/install.sh", @lines);
print " install.sh";

@lines = read_file("$base/install.bat");
map { s#^set SUITE=cpu2006#set SUITE=$lcsuite#o; s#^set SUITE_SIZE=.*#set SUITE_SIZE="~1500"#o; } @lines;
write_file("$base/install.bat", @lines);
print " install.bat";

my @benchmarks = <$base/benchspec/$suite/[0-9][0-9][0-9].*>;
my $firstbench = basename($benchmarks[0]);
$firstbench = '400.perlbench' unless defined($firstbench);
$firstbench =~ s/^\d{3}\.//;

@lines = read_file("$base/shrc");
map { s#cpu2006#$lcsuite#go; s#CPU2006#$suite#o; s#ME perl#ME $firstbench#o } @lines;
write_file("$base/shrc", @lines);
print " shrc";

@lines = read_file("$base/cshrc");
map { s#cpu2006#$lcsuite#go; s#CPU2006#$suite#o; s#ME perl#ME $firstbench#o } @lines;
write_file("$base/cshrc", @lines);
print " cshrc";

@lines = read_file("$base/bin/scripts.misc/do_go.csh");
map { s#cpu2006#$lcsuite#go; s#CPU2006#$suite#o; s#ME perl#ME $firstbench#o } @lines;
write_file("$base/bin/scripts.misc/do_go.csh", @lines);
print " bin/scripts.misc/do_go.csh";

@lines = read_file("$base/bin/relocate");
map { s#\#!/spec/cpu2006#\#!/spec/$lcsuite#go } @lines;
write_file("$base/bin/relocate", @lines);
print " relocate";

@lines = read_file("$base/bin/dumpsrcalt");
map { s#\#!/spec/cpu2006#\#!/spec/$lcsuite#go; s#CPU2006#$suite#go } @lines;
write_file("$base/bin/dumpsrcalt", @lines);
print " dumpsrcalt";

@lines = read_file("$base/bin/vars_common.pl");
# Try hard to figure out what the revision of runspec is _going_ to be
my $runspec_rev = undef;
open(SVNPIPE, '-|', 'svn', 'info', $base) or die "Can't run svn on $base: $!\n";
while(my $line = <SVNPIPE>) {
  next unless $line =~ /^Revision: (\d+)/;
  $runspec_rev = $1 + 1;
  last;
}
close(SVNPIPE);
map { s/^\$::year = '2006'/\$::year = '$year'/o;  s/^\$::suite = 'CPU'/\$::suite = '$name'/o; } @lines;
if (defined($runspec_rev) && $runspec_rev > 0) {
    map { s/^\$::current_runspec = \d+/\$::current_runspec = $runspec_rev/o; } @lines;
}
write_file("$base/bin/vars_common.pl", @lines);
print " vars_common.pl";

@lines = read_file("$base/bin/makesrcalt");
map { s/^my \$suite = 'CPU2006'/my \$suite = '$suite'/go } @lines;
write_file("$base/bin/makesrcalt", @lines);
print " makesrcalt";

@lines = read_file("$base/bin/packagetools");
map { s/^SUITE=cpu2006/SUITE=$lcsuite/go } @lines;
write_file("$base/bin/packagetools", @lines);
print " packagetools";

@lines = read_file("$base/bin/packagetools.bat");
map { s/^set SUITE=cpu2006/set SUITE=$lcsuite/go } @lines;
write_file("$base/bin/packagetools.bat", @lines);
print " packagetools.bat";

@lines = read_file("$base/bin/port_progress");
map { s/^my \$year = 2006/my \$year = $year/o;  s/^my \$suite = 'CPU'/my \$suite = '$name'/o; } @lines;
write_file("$base/bin/port_progress", @lines);
print " port_progress";

@lines = read_file("$base/bin/scripts.misc/bset_header");
map { s/CPU/$name/; s/2006/$year/; } @lines;
write_file("$base/bin/scripts.misc/bset_header", @lines);
print " scripts.misc/bset_header";

@lines = read_file("$base/bin/scripts.misc/cleanuptree");
map { s/^suite=CPU2006/suite=$suite/; if ($name eq 'MPI') { s/^treeowner=.*/treeowner=keeper/; s/^treegroup=.*/treegroup=hpcdevel/; } } @lines;
write_file("$base/bin/scripts.misc/cleanuptree", @lines);
print " scripts.misc/cleanuptree";

if ($name eq 'MPI') {
    # No parallelism == no need to check signal handlers
    @lines = read_file("$base/bin/test/spec/signals.t");
    map { s/# NOSIGTEST: //; } @lines;
    write_file("$base/bin/test/spec/signals.t", @lines);
    print " test/spec/signals.t";
} else {
    print " test/spec/signals.t (NOT CHANGED)";
}

print " index (NOT CHANGED)";

if ($name eq 'MPI') {
    @lines = read_file("$base/bin/scripts.misc/historical/syncit");
    map { s/CPU2006/$suite/g; if ($name eq 'MPI') { s#pro.spec.org:/spec/pro/osg/cpu/cpu2006/cpu2006/.#pro.spec.org:/spec/pro/hpg/mpi/mpi2007/.#; } } @lines;
    write_file("$base/bin/scripts.misc/historical/syncit", @lines);
    print " scripts.misc/historical/syncit";

    @lines = read_file("$base/bin/scripts.misc/historical/syncit.local");
    map { s/CPU2006/$suite/g; if ($name eq 'MPI') { s#CPUtree/cpu2006/cpu2006#MPItree/mpi2007#; } } @lines;
    write_file("$base/bin/scripts.misc/historical/syncit.local", @lines);
    print " scripts.misc/historical/syncit.local";
}

@lines = read_file("$base/bin/scripts.misc/maketars");
map { s/^\$benchmark = 'cpu2006'/\$benchmark = '$lcsuite'/ } @lines;
write_file("$base/bin/scripts.misc/maketars", @lines);
print " scripts.misc/maketars";

# Do the version file
copy "$src/bin/version.txt", "$base/bin/versioncpubase" or die "Copy of CPU tools version failed: $!\nStopped";

print "\n\nDone!\n\n";
print "Now inspect the changes (if any; ignore changes to \$Id\$ and friends) and\n";
print "commit them.\n";
print "\nTHEN (this sounds bizarre, but do it anyway), remove $base/bin\n";
print "and run 'svn update'.  This will put back all of the SVN variables\n";
print "('\$Id\$', etc) which were different in CPU2006.\n\n";

sub read_files {
  my ($start, $prepend, $exclude) = @_;
  my @rc = ();

  return () unless -d $start;
  my $dh = new IO::Dir $start;
  die "Can't open $start for reading: $!\nStopped" unless defined($dh);
  while(my $name = $dh->read()) {
      next if ($name eq '.' || $name eq '..' || $name eq '.svn' ||
               $name eq 'lib' || $name eq 'CVS');
      next if defined($exclude) && isa($exclude, 'ARRAY') && grep { $name eq $_ } @{$exclude};
      if (-d "$start/$name") {
         push @rc, read_files("$start/$name", $prepend.$name.'/', $exclude);
      } elsif (-f "$start/$name") {
         push @rc, $prepend.$name;
      }
  }

  return @rc;
}

sub read_file {
    my ($path) = @_;

    my $fh = new IO::File "<$path";
    die "\nCouldn't open $path for reading: $!\nStopped" unless defined($fh);
    my @lines = <$fh>;
    $fh->close();

    return @lines;
}

sub write_file {
    my ($path, @lines) = @_;

    my $fh = new IO::File ">$path";
    die "\nCouldn't open $path for writing: $!\nStopped" unless defined($fh);
    $fh->print(@lines);
    $fh->close();
}

