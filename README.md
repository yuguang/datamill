##Installation on Ubuntu

A short installation synopsis (incomplete):

    # Install Non-Python dependencies
    sudo apt-get install postgresql postgresql-client python-psycopg2 libpq-dev python-glpk libglpk-dev git curl python-dev

    # Setup Postgres
    su postgres                            # or whatever user your distro defines
    createuser -S -d -P datamill-dbuser # set password to datamill-password
    createdb datamilldb -O datamill-dbuser
    exit                                   # to get back to root or whatever user
    /etc/init.d/postgresql* start

    # Get the DataMill Source
    git clone git@bitbucket.org:yuguang/datamill.git && cd datamill/master
    
	# On more recent versions of Ubuntu, compile glpk 4.45 from source
	# http://en.wikibooks.org/wiki/GLPK/Linux_OS#Install
	
    #Set up virtual env
    virtualenv dev-python
    
    source dev-python/bin/activate
      
    #jpeg support
    sudo apt-get install libjpeg-dev
    #tiff support
    sudo apt-get install libtiff-dev
    #freetype support
    sudo apt-get install libfreetype6-dev
    #openjpeg200support (needed to compile from source)
    wget http://downloads.sourceforge.net/project/openjpeg.mirror/2.0.1/openjpeg-2.0.1.tar.gz
    tar xzvf openjpeg-2.0.1.tar.gz
    cd openjpeg-2.0.1/
    sudo apt-get install cmake
    cmake .
    sudo make install
    #install pillow
    pip install pillow
    
    #Install dependencies
    pip install -r dependencies.pip
        
    # Setup logging directory (or change the location of the logging directory)
    mkdir -p /var/datamill/log
    chown -R <webserver-user>:<webserver-user> /var/datamill
	
	# If you need to adjust any settings for your local dev machine, avoid changing the configuration on the actual master node
	cp datamill_django_project/example_local_settings.py \
       datamill_django_project/local_settings.py

    # Add DataMill Schema to our database
    ./manage.py syncdb --all        # create admin/password for website when prompted
    ./manage.py migrate
    ./manage.py createinitialrevisions
    ./manage.py runserver
	./manage.py loaddata  datamill_test_fixture
    

##Development

All django web related code is roughly organized as follows:

- models are in `datamill_website/models.py`
- templates are in `datamill_website/templates`
- views are in `datamill_website/views.py` (only make new views if the django generic ones don't suffice)

Read up on south (http://south.aeracode.org/) before doing DB stuff

    cd eval-lab/master
    ./manage.py runserver                               # to start the server
    ./manage.py test                                    # to run all tests
    ./manage.py schemamigration datamill_website --auto # to generate migrations (schema changes) when you change the DB structure in models.py
    ./manage.py migrate datamill_website                # to apply pending migrations on the database
    ./manage.py syncdb --all                            # to update Meta information (such as perms)

When the server is up and running, you can visit the local DataMill
page at port 8000 with any webclient:

    ./manage.py runserver
    curl localhost:8000/experiment/1/ goes to the expermient detail view
    curl localhost:8000/admin goes to admin

For access to the admin interface, use the password you set when you ran syncdb
